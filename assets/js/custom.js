var select2_param = {};
var cbx_iframe;
var otb;
$(function(){
	jQuery(document)
		.ajaxStart(function(){ 
			$("#loader").fadeIn();
		 })
		.ajaxStop(function(){ 
			$("#loader").fadeOut();
		});
	$("body").on('click','.delete',function(){
		return confirm('yakin menghapus data ini ?');
	})

	$('.data-table.std').dataTable({
		"bJQueryUI": true,
		"bFilter": true,
		"bLengthChange": false,
		"sPaginationType": "full_numbers",
		"sDom": '<""l>t<"F"fp>'
	});
	$('.data-table.std100').dataTable({
		"bJQueryUI": true,
		"bFilter": true,
		"bPaginate":false,
		"sDom": '<""l>t<"F"fp>'
	});
	otb = $('.def_table_y').dataTable( {
		"sPaginationType": "bootstrap",
		"sScrollY": "200px",
		"bPaginate": false,
		"bFilter":false,
		"bInfo":false,
		"bSort": false
	});
	$('.def_table_y_std300').dataTable( {
		"sPaginationType": "bootstrap",
		"sScrollY": "300px",
		"bPaginate": false,
		"bFilter":false,
		"bInfo":false,
		"bSort": false
	});

	$("body").on('click','.ajaxDelete',function(event){
		event.preventDefault();
		var rowIndex = $(this).parent().parent().index();
		if(confirm('yakin mau meghapus data ini ?')){
			$.get($(this).attr('href'), function(data) {
				if(data == "success")
					otb.fnDeleteRow(rowIndex);
			});
		}
	})

	$( "#styleSearch" ).keyup(function(val) {
		$(".dataTables_filter").find('input').val($(this).val());
		$(".dataTables_filter").find('input').keyup();
	});
	$('.chzn').chosen();

	$("#select2_icd10").select2({
		placeholder: "masukkan icd 10",
		minimumInputLength: 3,
		id: function(e) { return e},
		ajax: { 
			url: BASE+"master/json/icd10/getjsonselect/",
			dataType: 'jsonp',
			type:'post',
			data: function (term, page) {
				select2_param['q'] = term;
				select2_param[CSRF_NAME] = CSRF_HASH;
				return select2_param;
			},
			results: function (data, page) {
				return {results: data.icd};
			}
		},
        formatResult: diagFormatResult,
        formatSelection: diagFormatSelection, 
        dropdownCssClass: "bigdrop", 
        escapeMarkup: function (m) { return m; }
    })

    $("#select2_tindakan").select2({
		placeholder: "masukkan kode / nama tindakan",
		minimumInputLength: 2,
		id: function(e) { return e},
		ajax: { 
			url: BASE+"master/json/tindakan/getjsonselect/",
			dataType: 'jsonp',
			type:'post',
			data: function (term, page) {
				select2_param['q'] = term;
				select2_param[CSRF_NAME] = CSRF_HASH;
				return select2_param;
			},
			results: function (data, page) {
				return {results: data.icd};
			}
		},
        formatResult: tindakanFormatResult,
        formatSelection: tindakanFormatSelection, 
        dropdownCssClass: "bigdrop", 
        escapeMarkup: function (m) { return m; }
    })

    $("#select2_obat").select2({
		placeholder: "masukkan kode / nama obat",
		minimumInputLength: 2,
		id: function(e) { return e},
		ajax: { 
			url: BASE+"master/json/obat/getjsonselect/",
			dataType: 'jsonp',
			type:'post',
			data: function (term, page) {
				select2_param['q'] = term;
				select2_param[CSRF_NAME] = CSRF_HASH;
				return select2_param;
			},
			results: function (data, page) {
				return {results: data.icd};
			}
		},
        formatResult: obatFormatResult,
        formatSelection: obatFormatSelection, 
        dropdownCssClass: "bigdrop", 
        escapeMarkup: function (m) { return m; }
    })

    cbx_iframe = $(".cbx_iframe").colorbox({iframe:true, width:"750", height:"500"});

    var myDate = new Date();
    var prettyDate =(myDate.getDate()) + '-' + (myDate.getMonth()+1) + '-' +
    myDate.getFullYear();
    $(".today").val(prettyDate);
    $(".datepicker").datepicker({"dateFormat":"dd-mm-yy"});
})

function diagFormatResult(diag) {
	var markup = "<table class='movie-result'><tr>";
	markup += "<td class='movie-info'><div class='movie-title'>" + diag.title + "</div>";
	markup += "</td></tr></table>"
	return markup;
}

function diagFormatSelection(diag){
	$("#hd_diag").val(diag.key);
	$("#hd_diag_desc").val($("<div>"+diag.title+"</div>").text());
	return diag.title;
}

function tindakanFormatResult(tdk) {
	var markup = "<table class='movie-result'><tr>";
	markup += "<td class='movie-info'><div class='movie-title'>" +"<b>"+tdk.code+"</b> "+tdk.title + "</div>";
	markup += "</td></tr></table>"
	return markup;
}

function tindakanFormatSelection(tdk){
	$("#hd_tindakan").val(tdk.key);
	$("#tdk_deskripsi").val(tdk.code+" "+tdk.title);
	return "<b>"+tdk.code+"</b> "+tdk.title ;
}

function obatFormatResult(tdk) {
	var markup = "<table class='movie-result'><tr>";
	markup += "<td class='movie-info'><div class='movie-title'>" +"<b>"+tdk.code+"</b> "+tdk.title + "</div>";
	markup += "</td></tr></table>"
	return markup;
}

function obatFormatSelection(tdk){
	$("#hd_obat").val(tdk.key);
	return "<b>"+tdk.code+"</b> "+tdk.title ;
}

function preventSubmit(){
	$(document).ready(function() {
		$(window).keydown(function(event){
			if(event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
	});
}

function show_loading(){
	$('body').append("<div id='loading-block'>loading . . .</div>");
}

function hide_loading(){
	$("#loading-block").remove();
}