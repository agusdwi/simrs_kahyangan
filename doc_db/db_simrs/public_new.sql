/*
Navicat PGSQL Data Transfer

Source Server         : jike
Source Server Version : 90103
Source Host           : localhost:5432
Source Database       : simrs_new
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90103
File Encoding         : 65001

Date: 2012-10-16 14:47:03
*/


-- ----------------------------
-- Sequence structure for "public"."com_code_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_code_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 30
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_menu_role_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_menu_role_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 30
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_menu_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_menu_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 31
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_modul_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_modul_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 33
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_user_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_user_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 24
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_user_tipe_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_user_tipe_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 24
 CACHE 1;

-- ----------------------------
-- Table structure for "public"."com_code"
-- ----------------------------
CREATE TABLE "public"."com_code" (
"id" int4 DEFAULT nextval('com_code_seq'::regclass) NOT NULL,
"title" varchar(200) NOT NULL,
"value_1" text,
"value_2" text,
"value_3" text,
"status" int4 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_code
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."com_menu"
-- ----------------------------
CREATE TABLE "public"."com_menu" (
"menu_id" int2 DEFAULT nextval('com_menu_seq'::regclass) NOT NULL,
"modul_id" int2 NOT NULL,
"menu_nama" varchar(200) NOT NULL,
"menu_url" varchar(200) NOT NULL,
"menu_icon" varchar(200),
"menu_parent" varchar(200) NOT NULL,
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_menu
-- ----------------------------
INSERT INTO "public"."com_menu" VALUES ('30', '30', 'com modul', 'setting/com_modul', 'router.txt', 'setting/com_modul/com_test', '1');
INSERT INTO "public"."com_menu" VALUES ('31', '30', 'com menu', 'setting/com_menu', null, 'setting/com_menu/com_menus', '1');

-- ----------------------------
-- Table structure for "public"."com_menu_role"
-- ----------------------------
CREATE TABLE "public"."com_menu_role" (
"role_id" int4 DEFAULT nextval('com_menu_role_seq'::regclass) NOT NULL,
"group_id" int4 NOT NULL,
"user_tipe_id" int4 NOT NULL,
"view" int4 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_menu_role
-- ----------------------------
INSERT INTO "public"."com_menu_role" VALUES ('1', '1', '1', '1');
INSERT INTO "public"."com_menu_role" VALUES ('2', '1', '2', '1');
INSERT INTO "public"."com_menu_role" VALUES ('3', '1', '3', '1');
INSERT INTO "public"."com_menu_role" VALUES ('4', '2', '1', '1');
INSERT INTO "public"."com_menu_role" VALUES ('5', '2', '2', '1');
INSERT INTO "public"."com_menu_role" VALUES ('6', '2', '3', '1');
INSERT INTO "public"."com_menu_role" VALUES ('7', '3', '1', '1');
INSERT INTO "public"."com_menu_role" VALUES ('8', '3', '2', '1');
INSERT INTO "public"."com_menu_role" VALUES ('9', '3', '3', '1');
INSERT INTO "public"."com_menu_role" VALUES ('10', '4', '1', '1');
INSERT INTO "public"."com_menu_role" VALUES ('11', '4', '2', '1');
INSERT INTO "public"."com_menu_role" VALUES ('12', '4', '3', '1');
INSERT INTO "public"."com_menu_role" VALUES ('13', '5', '1', '1');
INSERT INTO "public"."com_menu_role" VALUES ('14', '5', '2', '1');
INSERT INTO "public"."com_menu_role" VALUES ('15', '5', '3', '1');
INSERT INTO "public"."com_menu_role" VALUES ('16', '6', '1', '0');
INSERT INTO "public"."com_menu_role" VALUES ('17', '6', '2', '0');
INSERT INTO "public"."com_menu_role" VALUES ('18', '6', '3', '0');
INSERT INTO "public"."com_menu_role" VALUES ('19', '7', '1', '1');
INSERT INTO "public"."com_menu_role" VALUES ('20', '7', '2', '1');
INSERT INTO "public"."com_menu_role" VALUES ('21', '7', '3', '1');
INSERT INTO "public"."com_menu_role" VALUES ('22', '8', '1', '1');
INSERT INTO "public"."com_menu_role" VALUES ('23', '8', '2', '1');
INSERT INTO "public"."com_menu_role" VALUES ('24', '8', '3', '1');

-- ----------------------------
-- Table structure for "public"."com_modul"
-- ----------------------------
CREATE TABLE "public"."com_modul" (
"modul_id" int2 DEFAULT nextval('com_modul_seq'::regclass) NOT NULL,
"modul_nama" varchar(200) NOT NULL,
"modul_url" varchar(200) NOT NULL,
"modul_icon" varchar(200),
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_modul
-- ----------------------------
INSERT INTO "public"."com_modul" VALUES ('30', 'setting', 'setting', null, '1');
INSERT INTO "public"."com_modul" VALUES ('33', 'pendaftaran', 'pendaftaran', '', '1');

-- ----------------------------
-- Table structure for "public"."com_user"
-- ----------------------------
CREATE TABLE "public"."com_user" (
"user_id" int4 DEFAULT nextval('com_user_seq'::regclass) NOT NULL,
"username" varchar(100) NOT NULL,
"password" varchar(200) NOT NULL,
"nama" varchar(100),
"avatar" varchar(200),
"user_tipe" int4 NOT NULL,
"jenis_kelamin" varchar(200),
"email" varchar(100),
"last_login" timestamp(6) NOT NULL,
"status" int4 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_user
-- ----------------------------
INSERT INTO "public"."com_user" VALUES ('3', 'jike', '49deebdfb953a2f52e2ac0931cf29b72', 'jike', '', '1', 'Laki-laki', 'jike@yahoo.com', '2012-10-09 13:41:45', '1');

-- ----------------------------
-- Table structure for "public"."com_user_tipe"
-- ----------------------------
CREATE TABLE "public"."com_user_tipe" (
"tipe_id" int4 DEFAULT nextval('com_user_tipe_seq'::regclass) NOT NULL,
"tipe_nama" varchar(100) NOT NULL,
"tipe_home" varchar(200),
"tipe_homebase" varchar(200) NOT NULL,
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_user_tipe
-- ----------------------------
INSERT INTO "public"."com_user_tipe" VALUES ('1', 'super_admin', 'super_admin', 'super_admin', '1');
INSERT INTO "public"."com_user_tipe" VALUES ('24', 'admin', 'admin', 'admin', '1');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table "public"."com_code"
-- ----------------------------
ALTER TABLE "public"."com_code" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table "public"."com_menu"
-- ----------------------------
ALTER TABLE "public"."com_menu" ADD PRIMARY KEY ("menu_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_menu_role"
-- ----------------------------
ALTER TABLE "public"."com_menu_role" ADD PRIMARY KEY ("role_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_modul"
-- ----------------------------
ALTER TABLE "public"."com_modul" ADD PRIMARY KEY ("modul_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_user"
-- ----------------------------
ALTER TABLE "public"."com_user" ADD PRIMARY KEY ("user_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_user_tipe"
-- ----------------------------
ALTER TABLE "public"."com_user_tipe" ADD PRIMARY KEY ("tipe_id");
