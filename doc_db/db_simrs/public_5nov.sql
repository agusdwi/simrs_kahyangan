--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.3
-- Dumped by pg_dump version 9.1.3
-- Started on 2012-11-05 17:38:48

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 174 (class 3079 OID 11639)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1919 (class 0 OID 0)
-- Dependencies: 174
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 173 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 1920 (class 0 OID 0)
-- Dependencies: 173
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET search_path = public, pg_catalog;

--
-- TOC entry 169 (class 1259 OID 40978)
-- Dependencies: 5
-- Name: com_code_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE com_code_seq
    START WITH 30
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.com_code_seq OWNER TO postgres;

--
-- TOC entry 1921 (class 0 OID 0)
-- Dependencies: 169
-- Name: com_code_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('com_code_seq', 30, false);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 168 (class 1259 OID 40960)
-- Dependencies: 1890 1891 5
-- Name: com_code; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE com_code (
    id integer DEFAULT nextval('com_code_seq'::regclass) NOT NULL,
    title character varying(200) NOT NULL,
    value_1 text,
    value_2 text,
    value_3 text,
    status integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.com_code OWNER TO postgres;

--
-- TOC entry 166 (class 1259 OID 32982)
-- Dependencies: 5
-- Name: com_menu_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE com_menu_seq
    START WITH 24
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.com_menu_seq OWNER TO postgres;

--
-- TOC entry 1922 (class 0 OID 0)
-- Dependencies: 166
-- Name: com_menu_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('com_menu_seq', 84, true);


--
-- TOC entry 163 (class 1259 OID 32968)
-- Dependencies: 1888 1889 5
-- Name: com_menu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE com_menu (
    menu_id smallint DEFAULT nextval('com_menu_seq'::regclass) NOT NULL,
    modul_id smallint NOT NULL,
    menu_nama character varying(200) NOT NULL,
    menu_url character varying(200) NOT NULL,
    menu_icon character varying(200),
    menu_parent character varying(200),
    status smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.com_menu OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 40987)
-- Dependencies: 5
-- Name: com_menu_role_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE com_menu_role_seq
    START WITH 30
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.com_menu_role_seq OWNER TO postgres;

--
-- TOC entry 1923 (class 0 OID 0)
-- Dependencies: 171
-- Name: com_menu_role_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('com_menu_role_seq', 30, false);


--
-- TOC entry 170 (class 1259 OID 40981)
-- Dependencies: 1892 1893 5
-- Name: com_menu_role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE com_menu_role (
    role_id integer DEFAULT nextval('com_menu_role_seq'::regclass) NOT NULL,
    group_id integer NOT NULL,
    user_tipe_id integer NOT NULL,
    view integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.com_menu_role OWNER TO postgres;

--
-- TOC entry 167 (class 1259 OID 32984)
-- Dependencies: 5
-- Name: com_modul_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE com_modul_seq
    START WITH 24
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.com_modul_seq OWNER TO postgres;

--
-- TOC entry 1924 (class 0 OID 0)
-- Dependencies: 167
-- Name: com_modul_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('com_modul_seq', 133, true);


--
-- TOC entry 172 (class 1259 OID 49152)
-- Dependencies: 1894 1895 5
-- Name: com_modul; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE com_modul (
    modul_id smallint DEFAULT nextval('com_modul_seq'::regclass) NOT NULL,
    modul_nama character varying(200) NOT NULL,
    modul_url character varying(200) NOT NULL,
    modul_icon character varying(200),
    status smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.com_modul OWNER TO postgres;

--
-- TOC entry 164 (class 1259 OID 32977)
-- Dependencies: 5
-- Name: com_user_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE com_user_seq
    START WITH 24
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.com_user_seq OWNER TO postgres;

--
-- TOC entry 1925 (class 0 OID 0)
-- Dependencies: 164
-- Name: com_user_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('com_user_seq', 24, false);


--
-- TOC entry 161 (class 1259 OID 32769)
-- Dependencies: 1884 1885 5
-- Name: com_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE com_user (
    user_id integer DEFAULT nextval('com_user_seq'::regclass) NOT NULL,
    username character varying(100) NOT NULL,
    password character varying(200) NOT NULL,
    nama character varying(100),
    avatar character varying(200),
    user_tipe integer NOT NULL,
    jenis_kelamin character varying(200),
    email character varying(100),
    last_login timestamp without time zone NOT NULL,
    status integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.com_user OWNER TO postgres;

--
-- TOC entry 165 (class 1259 OID 32979)
-- Dependencies: 5
-- Name: com_user_tipe_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE com_user_tipe_seq
    START WITH 24
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.com_user_tipe_seq OWNER TO postgres;

--
-- TOC entry 1926 (class 0 OID 0)
-- Dependencies: 165
-- Name: com_user_tipe_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('com_user_tipe_seq', 24, true);


--
-- TOC entry 162 (class 1259 OID 32775)
-- Dependencies: 1886 1887 5
-- Name: com_user_tipe; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE com_user_tipe (
    tipe_id integer DEFAULT nextval('com_user_tipe_seq'::regclass) NOT NULL,
    tipe_nama character varying(100) NOT NULL,
    tipe_home character varying(200),
    tipe_homebase character varying(200) NOT NULL,
    status smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.com_user_tipe OWNER TO postgres;

--
-- TOC entry 1911 (class 0 OID 40960)
-- Dependencies: 168
-- Data for Name: com_code; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY com_code (id, title, value_1, value_2, value_3, status) FROM stdin;
\.


--
-- TOC entry 1910 (class 0 OID 32968)
-- Dependencies: 163
-- Data for Name: com_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY com_menu (menu_id, modul_id, menu_nama, menu_url, menu_icon, menu_parent, status) FROM stdin;
2	1	pendaftaran rawat jalan	pendaftaran/pendaftaran_rawat_jalan	\N	\N	1
3	1	pendaftaran rawat inap	pendaftaran/pendaftaran_rawat_inap	\N	\N	1
4	1	IGD	pendaftaran/IGD	\N	\N	1
5	2	informasi pasien	pelayanan_informasi/informasi_pasien	\N	\N	1
6	2	jadwal dokter	pelayanan_informasi/jadwal_dokter	\N	\N	1
7	2	informasi kamar	pelayanan_informasi/informasi_kamar	\N	\N	1
8	2	informasi paket	pelayanan_informasi/informasi_paket	\N	\N	1
9	3	poli gigi	rawat_jalan/poli_gigi	\N	\N	1
10	3	poli tulang	rawat_jalan/poli_tulang	\N	\N	1
1	1	pendaftaran baru	pendaftaran/pendaftaran_baru	\N		1
11	4	rawat jalan	kasir/rawat_jalan	\N	\N	1
12	4	IGD	kasir/IGD	\N	\N	1
13	4	rawat inap	kasir/rawat_inap	\N	\N	1
\.


--
-- TOC entry 1912 (class 0 OID 40981)
-- Dependencies: 170
-- Data for Name: com_menu_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY com_menu_role (role_id, group_id, user_tipe_id, view) FROM stdin;
\.


--
-- TOC entry 1913 (class 0 OID 49152)
-- Dependencies: 172
-- Data for Name: com_modul; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY com_modul (modul_id, modul_nama, modul_url, modul_icon, status) FROM stdin;
2	pelayanan informasi	pelayanan_informasi		1
3	rawat jalan	rawat_jalan		1
1	pendaftaran	pendaftaran		1
4	kasir	kasir		1
\.


--
-- TOC entry 1908 (class 0 OID 32769)
-- Dependencies: 161
-- Data for Name: com_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY com_user (user_id, username, password, nama, avatar, user_tipe, jenis_kelamin, email, last_login, status) FROM stdin;
1	jike	49deebdfb953a2f52e2ac0931cf29b72	jike		1	Laki-laki	jike@yahoo.com	2012-10-09 13:41:45	1
\.


--
-- TOC entry 1909 (class 0 OID 32775)
-- Dependencies: 162
-- Data for Name: com_user_tipe; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY com_user_tipe (tipe_id, tipe_nama, tipe_home, tipe_homebase, status) FROM stdin;
24	admin	admin	admin	1
1	super_admin	super_admin	home	1
\.


--
-- TOC entry 1903 (class 2606 OID 40967)
-- Dependencies: 168 168
-- Name: com_code_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY com_code
    ADD CONSTRAINT com_code_pkey PRIMARY KEY (id);


--
-- TOC entry 1901 (class 2606 OID 32976)
-- Dependencies: 163 163
-- Name: com_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY com_menu
    ADD CONSTRAINT com_menu_pkey PRIMARY KEY (menu_id);


--
-- TOC entry 1905 (class 2606 OID 40985)
-- Dependencies: 170 170
-- Name: com_menu_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY com_menu_role
    ADD CONSTRAINT com_menu_role_pkey PRIMARY KEY (role_id);


--
-- TOC entry 1907 (class 2606 OID 49161)
-- Dependencies: 172 172
-- Name: com_modul_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY com_modul
    ADD CONSTRAINT com_modul_pkey PRIMARY KEY (modul_id);


--
-- TOC entry 1897 (class 2606 OID 32912)
-- Dependencies: 161 161
-- Name: com_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY com_user
    ADD CONSTRAINT com_user_pkey PRIMARY KEY (user_id);


--
-- TOC entry 1899 (class 2606 OID 32782)
-- Dependencies: 162 162
-- Name: user_tipe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY com_user_tipe
    ADD CONSTRAINT user_tipe_pkey PRIMARY KEY (tipe_id);


--
-- TOC entry 1918 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2012-11-05 17:38:49

--
-- PostgreSQL database dump complete
--

