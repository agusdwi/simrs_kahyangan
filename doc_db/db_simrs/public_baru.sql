/*
Navicat PGSQL Data Transfer

Source Server         : jike
Source Server Version : 90103
Source Host           : localhost:5432
Source Database       : simrs_new
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90103
File Encoding         : 65001

Date: 2012-10-20 10:03:29
*/


-- ----------------------------
-- Sequence structure for "public"."com_code_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_code_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 30
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_menu_role_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_menu_role_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 30
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_menu_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_menu_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 68
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_modul_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_modul_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 129
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_user_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_user_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 24
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_user_tipe_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_user_tipe_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 24
 CACHE 1;

-- ----------------------------
-- Table structure for "public"."com_code"
-- ----------------------------
CREATE TABLE "public"."com_code" (
"id" int4 DEFAULT nextval('com_code_seq'::regclass) NOT NULL,
"title" varchar(200) NOT NULL,
"value_1" text,
"value_2" text,
"value_3" text,
"status" int4 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_code
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."com_menu"
-- ----------------------------
CREATE TABLE "public"."com_menu" (
"menu_id" int2 DEFAULT nextval('com_menu_seq'::regclass) NOT NULL,
"modul_id" int2 NOT NULL,
"menu_nama" varchar(200) NOT NULL,
"menu_url" varchar(200) NOT NULL,
"menu_icon" varchar(200),
"menu_parent" varchar(200),
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_menu
-- ----------------------------
INSERT INTO "public"."com_menu" VALUES ('45', '114', 'informasi pasien', 'pelayanan_informasi/informasi_pasien', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('46', '114', 'jadwal dokter', 'pelayanan_informasi/jadwal_dokter', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('47', '115', 'registrasi rekam medis', 'rekam_medik/registrasi_rekam_medis', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('48', '115', 'pelayanan', 'rekam_medik/pelayanan', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('49', '115', 'administrasi', 'rekam_medik/administrasi', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('50', '115', 'diagnosa', 'rekam_medik/diagnosa', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('51', '115', 'laporan', 'rekam_medik/laporan', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('52', '116', 'EMR', 'rawat_jalan/EMR', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('53', '116', 'laporan', 'rawat_jalan/laporan', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('54', '117', 'EMR', 'rawat_inap/EMR', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('55', '117', 'laporan', 'rawat_inap/laporan', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('56', '118', 'laporan', 'UGD/laporan', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('57', '120', 'laporan', 'ICU/laporan', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('58', '123', 'radiologi', 'pelayanan_penunjang/radiologi', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('59', '114', 'laborat', 'pelayanan_informasi/laborat', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('60', '125', 'registrasi', 'mobilisasi_dana/registrasi', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('61', '125', 'pembayaran', 'mobilisasi_dana/pembayaran', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('62', '127', 'front office', 'keuangan/front_office', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('63', '127', 'perbendaharaan', 'keuangan/perbendaharaan', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('64', '127', 'penyusunan anggaran', 'keuangan/penyusunan_anggaran', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('65', '128', 'general leader', 'akutansi/general_leader', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('66', '128', 'laporan akutansi manajemen', 'akutansi/laporan_akutansi_manajemen', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('67', '128', 'unit cost', 'akutansi/unit_cost', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('68', '128', 'verifikasi', 'akutansi/verifikasi', null, null, '1');

-- ----------------------------
-- Table structure for "public"."com_menu_role"
-- ----------------------------
CREATE TABLE "public"."com_menu_role" (
"role_id" int4 DEFAULT nextval('com_menu_role_seq'::regclass) NOT NULL,
"group_id" int4 NOT NULL,
"user_tipe_id" int4 NOT NULL,
"view" int4 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_menu_role
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."com_modul"
-- ----------------------------
CREATE TABLE "public"."com_modul" (
"modul_id" int2 DEFAULT nextval('com_modul_seq'::regclass) NOT NULL,
"modul_nama" varchar(200) NOT NULL,
"modul_url" varchar(200) NOT NULL,
"modul_icon" varchar(200),
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_modul
-- ----------------------------
INSERT INTO "public"."com_modul" VALUES ('114', 'pelayanan informasi', 'pelayanan_informasi', '', '1');
INSERT INTO "public"."com_modul" VALUES ('115', 'rekam medik', 'rekam_medik', '', '1');
INSERT INTO "public"."com_modul" VALUES ('116', 'rawat jalan', 'rawat_jalan', '', '1');
INSERT INTO "public"."com_modul" VALUES ('117', 'rawat inap', 'rawat_inap', '', '1');
INSERT INTO "public"."com_modul" VALUES ('118', 'UGD', 'UGD', '', '1');
INSERT INTO "public"."com_modul" VALUES ('120', 'ICU', 'ICU', '', '1');
INSERT INTO "public"."com_modul" VALUES ('121', 'bedah sentral', 'bedah_sentral', '', '1');
INSERT INTO "public"."com_modul" VALUES ('122', 'anestesi reanimasi rehab medic', 'anestesi_reanimasi_rehab_medic', '', '1');
INSERT INTO "public"."com_modul" VALUES ('123', 'pelayanan penunjang', 'pelayanan_penunjang', '', '1');
INSERT INTO "public"."com_modul" VALUES ('124', 'apotek dan inventory', 'apotek_dan_inventory', '', '1');
INSERT INTO "public"."com_modul" VALUES ('125', 'mobilisasi dana', 'mobilisasi_dana', '', '1');
INSERT INTO "public"."com_modul" VALUES ('126', 'kasir', 'kasir', '', '1');
INSERT INTO "public"."com_modul" VALUES ('127', 'keuangan', 'keuangan', '', '1');
INSERT INTO "public"."com_modul" VALUES ('128', 'akutansi', 'akutansi', '', '1');
INSERT INTO "public"."com_modul" VALUES ('129', 'SDM', 'SDM', '', '1');

-- ----------------------------
-- Table structure for "public"."com_user"
-- ----------------------------
CREATE TABLE "public"."com_user" (
"user_id" int4 DEFAULT nextval('com_user_seq'::regclass) NOT NULL,
"username" varchar(100) NOT NULL,
"password" varchar(200) NOT NULL,
"nama" varchar(100),
"avatar" varchar(200),
"user_tipe" int4 NOT NULL,
"jenis_kelamin" varchar(200),
"email" varchar(100),
"last_login" timestamp(6) NOT NULL,
"status" int4 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_user
-- ----------------------------
INSERT INTO "public"."com_user" VALUES ('3', 'jike', '49deebdfb953a2f52e2ac0931cf29b72', 'jike', '', '1', 'Laki-laki', 'jike@yahoo.com', '2012-10-09 13:41:45', '1');

-- ----------------------------
-- Table structure for "public"."com_user_tipe"
-- ----------------------------
CREATE TABLE "public"."com_user_tipe" (
"tipe_id" int4 DEFAULT nextval('com_user_tipe_seq'::regclass) NOT NULL,
"tipe_nama" varchar(100) NOT NULL,
"tipe_home" varchar(200),
"tipe_homebase" varchar(200) NOT NULL,
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_user_tipe
-- ----------------------------
INSERT INTO "public"."com_user_tipe" VALUES ('1', 'super_admin', 'super_admin', 'home', '1');
INSERT INTO "public"."com_user_tipe" VALUES ('24', 'admin', 'admin', 'admin', '1');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table "public"."com_code"
-- ----------------------------
ALTER TABLE "public"."com_code" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table "public"."com_menu"
-- ----------------------------
ALTER TABLE "public"."com_menu" ADD PRIMARY KEY ("menu_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_menu_role"
-- ----------------------------
ALTER TABLE "public"."com_menu_role" ADD PRIMARY KEY ("role_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_modul"
-- ----------------------------
ALTER TABLE "public"."com_modul" ADD PRIMARY KEY ("modul_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_user"
-- ----------------------------
ALTER TABLE "public"."com_user" ADD PRIMARY KEY ("user_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_user_tipe"
-- ----------------------------
ALTER TABLE "public"."com_user_tipe" ADD PRIMARY KEY ("tipe_id");
