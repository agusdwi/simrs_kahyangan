/*
Navicat PGSQL Data Transfer

Source Server         : jike
Source Server Version : 90103
Source Host           : localhost:5432
Source Database       : simrs_new
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90103
File Encoding         : 65001

Date: 2012-10-09 22:07:34
*/


-- ----------------------------
-- Sequence structure for "public"."com_menu_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_menu_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 24
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_modul_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_modul_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 28
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_user_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_user_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 24
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_user_tipe_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_user_tipe_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 24
 CACHE 1;

-- ----------------------------
-- Table structure for "public"."com_menu"
-- ----------------------------
CREATE TABLE "public"."com_menu" (
"menu_id" int2 DEFAULT nextval('com_menu_seq'::regclass) NOT NULL,
"modul_id" int2 NOT NULL,
"menu_nama" varchar(200) NOT NULL,
"menu_url" varchar(200) NOT NULL,
"menu_icon" varchar(200) NOT NULL,
"menu_parent" varchar(200) NOT NULL,
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_menu
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."com_modul"
-- ----------------------------
CREATE TABLE "public"."com_modul" (
"modul_id" int2 DEFAULT nextval('com_modul_seq'::regclass) NOT NULL,
"modul_nama" varchar(200) NOT NULL,
"modul_url" varchar(200) NOT NULL,
"modul_icon" varchar(200),
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_modul
-- ----------------------------
INSERT INTO "public"."com_modul" VALUES ('27', 'nama modul', 'naama  modul', null, '1');
INSERT INTO "public"."com_modul" VALUES ('28', 'asfasf', 'asfasdasfas', '55545_451982308186400_1914389132_o.jpg', '1');

-- ----------------------------
-- Table structure for "public"."com_user"
-- ----------------------------
CREATE TABLE "public"."com_user" (
"user_id" int4 DEFAULT nextval('com_user_seq'::regclass) NOT NULL,
"username" varchar(100) NOT NULL,
"password" varchar(200) NOT NULL,
"nama" varchar(100),
"avatar" varchar(200),
"user_tipe" int4 NOT NULL,
"jenis_kelamin" varchar(200),
"email" varchar(100),
"last_login" timestamp(6) NOT NULL,
"status" int4 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_user
-- ----------------------------
INSERT INTO "public"."com_user" VALUES ('3', 'jike', '49deebdfb953a2f52e2ac0931cf29b72', 'jike', '', '1', 'Laki-laki', 'jike@yahoo.com', '2012-10-09 13:41:45', '1');

-- ----------------------------
-- Table structure for "public"."com_user_tipe"
-- ----------------------------
CREATE TABLE "public"."com_user_tipe" (
"tipe_id" int4 DEFAULT nextval('com_user_tipe_seq'::regclass) NOT NULL,
"tipe_nama" varchar(100) NOT NULL,
"tipe_home" varchar(200),
"tipe_homebase" varchar(200) NOT NULL,
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_user_tipe
-- ----------------------------
INSERT INTO "public"."com_user_tipe" VALUES ('1', 'super_admin', 'super_admin', 'super_admin', '1');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table "public"."com_menu"
-- ----------------------------
ALTER TABLE "public"."com_menu" ADD PRIMARY KEY ("menu_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_modul"
-- ----------------------------
ALTER TABLE "public"."com_modul" ADD PRIMARY KEY ("modul_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_user"
-- ----------------------------
ALTER TABLE "public"."com_user" ADD PRIMARY KEY ("user_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_user_tipe"
-- ----------------------------
ALTER TABLE "public"."com_user_tipe" ADD PRIMARY KEY ("tipe_id");
