-- data modul
INSERT INTO "com_modul" VALUES ('1', 'pendaftaran', 'pendaftaran', '', '1');
INSERT INTO "com_modul" VALUES ('2', 'pelayanan informasi', 'pelayanan_informasi', '', '1');
INSERT INTO "com_modul" VALUES ('3', 'rawat jalan', 'rawat_jalan', '', '1');
INSERT INTO "com_modul" VALUES ('4', 'kasir', 'kasir', '', '1');
INSERT INTO "com_modul" VALUES ('5', 'apotek', 'apotek', '', '1');
INSERT INTO "com_modul" VALUES ('6', 'gudang farmasi', 'gudang_farmasi', '', '1');


-- data menu
INSERT INTO "com_menu" VALUES ('14', '5', 'resep pasien', 'apotek/resep_pasien', null, '', '1');
INSERT INTO "com_menu" VALUES ('13', '4', 'rawat inap', 'kasir/rawat_inap', null, '', '1');
INSERT INTO "com_menu" VALUES ('12', '4', 'IGD', 'kasir/IGD', null, '', '1');
INSERT INTO "com_menu" VALUES ('11', '4', 'rawat jalan', 'kasir/rawat_jalan', null, '', '1');
INSERT INTO "com_menu" VALUES ('10', '3', 'poli tulang', 'rawat_jalan/poli_tulang', null, '', '1');
INSERT INTO "com_menu" VALUES ('9', '3', 'poli gigi', 'rawat_jalan/poli_gigi', null, '', '1');
INSERT INTO "com_menu" VALUES ('8', '2', 'informasi paket', 'pelayanan_informasi/informasi_paket', null, '', '1');
INSERT INTO "com_menu" VALUES ('7', '2', 'informasi kamar', 'pelayanan_informasi/informasi_kamar', null, '', '1');
INSERT INTO "com_menu" VALUES ('6', '2', 'jadwal dokter', 'pelayanan_informasi/jadwal_dokter', null, '', '1');
INSERT INTO "com_menu" VALUES ('5', '2', 'informasi pasien', 'pelayanan_informasi/informasi_pasien', null, '', '1');
INSERT INTO "com_menu" VALUES ('4', '1', 'IGD', 'pendaftaran/IGD', null, '', '1');
INSERT INTO "com_menu" VALUES ('3', '1', 'pendaftaran rawat inap', 'pendaftaran/pendaftaran_rawat_inap', null, '', '1');
INSERT INTO "com_menu" VALUES ('2', '1', 'pendaftaran rawat jalan', 'pendaftaran/pendaftaran_rawat_jalan', null, '', '1');
INSERT INTO "com_menu" VALUES ('1', '1', 'pendaftaran baru', 'pendaftaran/pendaftaran_baru', null, '', '1');
INSERT INTO "com_menu" VALUES ('15', '5', 'pembelian lansung', 'apotek/pembelian_langsung', null, '', '1');
INSERT INTO "com_menu" VALUES ('20', '6', 'stok', 'gudang_farmasi/stok', null, '', '1');
INSERT INTO "com_menu" VALUES ('19', '6', 'retur', 'gudang_farmasi/retur', null, '', '1');
INSERT INTO "com_menu" VALUES ('18', '6', 'receive item', 'gudang_farmasi/receive_item', null, '', '1');
INSERT INTO "com_menu" VALUES ('17', '6', 'purchase order', 'gudang_farmasi/purchase_order', null, '', '1');
INSERT INTO "com_menu" VALUES ('16', '6', 'purchase request', 'gudang_farmasi/purchase_request', null, '', '1');
INSERT INTO "com_menu" VALUES ('21', '6', 'transfer item', 'gudang_farmasi/transfer_item', null, '', '1');