/*
Navicat PGSQL Data Transfer

Source Server         : jike
Source Server Version : 90103
Source Host           : localhost:5432
Source Database       : simrs_new
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90103
File Encoding         : 65001

Date: 2012-10-24 00:20:39
*/


-- ----------------------------
-- Sequence structure for "public"."com_code_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_code_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 30
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_menu_role_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_menu_role_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 30
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_menu_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_menu_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 79
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_modul_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_modul_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 132
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_user_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_user_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 24
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."com_user_tipe_seq"
-- ----------------------------
CREATE SEQUENCE "public"."com_user_tipe_seq"
 INCREMENT 1
 MINVALUE 0
 MAXVALUE 9223372036854775807
 START 24
 CACHE 1;

-- ----------------------------
-- Table structure for "public"."com_code"
-- ----------------------------
CREATE TABLE "public"."com_code" (
"id" int4 DEFAULT nextval('com_code_seq'::regclass) NOT NULL,
"title" varchar(200) NOT NULL,
"value_1" text,
"value_2" text,
"value_3" text,
"status" int4 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_code
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."com_menu"
-- ----------------------------
CREATE TABLE "public"."com_menu" (
"menu_id" int2 DEFAULT nextval('com_menu_seq'::regclass) NOT NULL,
"modul_id" int2 NOT NULL,
"menu_nama" varchar(200) NOT NULL,
"menu_url" varchar(200) NOT NULL,
"menu_icon" varchar(200),
"menu_parent" varchar(200),
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_menu
-- ----------------------------
INSERT INTO "public"."com_menu" VALUES ('1', '1', 'pendaftaran baru', 'pendaftaran/pendaftaran_baru', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('2', '1', 'pendaftaran rawat jalan', 'pendaftaran/pendaftaran_rawat_jalan', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('3', '1', 'pendaftaran rawat inap', 'pendaftaran/pendaftaran_rawat_inap', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('4', '1', 'IGD', 'pendaftaran/IGD', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('5', '2', 'informasi pasien', 'pelayanan_informasi/informasi_pasien', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('6', '2', 'jadwal dokter', 'pelayanan_informasi/jadwal_dokter', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('7', '2', 'informasi kamar', 'pelayanan_informasi/informasi_kamar', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('8', '2', 'informasi paket', 'pelayanan_informasi/informasi_paket', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('9', '3', 'poli gigi', 'rawat_jalan/poli_gigi', null, null, '1');
INSERT INTO "public"."com_menu" VALUES ('10', '3', 'poli tulang', 'rawat_jalan/poli_tulang', null, null, '1');

-- ----------------------------
-- Table structure for "public"."com_menu_role"
-- ----------------------------
CREATE TABLE "public"."com_menu_role" (
"role_id" int4 DEFAULT nextval('com_menu_role_seq'::regclass) NOT NULL,
"group_id" int4 NOT NULL,
"user_tipe_id" int4 NOT NULL,
"view" int4 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_menu_role
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."com_modul"
-- ----------------------------
CREATE TABLE "public"."com_modul" (
"modul_id" int2 DEFAULT nextval('com_modul_seq'::regclass) NOT NULL,
"modul_nama" varchar(200) NOT NULL,
"modul_url" varchar(200) NOT NULL,
"modul_icon" varchar(200),
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_modul
-- ----------------------------
INSERT INTO "public"."com_modul" VALUES ('1', 'pendaftaran', 'pendaftaran', '', '1');
INSERT INTO "public"."com_modul" VALUES ('2', 'pelayanan informasi', 'pelayanan_informasi', '', '1');
INSERT INTO "public"."com_modul" VALUES ('3', 'rawat jalan', 'rawat_jalan', '', '1');

-- ----------------------------
-- Table structure for "public"."com_user"
-- ----------------------------
CREATE TABLE "public"."com_user" (
"user_id" int4 DEFAULT nextval('com_user_seq'::regclass) NOT NULL,
"username" varchar(100) NOT NULL,
"password" varchar(200) NOT NULL,
"nama" varchar(100),
"avatar" varchar(200),
"user_tipe" int4 NOT NULL,
"jenis_kelamin" varchar(200),
"email" varchar(100),
"last_login" timestamp(6) NOT NULL,
"status" int4 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_user
-- ----------------------------
INSERT INTO "public"."com_user" VALUES ('1', 'jike', '49deebdfb953a2f52e2ac0931cf29b72', 'jike', '', '1', 'Laki-laki', 'jike@yahoo.com', '2012-10-09 13:41:45', '1');

-- ----------------------------
-- Table structure for "public"."com_user_tipe"
-- ----------------------------
CREATE TABLE "public"."com_user_tipe" (
"tipe_id" int4 DEFAULT nextval('com_user_tipe_seq'::regclass) NOT NULL,
"tipe_nama" varchar(100) NOT NULL,
"tipe_home" varchar(200),
"tipe_homebase" varchar(200) NOT NULL,
"status" int2 DEFAULT 1 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of com_user_tipe
-- ----------------------------
INSERT INTO "public"."com_user_tipe" VALUES ('1', 'super_admin', 'super_admin', 'home', '1');
INSERT INTO "public"."com_user_tipe" VALUES ('24', 'admin', 'admin', 'admin', '1');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table "public"."com_code"
-- ----------------------------
ALTER TABLE "public"."com_code" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table "public"."com_menu"
-- ----------------------------
ALTER TABLE "public"."com_menu" ADD PRIMARY KEY ("menu_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_menu_role"
-- ----------------------------
ALTER TABLE "public"."com_menu_role" ADD PRIMARY KEY ("role_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_modul"
-- ----------------------------
ALTER TABLE "public"."com_modul" ADD PRIMARY KEY ("modul_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_user"
-- ----------------------------
ALTER TABLE "public"."com_user" ADD PRIMARY KEY ("user_id");

-- ----------------------------
-- Primary Key structure for table "public"."com_user_tipe"
-- ----------------------------
ALTER TABLE "public"."com_user_tipe" ADD PRIMARY KEY ("tipe_id");
