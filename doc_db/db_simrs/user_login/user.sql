/*
Navicat PGSQL Data Transfer

Source Server         : jike
Source Server Version : 90103
Source Host           : localhost:5432
Source Database       : simrs_new
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90103
File Encoding         : 65001

Date: 2012-10-08 11:18:02
*/


-- ----------------------------
-- Table structure for "public"."user"
-- ----------------------------
CREATE TABLE "public"."user" (
"user_id" int4 NOT NULL,
"username" varchar(100),
"password" varchar(255) NOT NULL,
"nama" varchar(100),
"avatar" varchar(255) NOT NULL,
"user_tipe" int4,
"jenis_kelamin" varchar(255) NOT NULL,
"email" varchar(100),
"last_login" timestamp(6),
"status" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO "public"."user" VALUES ('3', 'jike', 'a461c496df484b8aebeb6ca67ac96bfc', 'jike', '', '1', 'Laki-laki', null, null, '1');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
