/*
Navicat PGSQL Data Transfer

Source Server         : jike
Source Server Version : 90103
Source Host           : localhost:5432
Source Database       : simrs_new
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90103
File Encoding         : 65001

Date: 2012-10-08 23:28:09
*/


-- ----------------------------
-- Table structure for "public"."user_tipe"
-- ----------------------------
CREATE TABLE "public"."user_tipe" (
"tipe_id" int4 NOT NULL,
"tipe_nama" varchar(100) NOT NULL,
"tipe_home" varchar(255) NOT NULL,
"status" int4 NOT NULL,
"homebase" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of user_tipe
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."user"
-- ----------------------------
CREATE TABLE "public"."user" (
"user_id" int4 NOT NULL,
"username" varchar(100),
"password" varchar(255) NOT NULL,
"nama" varchar(100),
"avatar" varchar(255) NOT NULL,
"user_tipe" int4,
"jenis_kelamin" varchar(255) NOT NULL,
"email" varchar(100),
"last_login" timestamp(6),
"status" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO "public"."user" VALUES ('3', 'jike', '49deebdfb953a2f52e2ac0931cf29b72', 'jike', '', '1', 'Laki-laki', null, null, '1');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
