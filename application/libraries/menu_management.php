<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Menu_management {
	
	private $menu = array();
	var $html;
    function built_tree($data, $parent = 0) {

        static $i = -1;
        // debug_array($data[$parent]);
        if (isset($data[$parent])) {
   
            $i++;
            $html = '';
            foreach ($data[$parent] as $v) {

            	$jml_child = 0;
            	if(isset($data[$v->menu_id])){
            		foreach ($data[$v->menu_id] as $k) {
		        		if($k->menu_parent == $v->menu_id){
		        			$jml_child += 1;
		        		}
		        	}
            	}

            	if($data['current'] == $v->menu_id){
            		$klas = 'active open';
            	}else{
            		$klas = ' ';
            	}

	        	// debug_array($jml_child,true);

	        	if($jml_child == 0){
	        		$html .= "<li class='$klas'><a href='".base_url().$v->menu_url."' ><i class='icon icon-home'></i> <span>".$v->menu_name."</span></a>";
	        	}else{
	        		$html .= "<li class='submenu $klas'><a href='".base_url().$v->menu_url."' ><i class='icon icon-home'></i> <span>".$v->menu_name."</span></a>";
	        	}
            	
				$child = $this->built_tree($data, $v->menu_id);
                if ($child) {
                	$html .= "<ul>";
                	$html .= $child;
                	$html .= "</ul>";
                    $i--;
				}
				$html .= "</li>";
			}
			// debug_array($html,true);
            return $html;
        } else {
            return false;
        }
    }
	
	function get($modul_id,$current)
	{
		$CI =& get_instance();
		$CI->db->order_by('menu_id');
		$ds = $CI->db->get_where('sys_menu', array('menu_status' => 1,'modul_id'=>$modul_id));
		//debug_array($ds->result());
		$data = array();
		foreach ($ds->result() as $row) {
			$data[$row->menu_parent][] = $row; 
		}
		$data['current'] = $current;
		// debug_array($data);
		return $this->built_tree($data);
	}
}