<div class="userinfo">
    <img src="<?=base_url()?>assets/images/thumbs/avatar.png" alt="" />
    <span><?=get_user('username')?></span>
</div><!--userinfo-->
            
<div class="userinfodrop">
    <div class="userdata">
        <span class="email"></span>
        <ul>
            <li><a href="editprofile.html">Edit Profile</a></li>
            <li><a href="accountsettings.html">Account Settings</a></li>
            <li><a href="help.html">Help</a></li>
            <li><a href="<?=base_url()?>logout">Sign Out</a></li>
        </ul>
    </div><!--userdata-->
</div><!--userinfodrop-->