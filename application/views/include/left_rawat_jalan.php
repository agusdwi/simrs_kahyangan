<?
	$mst_poli 	= $this->db->get_where('trx_poly',array('pl_status'=>1));
	$poli_id	= (isset($cur_poli)? $cur_poli->pl_id:"");
?>
<ul style="display: block;">
	<?foreach ($mst_poli->result() as $key): ?>
		<li class="<?=($key->pl_id == $poli_id)? 'active open': '' ;?>">
			<a href="<?=base_url()?>rawat_jalan/poli/antrian/<?=$key->pl_id;?>">
				<i class="icon icon-home">
				</i> <span><?=$key->pl_name;?></span>
			</a>
		</li>	
	<?endforeach ?>
	<li class="<?=($poli_id == 99)? 'active open': '' ;?>">
		<a href="<?=base_url()?>rawat_jalan/manual">
			<i class="icon icon-home">
			</i> <span>Manual Input</span>
		</a>
	</li>	
</ul>