<?
   $CI =& get_instance();
    $modul = "SELECT * FROM com_modul where status = 1 ORDER BY sort ASC";
    $dm = $CI->db->query($modul);

    $uri = $this->uri->segment(1);
?>

<br clear='all'>
<div class="header">
    <ul class="headermenu" style="margin-left:2px !important;width:100%">
		<li>
			<a href="<?=base_url()?>" style="min-width:0px !important">
				<img src="<?=base_url()?>assets/img/icons/24/home.png"> 
			</a>
		</li>
        <? foreach ($dm->result() as $d):?>
        <li>
            <a class="<?=cek_current($d->modul_nama,$uri);?>" href="<?=base_url()?><?=$d->modul_url?>" >
                <img src="<?=base_url()?>assets/images/icons/star.png" style="margin:2px 6px" /> 
                <?=$d->modul_nama?>
            </a>
        </li>
        <?endforeach?>
        <li style="float:right">
            <a href="<?=base_url()?>login/logout" style="min-width:0px !important">
                <img src="<?=base_url()?>assets/img/icons/24/off.png"> 
                logout
            </a>
        </li>
    </ul>
 </div>

<?
    function cek_current($n,$uri){
        if($uri == strtolower(str_replace(' ', '_', $n)))
            return 'active';
        else{
            if($uri == "master" && strtolower(str_replace(' ', '_', $n)) == "master_data")
                return 'active';
            else if($uri == "user" && strtolower(str_replace(' ', '_', $n)) == "manajemen_user")
                return 'active';
            else return '';
        }
    }
?>