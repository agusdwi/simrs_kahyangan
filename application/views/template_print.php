<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css" />
	    <link rel="stylesheet" href="<?=base_url()?>assets/css/unicorn.main.css" />
	    <style type="text/css">
	    	*{
	    		font-size: 100%;
	    	}
	    </style>
	</head>
	<body>
		<? $this->load->view("$main_view");?>
	</body>
</html>