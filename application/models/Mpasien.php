<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpasien extends CI_Model { 
	function __construct() {
		parent::__construct();
	}

	function get_profil($id){

	}

	function get_alergi($id){
		return $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$id))->row()->sd_alergi;
	}

	function set_alergi($id,$alergi){
		$data = array(
		               'sd_alergi' =>  $alergi
		            );
		$this->db->where('sd_rekmed', $id);
		$this->db->update('ptn_social_data', $data); 
	}

	function get_rekmed($id){
		$data['medical'] 	= $this->db->get_where('v_rekmed_medical',array('mdc_id'=>$id))->row();
		$data['diagnosis'] 	= $this->get_rekmed_diag($id);
		$data['tindakan'] 	= $this->db->get_where('v_trx_treat',array('mdc_id'=>$id,'is_delete'=>0));
		$data['obat'] 		= $this->db->get_where('v_trx_recipe',array('mdc_id'=>$id,'is_delete'=>0));
		return $data;
	}

	function get_rekmed_diag($id){
		$r = array();
		$d = $this->db->get_where('v_trx_diag',array('mdc_id'=>$id,'is_delete'=>0));
		foreach ($d->result() as $key) {
			if (!empty($key->diag_name)) {
				$r['diagnosa'][] = $key->diag_code.' '.$key->diag_name;
			}
			if (!empty($key->anam_title)) {
				$r['anamnesa'][] = $key->anam_title.' '.$key->anam_desc;
			}
			if (!empty($key->pemrk_fisik)) {
				$r['pemeriksaan fisik'][] = $key->pemrk_fisik;
			}
			if (!empty($key->pemrk_penunjang)) {
				$r['pemeriksaan penunjang'][] = $key->pemrk_penunjang;
			}
		}
		return $r;
	}

	function get_harga_obat($id,$ins){
		$sql = "SELECT t.*, 
				(select mi_price from mst_medicine_insurance where mdcn_id = t.mdcn_id and ins_id = $ins) as harga 
					from v_trx_recipe t where t.mdc_id = $id and is_delete = 0" ;
		$obat = $this->db->query($sql);
		$d = array();
		foreach ($obat->result() as $key) {
			$key->harga = ($key->harga == '')? 0 : $key->harga ;
			$d[] = $key;
		}
		return $d;
		// $obat = $this->db->get_where('',); 
	}

	function get_harga_obat_ranap($id,$ins){
		$sql = "SELECT t.*, 
				(select mi_price from mst_medicine_insurance where mdcn_id = t.mdcn_id and ins_id = $ins) as harga 
					from v_rnp_recipe t where t.rnp_id = '$id' and is_delete = 0" ;
		$obat = $this->db->query($sql);
		$d = array();
		foreach ($obat->result() as $key) {
			$key->harga = ($key->harga == '')? 0 : $key->harga ;
			$d[] = $key;
		}
		return $d;
		// $obat = $this->db->get_where('',); 
	}

	function get_harga_obat_rnp($id,$ins,$tgl=""){
		$sql = "SELECT t.*, 
				(select mi_price from mst_medicine_insurance where mdcn_id = t.mdcn_id and ins_id = $ins) as harga 
					from v_rnp_recipe t where t.rnp_id = '$id' and is_delete = 0" ;
		$sql .= ($tgl != "" )? " and tgl = '$tgl'" :" ";
		$obat = $this->db->query($sql);

		$d = array();
		foreach ($obat->result() as $key) {
			$key->harga = ($key->harga == '')? 0 : $key->harga ;
			$d[] = $key;
		}
		return $d;
		// $obat = $this->db->get_where('',); 
	}

	function get_rekmed_ranap($id){
		$data['medical'] 	= $this->db->get_where('v_rnp_medical',array('rnp_id'=>$id))->row();
		$data['diagnosis'] 	= $this->get_rekmed_diag_ranap($id);
		$data['tindakan'] 	= $this->get_rekmed_treat_ranap($id);
		$data['obat'] 		= $this->get_rekmed_obat_ranap($id);
		return $data;
	}

	function get_rekmed_diag_ranap($id,$date=""){
		if ($date=="") {
			$criteria = array('rnp_id'=>$id,'is_delete'=>0);
		}else{
			$criteria = array('rnp_id'=>$id,'is_delete'=>0,'tgl'=>$date);
		}
		$d = $this->db->get_where('v_rnp_diag',$criteria);
		$r = array();
		foreach ($d->result() as $key) { 
			
			if (!empty($key->diag_name)) {
				$r[$key->tgl]['diagnosa'][] = $key->diag_code.' '.$key->diag_name;
			}
			if (!empty($key->anam_title)) {
				$r[$key->tgl]['anamnesa'][] = $key->anam_title.' '.$key->anam_desc;
			}
			if (!empty($key->pemrk_fisik)) {
				$r[$key->tgl]['pemeriksaan fisik'][] = $key->pemrk_fisik;
			}
			if (!empty($key->pemrk_penunjang)) {
				$r[$key->tgl]['pemeriksaan penunjang'][] = $key->pemrk_penunjang;
			}
		}
		ksort($r);
		return $r;
	}

	function get_rekmed_treat_ranap($id){
		$trx = $this->db->get_where('v_rnp_treat',array('rnp_id'=>$id,'is_delete'=>0));
		$r = array();
		foreach ($trx->result() as $key) {
			$r[$key->tgl][] = $key;
		}
		return $r;
	}

	function get_rekmed_obat_ranap($id){
		$trx = $this->db->get_where('v_rnp_recipe',array('rnp_id'=>$id,'is_delete'=>0));
		$r = array();
		foreach ($trx->result() as $key) {
			$r[$key->tgl][] = $key;
		}
		ksort($r);
		return $r;
	}

	function get_date_rekmed($id){
		$tgl = array();
		$rajal		= $this->db->get_where('v_rekmed_date',array('sd_rekmed'=>$id,'mdc_status'=>3));
		$ranap		= $this->db->get_where('v_rnp_rekmed_date',array('sd_rekmed'=>$id,'rnp_status'=>1));
		foreach ($rajal->result() as $key) {
			$tgl[substr($key->mdc_in, 0,10)][] =array('tp'=>'rajal','value'=>$key);
		}
		foreach ($ranap->result() as $key) {
			$tgl[substr($key->rnp_in, 0,10)][] =array('tp'=>'ranap','value'=>$key);
		}
		ksort($tgl);
		return $tgl;
	}

	function get_rekmed_date($id){
		$date = array();
		$rajal = $this->db->get_where('v_rekmed_date',array('sd_rekmed'=>$this->ptn->sd_rekmed,'mdc_status'=>3));
		foreach ($rajal->result() as $key) {
			$date[$key->mdc_in] = array(
					'tipe'	=> 'rajal',
					'id'	=> $key->mdc_id
				);
		}
		//print_r($this->ptn->sd_rekmed);
		//exit();
		$ranap = $this->db->get_where('v_rnp_rekmed_date',array('sd_rekmed'=>$this->ptn->sd_rekmed, 'rnp_status'=>3));
		foreach ($ranap->result() as $key) {
			$date[$key->rnp_in] = array(
					'tipe'	=> 'ranap',
					'id'	=> $key->rnp_id,
					'out'	=> $key->rnp_out,
				);
		}
		krsort($date);
		return $date;
	}

	function get_rnp_rekmed_date($id){
		$date = array();
		$ranap = $this->db->get_where('v_rnp_rekmed_date',array('sd_rekmed'=>$this->ptn->sd_rekmed,'rnp_status'=>1));
		foreach ($ranap->result() as $key) {
			$date[$key->rnp_in] = array(
					'tipe'	=> 'ranap',
					'id'	=> $key->rnp_id
				);
		}
		$ranap = $this->db->get_where('v_rnp_medical',array('rnp_status'=>1,'sd_rekmed'=>$this->ptn->sd_rekmed));
		foreach ($ranap->result() as $key) {
			$date[$key->rnp_in] = array(
					'tipe'	=> 'ranap',
					'id'	=> $key->rnp_id,
					'out'	=> $key->rnp_out,
				);
		}
		ksort($date);
		//print_r($date);
		return $date;
	}

	function get_total_harga_ranap($id){
		$mdc = $this->db->get_where('v_rnp_medical',array('rnp_id'=>$id))->row();

		$date = substr($mdc->rnp_in, 0,10);

		$dstart = new DateTime($date);
		if($mdc->rnp_out == ''){
			$dend   = new DateTime();
		}else{
			$dend = new DateTime(substr($mdc->rnp_out, 0,10));
			$dend->modify( 'next day' );
		}

		$result = array();

		while ( $dstart < $dend){

			$treat		= $this->db->get_where('v_rnp_treat_param_name',array('rnp_id'=>$id,'is_delete'=>0,'tgl'=>$dstart->format('Y-m-d')));
			//debug_array($treat->result());
			$obat		= $this->Mpasien->get_harga_obat_rnp($id,$this->rnp->tp_insurance,$dstart->format('Y-m-d'));
			
			$kamar		= $this->db->get_where('v_rnp_kamar ',array('rnp_id'=>$id,'tgl'=>$dstart->format('Y-m-d')));

			if(($treat->num_rows() > 0) || (count($obat) > 0 || $kamar->num_rows > 0)){
				$result[$dstart->format('Y-m-d')] = array();

				if($treat->num_rows() > 0)
					$result[$dstart->format('Y-m-d')]['treat'] = $treat->result();

				if(count($obat) > 0)
					$result[$dstart->format('Y-m-d')]['obat'] = $obat;

				if($kamar->num_rows() > 0)
					$result[$dstart->format('Y-m-d')]['kamar'] = $kamar->result();
			}

			$dstart->modify( 'next day' );
		}
		return $result;
	}

	function get_rnp_id(){
		$ym = date('ym');
		$a = $this->db->like('queo_id',$ym,'after')->order_by('queo_id','DESC')->get('trx_queue_outpatient',1,0)->row();
		$queo = count($a) == 0 ?date('ym')."0001" : $a->queo_id+1;
		//echo $queo;
		$no = count($a) == 0 ? 1 : $a->queo_no+1;
		$user = get_user();
		$data = array('queo_id'	=> $queo,'queo_status'=>0);		
		$this->db->insert('trx_queue_outpatient',$data);
		return $queo;
	}
}