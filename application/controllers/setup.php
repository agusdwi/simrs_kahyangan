<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup extends CI_Controller {

	var $path = "application/db/";

	public function index()
	{
		$files = glob($this->path."*.*");
		$d = array();
		foreach ($files as $key) {
			$d[] = array(
				'name'		=> $key,
				'permit'	=> substr(sprintf('%o', fileperms($key)), -4)
			);
		}
		debug_array($d);
	}

	function proses(){
		$this->chmod_R($this->path,'777','777');
	}

	function chmod_R($path, $filemode, $dirmode) { 
		if (is_dir($path) ) { 
			if (!chmod($path, $dirmode)) { 
				$dirmode_str=decoct($dirmode); 
				print "Failed applying filemode '$dirmode_str' on directory '$path'\n"; 
				print "  `-> the directory '$path' will be skipped from recursive chmod\n"; 
				return; 
			} 
			$dh = opendir($path); 
			while (($file = readdir($dh)) !== false) { 
            if($file != '.' && $file != '..') {  // skip self and parent pointing directories 
            	$fullpath = $path.'/'.$file; 
            	chmod_R($fullpath, $filemode,$dirmode); 
            } 
        } 
        closedir($dh); 
    } else { 
    	if (is_link($path)) { 
    		print "link '$path' is skipped\n"; 
    		return; 
    	} 
    	if (!chmod($path, $filemode)) { 
    		$filemode_str=decoct($filemode); 
    		print "Failed applying filemode '$filemode_str' on file '$path'\n"; 
    		return; 
    	} 
    	} 
	} 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */