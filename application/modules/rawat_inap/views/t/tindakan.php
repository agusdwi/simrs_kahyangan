<?=$this->load->view('t/a_info');?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">			
			<?=$this->load->view('t/a_date');?>
			<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0;margin-left:100px;" >
				<?=form_open('',array('id' => 'form_tdk')); ?>
					<?=$this->load->view('t/a_menu_tab');?>
					
					<div id="dtable" style="padding:10px;border-bottom:1px solid #dddddd">
						<table class="table table-bordered def_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
							<thead>
								<tr role="row">
									<th class="sorting" style="width:40% !important">Tindakan</th>
									<th class="sorting" style="width:40% !important">Data</th>
									<th class="sorting" style="width:20% !important">Jumlah</th>
								</tr>
							</thead>
							<tbody>
								<?foreach ($ds->result() as $key): ?>
									<tr>
										<td><?=$key->treat_code;?></td>
										<td><?=$key->treat_name;?></td>
										<td>
											<?=$key->jumlah;?>
											<a class="label label-important ajaxDelete onhover" href="<?=cur_url()?><?=$key->rnp_treat_id;?>">
												delete
											</a>
										</td>
									</tr>	
								<?endforeach ?>
							</tbody>
						</table>
					</div>	

					<div id="dform" class="well" style="width:365px;margin-left:10px;margin-top:10px;padding:10px">					
						<b>Input Tindakan</b><br>
						<input type="hidden" class="bigdrop" id="select2_tindakan" style="width:300px;" autofocus/>
						<input type="hidden" id="hd_tindakan" name="ds[treat_id]"/>
						<textarea style="margin-bottom:0px;width:350px" rows="2" id="tdk_deskripsi" placeholder="deskripsi" tabIndex="-1" readonly></textarea>
						<p class="inline_form">Jumlah <input type="text" name="ds[jumlah]" style="width:50px" id="jumlah" value="1"></p>
						<button id="tambah" type="button" name="save" style="float:right;margin-top:10px" class="btn btn-small btn-primary simpan">tambahkan</button>
						<div class="clearfix"></div>
					</div>	
					<?=$this->load->view('t/a_bottom');?>
				<?=form_close()?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var select2placeholder = "";
	$(function(){
		preventSubmit();
		$("body").on('click','#tambah',function(){
			$.post( "", $("#form_tdk").serialize(),function(data){
				$("#form_tdk").find("input[type=text], textarea").val("");
				$("#hd_diag").val("");
				$("#jumlah").val("1");
				$("#select2_tindakan").select2("val", "");
				var d = data.data;
				var del = '<a class="label label-important ajaxDelete onhover" href="'+CUR+d.rnp_treat_id+'">delete</a>';
				otb.dataTable().fnAddData([d.treat_code,d.treat_name,d.jumlah+del]);
				$("#select2_tindakan").select2('focus');
			},'json');
			return false;
		})
	})

</script>