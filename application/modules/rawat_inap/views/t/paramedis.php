<?=$this->load->view('t/a_info');?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<?=$this->load->view('t/a_date');?>			
			<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0px;margin-left:0px;" >
				<?=form_open('',array('id' => 'form_tdk')); ?>
					<?=$this->load->view('t/a_menu_tab');?>
					<div class="row-fluid">
						<div class="span5">
							<div id="dtable" style="padding:10px;border-bottom:1px solid #dddddd" >
								<table id="tb_tindakan" class="table table-bordered def_table_y_std300 dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
									<thead>
										<tr role="row">
											<th class="sorting" style="width:40% !important">Tindakan</th>
											<th class="sorting" style="width:40% !important">Data</th>
											<th class="sorting" style="width:20% !important">Jumlah</th>
										</tr>
									</thead>
									<tbody>
										<?foreach ($ds->result() as $key): ?>
										<tr id="tr-<?=$key->treat_id;?>" tid="<?=$key->treat_id;?>">
											<td><?=$key->treat_code;?></td>
											<td><?=$key->treat_name;?></td>
											<td>
												<?=$key->jumlah;?>
												<div style="float:right" class='icon-right'></div>
											</td>
										</tr>	
										<?endforeach ?>
									</tbody>
								</table>
							</div>	
							<table style="margin-left:10px;margin-top:10px">
								<tr>
									<td>
										<i class="icon-warning-sign"></i>
									</td>
									<td>Belum disimpan</td>
									<td></td>
									<td><i class="icon-ok"></i></td>
									<td>Sudah disimpan</td>
								</tr>
							</table>
						</div>
						<div class="span7">
							<b>Detail Paramedis : </b>
							<div id="detail_param" style="padding-right:10px;"></div>
						</div>
						<br class="clearfix">
					</div>
					<br class="clearfix">
					<?=$this->load->view('t/a_bottom');?>
				<?=form_close()?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var def_dr_id = <?=$this->rnp->dr_id?>;
	$(function(){
		check_save()
		$("#tb_tindakan").on('click','tbody tr',function(e){
			e.preventDefault();
			var tid = $(this).attr('tid');
			$('#tb_tindakan .active').removeClass('active');
			$(this).addClass('active');
			$("#detail_param").load(CUR+tid);
		})
		$("#tb_tindakan tbody tr:eq(0)").trigger('click');
		$("body").on('click','#btnSimpan',function(e){
			e.preventDefault();
			var url  = $("#frmtarif").attr('action');
			var data = $("#frmtarif").serialize();
			$.post(url,data, function(data) {
				$("#tb_tindakan tbody tr.active").trigger('click');
				check_save();
			});
		})
		$("body").on('click','#tambah_row',function(e){
			e.preventDefault();
			$("#pre tbody").find('.sl_role').attr('name','r['+irow+']');
			$("#pre tbody").find('.tp').attr('name','p['+irow+']');
			$("#pre tbody").find('.tp').addClass('ajchzn'+irow);
			var a = $("#pre tbody .th").attr('name','t['+irow+']');
			$("#tb_main tbody").append($("#pre tbody").html());
			$("#pre tbody").find('.tp').removeClass('ajchzn'+irow);
			$('.ajchzn'+irow).chosen();
			irow++;
		})
		$("body").on('click','.del',function(e){
			e.preventDefault();
			$(this).parent().parent().remove();
		})

	})

	function check_save(){
		$("#tb_tindakan tbody tr").each(function(index) {
			$(this).find('.icon-right').html('<i class="icon-warning-sign"></i>');
		});

		var url = '<?=base_url('rawat_inap/periksa/cek_saved_tread')?>/<?=$this->uri->segment(4);?>';
		$.getJSON(url, function(data) {
			$.each(data, function( index, value ) {
				$("#tr-"+value.treat_id).find('.icon-right').html('<i class="icon-ok"></i>');
			});
		});	
	}
</script>