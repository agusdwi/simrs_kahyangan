<?
    $dstart = new DateTime($this->rnp->rnp_in);
    $dstartori = new DateTime($this->rnp->rnp_in);

    $dend   = new DateTime();
    if ($this->rnp->rnp_out != '') {
        $dend = new DateTime($this->rnp->rnp_out);
        $dend->modify( 'next day' );
    }

    $t3  = $this->uri->segment(3);
    $d5 = $this->uri->segment(5);
?>
<style type="text/css">
    li.pilih a{
        border-color: #ddd transparent #ddd #ddd;
    }
    #total-harga{
        margin-top: 25px;
    }
    li.main-menu a{
        font-size: 12pt;
        background: #fb9338;
        color: white;
    }
    li.main-menu.active a{
        background: #eeeeee;
    }
    li.main-menu a:hover{
        background: #FFB26E;
    }
</style>
<div class="tabbable tabs-left ">
    <ul class="nav nav-tabs" id="list_rekmed2" style="margin-top:50px">
        <?while ( $dstart < $dend):?>
            <li class="pilih <?=($d5==$dstart->format('Y-m-d'))?" active":"";?>">
                <a href="<?=cur_url(-1);?><?=$dstart->format('Y-m-d');?>"><?=$dstart->format('d-m-Y');?></a>
            </li>
        <?$dstart->modify( 'next day' );endwhile;?>

        <li id="total-harga" class="main-menu <?=$this->uri->segment(3)=='tinfo' ? 'active': '' ?>">
            <a href="<?=cur_url(-3);?>tinfo/<?=$this->rnp->rnp_id?>/<?=$dstartori->format('Y-m-d');?>">Info Umum</a>
        </li>
        <li  class="main-menu <?=$this->uri->segment(3)=='trekmed' ? 'active': '' ?>">
            <a href="<?=cur_url(-3);?>trekmed/<?=$this->rnp->rnp_id?>/<?=$dstartori->format('Y-m-d');?>">Rekam Medis</a>
        </li>
        <li class="main-menu <?=$this->uri->segment(3)=='truang' ? 'active': '' ?>">
            <a href="<?=cur_url(-3);?>truang/<?=$this->rnp->rnp_id?>/<?=$dstartori->format('Y-m-d');?>">Penggunaan Ruang</a>
        </li>
        <li class="main-menu <?=$this->uri->segment(3)=='tharga' ? 'active': '' ?>">
            <a href="<?=cur_url(-3);?>tharga/<?=$this->rnp->rnp_id?>/<?=$dstartori->format('Y-m-d');?>">Total Harga</a>
        </li>
        <li class="main-menu <?=$this->uri->segment(3)=='tringkasan' ? 'active': '' ?>">
            <a href="<?=cur_url(-3);?>tringkasan/<?=$this->rnp->rnp_id?>/<?=$dstartori->format('Y-m-d');?>">Ringkasan</a>
        </li>
    </ul>
</div>