<style type="text/css">
	#dform p{margin-bottom: 0px}
</style>
<?=$this->load->view('t/a_info');?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">			
			<?=$this->load->view('t/a_date');?>
			<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0;margin-left:100px;" >
				<?=form_open('',array('id' => 'form_obt')); ?>
					<?=$this->load->view('t/a_menu_tab');?>
					
					<div id="dtable" style="padding:10px;border-bottom:1px solid #dddddd">
						<table class="table table-bordered def_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
							<thead>
								<tr role="row">
									<th class="sorting" style="width:15%">Racik</th>
									<th class="sorting" style="width:15%">Nama Obat</th>
									<th class="sorting" style="width:40%">Keterangan</th>
									<th class="sorting" style="width:40%">Jumlah</th>
								</tr>
							</thead>
							<tbody>
								<?foreach ($ds->result() as $key): ?>
									<tr>
										<td><?=($key->is_racik == 1)? 'ya' : '-' ;?></td>
										<td>
											<?if ($key->is_racik == 1): ?>
												<?=$key->recipe_racik;?>
											<? else: ?>
												<?=$key->mdcn_code;?>, <?=$key->mdcn_name;?>
											<?endif ?>
										</td>
										<td><?=$key->recipe_rule;?></td>
										<td>
											<?=$key->recipe_qty;?>
											<a class="label label-important ajaxDelete onhover" href="<?=cur_url()?><?=$key->rnp_recipe_id;?>">
												delete
											</a>
										</td>
									</tr>
								<?endforeach ?>
							</tbody>
						</table>
					</div>	

					<div id="dform" class="well" style="width:365px;margin-left:10px;margin-top:10px;padding:10px">					
						<b>Racikan : <input style="margin:0px" id="cx_racik" name="ds[is_racik]" value="1" type="checkbox"></b><br>
						<p>
							<input type="hidden" class="bigdrop" id="select2_obat" style="width:300px;" autofocus/>
							<input type="hidden" id="hd_obat" name="ds[mdcn_id]"/>
						</p>
						<p><textarea placeholder="ketikkan obat" id="txRacik" style="width:350px;display:none" rows="3" name="ds[recipe_racik]"></textarea></p>
						<p><input id="jumlah" type="text" placeholder="jumlah" style="width:100px" value="1" name="ds[recipe_qty]"> satuan</p>
						<p><input	type="text" placeholder="dosis / catatan" style="width:350px" name="ds[recipe_rule]"></p>
						<div class="clearfix"></div>
						<button type="button" name="save" style="float:right;margin-top:10px" id="tambah" class="btn btn-small btn-primary simpan">tambahkan</button>
						<div class="clearfix"></div>
					</div>	
					<?=$this->load->view('t/a_bottom');?>
				<?=form_close()?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#cx_racik").change(function(){
			if($(this).prop('checked') == true){
				$(".select2-container").hide();
				$("#txRacik").show();
			}else{
				$("#txRacik").hide();
				$(".select2-container").show();
			}
		})

		preventSubmit();
		$("body").on('click','#tambah',function(){
			$.post( "", $("#form_obt").serialize(),function(data){
				$("#form_obt").find("input[type=text], textarea").val("");
				$("#hd_obat").val("");
				$("#jumlah").val("1");
				$("#select2_obat").select2("val", "");
				var d = data.data;
				console.log(d);
				var del = '<a class="label label-important ajaxDelete onhover" href="'+CUR+d.rnp_recipe_id+'">delete</a>';
				otb.dataTable().fnAddData([((d.is_racik==1)?'ya':'-'),((d.is_racik==1)?d.recipe_racik:d.mdcn_code+" ,"+d.mdcn_name),d.recipe_rule,d.recipe_qty+del]);
				$("#select2_obat").select2('focus');
			},'json');
			return false;
		})
	})
</script>