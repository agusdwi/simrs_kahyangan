<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" style="background:#E5E5E5;">
            <div class="title" style="margin-bottom:0px;"><h3>Data Pasien</h3></div>
            <br clear="all">
            <table class="info" style="width:800px">
            	<tr>
            		<td>
            			<table>
            				<tr>
            					<td><b>Nomer RM</b></td>
            					<td><b>: <?=$this->ptn->sd_rekmed;?></b></td>
            				</tr>
            				<tr>
            					<td><b>Nama</b></td>
            					<td><b>: <?=$this->ptn->sd_name;?></b></td>
            				</tr>
            			</table>
            		</td>
            		<td>
            			<table>
            				<tr>
            					<td><b>Jenis Kelamin</b></td>
            					<td><b>: <?=strtolower($this->ptn->sd_sex) == 'l'? 'Laki-laki' : 'Perempuan'; ?></b></td>
            				</tr>
            				<tr>
            					<td><b>Umur</b></td>
            					<td><b>: <?=get_umur($this->ptn);?> </b></td>
            				</tr>
            			</table>
            		</td>
            		<td>
            			<table>
            				<tr>
            					<td><b>Gol Darah</b></td>
            					<td><b>: <?=$this->ptn->sd_blood_tp;?></b></td>
            				</tr>
            				<tr>
            					<td>&nbsp;</td>
            					<td>&nbsp;</td>
            				</tr>
            			</table>
            		</td>
            	</tr>
            </table>
        </div>
    </div>
</div>