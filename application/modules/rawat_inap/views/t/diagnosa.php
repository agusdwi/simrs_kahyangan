<style type="text/css">
	.chzn-container .chzn-results{
		max-height: 65px;
	}
	.select2-container{
		float: left;
		margin-right: 5px;
	}
</style>
<?=$this->load->view('t/a_info');?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">			
			<?=$this->load->view('t/a_date');?>
			<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0;margin-left:100px;" >
				<?=form_open('',array('id' => 'form_diag')); ?>
					<?=$this->load->view('t/a_menu_tab');?>		
					<div id="dtable" style="padding:10px;border-bottom:1px solid #dddddd">
						<table class="table table-bordered def_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
							<thead>
								<tr role="row">
									<th class="sorting" style="width:15%">ICD</th>
									<th class="sorting" style="width:35%">Diagnosa</th>
									<th class="sorting" style="width:5%">primer</th>
									<th class="sorting" style="width:40%">Anamnesa</th>
									<th class="sorting" style="width:40%">P. Fisik</th>
									<th class="sorting" style="width:40%">P. Penunjang</th>
								</tr>
							</thead>
							<tbody>
								<?foreach ($ds->result() as $key): ?>
									<tr>
										<td><?=$key->diag_code?></td>
										<td><?=$key->diag_name?></td>
										<td><?=$key->is_primer?></td>
										<td><?=$key->anam_title?> <?=$key->anam_desc?></td>
										<td><?=$key->pemrk_fisik?></td>
										<td>
											<?=$key->pemrk_penunjang?>
											<a class="label label-important ajaxDelete onhover" href="<?=cur_url()?><?=$key->rnp_diag_id;?>">
												delete
											</a>
										</td>
									</tr>
								<?endforeach ?>
							</tbody>
						</table>
					</div>	

					<div id="dform" class="well" style="width:516px;margin-left:10px;margin-top:10px;padding:10px">
						<div class="tabbable tabs-left">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#lA" data-toggle="tab">Diagnosa</a></li>
								<li><a href="#lB" data-toggle="tab">Anamnesa</a></li>
								<li><a href="#lC" data-toggle="tab">Pemeriksaan Fisik</a></li>
								<li><a href="#lD" data-toggle="tab">Pemeriksaan Penunjang</a></li>
							</ul>
							<div class="tab-content" style="border-top:none">
								<div class="tab-pane active" id="lA">
									<input type="hidden" class="bigdrop" id="select2_icd10" style="width:300px;" autofocus/>
									<a href="<?=base_url('master/json/icd10')?>" class="btn btn-info btn-small fl_left cbx_iframe"><i class="icon-search icon-white"></i></a>
									<textarea id="hd_diag_desc" style="margin-bottom:0px;width:350px" rows="4" readonly></textarea>
									<input type="checkbox" name="ds[is_primer]" value="ya">
									<input type="hidden" name="ds[diag_id]" id="hd_diag"> primer
								</div>
								<div class="tab-pane" id="lB">
									<input	type="text" name="ds[anam_title]">
									<textarea style="margin-bottom:0px;width:350px" rows="4" name="ds[anam_desc]"></textarea>
								</div>
								<div class="tab-pane" id="lC">
									<textarea style="margin-bottom:0px;width:350px" rows="6" name="ds[pemrk_fisik]"></textarea>
								</div>
								<div class="tab-pane" id="lD">
									<textarea style="margin-bottom:0px;width:350px" rows="6" name="ds[pemrk_penunjang]"></textarea>
								</div>
							</div>
						</div> <!-- /tabbable -->
						<div class="clearfix"></div>
						<button type="button" style="float:right;" class="btn btn-small btn-primary simpan" id="tambah">tambahkan</button>
						<div class="clearfix"></div>
					</div>
					<?=$this->load->view('t/a_bottom');?>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var select2placeholder = "";
	var otb;
	$(function(){
		$("body").on('click','#tambah',function(e){
			e.preventDefault();
			$.post( "", $("#form_diag").serialize(),function(data){
				$("#form_diag").find("input[type=text], textarea").val("");
				$("#form_diag").find("input[type=checkbox]").attr('checked', false);
				$("#hd_diag").val("");
				$(".select2-chosen").html(select2placeholder);
				$(".select2-choice").addClass('select2-default');
				$("#select2_icd10").select2("val", "");
				var d = data.data;
				var del = '<a class="label label-important ajaxDelete onhover" href="'+CUR+d.rnp_diag_id+'">delete</a>';
				otb.dataTable().fnAddData([d.diag_code,d.diag_name,d.is_primer,d.anam_title+' '+d.anam_desc,d.pemrk_fisik,d.pemrk_penunjang+del]);
				$("#select2_icd10").select2("focus");
			},'json');
		})
		select2placeholder = $(".select2-chosen").html();
	})

	function pilih(obj){
		$(".select2-chosen").html(obj.title);
		$(".select2-choice").removeClass('select2-default');
		$("#hd_diag").val(obj.key);
		$("#hd_diag_desc").val($("<div>"+obj.title+"</div>").text());
	}
</script>