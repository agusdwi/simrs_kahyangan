<div class="span12" style="padding:0px 1%">
	<b>Rawat Jalan [<?=$medical->pl_name;?>]</b><br>
	<table id="rekmed" style="width:100%" class="table table-bordered">
		<tr>
			<td style="width:30%">Tanggal & Jam</td><td><?=pretty_date($medical->mdc_in);?></td>
		</tr>
		<tr>
			<td>Poli / unit</td><td><?=$medical->pl_name;?></td>
		</tr>
		<tr>
			<td>Dokter</td><td><?=$medical->dr_name;?></td>
		</tr>
		<tr>
			<td>Diagnosa</td>
			<td>
				<?foreach ($diagnosis as $key => $value): ?>
					<b><?=$key;?></b><br>
					<?foreach ($value as $k): ?>
						<p>- <?=$k?></p>
					<?endforeach ?>
				<?endforeach ?>
			</td>
		</tr>
		<tr>
			<td>Tindakan</td>
			<td>
				<?foreach ($tindakan->result() as $key): ?>
					<p>- <?=$key->jumlah;?>x <?=$key->treat_code;?>, <?=$key->treat_name;?></p> 
				<?endforeach ?>
			</td>
		</tr>
		<tr>
			<td>Obat yang digunakan</td>
			<td>
				<table>
					<?foreach ($obat->result() as $key): ?>
						<tr>
							<td>
								<?=($key->is_racik == 1)? '- racik,' : '-' ;?>
								<?if ($key->is_racik == 1): ?>
									<?=$key->recipe_racik;?>
								<? else: ?>
									<?=$key->mdcn_code;?>, <?=$key->mdcn_name;?>
								<?endif ?>
							</td>
							<td><?=$key->recipe_rule;?></td>
							<td>
								<?=$key->recipe_qty;?>
							</td>
						</tr>
					<?endforeach?>
				</table>
			</td>
		</tr>
		<tr>
			<td>Status Pulang</td><td><?=$medical->status_pulang;?></td>
		</tr>
		<tr>
			<td>Catatan</td><td><?=$medical->note;?></td>
		</tr>
	</table>
</div>