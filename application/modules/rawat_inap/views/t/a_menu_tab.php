<?
    $tbs = 'ui-state-active ui-tabs-selected';
    $t3  = $this->uri->segment(3);
    $d3 = $this->uri->segment(5);
?>

<ul id="menu-tab" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" style="padding-left:0px;margin-left:0px;">
    <!-- <li class="<?=($t3=='tinfo')? $tbs : '' ;?>" ><a href="<?=cur_url(-3);?>tinfo/<?=$this->rnp->rnp_id?>/<?=$d3;?>" id="tab_rekmed">Info Umum</a></li> -->
    <li class="<?=($t3=='tdiagnosa')? $tbs : '' ;?>" ><a href="<?=cur_url(-3);?>tdiagnosa/<?=$this->rnp->rnp_id?>/<?=$d3;?>" id="tab_diagnosa">Diagnosa</a></li>
    <li class="<?=($t3=='ttindakan')? $tbs : '' ;?>" ><a href="<?=cur_url(-3);?>ttindakan/<?=$this->rnp->rnp_id?>/<?=$d3;?>" id="tab_tindakan">Tindakan</a></li>
    <li class="<?=($t3=='tresep')? $tbs : '' ;?>" ><a href="<?=cur_url(-3);?>tresep/<?=$this->rnp->rnp_id?>/<?=$d3;?>" id="tab_obat">Resep & Obat</a></li>
    <li class="<?=($t3=='tparamedis')? $tbs : '' ;?>" ><a href="<?=cur_url(-3);?>tparamedis/<?=$this->rnp->rnp_id?>/<?=$d3;?>" id="tab_obat">Paramedis</a></li>
    <!-- <li class="<?=($t3=='tharga')? $tbs : '' ;?>" ><a href="<?=cur_url(-3);?>tharga/<?=$this->rnp->rnp_id?>/<?=$d3;?>" id="tab_harga">Harga</a></li>
    <li class="<?=($t3=='tringkasan')? $tbs : '' ;?>" ><a href="<?=cur_url(-3);?>tringkasan/<?=$this->rnp->rnp_id?>/<?=$d3;?>" id="tab_ringkasan">Ringkasan</a></li> -->
</ul>
<div class="clear"></div>
<? if (empty($this->rnp->dr_id)): ?>
    <script type="text/javascript">
        $(function(){
           $("#menu-tab").on('click','a',function(){
                alert('silahkan memilih dokter terlebih dahulu dan simpan !');
                return false;
           })
        })
    </script>
<? endif ?>