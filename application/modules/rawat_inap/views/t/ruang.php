<?
    $rdstart = new DateTime($this->rnp->rnp_in);

    $rdend   = new DateTime();
    if ($this->rnp->rnp_out != '') {
        $rdend = new DateTime($this->rnp->rnp_out);
        $rdend->modify( 'next day' );
    }
?>
<?=$this->load->view('t/a_info');?>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <?=$this->load->view('t/a_date');?>
            <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0px;margin-left:0px;" >
                <?=$this->load->view('t/a_menu_tab');?>
                <?=form_open()?>
                    <div id="page">
                        <div class="container-fluid">
                            <div class="row-fluid">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>No</td>
                                            <td>Tanggal</td>
                                            <td>Kamar</td>
                                            <td>Harga</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?$i=0;while ( $rdstart < $rdend):$i++;?>
                                            <? $dd = $rdstart->format('d-m-Y');?>
                                            <tr>
                                                <td><?=$i;?></td>
                                                <td><?=$dd;?></td>
                                                <td>
                                                    <select class='ruang' name="ds[<?=$dd;?>][kamar]">
                                                        <option>-- pilih kamar --</option>
                                                        <?$o="";foreach ($ruang->result() as $k): ?>
                                                            <?if ($o != $k->r_nama): $o=$k->r_nama;?>
                                                                <optgroup label="<?=$k->r_nama;?>"></optgroup>
                                                            <?endif;?>
                                                            <option data-harga='<?=$k->k_harga;?>' <?=cek_kamar($dd,$ds,$k->id,$this->rnp->ruang_id)?> value="<?=$k->id;?>"><?=$k->k_nama;?></option>
                                                        <?endforeach;?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input class="tharga" type="text" name="ds[<?=$dd;?>][harga]" value="<?=(isset($ds[$dd])? $ds[$dd]['kamar_harga'] : '0' )?>">
                                                </td>
                                            </tr>
                                        <?$rdstart->modify( 'next day' );endwhile;?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" class="money"><b>Total</b></td>
                                            <td>
                                                <b id="total"></b>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>  
                    </div>
                <?=$this->load->view('t/a_bottom');?>
                </form>
            </div>
        </div>
    </div>
</div>

<?
    function cek_kamar($tgl,$ds,$id,$current){
        if(isset($ds[$tgl])){
            if($ds[$tgl]['kamar_id'] == $id)
                return "selected='selected' current=0";
        }else if($current == $id)
            return "selected='selected' current=1";
    }
?>

<script type="text/javascript">
    $(function(){
        $('body').on('change','.ruang',function (e) {
            e.preventDefault();
            var harga = $('.ruang option:selected').attr('data-harga');
            $(this).parent().parent().find('input').val(harga);
        });

        $("select.ruang").each(function(index) {
            if($(this).find('option:selected').attr('current') == 1)
                $(this).trigger('change');
        });

        var total = 0;
        $(".tharga").each(function(index) {
            total += parseInt($(this).val());
        });
        $("#total").html(total);

    })
</script>