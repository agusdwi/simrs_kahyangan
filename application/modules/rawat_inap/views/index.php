<style type="text/css">
	.proses{
		display: none;
		float: right;
	}
	.hover:hover .proses{
		display: block;
		position: absolute;
		top: 0px;
		right: 0px;
		z-index: 99999999999999;
	}
</style>
<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>       
</div><!--pageheader-->
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span8">
			<div class="title">
				<h3>Daftar Pasien Rawat Inap</h3>
			</div>
			<div class="chatsearch" style="width: 230px;float: right;">
				<input id="q" type="text" name="" placeholder="Search" style="width:188px;margin:auto;">
			</div>
			<br clear="all">
			<div>
				<b style="margin-top: 5px;display: block;float: left;margin-right: 6px;">Ruang : </b>
				<select style="width:175px" id="room">
					<option value="">Semua Ruang</option>
					<?foreach ($ruang->result() as $key): ?>
						<option value="<?=$key->r_nama;?>"><?=$key->r_nama;?></option>
					<?endforeach ?>
				</select>
			</div>
			<table class="table table-bordered custom_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
				<thead>
					<tr role="row">
						<th class="sorting" style="width:15%">No</th>
						<th class="sorting" style="width:40%">Tgl Masuk</th>
						<th class="sorting" style="width:40%">Pasien</th>
						<th class="sorting" style="width:45%">Dokter</th>
						<th class="sorting" style="width:45%">Ruang</th>
					</tr>
				</thead>
				<tbody>
					<?$i=0;foreach ($ptn->result() as $key): $i++;?>
						<? $dte = explode(",", pretty_date($key->rnp_in));?>
						<tr class="hover">
							<td><?=$i;?></td>
							<td style="width:134px"><?=$dte[0]?><br><?=$dte[1];?></td>
							<td><b><?=$key->sd_name;?></b><br><i style="font-size:7.5pt"><?=$key->sd_address;?></i></td>
							<td><?=$key->dr_name?></td>
							<td>
								<div style="position:relative">
									<? if (isset($conv[$key->ruang_id])): ?>
										<?=$conv[$key->ruang_id][0];?>, <?=$conv[$key->ruang_id][1];?>																								
									<? endif ?>
									<a class="proses" href="<?=base_url('rawat_inap/proses')?>/<?=$key->rnp_id;?>">
										<span class="label label-success">proses</span>
									</a>
								</div>
							</td>
						</tr>
					<?endforeach ?>
				</tbody>
			</table>
		</div>
		<div class="span4">
			<div class="title">
				<h3>Status Ruang & Kamar</span>
					<span class="tgl-sekarang"></span> 
				</h3>
			</div>
			<div style="width:100%;position:relative;">
				<table class="table std table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Kamar</th>
							<th>Kapasitas</th>
							<th>Terisi</th>
						</tr>
					</thead>
					<tbody>
						<? $r=0;foreach ($ds[0]->result() as $key): $r++;?>
							<tr>
								<td style="width:30px"><?=$r;?></td>
								<td colspan="3"><b><?=$key->r_nama;?></b></td>
							</tr>
							<? if (isset($ds[1][$key->id])): ?>
								<? $j=0;foreach ($ds[1][$key->id] as $k): $j++;?>
									<tr>
										<td><?=$r;?>.<?=$j;?></td>	
										<td><?=$k->k_nama;?></td>
										<td><?=$k->k_kapasitas;?></td>
										<td class="tb_aksi2">
											<?if (isset($isi[$k->id])): ?>
												<?=$isi[$k->id];?> pasien
											<?else: ?>	
												-
											<?endif ?>
										</td>
									</tr>
								<?endforeach ?>
							<? endif ?>
						<?endforeach ?>
					</tbody>
				</table>  		
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('.custom_table_y').dataTable( {
			"sPaginationType": "bootstrap",
			"sScrollY": "350px",
			"bPaginate": false,
			"bFilter":true,
			"bInfo":false,
			"bSort": false
		});
		$("#room").change(function(){
			cari();
		})
		$("#q").keyup(function(){
			cari();
		})
	})

	function cari(){
		var room = $("#room").val();
		var q = $("#q").val();
		var filter = $(".dataTables_filter").find('input');
		filter.val(room+" "+q);
		filter.trigger('keyup');
	}
</script>