<style type="text/css">
	.proses{
		display: none;
		float: right;
	}
	.hover:hover .proses{
		display: block;
		position: absolute;
		top: 0px;
		right: 0px;
		z-index: 99999999999999;
	}
</style>
<style type="text/css" media="screen">
	#fx_pasien_ctr.ffb{
		width:300px !important;
		top: 28px !important;
	}

	#gritter-notice-wrapper{
		right: 13%;
		top: 100px;
	}
	#fx_pasien_ctr .row .col1{
		float:left;
		/*width:50px;*/
	}
	#fx_pasien_ctr .row .col2{
		float:left;
		margin-left: 10px;
		/*width:200px;*/
	}
	#fx_pasien_input{
		width: 207px !important;
	}
</style>
<script type="text/javascript" charset="utf-8">
	var objPasien = <?=json_encode($ptn->result())?>;
	/*
	$(function(){
		$('.custom_table_y').dataTable( {
			"sPaginationType": "bootstrap",
			"sScrollY": "350px",
			"bPaginate": false,
			"bFilter":true,
			"bInfo":false,
			"bSort": false
		});
	})
	*/
</script>
<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>       
</div><!--pageheader-->
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span6">
			<div class="title">
				<h3>Daftar Pasien Rawat Inap</h3>
			</div>
			<div class="chatsearch" style="width: 230px;float: right;">
				<input id="q" type="text" name="" placeholder="Search" style="width:188px;margin:auto;">
			</div>
			<br clear="all">
			<div>
				<b style="margin-top: 5px;display: block;float: left;margin-right: 6px;">Ruang : </b>
				<select style="width:175px" id="room">
					<option value="">Semua Ruang</option>
					<?foreach ($ruang->result() as $key): ?>
						<option value="<?=$key->r_nama;?>"><?=$key->r_nama;?></option>
					<?endforeach ?>
				</select>
			</div>
			<table class="table table-bordered custom_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
				<thead>
					<tr role="row">
						<th class="sorting" style="width:15%">No</th>
						<th class="sorting" style="width:40%">Tgl Masuk</th>
						<th class="sorting" style="width:40%">Pasien</th>
						<th class="sorting" style="width:45%">Dokter</th>
						<th class="sorting" style="width:45%">Ruang</th>
					</tr>
				</thead>
				<tbody>
					<?$i=0;foreach ($ptn->result() as $key): $i++;?>
						<? $dte = explode(",", pretty_date($key->rnp_in));?>
						<tr class="hover">
							<td><?=$i;?></td>
							<td style="width:134px"><?=$dte[0]?><br><?=$dte[1];?></td>
							<td><b><?=$key->sd_name;?></b><br><i style="font-size:7.5pt"><?=$key->sd_address;?></i></td>
							<td><?=$key->dr_name?></td>
							<td>
								<div style="position:relative">
									<? if (isset($conv[$key->ruang_id])): ?>
										<?=$conv[$key->ruang_id][0];?>, <?=$conv[$key->ruang_id][1];?>																								
									<? endif ?>
									<a class="proses" href="<?=base_url('rawat_inap/proses')?>/<?=$key->rnp_id;?>">
										<span class="label label-success">proses</span>
									</a>
								</div>
							</td>
						</tr>
					<?endforeach ?>
				</tbody>
			</table>
		</div>
		<div class="span6">
			<div class="title">
				<h3>Input Pemeriksaan Pasien Baru</h3>
			</div>
			<div style="width:100%;border:1px solid #DDD;position:relative;">
				<div class="black_loader">
					<img src="<?=get_loader(11)?>">
				</div>
				<?=form_open(cur_url(),array('class' => 'stdform','id' => 'formAntrian')); ?>
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;float:left;height:40px;text-align:right;">
							<label>Tanggal</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<input type="text" name="ds[date]" class="datepicker" value="<?=DATE('d-m-Y');?>">
						</div>
					</div><br clear="all"> 
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;float:left;height:40px;text-align:right;">
							<label>Dokter</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<?=get_dropdown_dokter('ds[dr_id]');?>
						</div>
					</div> 
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;height:40px;float:left;text-align:right;">
							<label>No Rekmed</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<div id="fx_pasien" name="fx_pasien"></div>	
						</div>
					</div> 
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;float:left;height:40px;text-align:right;">
							<label>Nama Pasien</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b id="name">-</b>
						</div>
					</div> 
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;height:40px;float:left;text-align:right;">
							<label>Asuransi</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<div>
								<div class="controls">
									<select id="tp_insurance" style="width:140px" name="ds[ins_id]">
										<?foreach ($tp_insurance->result() as $key): ?>
											<option value="<?=$key->ins_id?>"><?=$key->ins_name?></option>	
										<?endforeach ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;height:40px;float:left;text-align:right;">
							<label>Ruang</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<div>
								<div class="controls">
									<select id="ruang" style="width:140px" name="ds[ruang_id]">
										<option value="" selected="selected">Choose One</option>
										<?foreach ($ruang->result() as $key): ?>
										<option value="<?=$key->id?>"><?=$key->r_nama?></option>	
										<?endforeach ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;height:40px;float:left;text-align:right;">
							<label>Kamar</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<div>
								<div class="controls">
									<select id="cls" style="width:140px" name="ds[cls_id]">
										<option value="" selected="selected">Choose One</option>
										<? for($i=1; $i<=count($ds[1]); $i++) { ?>
										<? $j=0;foreach ($ds[1][$i] as $k): $j++;?>
										<option value="<?=$k->id?>"><?=$k->k_nama?></option>
										<? endforeach ?>
										<? } ?>	
									</select>
								</div>
							</div>
						</div>
					</div>
					<br clear="all">
					<div class="form-actions" style="margin:0px;vertical-align:bottom;">
						<button type="submit" class="btn btn-primary">Tindakan Lanjut</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('.custom_table_y').dataTable( {
			"sPaginationType": "bootstrap",
			"sScrollY": "350px",
			"bPaginate": false,
			"bFilter":true,
			"bInfo":false,
			"bSort": false
		});
		$("#room").change(function(){
			cari();
		})
		$("#q").keyup(function(){
			cari();
		})
	})

	function cari(){
		var room = $("#room").val();
		var q = $("#q").val();
		var filter = $(".dataTables_filter").find('input');
		filter.val(room+" "+q);
		filter.trigger('keyup');
	}
</script>
<script type="text/javascript">
	$(function(){
		$('#fx_pasien').flexbox(BASE+"pendaftaran/get_pasien", {
			//method : 'POST',
			paging: false,
			showArrow: false ,
			maxVisibleRows : 10,
			width : 100,
			resultTemplate: '<div class="col1">{name}</div><div class="col2">{id}</div>',
			onSelect:function(){
				$("#name").html($("#fx_pasien_hidden").val());
			}

		});
		$("#fx_pasien_input").attr('placeholder','nama / rekmed pasien')
		$("#formAntrian").submit(function(){
			if ($("#poli").val() == '') {
				alert('silahkan memilih poli');
			}else if($("#dd_dokter").val() == ''){
				alert('silahkan memilih dokter');
			}else if($("#fx_pasien_hidden").val() == ''){
				alert('silahkan memilih pasien');
			}else if($("#cls").val() == ''){
				alert('silahkan memilih ruang & kamar');
			}else{
				return true;
			}
			return false;
		})

		$("#ruang").change(function(){
            $("#cls").html('<option value="">Choose One</option>');
            $.each(str_cls[$(this).val()], function( index, value ) {
                $("#cls").append('<option value="'+value[0]+'">'+value[1]+'</option>');
            });
        })
        $("#ruang").val(16);
        $("#ruang").trigger('change');
	})
	<?
        $str = array();
        foreach ($kamar->result() as $key) {
            $str[$key->ruang_id][] = array($key->id,$key->k_nama);
        }
        $str_cls = json_encode($str);
    ?>
    var str_cls = <?=$str_cls?>;
    var rmLst   = "";
</script>