<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Periksa extends CI_Controller {
	// status periksa, 
	// 1 => pasien aktif
	// 2 => pasien keluar, tapi belum bayar
	// 3 => pasien keluar, udah bayar lunas

	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 9
			);
		$this->load->model('Mpasien');
		$this->ptn_class = 1;
	}

	private function tdata($id,&$data){
		$this->rnp 		= $this->db->get_where('v_rnp_aktif',array('rnp_id'=>$id))->row();
		$this->ptn 		= $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$this->rnp->sd_rekmed))->row();
		$data['cf']			=  $this->cf;
		$data['title']		= 'Proses Pemeriksaan Rawat Inap';
		$data['current']	= 39;
	}


	public function tinfo($id,$date){
		$this->tdata($id,$data);

		if (is_post()) {
			$input = $this->input->post();
			$insert = $input['ds'];
			$insert['rnp_in'] = format_date_time($insert['rnp_in'], false).' '.DATE('h:i:s');
			$insert['rnp_out'] = ($insert['rnp_out'] != '') ? format_date_time($insert['rnp_out'], false).' '.DATE('h:i:s') : '';
			$this->db->where('rnp_id', $id);
			$this->db->update('rnp_medical', $insert);
			$this->Mpasien->set_alergi($this->rnp->sd_rekmed,$input['ptn']['alergi']);
			if(isset($input['savenext'])){
				redirect(cur_url(-3)."trekmed/$id/$date");
			}
			$this->tdata($id,$data);
		}

		$data['main_view']	= 't/info';
		$data['insurance']	= $this->db->get('mst_ref_insurance');
		$data['alergi']		= $this->Mpasien->get_alergi($this->rnp->sd_rekmed);
		$this->load->view('template',$data);
	}

	public function trekmed($id,$date="",$mdc="",$tipe=""){
		// $this->Mpasien->get_rnp_rekmed_date($id,$date);
		if ($this->input->is_ajax_request()) {
			if ($tipe=='rajal') {
				$data = $this->Mpasien->get_rekmed($mdc);
				$this->load->view('t/rekmed_single',$data);
			}else{
				$data = $this->Mpasien->get_rekmed_ranap($mdc);
				$this->load->view('t/rekmed_single_ranap',$data);
			}
		}else{
			$this->tdata($id,$data);	
			$data['main_view']	= 't/rekmed';
			$data['date']		= $this->Mpasien->get_rekmed_date($id);
			$this->load->view('template',$data);
		}
	}

	function tdiagnosa($id,$date,$diagid=""){
		if ($this->input->is_ajax_request()) {
			if($diagid!=""){
				$data = array('is_delete' =>  1);
				$this->db->where('rnp_diag_id', $diagid);
				$this->db->update('rnp_diagnosa', $data); 
				echo "success";
			}
			if (is_post()) {
				$input = $this->input->post();
				$insert = $input['ds'];
				$insert['rnp_id'] = $id;
				$insert['diag_date'] = $date." ".date("H:i:s");
				$this->db->insert('rnp_diagnosa',$insert);
				$id = $this->db->insert_id();
				$row = $this->db->get_where('v_rnp_diag ',array('rnp_diag_id'=>$id))->row();
				echo json_encode(array('result'=>'success','data'=>$row));
			}
		}else{
			if (is_post()) {
				$input = $this->input->post();
				if(isset($input['savenext'])){
					redirect(cur_url(-3)."ttindakan/$id/$date");
				}
			}
			$this->tdata($id,$data);
			$data['main_view']	= 't/diagnosa';
			$data['ds']			= $this->db->get_where('v_rnp_diag ',array('rnp_id'=>$id,'is_delete'=>0,'tgl'=>$date));
			$this->load->view('template',$data);			
		}
	}

	function ttindakan($id,$date,$tdk_id=""){
		if ($this->input->is_ajax_request()) {
			if($tdk_id!=""){
				$data = array('is_delete' =>  1);
				$this->db->where('rnp_treat_id', $tdk_id);
				$this->db->update('rnp_treathment', $data); 
				echo "success";
			}
			if (is_post()) {
				$input = $this->input->post();
				$insert = $input['ds'];
				$insert['rnp_id'] = $id;
				$insert['treat_date'] = $date." ".date("H:i:s");
				$this->db->insert('rnp_treathment',$insert);
				$id = $this->db->insert_id();
				$row = $this->db->get_where('v_rnp_treat ',array('rnp_treat_id'=>$id))->row();
				echo json_encode(array('result'=>'success','data'=>$row));
			}
		}else{
			if (is_post()) {
				$input = $this->input->post();
				if(isset($input['savenext'])){
					redirect(cur_url(-3)."tresep/$id/$date");
				}
			}
			$this->tdata($id,$data);	
			$data['main_view']	= 't/tindakan';
			$data['ds']			= $this->db->get_where('v_rnp_treat ',array('rnp_id'=>$id,'is_delete'=>0,'tgl'=>$date));
			$this->load->view('template',$data);			
		}
	}

	function tresep($id,$date,$mdc_id=""){
		if ($this->input->is_ajax_request()) {
			if($mdc_id!=""){
				$data = array('is_delete' =>  1);
				$this->db->where('rnp_recipe_id', $mdc_id);
				$this->db->update('rnp_recipe', $data); 
				echo "success";
			}
			if (is_post()) {
				$input = $this->input->post();
				$insert = $input['ds'];
				$insert['rnp_id'] = $id;
				$insert['recipe_date'] = $date." ".date("H:i:s");
				$this->db->insert('rnp_recipe',$insert);
				$id = $this->db->insert_id();
				$row = $this->db->get_where('v_rnp_recipe',array('rnp_recipe_id'=>$id))->row();
				echo json_encode(array('result'=>'success','data'=>$row));
			}
		}else{
			if (is_post()) {
				$input = $this->input->post();
				if(isset($input['savenext'])){
					redirect(cur_url(-3)."tparamedis/$id/$date");
				}
			}
			$this->tdata($id,$data);	
			$data['main_view']	= 't/resep';
			$data['ds']			= $this->db->get_where('v_rnp_recipe',array('rnp_id'=>$id,'is_delete'=>0,'tgl'=>$date));
			$this->load->view('template',$data);			
		}
	}

	function tparamedis($id,$date,$tid=""){
		if ($this->input->is_ajax_request()){
			if (is_post()) {
				$this->db->delete('rnp_param_tarif', array('mdc_id'=>$id,'treat_id'=>$tid,'param_date'=>$date)); 
				$input = $this->input->post();
				foreach ($input['r'] as $key => $value) {
					$in = array(
						'mdc_id'			=> $id,
						'treat_id'			=> $tid,
						'role_id'			=> $value,
						'param_id'			=> $input['p'][$key],
						'treat_tarif_harga'	=> $input['t'][$key],
						'param_date'		=> $this->uri->segment(5)
					);	
					$this->db->insert('rnp_param_tarif',$in);
				}
			}else{
				$data['ds']			= $this->db->get_where('mst_treathment_tarif',array('treat_id'=>$tid,'cls_id'=>$this->ptn_class));
				$data['role'] 		= $this->db->get('mst_ref_role_paramedis');
				$data['tarif'] 		= $this->db->get_where('rnp_param_tarif',array('mdc_id'=>$id,'treat_id'=>$tid,'param_date'=>$date));
				$data['dr'] 		= $this->db->get_where('trx_doctor',array('dr_status'=>1));
				$this->load->view('t/param_detail',$data);
			}
		}else{
			$input = $this->input->post();
			if(isset($input['savenext'])){
				redirect(cur_url(-3).'tharga/'.$id.'/'.$date);
			}
			$this->tdata($id,$data);	
			$data['main_view']	= 't/paramedis';
			$data['ds']			= $this->db->get_where('v_rnp_treat ',array('rnp_id'=>$id,'is_delete'=>0,'tgl'=>$date));
			$this->load->view('template',$data);
		}
	}

	function cek_saved_tread($id){
		echo json_encode($this->db->group_by('treat_id')->get_where('rnp_param_tarif',array('mdc_id'=>$id))->result());
	}

	function tharga($id,$date){
		if ($this->input->is_ajax_request()) {
			if (is_post()) {
				$this->update_bill($id);
			}
		}else{
			if (is_post()) {
				$this->update_bill($id);
				$input = $this->input->post();
				if(isset($input['savenext'])){
					redirect(cur_url(-3).'tringkasan/'.$id.'/'.$date);
				}
			}
			$this->tdata($id,$data);	
			$data['main_view']	= 't/harga';
			
			$data['ds']			= $this->Mpasien->get_total_harga_ranap($id);
			
			$data['bill']		= $this->db->get_where('rnp_bill',array('mdc_id'=>$id));

			$this->load->view('template',$data);			
		}
	}

	function tringkasan($id,$date){
		$this->tdata($id,$data);
		if (is_post()) {
			$in = $this->input->post();
			
			$manual = $this->db->get_where('rnp_medical',array('rnp_id'=>$id))->row()->rnp_out;
			$status = 3;
			if (empty($manual)) {
				$status = 2;
				$manual = DATE('Y-m-d h:i:s');
			}
			if(isset($in['savenext'])){
				// update status medical
				$data = array(
					'rnp_status' 	=> $status,
					'rnp_out'		=> $manual
					);
				$this->db->where('rnp_id', $id);
				$this->db->update('rnp_medical', $data);

				// update antrian
				$data = array('queo_status' =>0 );
				$this->db->where('queo_id', $id);
				$this->db->update('trx_queue_outpatient', $data); 

				$this->session->set_flashdata('message','pemeriksaan selesai');

				if ($manual == 0) {
					redirect(cur_url(-2).'antrian/'.$this->poli->pl_id);
				}else{
					redirect(base_url('rawat_inap'));
				}

				
			}else{
				$input = $this->input->post('ds');
				$rujuk = $this->input->post('rujukan');
				if($input['rnp_status_pulang'] == 'rujukan')
					$input['rnp_status_pulang'] .= ','.$rujuk;
				//$input['date_control'] = date_to_sql($this->input->post('date_control'));
				$this->db->where('rnp_id', $id);
				$this->db->update('rnp_medical', $input); 
			}
		}
		$data['main_view']	= 't/ringkasan';
		$data['ds']			= $this->Mpasien->get_rekmed_ranap($id);
		$this->load->view('template',$data);			
	}

	function update_bill($id){
		$input = $this->input->post();
		$input['bill']['mdc_id'] = $id;

		$d = $this->db->get_where('rnp_bill',array('mdc_id'=>$id));
		if($d->num_rows() == 1){
			$this->db->where('mdc_id', $id);
			$this->db->update('rnp_bill', $input['bill']); 
		}else{
			$this->db->insert('rnp_bill', $input['bill']); 
		}
	}

	function truang($id,$date){
		if (is_post()) {
			$this->db->delete('rnp_kamar', array('rnp_id' => $id)); 
			$input = $this->input->post('ds');
			foreach ($input as $key => $value) {
				$i = array(
					'rnp_id'		=> $id,
					'tgl'			=> date_to_sql($key),
					'kamar_id'		=> $value['kamar'],
					'kamar_harga'	=> $value['harga']
				);
				$this->db->insert('rnp_kamar',$i);
			}
			$this->session->set_flashdata('message','kamar berhasil di update');
			redirect(cur_url());
		}
		$this->tdata($id,$data);
		$data['main_view']	= 't/ruang';

		$ruang = $this->db->get_where('rnp_kamar',array('rnp_id'=>$id));
		$ds = array();
		foreach ($ruang->result() as $key) {
			$ds[format_date_time($key->tgl,false)] = array(
				'id'			=> $key->id,
				'kamar_id'		=> $key->kamar_id,
				'kamar_harga'	=> $key->kamar_harga
			);
		}
		$data['ds']			= $ds;
		$data['ruang']		= $this->db->get_where('v_ruang_kamar',array('status'=>1));
		$this->load->view('template',$data);			
	}

	
}