<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rawat_inap extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 9
			);
		$this->load->model('Mpasien','mp');
	}

	public function index(){
		$data['main_view']	= 'index';
		$data['title']		= 'Rawat Inap';
		$data['cf']			=  $this->cf;
		$data['current']	= 37;
		$data['ruang']		= $this->db->get_where('mst_ruang',array('status'=>1));
		$raw = $this->get_data();
		$data['ds'] 		= $raw[0];
		$data['ptn'] 		= $raw[1];
		$data['isi'] 		= $raw[2];
		$data['conv'] 		= $raw[3];
		$this->load->view('template',$data);
	}

	public function add(){
		if(is_post()){
			$input = $this->input->post();
			$ds = $this->input->post('ds');
			$rnp = "r-".$this->mp->get_rnp_id();	
			$data = array(
				"rnp_id"		=> $rnp,
				"sd_rekmed"		=> $input['fx_pasien_input'],
				"rnp_in"		=> date_now(),
				"dr_id"			=> $ds['dr_id'],
				"tp_insurance"	=> $ds['ins_id'],
				"cls_id"		=> $ds['cls_id'],
				"ruang_id"		=> $ds['ruang_id'],
				//"mdc_in"	=> date_to_sql($ds['date']).' '.DATE('H:i:s'),
				"rnp_status"	=> 1
			);
			$this->db->insert('rnp_medical',$data);
			redirect(cur_url(-1).'proses/'.$rnp);
		}
		$data['main_view']	= 'add';
		$data['title']		= 'Rawat Inap';
		$data['cf']			=  $this->cf;
		$data['current']	= 38;
		$data['tp_insurance'] = $this->db->get('mst_ref_insurance');
		$data['kamar']		= $this->db->get_where('mst_kamar',array('status'=>1));
		$data['ruang']		= $this->db->get_where('mst_ruang',array('status'=>1));
		$raw = $this->get_data();
		$data['ds'] 		= $raw[0];
		$data['ptn'] 		= $raw[1];
		$data['isi'] 		= $raw[2];
		$data['conv'] 		= $raw[3];
		$this->load->view('template',$data);
	}

	private function get_data(){
		$conv = array();
		$ruang = $this->db->get_where('mst_ruang',array('status'=>1));
		$kamar = $this->db->get_where('v_ruang_kamar',array('status'=>1));
		$ds = array();
		foreach ($kamar->result() as $key) {
			$ds[$key->ruang_id][] = $key;
		}
		$r = array($ruang,$ds);

		$p = $this->db->get_where('v_rnp_aktif',array('rnp_status'=>1));
		$isi = array();
		foreach ($p->result() as $key) {
			if (isset($isi[$key->ruang_id])) {
				$isi[$key->ruang_id] +=1;
			}else $isi[$key->ruang_id] = 1;
		}

		foreach ($ruang->result() as $key) {
			if (isset($ds[$key->id])){
				foreach ($ds[$key->id] as $k){
					$conv[$k->id] = array($key->r_nama,$k->k_nama);
				}
			}
		}
		return array($r,$p,$isi,$conv);
	}

	public function proses($id){
		$rnp = $this->db->get_where('rnp_medical',array('rnp_id'=>$id))->row();
		$date = explode(" ", $rnp->rnp_in);
		redirect(base_url("rawat_inap/periksa/tinfo/$id/$date[0]"));
	}
}