<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendaftaran_baru extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 1
			);
	}

	public function index(){
		$data['main_view']	= 'pendaftaran_baru/index';
		$data['title']		= 'Pendaftaran baru ';
		$data['current']	= 1;
		$modul = $this->cf;
		$data['cf']			=  $modul;
		$data['religi']		= $this->db->get('mst_religion');
		$data['regency']	= $this->db->get('mst_regency');
		$data['province']	= $this->db->get('mst_province');
		$data['occupation']	= $this->db->get('mst_occupation');
		$data['nationality']= $this->db->get('mst_nationality');
		$data['education']  = $this->db->get('mst_education');
		$data['ref_title']  = $this->db->get('mst_ref_title');
		$rmo = $this->get_rm();
		$data['rekmed']		= $rmo[0];
		$data['rekmed_bayi']= $rmo[1];
		$data['city_provide'] = $this->convert_type_ahaed($this->db->get('mst_regency'),'mre_name');
		$data['alergi_provide'] = $this->convert_type_ahaed($this->db->get('mst_alergi'),'alg_name');
		$data['desc']		= 'Description. ';
		$this->load->view('template',$data);
	}

	function get_rm(){
		$ds = $this->db->get('v_get_last_rm');
		$last = $rm = "";
		if ($ds->num_rows() == 0) {
			$rm = array(DATE('Y'),sprintf('%1$06d', "1"));
		}else{
			$ds = $ds->row();
			$last = substr($ds->sd_rekmed, 5,6);
			// rekmed up to 999999 not handle in here, need update
			$no = intval($last)+1;
			$rm = array(DATE('Y'),sprintf('%1$06d', $no));
		}

		$ds2 = $this->db->get('v_get_last_rm_anak_bayi');

		$last2 = $rm2 = "";
		if ($ds2->num_rows() == 0) {
			$rm2 = array(DATE('Y'),"10".sprintf('%1$04d', "1"));
		}else{
			$ds2 = $ds2->row();
			$sepuluh = substr($ds2->sd_rekmed, 5,2);
			if ($sepuluh == 10) {
				$last2 = substr($ds2->sd_rekmed, 5,6);
				$no2 = intval($last2)+1;
				$rm2 = array(DATE('Y'),sprintf('%1$06d', $no2));
			}else{
				$rm2 = array(DATE('Y'),"10".sprintf('%1$04d', "1"));
			}
		}
		return array($rm,$rm2);
	}

	function create(){
		$rm 	= $this->get_input_rm($this->input->post());
		$cekrm = $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$rm,'sd_status'=>1));
		if ($cekrm->num_rows() > 0) {
			echo json_encode(array('false'));
		}else{
			$data 	= $this->input->post('ds');
			$tgl 	= $this->input->post('tgl');krsort($tgl);
			$rt  	= $this->input->post('rt');
			$data['sd_date_of_birth']	= implode("-", $tgl);
			$data['sd_rt_rw']			= implode("/", $rt);
			$data['sd_rekmed']			= $rm;
			$data['sd_reg_date'] = date_to_sql($data['sd_reg_date']);
			$data['modi_datetime'] = DATE('Y-m-d h:i:s');
			$this->db->insert('ptn_social_data', $data);
			$data_fam 			= $this->input->post();
			if (!(empty($data_fam['rows']))) {
				foreach($data_fam['rows'] as $r){
					$dt = explode('|',$r);
					$dt = array(
						'sd_rekmed'			=> $rm,
						'fm_name'			=> $dt[0],
						'fm_sex'			=> $dt[1],
						'fm_relation'		=> $dt[2],
						'fm_address'		=> $dt[3],
						'fm_telp'			=> $dt[4],
						'fm_phone'			=> $dt[5],
						);
					$this->db->insert('ptn_family', $dt);
				}
			}
			echo json_encode(array('true',cur_url(-1)."cetak/$rm",$rm));
		}
	}

	function get_input_rm($input){
		return $input['rm_1'].'-'.$input['rm_2'];
	}

	function update($id){
		$data = $this->input->post('ds');	
		$this->db->where('', $id);
		$this->db->update('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('', $id);
		$this->db->update('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil dihapus'));
		redirect(cur_url(-2));
	}

	function cetak($rm){
		$data['ds']			= db_conv($this->db->get_where('v_data_patient',array('sd_rekmed'=>$rm)));
		$data['main_view']	= 'pendaftaran_baru/cetak';
		$this->load->view('template_print',$data);
	}
 
	function proses($rm){
		$modul = $this->cf;
		$data['cf']			=  $modul;
		$data['current']	= 1;
		$data['rekmed']		=  $rm;
		$data['title']		= 'Pendaftaran baru Lanjut';
		$data['main_view']	= 'pendaftaran_baru/proses';
		$this->load->view('template',$data);
	}

	function convert_type_ahaed($ds,$kk){
		$data = array();
		foreach ($ds->result_array() as $key) {
			$data[] = '"'.$key[$kk].'"';
		}
		return "[".implode(",", $data)."]";
	}
}