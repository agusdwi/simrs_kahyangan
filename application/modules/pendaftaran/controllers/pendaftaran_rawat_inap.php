<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendaftaran_rawat_inap extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 1
			);
	}

	public function index($no_rekmed=0){
		$data['main_view']	= 'pendaftaran_rawat_inap/index';
		$data['title']		= 'Title ';
		$data['cf']			=  $this->cf;
		$data['current']	= 3;
		$data['desc']		= 'Description. ';
		$data['no_rekmed']	= $no_rekmed == 0? '' :$no_rekmed;
		$data['class'] 		= $this->db->get('mst_class');
		$this->load->view('template',$data);
	}

	function create(){
		$data = $this->input->post('ds');
		$this->db->insert('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		redirect(cur_url(-1));
	}

	function update($id){
		$data = $this->input->post('ds');	
		$this->db->where('', $id);
		$this->db->update('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('', $id);
		$this->db->update('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil dihapus'));
		redirect(cur_url(-2));
	}
}