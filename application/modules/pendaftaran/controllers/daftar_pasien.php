<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daftar_pasien extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 1
			);
		$this->load->model('Mpasien');
	}

	public function index(){
		$data['main_view']	= 'daftar_pasien/index';
		$data['title']		= 'Daftar Pasien ';
		$data['cf']			=  $this->cf;
		$data['current']	= 22;
		$data['desc']		= 'Description. ';
		$this->load->view('template',$data);
	}

	function dt_all_pasien(){
		$config['sTable'] 			= 'ptn_social_data';
		$config['aColumns'] 		= array('sd_rekmed','name_address','sd_age','sd_blood_tp','sd_reg_date');
		$config['key'] 				= 'sd_rekmed';
		$config['php_format'] 		= array('','','','','date','');
		$config['aColumns_format'] 	= array("sd_rekmed","sd_name||'<br/>'||sd_address as name_address","sd_age||' tahun' as sd_age","sd_blood_tp","sd_reg_date");
		$config['searchColumn'] 	= array('sd_name','sd_address','sd_rekmed');
		$config['aksi'] = array(
								'stat'  	=> true,
								'key'		=> 'sd_rekmed',
								'detail'	=> base_url().'pendaftaran/daftar_pasien/detail/',
								);
		init_datatable($config);
	}

	function detail($id){
		$data['cf']			=  $this->cf;
		$data['pasien']		= db_conv($this->db->get_where('ptn_social_data',array('sd_rekmed'=>$id)));
		$data['current']	= 22;
		$data['title']		= 'Daftar Pasien ';
		$data['main_view']	= 'daftar_pasien/detail';
		$data['detail'] 	= db_conv($this->db->get_where('ptn_social_data',array('sd_rekmed'=>$id)));
		$this->load->view('template',$data);
	}

	function edit($id){
		if (is_post()) {
			$data = $this->input->post('ds');
			$cek_rm = $this->cek_rm($id,$data['sd_rekmed']);
			if ($cek_rm) {
				$tgl 	= $this->input->post('tgl');krsort($tgl);
				$rt  	= $this->input->post('rt');
				$data['sd_date_of_birth']	= implode("-", $tgl);
				$data['sd_rt_rw']			= implode("/", $rt);
				$data['sd_reg_date'] = date_to_sql($data['sd_reg_date']);
				$data['modi_datetime'] = DATE('Y-m-d h:i:s');
				$this->db->where('sd_rekmed', $id);
				$this->db->update('ptn_social_data', $data);
				$this->session->set_flashdata('message','pasien berhasil di ubah');
				echo json_encode(array('true',$data['sd_rekmed']));	
			}else{
				echo json_encode(array('false'));	
			}
		}else{
			$data['cf']			=  $this->cf;
			$data['p']			= db_conv($this->db->get_where('ptn_social_data',array('sd_rekmed'=>$id)));
			$data['current']	= 22;
			$data['title']		= 'Edit Pasien ';
			$data['main_view']	= 'daftar_pasien/edit';
			$data['detail'] 	= db_conv($this->db->get_where('ptn_social_data',array('sd_rekmed'=>$id)));

			$data['religi']		= $this->db->get('mst_religion');
			$data['regency']	= $this->db->get('mst_regency');
			$data['province']	= $this->db->get('mst_province');
			$data['occupation']	= $this->db->get('mst_occupation');
			$data['nationality']= $this->db->get('mst_nationality');
			$data['education']  = $this->db->get('mst_education');
			$data['ref_title']  	= $this->db->get('mst_ref_title');
			$data['city_provide'] = $this->convert_type_ahaed($this->db->get('mst_regency'),'mre_name');
			$data['alergi_provide'] = $this->convert_type_ahaed($this->db->get('mst_alergi'),'alg_name');

			$this->load->view('template',$data);
		}
	}

	function convert_type_ahaed($ds,$kk){
		$data = array();
		foreach ($ds->result_array() as $key) {
			$data[] = '"'.$key[$kk].'"';
		}
		return "[".implode(",", $data)."]";
	}

	function cek_rm($o,$n){
		if($o == $n)
			return true;
		else{
			$cek_rem = $this->db->get_where('ptn_social_data',array('sd_status'=>1,'sd_rekmed'=>$n));
			if ($cek_rem->num_rows() > 0) {
				return false;
			}else return true;
		}

	}

	function delete($rm){
		$this->db->delete('ptn_social_data', array('sd_rekmed' => $rm)); 
		$this->session->set_flashdata('message','pasien berhasil di hapus');
		redirect(cur_url(-2));
	}

	// start riwayat
	function riwayat($id,$mdc='',$tipe=""){
		if ($this->input->is_ajax_request()) {
		  //$this->load->model('Mpasien');
		  if ($tipe=='rajal') {
			$data = $this->Mpasien->get_rekmed($mdc);
			$this->mdc 		= $this->db->get_where('trx_medical',array('mdc_id'=>$mdc))->row();
			// pembayaran
			$data['bill_treat']		= $this->db->get_where('v_treat_param_name',array('mdc_id'=>$mdc,'is_delete'=>0));
			$data['bill_obat']		= $this->Mpasien->get_harga_obat($mdc,$this->mdc->tp_insurance);
			$data['bill_detail']	= $this->db->get_where('trx_bill',array('mdc_id'=>$mdc));
			$this->load->view('daftar_pasien/riwayat_single',$data);
		  }else{
		  	$data = $this->Mpasien->get_rekmed_ranap($mdc);
			$this->mdc 		= $this->db->get_where('rnp_medical',array('rnp_id'=>$mdc))->row();
			// pembayaran
			$data['bill_treat']		= $this->db->get_where('v_rnp_treat_param_name',array('rnp_id'=>$mdc,'is_delete'=>0));
			$data['bill_obat']		= $this->Mpasien->get_harga_obat_rnp($mdc,$this->mdc->tp_insurance);
			//$data['ds']				= $this->Mpasien->get_total_harga_ranap($id);
			$data['bill_detail']	= $this->db->get_where('rnp_bill',array('mdc_id'=>$mdc));
			$this->load->view('daftar_pasien/riwayat_single_ranap',$data);
		  }
			//print_r($data);

		}else{
			$this->ptn = $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$id))->row();
			$data['cf']			= $this->cf;
			$data['ptn']		= $this->ptn;
			$data['current']	= 22;
			$data['title']		= 'Riwayat Pasien ';
			$data['main_view']	= 'daftar_pasien/riwayat';
			/*
			$date = array();
			$rajal = $this->db->get_where('v_rekmed_date',array('sd_rekmed'=>$ptn->sd_rekmed,'mdc_status'=>3));
			foreach ($rajal->result() as $key) {
				$date[$key->mdc_in] = array(
						'tipe'	=> 'rajal',
						'id'	=> $key->mdc_id
					);
			}
			$ranap = $this->db->get_where('v_rnp_rekmed_date',array('sd_rekmed'=>$ptn->sd_rekmed, 'rnp_status'=>3));
			foreach ($ranap->result() as $key) {
				$date[$key->rnp_in] = array(
						'tipe'	=> 'ranap',
						'id'	=> $key->rnp_id,
						'out'	=> $key->rnp_out,
					);
			}
			krsort($date);
			*/
			//$data['date']		= $this->db->get_where('v_rekmed_date',array('sd_rekmed'=>$ptn->sd_rekmed,'mdc_status'=>3));
			$data['date']		= $this->Mpasien->get_rekmed_date($id);
			$this->load->view('template',$data);
		}
	}


}