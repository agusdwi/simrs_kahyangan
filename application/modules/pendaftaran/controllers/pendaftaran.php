<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pendaftaran extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 1
			);
	}

	public function index(){
		$data['main_view']	= 'index';
		$data['title']		= 'Pendaftaran Pasien';
		$data['cf']			= $this->cf;
		$this->load->view('template',$data);
	}
	
	function get_pasien(){
		$data = array();
		$q = strtolower($_GET['q']);
		$data = $this->db->select('ptn_rekmed as name,ptn_name as id')->like('lower(ptn_rekmed)', $q, 'both')->or_like('lower(ptn_name)', $q, 'both')->get('v_patient',100,0)->result_array();
		echo json_encode(array('results'=> $data));
	}	
}