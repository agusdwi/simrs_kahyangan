<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendaftaran_rawat_jalan extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 1
			);
	}

	public function index($no_rekmed=0){
		$data['main_view']	= 'pendaftaran_rawat_jalan/index';
		$data['title']		= 'Pendaftaran Rawat Jalan';
		$data['cf']			=  $this->cf;
		$data['current']	= 2;
		$data['desc']		= 'Description.';
		$data['no_rekmed']	= $no_rekmed == 0? '' :$no_rekmed;
		$data['name']		= '';
		if(!$no_rekmed==0){
			$a = $this->db->select('ptn_name')->where('ptn_rekmed',$no_rekmed)->get('v_patient',1,0)->row();
			$data['name'] = $a->ptn_name;
		}
		$this->load->view('template',$data);
	}

	function create(){
		$rekmed = $this->input->post('fx_pasien_input');
		if ($this->db->get_where('trx_medical',array('mdc_status'=>1,'sd_rekmed'=>$rekmed))->num_rows() > 0) {
			echo "false";
		}else if($this->db->get_where('trx_queue_outpatient',array('queo_status'=>1,'sd_rekmed'=>$rekmed))->num_rows() > 0){
			echo "false";
		}else{
			$this->db->trans_start();
			$ym = date('ym');
			$a = $this->db->like('queo_id',$ym,'after')->order_by('queo_id','DESC')->get('trx_queue_outpatient',1,0)->row();
			$queo = count($a) == 0 ?date('ym')."0001" : $a->queo_id+1;
			$no = count($a) == 0 ? 1 : $a->queo_no+1;
			$user = get_user();
			$data = array(
				'queo_id'	=> $queo,
				'sd_rekmed' => $rekmed,
				'queo_no'	=> $no,
				'queo_datetime' => date('Y-m-d h:i:s'),
				'dr_id'		=> $this->input->post('dr_id'),
				'pl_id'		=> $this->input->post('poli_id'),
				'sch_id'	=> $this->input->post('sch_id'),
				'modi_id'	=> $user['user_id']
				);
			$a = $this->db->insert('trx_queue_outpatient',$data);
			if($this->input->post('agent_name') !== ''){
				$data = array(
					'que_id'	=> $queo,
					'que_tp'	=> 'TP_OUTPATIENT',
					'agent_name'=> $this->input->post('agent_name'),
					'agent_relationship'=> $this->input->post('agent_relationship'),
					'agent_sex'=> $this->input->post('agent_sex'),
					'agent_address'=> $this->input->post('agent_address'),
					'agent_telp'=> $this->input->post('agent_telp'),
					'modi_id'	=> $user['user_id']
					);
				$b = $this->db->insert('trx_queue_agent',$data);
			}
			$this->db->trans_complete();
			echo "true";
		}
	}

	function update($id){
		$data = $this->input->post('ds');	
		$this->db->where('', $id);
		$this->db->update('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('', $id);
		$this->db->update('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil dihapus'));
		redirect(cur_url(-2));
	}

	function dt_doctor($pl_id=0,$tgl){
		$tgl=explode('-',$tgl);
		$d=$tgl[2];
		if($d<10){
			$d='0'.$d;
		}
		$m=$tgl[1];
		if($m<10){
			$m='0'.$m;
		}
		$y=$tgl[0];
		$tgl=$y.'-'.$m.'-'.$d;
		
		$sql = "select *, 
			(select count(*) from trx_queue_outpatient where dr_id = d.dr_id and queo_datetime like '$tgl%' and queo_status = 1) as antrian 
				from v_sch_doctor d where sch_time_start like '$tgl%' ";
		if ($pl_id != 0) {
			$sql .= "and pl_id = $pl_id";
		}
		$sql = $this->db->query($sql)->result();
		
		echo json_encode($sql);
	}

	function get_qr(){
		$this->load->view('pendaftaran_rawat_jalan/qrcode_scanner');
	}
}