<? 
    $this->load->view('pendaftaran/local/behav');
?>
 
<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.gritter.css" />

<style type="text/css" media="screen">
	.dataTables_scrollHead{
		margin-bottom:-25px;
	}
	.dataTables_filter{  
		display: none;
	}
	.dataTables_scrollBody{
		height: 200px;
	}
	.alert{
		background-color: transparent;
		border: 0px;
	}

	#ffb-input{
		width: 40%;
	}

	#fx_pasien_ctr.ffb{
		width:220px !important;
		top: 28px !important;
	}

	#gritter-notice-wrapper{
		right: 13%;
		top: 100px;
	}
	#fx_pasien_ctr .row .col1{
		float:left;
		/*width:50px;*/
	}
	#fx_pasien_ctr .row .col2{
		float:left;
		margin-left: 10px;
		/*width:150px;*/
	}
</style>

<div id="gritter-notice-wrapper" class="alert hide" style="width:750px;position:fixed">
		<div id="gritter-item-1" class="gritter-item-wrapper" style="margin:0 -17px 5px 0">
			<div class="gritter-top"></div>
			<div class="gritter-item">
				<div class="gritter-close" style="display: none; width:50px "></div>
				<img src="<?=base_url()?>assets/img/demo/envelope.png" class="gritter-image">
				<div class="gritter-with-image" style="width:448px">
					<span class="gritter-title" style="margin-left:36px">Message</span>
					<p>Data Berhasil Disimpan 	</p>
				</div>
				<div style="clear:both"></div>
			</div>
			<div class="gritter-bottom"></div>
		</div>
	</div>

    <div class="pageheader notab">
	    <h1 class="pagetitle">Antrian Pendaftaran Rawat Inap</h1>
	</div>

	<div class="container-fluid">
	    <div class="row-fluid">
	        <div class="span12">
	            <div class="widget-box" style="margin-top:0px">
	                <div class="widget-title">
	                    <ul class="nav nav-tabs">
	                        <li class="active"><a data-toggle="tab" href="#tab1">Tambah Antrian</a></li>
	                        <!-- <li><a data-toggle="tab" href="#tab2">Daftar Antrian</a></li> -->
	                    </ul>
	                </div>
	                
	                <?=form_open(cur_url().'',array('class' => 'form-horizontal','id' => 'form')); ?>
	                <div class="widget-content tab-content" style="overflow:hidden">
	                	<label class="control-label">No.Rekmed</label>
                        <div class="controls">
                            <div id="fx_pasien"></div>
                        </div>
                        <label class="control-label">Nama Pasien</label>
                        <div class="controls">
                            <input type="text" readonly="readonly" class="small" id="name"  placeholder="Nama Pasien">
                        </div>
                        <label class="control-label">Kelas</label>
                        <div class="controls">
                            <select id="class" style="width:140px"  name="ds[sd_reg_kab]">
                                <?foreach ($class->result() as $key): ?>
                                    <option value="<?=$key->mscl_id?>"><?=$key->mscl_name?></option>
                                <?endforeach ?>
                            </select>
                        </div>
                        <label class="control-label">Ruang</label>
                        <div class="controls">
                            <select id="poli" style="width:140px">
                                <option value="" >Choose One</option>
                                <option value="gigi">Melati</option>
                                <option value="tulang">Mawar</option>
                            </select>
                        </div>
                        <label class="control-label">Pilih Dokter</label>
                        <table class="table table-bordered def_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
							<thead>
								<tr role="row">
									<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:10%" aria-label="Nama: activate to sort column ascending">ID Dokter</th>
									<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:30%" aria-label="Harga: activate to sort column ascending">Nama Dokter</th>
									<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:20%" aria-label="Aksi: activate to sort column ascending">Kelas</th>
									<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:20%" aria-label="Aksi: activate to sort column ascending">Ruang</th>
									<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:20%" aria-label="Aksi: activate to sort column ascending">Jam</th>
								</tr>
							</thead>
							<tbody>
								<tr id="gigi">
									<td class="center"><b>DR-092</b></td>
									<td>Dr.Joko</td>
									<td>VIP</td>
									<td class="center">W2</td>
									<td class="center">09.00-10.00</td>
								</tr>
								<tr id="tulang">
									<td class="center"><b>DR-093</b></td>
									<td>Dr.Parno</td>
									<td>VVIP</td>
									<td class="center">W2</td>
									<td class="center">09.00-10.00</td>
								</tr>
								<tr id="tulang">
									<td class="center"><b>DR-094</b></td>
									<td>Dr.Budi</td>
									<td>VVIP</td>
									<td class="center">W2</td>
									<td class="center">09.00-10.00</td>
								</tr>
								<tr id="gigi">
									<td class="center"><b>DR-095</b></td>
									<td>Dr.Joko</td>
									<td>VIP</td>
									<td class="center">W2</td>
									<td class="center">09.00-10.00</td>
								</tr>

							</tbody>
							</table>
	                </div>
	                </div>
	                <div class="form-actions" style="margin-top: 12px;margin-bottom: -17px;">
	                    <!-- <a class="btn btn-primary" style="margin-left:90%" id="save" >Simpan</a> -->
	                    <button type="submit" style="margin-left:90%" class="btn btn-primary">Simpan</button>
	                </div> 
	                <?=form_close()?>                           
	            </div>
	        </div>
	    </div>
	</div>


<script type="text/javascript" charset="utf-8">
    $(function(){
        
		// click menu
         $(".tb_scrol tbody tr").click(function(){
         	$(".tb_scrol tbody tr.active").removeClass('active');
         	$("#poli").select
			$(this).addClass('active');
			var slt = $(this).attr('id');
			$("#poli").val(slt);
		})

          $("#form").submit(function(){
          	$(".alert").fadeIn().delay(500).fadeOut(function(){
          		$("#field2").fadeOut(function(){
          			$("#field1").fadeIn("fast");
          		});
          	});
            // $("#save").trigger('click');
            /*var url  = $(this).attr('action');
            var data = $(this).serialize();
            $.post(url,data, function(data){
            alert("jalsfasf");return false;
                $("#ifr").attr('src',data);
            });*/
            return false;
        })


    })


          function callback(){
          	$("#name").val($("#fx_pasien_hidden").val());

          }

</script>