<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>
<div class="container-fluid">
	<div class="row-fluid">
		<a href="<?=base_url()?>pendaftaran/daftar_pasien/delete/<?=$pasien->sd_rekmed;?>" style="float:right" class="btn btn-danger del_ptn"><i class="icon icon-trash icon-white"></i> Delete</a>
		<a href="<?=base_url()?>pendaftaran/daftar_pasien/riwayat/<?=$pasien->sd_rekmed;?>" style="float:right;margin-right:10px" class="btn btn-info"><i class="icon icon-edit icon-white"></i> Riwayat Pasien</a>
		<a href="<?=base_url()?>pendaftaran/daftar_pasien/edit/<?=$pasien->sd_rekmed;?>" style="float:right;margin-right:10px" class="btn btn-warning"><i class="icon icon-edit icon-white"></i> Edit</a>
		<a href="<?=base_url();?>pendaftaran/daftar_pasien" style="float:right;margin-right:10px" class="btn btn-warning"><i class="icon icon-chevron-left icon-white"></i>Kembali</a>
		<div class="span5" >
			<div class="title"><h3>Detail Pasien </h3></div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12">
				<table id='tb_dokter' class="table table-bordered">
					<tbody>
						<tr><td width="150px">No.REKMED</td><td><?=$pasien->sd_rekmed?></td></tr>
						<tr><td>Nama Pasien</td><td><?=$pasien->sd_title;?>. <?=$pasien->sd_name?></td></tr>
						<tr><td>Telp</td><td><?=$pasien->sd_telp;?></td></tr>
						<tr><td>Jenis Kelamin</td><td><?=change_sex($pasien->sd_sex);?></td></tr>
						<tr><td>Tempat Tgl Lahir</td><td><?=$pasien->sd_place_of_birth;?>, <?=format_date_time($pasien->sd_date_of_birth,false);?></td></tr>
						<tr><td>Umur</td><td><?=$pasien->sd_age;?> Th</td></tr>
						<tr><td>Golongan Darah</td><td><?=$pasien->sd_blood_tp;?></td></tr>
						<tr><td>Alamat</td>
							<td><?=$pasien->sd_address;?>
								<br>
								<?=$pasien->sd_reg_desa;?> <?=$pasien->sd_rt_rw;?>
								<?=$pasien->sd_reg_kec;?> <?=$pasien->sd_reg_kab;?> <?=$pasien->sd_reg_prov;?>
							</td></tr>
						<tr><td>Provinsi, Kabupaten</td><td><?=change_prov_kab($pasien->sd_reg_prov,$pasien->sd_reg_kab);?></td></tr>
						<tr><td>Kewarganegaraan</td><td><?=$pasien->sd_citizen;?></td></tr>
						<tr><td>Status Perkawinan</td><td><?=$pasien->sd_marital_st;?></td></tr>
						<tr><td>Agama</td><td><?=$pasien->sd_religion;?></td></tr>
						<tr><td>Pendidikan Terakhir</td><td><?=$pasien->sd_education;?></td></tr>
						<tr><td>Pekerjaan</td><td><?=$pasien->sd_occupation;?></td></tr>
						<tr><td>Tanggal Daftar</td><td><?=format_date_time($pasien->sd_reg_date,false);?></td></tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("body").on('click','.del_ptn',function(){
			return confirm('apakah anda yakin menghapus pasien ini ?\ndata yang dihapus tidak bisa dikembalikan !!!');
		})
	})
</script>