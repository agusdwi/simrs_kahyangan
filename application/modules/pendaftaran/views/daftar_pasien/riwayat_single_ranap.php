<div class="span12" style="padding:0px 1%">
	<b>Rawat Inap [ <?=format_date_time($medical->rnp_in,false);?>  -  <?=format_date_time($medical->rnp_out,false);?> ]</b><br>
	<table id="rekmed" style="width:100%" class="table table-bordered">
		<tr>
			<td style="width:30%">Tanggal </td><td> <?=pretty_date($medical->rnp_in);?>  -  <?=pretty_date($medical->rnp_out);?> </td>
		</tr>
		<tr>
			<td>Ruang / Kamar</td><td><?=$medical->r_nama;?> / <?=$medical->k_nama;?></td>
		</tr>
		<tr>
			<td>Dokter</td><td><?=$medical->dr_name;?></td>
		</tr>
		<tr>
			<td>Diagnosa</td>
			<td>
				<?foreach ($diagnosis as $key => $value): ?>
					<b class="group_date"># <?=format_date_time($key,false);?></b><br>
					<?foreach ($value as $k => $v): ?>
						<b><?=$k;?></b><br>
						<?foreach ($v as $kk): ?>
							<p>- <?=$kk?></p>
						<?endforeach ?>
					<?endforeach ?>
				<?endforeach;?>
			</td>
		</tr>
		<tr>
			<td>Tindakan</td>
			<td>
				<?foreach ($tindakan as $k => $value): ?>
					<b class="group_date"># <?=format_date_time($k,false);?></b><br>
					<?foreach ($value as $key): ?>
						<p>- <?=$key->jumlah;?>x <?=$key->treat_code;?>, <?=$key->treat_name;?></p> 
					<?endforeach ?>
				<?endforeach;?>
			</td>
		</tr>
		<tr>
			<td>Obat yang digunakan</td>
			<td>
				<table>
					<?foreach ($obat as $k => $value): ?>
						<tr>
							<td colspan="3"><b style="margin-left:-17px" class="group_date"># <?=format_date_time($k,false);?></b><br></td>
						</tr>
						<?foreach ($value as $key): ?>
							<tr>
								<td>
									<?=($key->is_racik == 1)? '- racik,' : '-' ;?>
									<?if ($key->is_racik == 1): ?>
										<?=$key->recipe_racik;?>
									<? else: ?>
										<?=$key->mdcn_code;?>, <?=$key->mdcn_name;?>
									<?endif ?>
								</td>
								<td><?=$key->recipe_rule;?></td>
								<td>
									<?=$key->recipe_qty;?>
								</td>
							</tr>
						<?endforeach?>
					<?endforeach;?>
				</table>
				
			</td>
		</tr>
		<tr>
			<td>Status Pulang</td><td><?=$medical->rnp_status_pulang;?></td>
		</tr>
		<tr>
			<td>Catatan</td><td><?=$medical->rnp_note;?></td>
		</tr>
	</table>
</div>