<style type="text/css">
	#menu-tab{
		padding-left: 0px !important;
		margin-left: 0px !important;
		width: 261px !important;
		border: 1px solid #ddd !important;
		margin: 0px 99px -1px !important;
		border-right: 1px !important;
	}
	.tab-pane{
		display: none;
	}
	#detail_rekmed {
		margin-left: 100px;
	}
</style>
<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>
<div class="container-fluid">
	<div class="row-fluid">
		<a href="<?=base_url();?>pendaftaran/daftar_pasien/detail/<?=$this->uri->segment(4);?>" style="float:right;margin-right:10px" class="btn btn-warning"><i class="icon icon-chevron-left icon-white"></i>Kembali</a>
		<div class="span5" >
			<div class="title"><h3><?=$ptn->sd_name;?> </h3></div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12">
				<div id="page">
					<div class="container-fluid">
						<div class="row-fluid">
							<?if (count($date) == 0): ?>
                				<center><h4>pasien belum memiliki riwayat medis</h4></center>
                			<?else: ?>	
	                			<div id="rekmed_tabbable" class="tabbable tabs-left ">
	                				<ul class="nav nav-tabs" id="list_rekmed">
	                					<?foreach($date as $key => $d): $v = (object)$d;?>
	                						<li class="pilih">
	                							<a data-tipe="<?=$v->tipe?>" href="<?=$v->id;?>">
                                                    <?=format_date_time($key,false);?>
                                                    <?if ($v->tipe == 'ranap'): ?>
                                                    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---
                                                    <br> <?=format_date_time($v->out,false);?>
                                                    <?endif;?>
                                                </a>
	                						</li>
	                					<?endforeach;?>
	                				</ul>
	                				<div class="tab-content" id="detail_rekmed">
	                					...
	                				</div>
	                			</div>
                			<?endif ?>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

<script>
    $(function(){
    	$(".pilih").on('click','a',function(event){
    		event.preventDefault();
            $(".pilih.active").removeClass('active');
    		$(this).parent().addClass('active');
    		$("#detail_rekmed").html('...');
    		$("#detail_rekmed").load(CUR+$(this).attr('href')+'/'+$(this).data('tipe'),function(){
    			$("#ttab1").trigger('click');
    		});
    	})
		$('ul#list_rekmed a:eq(0)').trigger('click')
		$("#menu-tab a").click(function(e){
			e.preventDefault();
			var target = $(this).attr('href');
			$(".ui-state-active").removeClass('ui-state-active');
			$(".ui-tabs-selected").removeClass('ui-tabs-selected');
			$(this).parent().addClass('ui-state-active');
			$(this).parent().addClass('ui-tabs-selected');
			$(".tab-pane").hide();
			$(target).show();
		})
    })
</script>