<style type="text/css" media="screen">
	.frm_search input{margin-top:-1px;float:right;margin-left:2px}
	.frm_search .btn{float:right;margin-left:5px}
	.frm_search .btn span{padding:2px 10px;}
	.search_choice a{font-weight:bold}
	.search_choice a.active{color:#fb9338}
	.search_choice {float:right;margin-right:95px}
	#advance{display:none}
	#filter{border-bottom: 1px dashed #DDD;}
	#body_search{margin-top:10px}
	.tname{font-size:110%}
	#dyntable tbody tr td{border-right:none}
	#dyntable tbody tr td + td + td + td + td {border-right:1px solid #DDD}
	#dyntable tr:nth-child(even){
		background:#F7F7F7;
	}
	.dataTables_scrollHead{
		margin-bottom: -22px;
	}
	.dataTables_info{
		margin-top: 20px;
	}
</style>
<script type="text/javascript" charset="utf-8">
	$(function(){
		var d_uri = "<?=base_url()?>pendaftaran/daftar_pasien/dt_all_pasien";
		oTb = $('#tb_dokter').dataTable( {
			"bProcessing": true,
			"bServerSide": true,
			"bLengthChange": false,		
			"bFilter": true,
			"sPaginationType": "full_numbers",
			"aoColumns": [null,null,null,null,null,{ "bSortable": false }],
			"sAjaxSource": d_uri,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				var newArray = $.merge(aoData, [{ "name": "<?=$this->security->get_csrf_token_name()?>", "value": "<?=$this->security->get_csrf_hash()?>" }]);
				$.ajax( {
					"dataType": 'json',
					"type": "POST", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				} );
			}
		});
		$("#tb_dokter_filter").hide();	
		$(".chatsearch input").keyup(function(e){
			$("#tb_dokter_filter input").val($(".chatsearch input").val()).trigger('keyup');
		})
		// buat init sort
		oTb.fnSort( [ [4,'desc']] );
	})
</script>

<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5" >
			<div class="title"><h3>Data Pasien</h3></div>
		</div>
		<div class="span5" style="float:right;">
			<div class="widgetbox" style="margin-top:20px;">
				<?=form_open('',array('class'=>'frm_search'));?>
					<div id="basic">
						<div class="chatsearch" >
                        	<input type="text" name="" placeholder="Search" style="width:91%;margin:auto;">
                    	</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12">
				<table id='tb_dokter' class="table table-bordered">
					<thead>
						<th width="130px">No.Rekmed</th>
						<th>Nama,alamat</th>
						<th width="50px	">Umur</th>
						<th width="50px	">Gol.Darah</th>
						<th width="100px">Tgl Pendaftaran</th>
						<th width="50px">Detail</th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
