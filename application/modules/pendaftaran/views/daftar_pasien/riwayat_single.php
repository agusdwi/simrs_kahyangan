<div id="tab1" class="span12 tab-pane" style="padding:0px;margin-left:0px">
	Rawat Jalan
	<table id="rekmed" style="width:100%" class="table table-bordered">
		<tr>
			<td style="width:30%">Tanggal & Jam</td><td><?=pretty_date($medical->mdc_in);?></td>
		</tr>
		<tr>
			<td>Poli / unit</td><td><?=$medical->pl_name;?></td>
		</tr>
		<tr>
			<td>Dokter</td><td><?=$medical->dr_name;?></td>
		</tr>
		<tr>
			<td>Diagnosa</td>
			<td>
				<?foreach ($diagnosis as $key => $value): ?>
					<b><?=$key;?></b><br>
					<?foreach ($value as $k): ?>
						<p>- <?=$k?></p>
					<?endforeach ?>
				<?endforeach ?>
			</td>
		</tr>
		<tr>
			<td>Tindakan</td>
			<td>
				<?foreach ($tindakan->result() as $key): ?>
					<p>- <?=$key->jumlah;?>x <?=$key->treat_code;?>, <?=$key->treat_name;?></p> 
				<?endforeach ?>
			</td>
		</tr>
		<tr>
			<td>Obat yang digunakan</td>
			<td>
				<table>
					<?foreach ($obat->result() as $key): ?>
						<tr>
							<td>
								<?=($key->is_racik == 1)? '- racik,' : '-' ;?>
								<?if ($key->is_racik == 1): ?>
									<?=$key->recipe_racik;?>
								<? else: ?>
									<?=$key->mdcn_code;?>, <?=$key->mdcn_name;?>
								<?endif ?>
							</td>
							<td><?=$key->recipe_rule;?></td>
							<td>
								<?=$key->recipe_qty;?>
							</td>
						</tr>
					<?endforeach?>
				</table>
			</td>
		</tr>
		<tr>
			<td>Status Pulang</td><td><?=$medical->status_pulang;?></td>
		</tr>
		<tr>
			<td>Catatan</td><td><?=$medical->note;?></td>
		</tr>
	</table>
</div>
<div id="tab2" class="span12 tab-pane" style="padding:0px;margin-left:0px">
	<?
	if ($bill_detail->num_rows() == 1) {
		$b = $bill_detail->row();
		$bl_note 		= $b->bl_note;
		$bl_discount 	= $b->bl_discount;
	}else{
		$bl_note 		= "";
		$bl_discount 	= "";
	}
	?>
	<div style="padding:0px 10px 0px 10px">
		<b>Detail Transaksi (<?=pretty_date($medical->mdc_in);?>): </b>
		<table class="table table-bordered table-striped table_tr" style="border-left:none;margin-bottom:0px;">
			<thead>
				<tr>
					<th>No.</th>
					<th>Jenis</th>
					<th>Harga Item</th>
					<th>Qty</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="5" style="text-align:left;font-weight:bold;border-left:0px;">Pembelian Obat</td>
				</tr>
				<?$i=$ztot=0;foreach ($bill_obat as $key): $i++?>
				<tr>
					<td style="border-left:0px;text-align:center;width:20px;"><?=$i;?></td>
					<td>
						<?if ($key->is_racik == 1): ?>
						<b>(racik)</b> <?=$key->recipe_racik;?>
						<?else:?>
						<b>(<?=$key->mdcn_code;?>)</b><?=$key->mdcn_name;?>
						<?endif;?> 
					</td>		 	
					<td class="money"> <?=int_to_money($key->harga);?></td>
					<td class="money"> <?=$key->recipe_qty;?></td>
					<td class="money"> 
						<?
						$itot = $key->recipe_qty*$key->harga;
						$ztot+= $itot;
						echo int_to_money($itot);
						?>
					</td>
				</tr>	
				<?endforeach;?>
				<tr>
					<td colspan="5" style="text-align:left;font-weight:bold;border-left:0px;">Tindakan / jasa medis</td>
				</tr>
				<?foreach ($bill_treat->result() as $key): $i++;?>
				<tr>
					<td><?=$i;?></td>
					<td><b>(<?=$key->treat_code;?>)</b> <?=$key->treat_name;?></td>
					<td class="money"><?=int_to_money($key->total);?></td>
					<td class="money"><?=$key->jumlah;?></td>
					<td class="money"> 
						<?
							$itot = $key->total*$key->jumlah;
							$ztot+= $itot;
							echo int_to_money($itot);
						?>
					</td>
				</tr>
				<?endforeach;?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Total</td>
					<td class="money"><?=int_to_money($ztot);?></td>
				</tr>
				<tr>
					<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Diskon</td>
					<td class="money"><?=int_to_money(empty($bl_discount)?0:$bl_discount);?></td>
				</tr>
				<tr>
					<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Grand Total</td>
					<td class="money"><?=int_to_money($ztot - $bl_discount);?></td>
				</tr>

			</tfoot>
		</table>
	</div>
</div>