<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box" style="margin-top:0px">
                <?=form_open('',array('class' => 'form-horizontal','id' => 'form')); ?>
                <div class="widget-content tab-content" style="overflow:hidden">
                    <div id="tab1" class="tab-pane active">
                        <div class="span6"> 
                            <label class="control-label">Nomor RM</label>
                            <div class="controls">
                                <input id="rm_2" name="ds[sd_rekmed]" class="" style="width:100px" type="text" value="<?=$p->sd_rekmed;?>"> 
                            </div>
                            <label class="control-label">Nama Pasien</label>
                            <div class="controls">
                                <select name="ds[sd_title]" class="pull-left" style="width:100px">
                                    <?foreach ($ref_title->result() as $key): ?>
                                        <option <?=$p->sd_title == $key->title_nama ? 'selected="selected"' : "" ?>><?=$key->title_nama;?></option>
                                    <?endforeach ?>
                                </select>
                                <input type="text" style="width:250px" placeholder="Nama Pasien" id="sd_name" name="ds[sd_name]" autofocus value="<?=$p->sd_name;?>">
                            </div>
                            <label class="control-label">No.Telp</label>
                            <div class="controls">
                                <input type="text" name="ds[sd_telp]" placeholder="No.Telp" value="<?=$p->sd_telp;?>">
                            </div>
                            <label class="control-label">Jenis Kelamin</label>
                            <div class="controls">
                                <table>
                                    <tr>
                                        <td style="width:100px"><label><input type="radio" name="ds[sd_sex]" value="P" <?=$p->sd_sex=='p'? 'checked="checked"': '';?>/> Perempuan</label></td>
                                        <td style="width:100px"><label><input type="radio" name="ds[sd_sex]" value="L" <?=$p->sd_sex=='l'? 'checked="checked"': '';?>/> Laki-laki</label></td>
                                    </tr>
                                </table>
                            </div>
                            <label class="control-label">Tempat Lahir</label>
                            <div class="controls">
                                <input type="text" name="ds[sd_place_of_birth]"  placeholder="Tempat Lahir" id="tempat_lahir" 
                                    data-provide="typeahead" autocomplete="off" data-source='<?=$city_provide;?>' value="<?=$p->sd_place_of_birth;?>"
                                >
                            </div>
                            <label class="control-label">Tgl Lahir</label>
                            <div class="controls">
                            	<? 
                            		$tl = $p->sd_date_of_birth;
                            		$a = explode(" ", $tl);
                            		$d = explode("-", $a[0]);
                            	?>
                                <select  name="tgl[0]" style="min-width:30px;width:90px" style="float:left" id="tgl">
                                    <option value="" >-- tgl --</option>
                                    <?=get_hari($d[2])?>
                                </select>
                                <select name="tgl[1]" style="width:90px" id="bln">
                                    <option value="">-- bulan --</option>
                                    <?=get_bulan($d[1])?>
                                </select>
                                <select name="tgl[2]" style="width:90px" id="thn">
                                    <option value="">-- tahun --</option>
                                    <?=get_tahun($d[0])?>
                                </select>
                                <label for="tgl" generated="true" class="error"></label>
                            </div>
                            <label class="control-label">Umur</label>
                            <div class="controls">
                                <input type="text" style="width:40px" id="umur" name="ds[sd_age]" placeholder="0" value="<?=$p->sd_age;?>"> Tahun
                            </div>
                            <label class="control-label">Gol.Darah</label>
                            <div class="controls">
                                <table>
                                    <tr>
                                        <td style="width:100px"><label><input type="radio" name="ds[sd_blood_tp]" value="-" <?=$p->sd_blood_tp=='-'? 'checked="checked"': '';?> /> -</label></td>
                                        <td style="width:100px"><label><input type="radio" name="ds[sd_blood_tp]" value="A" <?=$p->sd_blood_tp=='A'? 'checked="checked"': '';?>/> A</label></td>
                                        <td style="width:100px"><label><input type="radio" name="ds[sd_blood_tp]" value="B" <?=$p->sd_blood_tp=='B'? 'checked="checked"': '';?>/> B</label></td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px"><label><input type="radio" name="ds[sd_blood_tp]" value="AB" <?=$p->sd_blood_tp=='AB'? 'checked="checked"': '';?> /> AB</label></td>
                                        <td style="width:100px"><label><input type="radio" name="ds[sd_blood_tp]" value="O" <?=$p->sd_blood_tp=='O'? 'checked="checked"': '';?> /> O</label></td>
                                    </tr>
                                </table>
                            </div>
                            <label class="control-label">Alergi</label>
                            <div class="controls">
                                <input type="text" name="ds[sd_alergi]"  placeholder="alergi" id="alergi" 
                                    data-provide="typeahead" autocomplete="off" data-source='<?=$alergi_provide;?>'
                                    value="<?=$p->sd_alergi;?>"
                                >
                            </div>
                            <label class="control-label">Dusun/Kampung/Jln</label>
                            <div class="controls">
                                <input type="text" name="ds[sd_reg_street]" id="street" placeholder="Dusun/Kampung/Jln" value="<?=$p->sd_reg_street;?>">
                            </div>
                            <label class="control-label">&nbsp;</label>
                            <div class="controls">
                            	<?
                            		if (empty($rt)) {
                            			$rt = array('','');
                            		}else{
                            			$rt = explode('/', $p->sd_rt_rw);
                            		}
                            	?>
                                <table style="width:156px">
                                    <tr>
                                        <td style="width:4%">
                                            <label>RT</label>
                                        </td>
                                        <td style="width:10px">
                                            <label style="width:40px"><input type="text" id="rt" name="rt[0]" placeholder="RT" value="<?=$rt[0];?>"></label>
                                        </td>
                                        <td style="width:4%">
                                            <label>RW</label>
                                        </td>
                                        <td style="width:10px">
                                            <label style="width:40px"><input type="text" id="rw" name="rt[1]" placeholder="RT" value="<?=$rt[1];?>"></label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <label class="control-label">Kelurahan/Desa</label>
                            <div class="controls">
                                <input type="text" name="ds[sd_reg_desa]" id="desa" placeholder="Kelurahan/Desa" value="<?=$p->sd_reg_desa;?>">
                            </div>
                            <label class="control-label">Kecamatan</label>
                            <div class="controls">
                                <input type="text" name="ds[sd_reg_kec]" id="kec" placeholder="Kecamatan" value="<?=$p->sd_reg_kec;?>">
                            </div>
                            <label class="control-label">Provinsi</label>
                            <div class="controls">
                                <select style="width:250px" name="ds[sd_reg_prov]" id="provinsi">
                                    <?foreach ($province->result() as $key): ?>
                                        <option <?=$p->sd_reg_prov==$key->mpr_id? 'selected="selected"': '';?> value="<?=$key->mpr_id?>"><?=$key->mpr_name?></option>
                                    <?endforeach ?>
                                </select>
                            </div>
                            <label class="control-label">Kabupaten/Kotamadya</label>
                            <div class="controls">
                                <select style="width:250px"  name="ds[sd_reg_kab]" id="kota"></select>
                            </div>
                        </div>
                        <div class="span6">
                            <label class="control-label">Warga Negara</label>
                            <div class="controls">
                                 <ul class="thumbnails" style="margin-bottom:0px" >
                                    <?foreach ($nationality->result() as $key): ?>
                                        <li class="list"><input type="radio" name="ds[sd_citizen]" value="<?=$key->mna_name?>" <?=$p->sd_citizen==$key->mna_name? 'checked="checked"': '';?> />  <?=$key->mna_name?></li>
                                    <?endforeach?>
                                </ul>
                            </div>
                            <label class="control-label">Status</label>
                            <div class="controls">
                                <ul class="thumbnails" style="margin-bottom:0px" >
                                    <li class="list"><input type="radio" name="ds[sd_marital_st]"  <?=$p->sd_marital_st== "belum kawin" ? 'checked="checked"': '';?>  value="belum kawin" />  Belum Kawin</li>
                                    <li class="list"><input type="radio" name="ds[sd_marital_st]" <?=$p->sd_marital_st== "kawin" ? 'checked="checked"': '';?>  value="kawin" />  Kawin</li>
                                    <li class="list"><input type="radio" name="ds[sd_marital_st]" <?=$p->sd_marital_st== "duda/janda" ? 'checked="checked"': '';?>  value="duda/janda" />  Duda/Janda</li>
                                    <li class="list"><input type="radio" name="ds[sd_marital_st]" <?=$p->sd_marital_st== "tidak kawin" ? 'checked="checked"': '';?>  value="tidak kawin" />  Tidak Kawin</li>
                                </ul>
                            </div>
                            <label class="control-label">Agama</label>
                            <div class="controls">
                                <ul class="thumbnails" style="margin-bottom:0px" >
                                    <?foreach ($religi->result() as $key): ?>
                                        <li class="list"><input type="radio" name="ds[sd_religion]" <?=$p->sd_religion== $key->mr_name ? 'checked="checked"': '';?> value="<?=$key->mr_name?>" />  <?=$key->mr_name?></li>
                                    <?endforeach?>
                                </ul>
                            </div>
                            <label class="control-label">Pendidikan</label>
                            <div class="controls">
                                <ul class="thumbnails" style="margin-bottom:0px" >
                                    <?foreach ($education->result() as $key): ?>
                                        <li class="list"><input type="radio" name="ds[sd_education]" value="<?=$key->med_name?>" <?=$p->sd_education== $key->med_name ? 'checked="checked"': '';?> />  <?=$key->med_name?></li>
                                    <?endforeach?>
                                </ul>
                            </div>
                            <label class="control-label">Pekerjaan</label>
                            <div class="controls">
                                <ul class="thumbnails" style="margin-bottom:0px" >
                                    <?foreach ($occupation->result() as $key): ?>
                                        <li class="list"><input type="radio" name="ds[sd_occupation]" value="<?=$key->mo_name?>" <?=$p->sd_occupation== $key->mo_name ? 'checked="checked"': '';?> />  <?=$key->mo_name?></li>
                                    <?endforeach?>
                                </ul>
                            </div>
                            <label class="control-label">Alamat</label>
                            <div class="controls">
                                <textarea name="ds[sd_address]" id="address" rows="3" ><?=$p->sd_address;?></textarea>
                            </div>
                            <label class="control-label">Tanggal daftar</label>
                            <div class="controls">
                                <input type="text" name="ds[sd_reg_date]" placeholder="dd-mm-yy" class="datepicker" value="<?=format_date_time($p->sd_reg_date,false);?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions" style="margin-top: 12px;margin-bottom: -17px;">
                	<button type="submit" style="margin-left:90%" class="btn btn-primary">Simpan</button>
                </div> 
                <?=form_close();?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	function getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    var isvalid;

	$(function(){
		isvalid = $("#form").validate({
			rules: {
				'ds[sd_name]': "required",     
				'ds[sd_age]' : "required",
			},
			submitHandler: function(form) {
				var url  = "";
				var data = $(form).serialize();
				$.post(url,data, function(rslt){
					if (rslt[0]=="true") {
						window.location = "<?=cur_url(-2)?>/detail/"+rslt[1];
					}else{
						alert("No Rekam Medik sudah dipakai, silahkan pilih yang lain");
						$("#rm_2").focus();
					}
				},'json'); 
				return false;
			}
		});

		<?
			$str = array();
			foreach ($regency->result() as $key) {
				$str[$key->mpr_id][] = array($key->mre_id,$key->mre_name);
			}
			$str_kota = json_encode($str);
		?>
		var str_kota = <?=$str_kota?>;

		$("#provinsi").change(function(){
			$("#kota").html('');
			$.each(str_kota[$(this).val()], function( index, value ) {
				$("#kota").append('<option value="'+value[0]+'">'+value[1]+'</option>');
			});
		})
		$("#provinsi").trigger('change');

		$("#thn").change(function(){
            var tgl = $("#tgl").val();
            var bln = $("#bln").val();
            var thn = $("#thn").val();
            var birth = [thn,bln,tgl].join("/");
             $("#umur").val(getAge(birth));
        })
	})
</script>