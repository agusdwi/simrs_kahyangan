<div class="pageheader notab">
	    <h1 class="pagetitle"><?=$title?></h1>
	</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span5 center" style="text-align: center;">                 
			<ul class="quickstats">
				<li>
					<a href="pendaftaran/pendaftaran_baru">
						<img alt="" src="<?=base_url()?>assets/img/icons/medicoicons/prescription.png">
						<span> pendaftaran<br>pasien baru</span>
					</a>
				</li>
				<li>
					<a href="pendaftaran/pendaftaran_rawat_jalan">
						<img alt="" src="<?=base_url()?>assets/img/icons/medicoicons/medkit.png">
						<span> pendaftaran<br>rawat jalan</span>
					</a>
				</li>	
				<li>
					<a href="pendaftaran/daftar_pasien">
						<img alt="" src="<?=base_url()?>assets/img/icons/medicoicons/medkit.png">
						<span> daftar<br> pasien</span>
					</a>
				</li>	
			</ul>
 		</div>  
		<div class="span7 center" style="padding:10px">                 
			<div id="body-chart">
				
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
	var chart;
	$(document).ready(function() {
		chart = new Highcharts.Chart({
			chart: {
				renderTo: 'body-chart',
				defaultSeriesType: 'line'
			},
			title: {
				text: 'PENDAFTARAN PASIEN'
			},
			subtitle: {
				text: 'Januari 2014'
			},
			xAxis: {
				labels: {
					formatter: function() {
						return this.value; // clean, unformatted number for year
					}
				}
			},
			yAxis: {
				title: {
					text: 'layanan pasien'
				},
				labels: {
					formatter: function() {
						return this.value / 1000;
					}
				}
			},
			tooltip: {
				formatter: function() {
					return this.series.name +'  <b>'+
						Highcharts.numberFormat(this.y / 1000, 0) +'</b><br/>pada tanggal '+ this.x+ ' Oktober';
				}
			},
			plotOptions: {
				area: {
					pointStart: 1,
					marker: {
						enabled: false,
						symbol: 'circle',
						radius: 2,
						states: {
							hover: {
								enabled: true
							}
						}
					}
				}
			},
			series: [
			{
				name: 'pasien baru',
				data: [15915, 17385, 19055, 21205, 23044, 25393, 27935, 30062, 32049,
				33952, 35804, 37431, 39197, 45000, 43000, 41000, 39000, 37000,
				35000, 33000, 31000, 29000, 27000, 25000, 24000, 23000, 22000,
				21000, 20000, 19000, 18000, 18000]
			},{
				name: 'poli gigi',
				data: [
				1005, 1436, 2063, 3057, 4618, 6444, 9822, 15468, 20434, 24126,
				27387, 29459, 31056, 31982, 32040, 31233, 29224, 27342, 26662,
				26956, 27912, 28999, 28965, 27826, 25579, 25722, 24826, 24605,
				24605,23464, 23708,23464]
			},{
				name: 'poli tulang',
				data: [5, 25, 50, 120, 150, 200, 426, 660, 869, 1060, 1605, 2471, 3322,
				4238, 5221, 6129, 7089, 8339, 9399, 10538, 11643, 13092, 14478,
				15915, 17385, 19055, 21205, 23044, 25393, 27935, 30062, 32049]
			}]
		});
	});
</script>