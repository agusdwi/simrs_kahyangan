<script type="text/javascript" src='<?=base_url()?>assets/js/modul/informasi/dr_schedule.js'></script> 

<? 
    $this->load->view('pendaftaran/local/behav');
?>
 
<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.gritter.css" />

<style type="text/css" media="screen">
	.dataTables_filter{  
		display: none;
	}
	.dataTables_scrollBody{
		height: 200px;
	}
	.alert{
		background-color: transparent;
		border: 0px;
	}

	#ffb-input{
		width: 40%;
	}

	#fx_pasien_ctr.ffb{
		width:450px !important;
		top: 28px !important;
	}

	#gritter-notice-wrapper{
		right: 13%;
		top: 100px;
	}
	#fx_pasien_ctr .row .col1{
		float:left;
		/*width:50px;*/
	}
	#fx_pasien_ctr .row .col2{
		float:left;
		margin-left: 10px;
		/*width:200px;*/
	}
	.form-horizontal{
		margin-bottom: 17px;
	}
	#tb_doctor{
		width: 100% !important;
	}
</style>
<div>

	<div id="gritter-notice-wrapper" class="alert hide" style="width:750px;position:fixed">
		<div id="gritter-item-1" class="gritter-item-wrapper" style="margin:0 -17px 5px 0">
			<div class="gritter-top"></div>
			<div class="gritter-item">
				<div class="gritter-close" style="display: none; width:50px "></div>
				<img src="<?=base_url()?>assets/img/demo/envelope.png" class="gritter-image">
				<div class="gritter-with-image" style="width:448px">
					<span class="gritter-title" style="margin-left:36px">Message</span>
					<p>Data Berhasil Disimpan 	</p>
				</div>
				<div style="clear:both"></div>
			</div>
			<div class="gritter-bottom"></div>
		</div>
	</div>

    <div class="pageheader notab">
	    <h1 class="pagetitle">Antrian Pendaftaran Rawat Jalan</h1>
	</div>

	<div class="container-fluid">
	    <div class="row-fluid">
	        <div class="span12">
	            <div class="widget-box" style="margin-top:0px">
	                <div class="widget-title">
	                    <ul class="nav nav-tabs">
	                        <li class="active"><a data-toggle="tab" href="#tab1">Tambah Antrian</a></li>
	                        <!-- <li><a data-toggle="tab" href="#tab2">Data Pengantar</a></li> -->
	                    </ul>
	                </div>
	                
	                <?=form_open(base_url().'pendaftaran/pendaftaran_rawat_jalan/create',array('class' => 'form-horizontal','id' => 'form')); ?>
	                <div class="widget-content tab-content" style="overflow:hidden">
	                <div id="tab1" class="tab-pane active">
	                	<label class="control-label">No.Rekmed</label>
                        <div class="controls">
                            <div style="float:left" id="fx_pasien" name="fx_pasien"></div>
                            <!-- <a id="btn-qrcode" href="#myModal" data-toggle="modal" style="float:left" class="modals btn btn-success btn-mini">
                                scan qrcode
                            </a> -->
                            <div id="myModal" class="modal hide">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button">x</button>
                                    <h3>Scan Qrcode Pasien</h3>
                                </div>
                                <div class="modal-body">

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <label class="control-label">Nama Pasien</label>
                        <div class="controls">
                            <input type="text" readonly="readonly" class="small" id="name" placeholder="Nama Pasien">
                        </div>
                        <label class="control-label">Poli</label>
                        <div class="controls">
                        	<?php
                        	$poli=$this->db->query('select pl_id, pl_name from trx_poly')->result();
								//return print_r($poli);
                        	?>
                            <select id="poli" style="width:140px" onchange="get_data(this.value)">
                                <option value="0" selected="selected">Choose One</option>
                                <?php
								foreach($poli as $poli){
									?>
									<option value="<?=$poli->pl_id?>"><?=$poli->pl_name?></option>	
									<?php
								}
                                ?>
                            </select>
                        </div>
                       <!-- <label class="control-label">Tanggal</label>
                      	<div class="controls" >
                       		<input type="text" style="width:140px;" required name='tanggal' class="datepicker" data-date-format="dd-mm-yyyy">
                        </div> -->
                        <label class="control-label"><b>Pilih Dokter : </b></label><br /><br /><br />
                        <div class="row-fluid">
       <div class='nav-date span12'>
        <div class="span4" style="float:left;">
            <div class='nav left-nav-date'><a class='btn btn-primary' style="color:white;" href='#'></a></div>
        </div>
        <input type='hidden' id='gap' value='1'>
        <div class="span4" style="text-align:center;margin-top:-15px;color:#485b79">
            <b>Dokter Praktek Hari :</b><br>
            <b style="margin-top:100px;font-size: 14pt;border-bottom:2px solid #e4b559;padding:5px 20px 5px;" id="tglskrg">
            	<script>
              document.write(getDates(0));
              </script>
            </b>
        </div>
        <div class="span4" style="float:right;">
            <div class='nav right-nav-date'><a class='pull-right btn btn-primary' style="color:white;" href='#'></a></div>
        </div>
        </div> 
    </div>
                        <table id="tb_doctor" class="table table-bordered def_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:85%; ">
							<thead>
								<tr role="row">
									<!--<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:10%" aria-label="Nama: activate to sort column ascending">ID Dokter</th>-->
									<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:30%" aria-label="Harga: activate to sort column ascending">Nama Dokter</th>
									<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:20%" aria-label="Aksi: activate to sort column ascending">Poli</th>
									<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:20%" aria-label="Aksi: activate to sort column ascending">Jam</th>
									<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:10%" aria-label="Antrian: activate to sort column ascending">Antrian</th>
								</tr>
							</thead>
							<tbody>
						
							</tbody>
						</table>
	                </div>
	                <div id="tab2" class="tab-pane">
	                	<div class="title">
                                    <h3>Data Pengantar</h3>
                                </div><br clear="">
                                <label class="control-label">Nama Pengantar</label>
                                <div class="controls">
                                    <input type="text" class="medium"  placeholder="Nama Pengantar" name="agent_name" id="agent_name">
                                </div>
                                <label class="control-label">Hubungan Keluarga</label>
                                <div class="controls">
                                    <table>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="agent_relationship" value="orang tua" /> Orang Tua</label></td>
                                            <td style="width:100px"><label><input type="radio" name="agent_relationship" value="saudara" /> Saudara</label></td>
                                            <td style="width:100px"><label><input type="radio" name="agent_relationship" value="teman"/> Teman</label></td>
                                            <td style="width:100px"><label><input type="radio" name="agent_relationship" value="pengantar"/> Pengantar</label></td>
                                        </tr>
                                    </table>
                                </div>
                                <label class="control-label">Jenis Kelamin</label>
                                <div class="controls">
                                    <table>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="agent_sex" value="l" /> Laki-laki</label></td>
                                            <td style="width:100px"><label><input type="radio" name="agent_sex" value="p" /> Perempuan</label></td>
                                        </tr>
                                    </table>
                                </div>
                                <label class="control-label">Alamat</label>
                                <div class="controls">
                                    <textarea rows="3" class="medium" cols="2" name="agent_address"></textarea>
                                </div>
                                <label class="control-label">No.Telp</label>
                                <div class="controls">
                                    <input type="text" class="medium" placeholder="No.Telp" name="agent_telp">
                                </div>
	                	</div>
	                </div>
	                <div class="form-actions" style="margin-top: 12px;margin-bottom: -17px;">
	                    <button type="submit" style="margin-left:90%" class="btn btn-primary">Simpan</button>
	                </div> 
	                <?=form_close()?>                           
	            </div>
	        </div>
	    </div>
	</div>
</div>

<script type="text/javascript" charset="utf-8">
    $(function(){
        $("#fx_pasien_input").focus();
    	get_data();
    	now = new Date();
        $('.datepicker').datepicker({
           minDate: 0,
           dateFormat: "yy-mm-dd"
        });
    	$("#form").validate({
			rules: {
				'fx_pasien': "required"
			}
		})

    	$("#form").submit(function(){
    		if($("#name").val() == ""){
    			alert('nama pasien harus di isi');
    			$("#fx_pasien_input").focus();
    		}else{
    			var selected = $("#tb_doctor tr.active").length;
    			var poli = false;
    			if (selected > 0) {
    				poli = true;
    			}else{
    				if ($("#poli").val() == 0)
    					poli = false
    				else poli = true;
    			}
    			if (poli){
    				var url  = $(this).attr('action');
    				var post = $(this).serialize();
                    if(selected > 0){
                        poli_id = $('table#tb_doctor tr.active td:eq(1)').attr('poli_id');
                        dr_id = $('table#tb_doctor tr.active td:eq(0)').attr('dr_id');
                        sch_id = $('table#tb_doctor tr.active td:eq(2)').attr('sch_id');
                    }else{
                        poli_id = $("#poli").val();    
                        dr_id = '';
                        sch_id = '';
                    } 
    				var next = false;
    				post += '&dr_id='+dr_id+'&sch_id='+sch_id+'&poli_id='+poli_id;
    				if($('#agent_name').val() !== ''){
    					next = true;
    				}else{
    					// a = confirm('Form data pengantar masih kosong, lanjut?');
    					// if(a){
    						next = true;
    					// }
    				}
    				if(next){
    					$.post(url,post, function(data){   
                            if(data == true){
                                $("#fx_pasien_input").val('');
                                $("#name").val('');
                                get_data();
                                $(".alert").fadeIn().delay(1000).fadeOut(function(){
                                    $("#field2").fadeOut(function(){
                                        $("#field1").fadeIn("fast");
                                    });
                                });
                            }else{
                                alert('pasien masih dalam antrian / proses \nsilahkan selesaikan terlebih dahulu');
                            }
    					},"json");
    				}
    			}else{
    				alert('silahkan memilih poli');
    			}
    		}
            return false;
        })
		// temporary added by agus
		$("#fx_pasien_input").val('<?=$no_rekmed?>');
		$("#name").val('<?=$name?>');
        $('body').on('click','#btn-qrcode',function (e) {
            $(".modal-body").load(CUR+'get_qr');
            return false;
        });
    })
	function get_data(){
		poli_id = $("#poli").val();
		$('#tb_doctor tbody').html('');
		var uri = "<?=base_url()?>pendaftaran/pendaftaran_rawat_jalan/dt_doctor/"+poli_id+"/"+param_date;
		$.get(uri, function(data){
			$(data).each(function(d){
				html = "<tr><td dr_id='"+data[d].dr_id+"'>"+data[d].dr_name+"</td><td poli_id='"+data[d].pl_id+"'>"+data[d].pl_name+"</td><td sch_id='"+data[d].sch_id+"' class='center'>"+get_time(data[d].sch_time_start)+" - "+get_time(data[d].sch_time_end)+"</td><td>"+data[d].antrian+"</td></tr>"
				$('#tb_doctor tbody').append(html);
				$(".tb_scrol tbody tr").click(function(){
		         	$(".tb_scrol tbody tr.active").removeClass('active');
					$(this).addClass('active');
					//var slt = $(this).attr('id');
					//$("#poli").val(slt);
				})
			})
		},'json');
	}

	function callback(){
		$("#name").val($("#fx_pasien_hidden").val());
	}

	function get_time(dt){
		return dt.substring(10,16);
	}
</script>