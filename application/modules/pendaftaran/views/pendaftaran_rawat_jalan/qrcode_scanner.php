<div style="width: 350px; height: 350px;margin:0 auto" id="qrcodebox"></div>
<br>
<center>
	<input type="button" value="Start" id="btn_start" /> 
	<input type="button" value="Stop" id="btn_stop" />
</center>

<script type="text/javascript">
	(function($){
		$('document').ready(function(){
			$('#qrcodebox').WebcamQRCode({
				path : '../assets/',
				onQRCodeDecode: function( p_data ){
					$('#qrcode_result').html( p_data );
					$('.modal-header .close').trigger('click');
					$("#fx_pasien_input").val(p_data)
					$("#fx_pasien_input").trigger('change');
					$("#fx_pasien_input").focus();
				}
			});
			
			$('#btn_start').click(function(){
				$('#qrcodebox').WebcamQRCode().start();
			});
			
			$('#btn_stop').click(function(){
				$('#qrcodebox').WebcamQRCode().stop();
			});
		});
	})(jQuery);
</script>