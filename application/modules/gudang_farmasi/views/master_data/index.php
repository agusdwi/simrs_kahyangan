<style type="text/css" media="screen">
	.frm_search input{margin-top:-1px;float:right;margin-left:2px}
	.frm_search .btn{float:right;margin-left:5px}
	.frm_search .btn span{padding:2px 10px;}
	.search_choice a{font-weight:bold}
	.search_choice a.active{color:#fb9338}
	.search_choice {float:right;margin-right:95px}
	#advance{display:none}
	#filter{border-bottom: 1px dashed #DDD;}
	#body_search{margin-top:10px}
	.tname{font-size:110%}
	.dyntable tbody tr td{border-right:none}
	.dyntable tbody tr td + td + td{border-right:1px solid #DDD}
	.dyntable tr:nth-child(even){
		background:#F7F7F7;
	}
	.dataTables_scrollHead{
		margin-bottom: -22px;
	}
	.dataTables_info{
		margin-top: 20px;
	}
</style>
<script type="text/javascript" charset="utf-8">
	$(function(){
		$(".search_choice a").click(function(){
			$(".active").removeClass('active');
			$(this).addClass('active');
			if($(this).attr('atr') == 'bsc'){
				$("#advance").hide();
				$("#basic").show();
				$(".mediuminput").focus();
			}else{
				$("#advance").show();
				$("#basic").hide();
				$(".smallinput").focus();
			}
			return false;
		})

		$('.dyntable').dataTable( {
			"sPaginationType": "bootstrap",
			"sScrollY": "350px",
			  "bFilter": false,
			"bPaginate": false,
	    });
	})
</script>

<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>

<div id="tabs">
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12">
				<div class="widget-box" style="margin-top:0px">
					<div class="widget-title">
	                    <ul class="nav nav-tabs">
	                        <li class="active"><a data-toggle="tab" href="#tab1">Tipe Item</a></li>
	                        <li><a data-toggle="tab" href="#tab2">Master Item</a></li>
	                        <li><a data-toggle="tab" href="#tab3">Supplier</a></li>
	                        <!-- <li><a data-toggle="tab" href="#tab4">Master Pos</a></li> -->
	                    </ul>
	                </div>
	                <div class="widget-content tab-content" style="overflow:hidden">
	                	<div id="tab1" class="tab-pane active">
	                		<div class="row-fluid">
	                			<div class="span7">
	                				<div class="row-fluid">
										<div class="span5" >
											<div class="title"><h3>Tipe Item</h3></div>
										</div>
									</div>
									<div class="widgetbox">
										<div class="span12">
											<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered def_table_y dataTable tb_scrol dyntable" id="tbItem">
											    <!-- <thead>
											        <tr>
											            <th>Tipe Id</th>
											            <th>Nama</th>
											            <th width="60px">Action</th>
											        </tr>
											    </thead> -->
											    <tbody>
											    	<tr>
											            <th>Tipe Id</th>
											            <th>Nama</th>
											            <!-- <th width="60px">Action</th> -->
											        </tr>
													<?$i=0;foreach ($type->result() as $t):$i++;?>
														<tr>
												            <td style="width:60px"><?=$t->it_id?></td>
															<td><?=$t->it_name?></td>
												            <!-- <td style="text-align:center;">
												            	<a href="" class="btn btn_info"><span>Aksi</span></a>
												            </td> -->
												        </tr>
													<?endforeach?>
											    </tbody>
											</table>
										</div>
									</div>
	                			</div>
	                			<div class="span5">
	                				<div class="widget-box">
										<div class="widget-title">
											<span class="icon">
												<i class="icon-align-justify"></i>									
											</span>
											<h5>Tambah Tipe Item</h5>
										</div>
										<div class="widget-content nopadding">
											<?=form_open(cur_url().'add_type',array('class' => 'form-horizontal','id' => 'form_tipe')); ?>
												<div class="control-group">
													<label class="control-label">Nama Tipe</label>
													<div class="controls">
														<input type="text" placeholder="Nama Tipe" class="medium" id="it_name" name="ds[it_name]" >
													</div>
												</div>
												<div class="form-actions">
													<a href="" style="margin-left:-65%" class="btn">Back</a>
													<button style="margin-left:85%" id="reset" class="btn btn-warning" type="reset">reset</button>
													<button type="submit" class="btn btn-primary">Save</button>
												</div>
											<?=form_close()?>
										</div>
									</div>
	                			</div>
	                		</div>
	                	</div>
	                	<div id="tab2" class="tab-pane">
	                		<div class="row-fluid">
	                			<div class="span7">
	                				<div class="row-fluid">
										<div class="span5" >
											<div class="title"><h3>Master Item</h3></div>
										</div>
									</div>
									<div class="widgetbox">
										<div class="span12">
											<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered def_table_y dataTable tb_scrol dyntable" id="tbMaster">
											    <!-- <thead>
											        <tr>
											            <th>Kode Item</th>
											            <th>Nama</th>
											            <th width="60px">Action</th>
											        </tr>
											    </thead> -->
											    <tbody>
											    	<tr>
											            <th>Kode Item</th>
											            <th>Nama</th>
											            <!-- <th width="60px">Action</th> -->
											        </tr>
													<?$i=0;foreach ($master->result() as $t):$i++;?>
														<tr>
												            <td style="width:60px"><?=$t->im_id?></td>
															<td><?=$t->im_name?></td>
												           <!--  <td style="text-align:center;">
												            	<a href="" class="btn btn_info"><span>Aksi</span></a>
												            </td> -->
												        </tr>
													<?endforeach?>
											    </tbody>
											</table>
										</div>
									</div>
	                			</div>
	                			<div class="span5">
	                				<div class="widget-box">
										<div class="widget-title">
											<span class="icon">
												<i class="icon-align-justify"></i>									
											</span>
											<h5>Tambah Data Item Master</h5>
										</div>
										<div class="widget-content nopadding">
											<?=form_open(cur_url().'add_master',array('class' => 'form-horizontal','id' => 'form_master')); ?>
												<div class="control-group">
													<label class="control-label">Kode Master *</label>
													<div class="controls">
														<input type="text" placeholder="Kode Item" class="medium" id="im_id" name="ds[im_id]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Type *</label>
													<div class="controls">
														<select style="width:140px"  name="ds[im_type]">
						                                    <?foreach ($type->result() as $t): ?>
						                                        <option value="<?=$t->it_id?>"><?=$t->it_name?></option>
						                                    <?endforeach ?>
						                                </select>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Unit *</label>
													<div class="controls">
														<select style="width:140px"  name="ds[im_unit]">
						                                    <option value="ampul">ampul</option>
						                                    <option value="falcon">falcon</option>
						                                    <option value="fles">fles</option>
						                                    <option value="bungkus">bungkus</option>
						                                    <option value="botol">botol</option>
						                                </select>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Nama *</label>
													<div class="controls">
														<input type="text" placeholder="Nama Master" class="medium" id="im_name" name="ds[im_name]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Barcode *</label>
													<div class="controls">
														<input type="text" placeholder="Barcode" class="medium" id="im_barcode" name="ds[im_barcode]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Mata Uang *</label>
													<div class="controls">
														<select style="width:140px"  name="ds[im_currency_id]">
						                                    <?foreach ($currency->result() as $c): ?>
						                                        <option value="<?=$c->mc_id?>"><?=$c->mc_name?></option>
						                                    <?endforeach ?>
						                                </select>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Harga Beli *</label>
													<div class="controls">
														<input type="text" placeholder="Harga Beli" class="medium" id="im_item_price_buy" name="ds[im_item_price_buy]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Harga Jual *</label>
													<div class="controls">
														<input type="text" placeholder="Harga Jual" class="medium" id="im_item_price" name="ds[im_item_price]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Vat Status *</label>
													<div class="controls">
														<select style="width:140px"  name="ds[im_vat_status]">
						                                    <option value="no vat">No Vat</option>
						                                    <option value="include">Include</option>
						                                    <option value="exclude">Exclude</option>
						                                </select>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">PPn *</label>
													<div class="controls">
														<input type="text" placeholder="PPn" class="medium" id="im_ppn" name="ds[im_ppn]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Reorder Point *</label>
													<div class="controls">
														<input type="text" placeholder="Reorder Point" class="medium" id="im_reorder_point" name="ds[im_reorder_point]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Min Stok *</label>
													<div class="controls">
														<input type="text" placeholder="Min Stok" class="medium" id="im_min_stock" name="ds[im_min_stock]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Max Stok *</label>
													<div class="controls">
														<input type="text" placeholder="Max Stok" class="medium" id="im_max_stock" name="ds[im_max_stock]" >
													</div>
												</div>
												<div class="form-actions">
													<a href="" style="margin-left:-65%" class="btn">Back</a>
													<button style="margin-left:85%" class="btn btn-warning reset" type="reset">reset</button>
													<button type="submit" class="btn btn-primary">Save</button>
												</div>
											<?=form_close()?>
										</div>
									</div>
	                			</div>
	                		</div>
	                	</div>
	                	<div id="tab3" class="tab-pane">
	                		<div class="row-fluid">
	                			<div class="span7">
	                				<div class="row-fluid">
										<div class="span5" >
											<div class="title"><h3>Master Supplier</h3></div>
										</div>
									</div>
									<div class="widgetbox">
										<div class="span12">
											<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered def_table_y dataTable tb_scrol dyntable" id="tbSupplier">
											    <!-- <thead>
											        <tr>
											            <th style="width:60px">Kode Supplier</th>
											            <th>Nama Supplier</th>
											            <th>Alamat</th>
											            <th width="60px">Action</th>
											        </tr>
											    </thead> -->
											    <tbody>
											    	<tr>
											            <th style="width:100px">Kode Supplier</th>
											            <th>Nama Supplier</th>
											            <th>Alamat</th>
											            <!-- <th width="60px">Action</th> -->
											        </tr>
													<?$i=0;foreach ($supplier->result() as $s):$i++;?>
														<tr>
												            <td style="width:60px"><?=$s->msup_id?></td>
															<td><?=$s->msup_name?></td>
															<td><?=$s->msup_address?></td>
												            <!-- <td style="text-align:center;">
												            	<a href="" class="btn btn_info"><span>Aksi</span></a>
												            </td> -->
												        </tr>
													<?endforeach?>
											    </tbody>
											</table>
										</div>
									</div>
	                			</div>
	                			<div class="span5">
	                				<div class="widget-box">
										<div class="widget-title">
											<span class="icon">
												<i class="icon-align-justify"></i>									
											</span>
											<h5>Tambah Data Supplier</h5>
										</div>
										<div class="widget-content nopadding">
											<?=form_open(cur_url().'add_supplier',array('class' => 'form-horizontal','id' => 'form_supplier')); ?>
												<div class="control-group">
													<label class="control-label">Kode Supplier *</label>
													<div class="controls">
														<input type="text" placeholder="Kode Supplier" class="medium" id="msup_id" name="ds[msup_id]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Nama Supplier *</label>
													<div class="controls">
														<input type="text" placeholder="Nama Supplier" class="medium" id="msup_name" name="ds[msup_name]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Alamat Supplier *</label>
													<div class="controls">
														<textarea class="medium" id="msup_address" name="ds[msup_address]"></textarea>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Provinsi *</label>
													<div class="controls">
														<select style="width:140px"  name="ds[msup_province]">
						                                    <?foreach ($provinsi->result() as $p): ?>
						                                        <option value="<?=$p->mpr_name?>"><?=$p->mpr_name?></option>
						                                    <?endforeach ?>
						                                </select>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Kab/Kota *</label>
													<div class="controls">
														<select style="width:140px"  name="ds[msup_province]">
						                                    <?foreach ($regency->result() as $r): ?>
						                                        <option value="<?=$r->mre_name?>"><?=$r->mre_name?></option>
						                                    <?endforeach ?>
						                                </select>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Kode Pos *</label>
													<div class="controls">
														<input type="text" placeholder="Kode Pos" class="medium" id="msup_zip_code" name="ds[msup_zip_code]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Telephone *</label>
													<div class="controls">
														<input type="text" placeholder="Telephone" class="medium" id="msup_telp" name="ds[msup_telp]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Fax *</label>
													<div class="controls">
														<input type="text" placeholder="Fax" class="medium" id="msup_fax" name="ds[msup_fax]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Email *</label>
													<div class="controls">
														<input type="text" placeholder="Email" class="medium" id="msup_email" name="ds[msup_email]" >
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">NPWP *</label>
													<div class="controls">
														<input type="text" placeholder="NPWP" class="medium" id="msup_npwp" name="ds[msup_npwp]" >
													</div>
												</div>
												<div class="form-actions">
													<a href="" style="margin-left:-65%" class="btn">Back</a>
													<button style="margin-left:85%" class="btn btn-warning reset" type="reset">reset</button>
													<button type="submit" class="btn btn-primary">Save</button>
												</div>
											<?=form_close()?>
										</div>
									</div>
	                			</div>
	                		</div>
	                	</div>
	                	<div id="tab4" class="tab-pane">
	                		knasfasjfasf
	                	</div>
	                </div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("#form_tipe").validate({
            rules: {
                'ds[it_name]': "required",   
            },
            submitHandler: function(form) {
                var url  = "<?=base_url()?>gudang_farmasi/master_data/add_type";
                var data = jQuery(form).serialize();
                $.post(url,data, function(data){
                    $("#it_name").val("");
            		location.reload();
                }); 
                return false;
            }
        });
        $("#form_master").submit(function(){
            var url  = $(this).attr('action');
            var data = $(this).serialize();
            $.post(url,data, function(data){
            	$(".reset").trigger('click');
            	location.reload();
            }); 
            return false;
        }) 
        $("#form_supplier").submit(function(){
            var url  = $(this).attr('action');
            var data = $(this).serialize();
            $.post(url,data, function(data){
            	$(".reset").trigger('click');
            	location.reload();
            }); 
            return false;
        }) 
	})
</script>