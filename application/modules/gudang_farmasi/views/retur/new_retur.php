<script>
	var supplier = {};
		supplier.results = [
			{id:'1',name:'PT Jaya Banda'},
			{id:'2',name:'Stationary Budaya'},
			{id:'3',name:'Dental 2012'},
			{id:'4',name:'Dental Jaya'},
			{id:'5',name:'PT Tulang Bersinar'},
			{id:'6',name:'PT Tulang Kering'},
		];
		supplier.total = supplier.results.length;
	var petugas = {};
		petugas.results = [
			{id:'1',name:'Aya Suahaya'},
			{id:'2',name:'Budi Darmawan'},
			{id:'3',name:'Lilik Sukaesih'},
			{id:'4',name:'Darmadi Muhammad Samudra'},
			{id:'5',name:'Joko Widodo'},
			{id:'6',name:'Alek'},
		];
		petugas.total = petugas.results.length;
	$(function(){
		$('.supplier').flexbox(supplier);
		$('.petugas').flexbox(petugas);
		$('.dyntable').dataTable( {
			"sPaginationType": "bootstrap",
			"sScrollY": "300px",
			  "bFilter": false,
			"bPaginate": false,
	    });
	})
</script>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="pageheader notab">
		    <h1 class="pagetitle"><?=$title?></h1>
		</div>
	</div>
	<div class='row-fluid' style="padding:10px">
    	<div class="widget-content nopadding">
    		<div class="span5" style="margin-left:1%">
				<table>
					<tr>
						<td><label>Nomor Order</label></td><td><input type="text" name="order_number"></td>
					</tr>
					<tr>
						<td><label>Invoice</label></td><td><input type="text" name="invoice"></td>
					</tr>
					<tr>
						<td><label>Supplier</label></td><td><div id="supplier" class="supplier"></div></td>
					</tr>
					<tr>
						<td><label>Tanggal Penerimaan</label></td><td><input type="text" name="order_date" placeholder="dd/mm/yyyy"></td>
					</tr>
					<tr>
						<td><label>Mata Uang</label></td><td><select name="order_curr"><option value="IDR">RP</option></select></td>
					</tr>
					<tr>
						<td><label>Vat Status</label></td><td><select name="order_vat"><option>No Vat</option></select></td>
					</tr>
					<tr>
						<td><label>Ppn</label></td><td><select name="order_vat"><option>No Vat</option></select></td>
					</tr>
					<tr>
						<td><label>Petugas</label></td><td><div id="petugas" class="petugas"></div></td>
					</tr>
					<tr>
						<td><label>Note</label></td><td><textarea name="note"></textarea></td>
					</tr>
				</table>
			</div><!-- #span4 -->
	    	<div class="span6" style="margin-left:0px">
				<style>
					.dyntable input{
						margin:0px;
					}
				</style>
				Select The Order <br/>
				<table class="table table-bordered table-striped dyntable" id="table1">
					<thead>
						<tr>
							<th>No Order</th>
							<th>Invoice</th>
							<th>Tgl Order</th>
						</tr>
					</thead>
					<tbody>
						<?for($i=1;$i<=7;$i++):?>
							<tr>
								<td>ORD-<?=rand(10000,220000)?></td>
								<td>IVC-<?=rand(10000,220000)?></td>
								<td><i>Selasa, 23 Oktober 2012</i></td>
							</tr>
						<?endfor;?>
					</tbody>
				</table>
			</div><!-- #span7-->
		</div>
    </div>
				<div class='row-fluid'>
					<div class='form-actions span10' style="text-align:right;margin-top:20px">
						<button class="btn btn-warning">Batal</button>
						<button class="btn btn-primary">Simpan</button>
					</div>
				</div>
</div>