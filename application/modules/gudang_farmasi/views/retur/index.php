<div class="pageheader notab">
    <h1 class="pagetitle">Tambah <?=$title?></h1>
</div>

<div id="field1">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-align-justify"></i>                                  
                    </span>
                    <h5>Langkah 1 : Masukkan Supplier</h5>
                </div>
                <div class="widget-content nopadding">
                    <?=form_open(cur_url().'create_tipe',array('class' => 'form-horizontal','id' => 'form_tipe')); ?>
                        <div class="container-fluid">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                    <label class="control-label">No Order *</label>
                                    <div class="controls">
                                        <input type="text" style="width:45%" id="ipr_id" readonly="readonly" name="ds[ipr_id]" >
                                    </div>
                                    <label class="control-label">Invoice *</label>
                                    <div class="controls">
                                        <input type="text" class="medium" id="ipr_date" name="ds[ipr_date]" >
                                    </div>
                                    <label class="control-label">Supplier *</label>
                                    <div class="controls">
                                        <input type="text" class="medium" id="ipr_date" name="ds[ipr_date]" >
                                    </div>
                                    <label class="control-label">Tanggal Penerimaan *</label>
                                    <div class="controls">
                                        <input type="text" placeholder="Petugas" class="medium" id="ipr_operator" name="ds[ipr_operator]">
                                    </div>
                                    <label class="control-label">Mata Uang *</label>
                                    <div class="controls">
                                        <select id="poli" style="width:140px">
                                            <option value="" >Choose One</option>
                                            <option value="gigi">Rupiah</option>
                                            <option value="tulang">Dollar</option>
                                        </select>
                                    </div>
                                    <label class="control-label">Vat Status *</label>
                                    <div class="controls">
                                        <select id="poli" style="width:140px">
                                            <option value="" >Choose One</option>
                                            <option value="gigi">No VAT</option>
                                            <option value="tulang">Include</option>
                                            <option value="tulang">Exclude</option>
                                        </select>
                                    </div>
                                    <label class="control-label">PPN *</label>
                                    <div class="controls">
                                        <input type="text" placeholder="Petugas" class="medium" id="ipr_operator" name="ds[ipr_operator]">
                                    </div>
                                    <label class="control-label">Petugas *</label>
                                    <div class="controls">
                                        <input type="text" placeholder="Petugas" class="medium" id="ipr_operator" name="ds[ipr_operator]">
                                    </div>
                                    <label class="control-label">Catatan</label>
                                    <div class="controls">
                                        <textarea  class="medium" style="width:40%" id="ipr_note" name="ds['ipr_note']"></textarea>
                                    </div>
                                </div>
                                </div>
                                <div class="span6">
                                    <div class="widget-box">
                                    <div class="title" style="margin-bottom:0px"><h3>Order Aktif</h3></div>
                                        <div class="widget-content" style="border:0">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>No Order</th>
                                                        <th>Invoice</th>
                                                        <th>Tanggal Order</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Row 1</td>
                                                        <td>Row 2</td>
                                                        <td>Row 3</td>
                                                    </tr>
                                                </tbody>
                                            </table>                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button style="margin-left:85%" id="reset" class="btn btn-warning" type="reset">Reset</button>
                            <a id="next" class="btn btn-primary">Lanjut</a>
                        </div>
                    </form>
                </div>
            </div>                      
        </div>
    </div>
</div>
</div>

<div id="field2" class="hide">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-align-justify"></i>                                  
                    </span>
                    <h5 style="width:94%"><span>Langkah 2 : Masukkan Detail Pemesanan</span><span style="margin-left:60%">0081240 | 01-12-2012</span></h5>
                </div>
                    <?=form_open(cur_url().'create_tipe',array('class' => 'form-horizontal','id' => 'form_tipe')); ?>
                        <div class="frm_trx" style="width:360px;padding:10px;height:155px;margin:0 auto;margin-top:20px;margin-bottom:20px;border:1px dashed #969696;color:#AAA">
                            <div class="section">
                                <label style="width:100px;margin-top:12px;padding-left:20px;float:left;">Item *</label>
                                <div>
                                    <div style="float:left;margin-top:8px " id="fx_item"></div>
                                    <input type="text" style="width:180px" class="medium" id="ipr_id" name="ds[ipr_id]">
                                    <button style="margin: 8px -3px;" id="lookup" class="btn btn-success"><i class="icon-search"></i></button> 
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <label class="control-label" style="margin-left:-15%">Harga Satuan *</label>
                            <div class="controls">
                                <input type="text" name="" style="width:120px;margin-left:-55px" id="qty" autocomplete="off">
                            </div>
                            <div class="section">
                                <label style="width:100px;margin-top: 12px;margin-left:20px;float:left">Jumlah</label>
                                <div>
                                    <input type="text" name="" style="width:50px;margin-top:6px" id="qty" autocomplete="off">
                                </div>
                            </div>
                            <div class="section">
                                <label style="width:100px;margin-top: 12px;float:left"></label>
                                <div>
                                    <a class="btn" href="<?cur_url(-1);?>">clear</a>
                                    <a class="btn btn-info" id="tbh">tambah</a>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box">
                            <div class="widget-content" style="border:0">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Pos</th>
                                            <th>Jumlah</th>
                                            <th style="width:75px">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Row 1</td>
                                            <td>Row 2</td>
                                            <td>Row 3</td>
                                            <td>Row 4</td>
                                        </tr>
                                    </tbody>
                                </table> 
                            </br>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td style="width:70%">&nbsp;</td>
                                            <td style="width:10%">Sub Total</td>
                                            <td>120000</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>PPn</td>
                                            <td>202353</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Grand Total</td>
                                            <td>109235</td>
                                        </tr>
                                    </tbody>
                                </table>                            
                            </div>
                        </div>
                        <div class="form-actions">
                            <button href="javascript: history.go(-1)" style="margin-left:-60px" class="btn" id="back">Kembali</button>
                            <button style="margin-left:80%" id="reset" class="btn btn-warning" type="reset">Reset</button>
                            <button type="submit" class="btn btn-primary">Lanjut</button>
                        </div>
                    </form>
            </div>                      
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(function(){
        $("#next").click(function(){
            $("#field1").fadeOut(function(){
                $("#field2").fadeIn();
            })
            return false;
        })
        $("#back").click(function(){
            $("#field2").fadeOut(function(){
                $("#field1").fadeIn();
            })
            return false;
        })

    })
</script>