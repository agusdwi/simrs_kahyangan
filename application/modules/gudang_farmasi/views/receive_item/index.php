<? 
    $this->load->view('gudang_farmasi/local/behav');
?>

<style type="text/css" media="screen">
    

    #fx_suppliers_ctr.ffb{
        width:320px !important;
        top: 28px !important;
    }

    #fx_suppliers_ctr .row .col1{
        float:left;
        /*width:50px;*/
    }
    #fx_suppliers_ctr .row .col2{
        float:left;
        margin-left: 15px;
        width:100px;
    }
     #fx_suppliers_ctr .row .col3{
        float:left;
        margin-left: 15px;
        /*width:150px;*/
    }
    #fx_item.ffb{
        width:320px !important;
        top: 28px !important;
    }

    #fx_item_ctr .row .col1{
        float:left;
        /*width:50px;*/
    }
    #fx_item_ctr .row .col2{
        float:left;
        margin-left: 15px;
        width:100px;
    }
</style>
<script type="text/javascript">
    $(function(){
        $('.datepicker').datepicker({
            minDate: '0',
        });

    });
    </script>

<div class="pageheader notab">
    <h1 class="pagetitle">Tambah <?=$title?></h1>
</div>

<div id="field1">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-align-justify"></i>                                  
                    </span>
                    <h5>Langkah 1 : Masukkan Supplier</h5>
                </div>
                <div class="widget-content nopadding">
                    <?=form_open(cur_url().'add_recive',array('class' => 'form-horizontal','id' => 'form_sup')); ?>
                        <div class="container-fluid">
                            <div class="row-fluid">
                                <div class="span6">
                                    <div class="control-group">
                                    <label class="control-label">No Order *</label>
                                    <div class="controls">
                                        <input type="text" style="width:45%" id="iri_id" readonly="readonly" name="ds[iri_id]" value="<?=$id?>" >
                                    </div>
                                    <label class="control-label">Invoice *</label>
                                    <div class="controls">
                                        <input type="text" class="medium" id="iri_invoice" name="ds[iri_invoice]" value="<?=$invoice?>" >
                                    </div>
                                    <label class="control-label">Supplier *</label>
                                    <div class="controls">
                                        <div id="fx_suppliers" name="supplier"></div>
                                    </div>
                                    <label class="control-label">Tanggal Penerimaan *</label>
                                    <div class="controls">
                                        <input type="text" class="medium datepicker" id="iri_date" name="ds[iri_date]" data-date-format="dd-mm-yyyy">
                                    </div>
                                    <label class="control-label">Mata Uang *</label>
                                    <div class="controls">
                                        <select id="iri_currency" name="ds[iri_currency]" style="width:140px">
                                            <?foreach ($currency->result() as $c): ?>
                                                <option name="iri_currency" value="<?=$c->mc_id?>"><?=$c->mc_name?></option>
                                            <?endforeach ?>
                                        </select>
                                    </div>
                                    <label class="control-label">Vat Status *</label>
                                    <div class="controls">
                                        <select id="iri_vat" name="ds[iri_vat]" style="width:140px">
                                            <option value="" >Choose One</option>
                                            <option value="No vat">No VAT</option>
                                            <option value="Include">Include</option>
                                            <option value="Exclude">Exclude</option>
                                        </select>
                                    </div>
                                    <label class="control-label">PPN *</label>
                                    <div class="controls">
                                        <input type="text" placeholder="PPn" class="medium" id="iri_ppn" name="ds[iri_ppn]" value="0.00">
                                    </div>
                                    <label class="control-label">Petugas *</label>
                                    <div class="controls">
                                        <input type="text" placeholder="Petugas" class="medium" value="<?=get_user('username')?>" id="iri_operator" name="ds[iri_operator]">
                                    </div>
                                    <label class="control-label">Catatan</label>
                                    <div class="controls">
                                        <textarea  class="medium" style="width:80%" id="iri_note" name="ds[iri_note]"></textarea>
                                    </div>
                                </div>
                                </div>
                                <div class="span6">
                                    <div class="widget-box">
                                    <div class="title" style="margin-bottom:0px"><h3>Order Aktif</h3></div>
                                        <div class="widget-content" style="border:0">
                                            <table class="table table-bordered table-striped" id="tbOrder">
                                                <thead>
                                                    <tr>
                                                        <th>No Order</th>
                                                        <th>Invoice</th>
                                                        <th>Tanggal Order</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button style="margin-left:85%" id="reset" class="btn btn-warning" type="reset">Reset</button>
                            <a id="next" class="btn btn-primary">Lanjut</a>
                        </div>
                    </form>
                </div>
            </div>                      
        </div>
    </div>
</div>
</div>

<div id="field2" class="hide">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-align-justify"></i>                                  
                    </span>
                    <h5 style="width:94%"><span>No Invoice : <?=$invoice?></span><span style="margin-left:60%"><a id="add" style="float:right;margin-top:-9px" class="btn btn-success"><i class="icon-plus-sign icon-white"></i>Tambah</a></span></h5>
                </div>
                    <?=form_open(cur_url().'create_tipe',array('class' => 'form-horizontal','id' => 'form_tipe')); ?>
                        <div class="widget-box">
                            <div class="widget-content" style="border:0">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Pos</th>
                                            <th>Jumlah</th>
                                            <th style="width:75px">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>012209</td>
                                            <td>Apotek</td>
                                            <td>3</td>
                                            <td>Aksi</td>
                                        </tr>
                                    </tbody>
                                </table> 
                            </br>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td style="width:70%">&nbsp;</td>
                                            <td style="width:10%">Sub Total</td>
                                            <td>120000</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>PPn</td>
                                            <td>202353</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Grand Total</td>
                                            <td>109235</td>
                                        </tr>
                                    </tbody>
                                </table>                            
                            </div>
                        </div>
                        <div class="form-actions">
                            <button href="javascript: history.go(-1)" style="margin-left:-60px" class="btn" id="back">Kembali</button>
                            <button style="margin-left:80%" id="reset" class="btn btn-warning" type="reset">Reset</button>
                            <button type="submit" class="btn btn-primary">Lanjut</button>
                        </div>
                    </form>
            </div>                      
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(function(){
        $("#next").click(function(){
            $("#field1").fadeOut(function(){
                $("#field2").fadeIn();
            })
            return false;
        })
        $("#back").click(function(){
            $("#field2").fadeOut(function(){
                $("#field1").fadeIn();
            })
            return false;
        })

    })
</script>