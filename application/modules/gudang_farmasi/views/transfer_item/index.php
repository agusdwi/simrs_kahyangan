<!--untuk modul gudang farmasi lihat di laporan ta agus, di dropbox dengan nama 'dokumen gudang farmasi.pdf'-->
<style type="text/css" media="screen">
	.frm_search input{margin-top:-1px;float:right;margin-left:2px}
	.frm_search .btn{float:right;margin-left:5px}
	.frm_search .btn span{padding:2px 10px;}
	.search_choice a{font-weight:bold}
	.search_choice a.active{color:#fb9338}
	.search_choice {float:right;margin-right:95px}
	#advance{display:none}
	#filter{border-bottom: 1px dashed #DDD;}
	#body_search{margin-top:10px}
	.tname{font-size:110%}
	#dyntable tbody tr td{border-right:none}
	#dyntable tbody tr td + td + td{border-right:1px solid #DDD}
	#dyntable tr:nth-child(even){
		background:#F7F7F7;
	}
	.dataTables_scrollHead{
		margin-bottom: -22px;
	}
	.dataTables_info{
		margin-top: 20px;
	}
</style>
<script type="text/javascript" charset="utf-8">
	$(function(){
		$(".search_choice a").click(function(){
			$(".active").removeClass('active');
			$(this).addClass('active');
			if($(this).attr('atr') == 'bsc'){
				$("#advance").hide();
				$("#basic").show();
				$(".mediuminput").focus();
			}else{
				$("#advance").show();
				$("#basic").hide();
				$(".smallinput").focus();
			}
			return false;
		})

		$('#dyntable').dataTable( {
			"sPaginationType": "bootstrap",
			"sScrollY": "350px",
			  "bFilter": false,
			"bPaginate": false,
	    });
	})
</script>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="pageheader notab">
		    <h1 class="pagetitle"><?=$title?></h1>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span5">
			<!--div class="title"><h3>Transaksi Pasien</h3></div-->
			<a href="<?=base_url('gudang_farmasi/transfer_item/transfer')?>" class="btn btn-primary" style="margin:25px 0px 0px 5px;padding:10px 20px">Tambah Order</a>
		</div>
		<div class="span5" style="float:right;">
			<div class="widgetbox" style="margin-top:20px;">
				<?=form_open('',array('class'=>'frm_search'));?>
					<!-- <a href="" class="btn btn_orange btn_search radius50"><span>Search</span></a> -->
					<div id="basic">
						<div class="chatsearch" >
                        	<input type="text" name="" placeholder="Search" style="width:91%;margin:auto;">
                    	</div>
					</div>
					<!-- <div id="advance">
						<input type="text" class="smallinput" placeholder="masukkan alamat">
						<input type="text" class="smallinput" placeholder="masukkan nama pasien">
					</div>
					<br clear="all">
					<div class="search_choice">
						<a class="active" atr="bsc" href="#">sederhana</a> | <a atr="adv" href="#">pencarian lanjut</a>
					</div> -->
				</form>
			</div>
		</div>
	</div>
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12">
				<style>
					#dyntable td.curr{
						text-align: right;
					}
				</style>
				<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered def_table_y dataTable tb_scrol" id="dyntable">
				    <thead>
				        <tr>
				        	<th>Pos Asal</th>
				            <th>Pos Tujuan</th>
				            <th>Item</th>
				            <th>Qty</th>
				            <th>Old Stock</th>
				            <th>New Stock</th>
				            <th>Jenis Transfer</th>
				            <th>Tgl Transaksi</th>
				        </tr>
				    </thead>
				    <tbody>
						<?$no = 20121020001; for ($i=0;$i<100;$i++): ?>
							<tr>
					            <td>
									<b class="tname">Gudang Pusat</b>
								</td>
								<td><b class="tname">Lemari BML-<?=$no - rand(0,10)?></b></td>
								<td><b class="tname">Amox <?=number_format(rand(0,50))?> Gram</b></td>
								<td class="curr"><b class="tname"><?$a = number_format(rand(0,50),2);echo $a;?></b></td>
								<td class="curr"><b class="tname"><?$b = number_format(rand(0,50),2);echo $b;?></b></td>
								<td class="curr"><b class="tname"><?=number_format($a+$b,2)?></b></td>
								<td><b class="tname">in</b></td>
								<td style="text-align:right"><i>Selasa, 3 November 2012</i></td>
					        </tr>
						<?endfor ?>
				    </tbody>
				    <tfoot>
				    </tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

