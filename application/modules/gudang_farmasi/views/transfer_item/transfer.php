<div class="container-fluid">
	<div class="row-fluid">
		<div class="pageheader notab">
		    <h1 class="pagetitle"><?=$title?></h1>
		</div>
	</div>
	<script>
		var pos = {};
			pos.results = [
				{id:'1',name:'Gudang Utama'},
				{id:'2',name:'Gudang Melati'},
				{id:'3',name:'Gudang Mawar'},
				{id:'4',name:'Gudang Kamboja'},
				{id:'5',name:'Lemari Melati 001'},
				{id:'6',name:'Lemari Melati 002'},
				{id:'7',name:'Lemari Mawar 001'},
				{id:'8',name:'Lemari Mawar 002'},
				{id:'9',name:'Lemari Mawar 003'},
				{id:'10',name:'Lemari Mawar 004'},
				{id:'11',name:'Lemari Kamboja 001'},
			];
			pos.total = pos.results.length;
		var item = {};
			item.results = [
				{id:'1',name:'A-B VASK TABLET 10 MG'},
				{id:'2',name:'3TC-HBV TABLET'},
				{id:'3',name:'A-B VASK TABLET 5 MG'},
				{id:'4',name:'ABBOTIC XL'},
				{id:'5',name:'ACCOLATE TABLET'},
				{id:'6',name:'ACCUPRIL TABLET 10 MG'},
				{id:'7',name:'ACCUPRIL TABLET 20 MG'},
				{id:'8',name:'ACCUPRIL TABLET 5 MG'},
				{id:'9',name:'ACEPRESS TABLET 12,5 MG'},
				{id:'10',name:'ACEPRESS TABLET 25 MG'},
				{id:'11',name:'ACETENSA TABLET'},
				{id:'12',name:'ACIFAR CAPLET 200 MG'},
				{id:'13',name:'ACIFAR CAPLET 400 MG'},
				{id:'14',name:'ACIFAR CREAM'},
				{id:'15',name:'ACITRAL TABLET'},
			];
			item.total = item.results.length;
		var people = {};
			people.results = [
				{id:'1',name:'Admin'},
				{id:'2',name:'Hanif Burhanuddin'},
				{id:'3',name:'Sigit Hanafi'},
				{id:'4',name:'Hanief'},
				{id:'5',name:'Agus Dwi Prrayogo'},
				{id:'6',name:'Krishna Yuniar'},
			];
			people.total = people.results.length;
		$(function(){
			$(".pos_origin").flexbox(pos);
			$(".pos_destination").flexbox(pos);
			$(".item").flexbox(item);
			$(".people").flexbox(people);
			$(".ffb-arrow").remove();
		})
	</script>
	<style>
		#table1 input{
			margin:0px;
		}
	</style>
	<div class='row-fluid' style="padding:10px">
		<div class="span5">
			<table style="width:100%">
				<tr>
					<td>Tanggal</td><td><input type="text" name="trans_date" placeholder="dd/mm/yyyy"></td>
				</tr>
				<tr>
					<td>Pos Asal</td><td><div class="pos_origin" id="pos_origin"></td>
				</tr>
				<tr>
					<td>Pos Tujuan</td><td><div class="pos_origin" id="pos_destination"></td>
				</tr>
				<tr>
					<td>Petugas</td><td><div class="people" id="people[]"></div></td>
				</tr>
				<tr>
					<td>Note</td><td><textarea name="note"></textarea></div></td>
				</tr>
			</table>
		</div><!-- #span7 -->
		<div class="span5">
			<table id="table1" class="table table-bordered">
				<thead>
					<tr>
						<th>Item</th>
						<th>Jumlah</th>
					</tr>
				</thead>
				<tbody>
					<?for($i=1;$i<=5;$i++):?>
						<tr>
							<td><div class="item" id="item[]"></div></td>
							<td><input type="text" name="amount[]" value="0.00" style="max-width:50px"></td>
						</tr>
					<?endfor;?>
				</tbody>
			</table>
		</div>
		<div class='row-fluid'>
			<div class='form-actions span8' style="text-align:right;margin-top:20px">
				<button class="btn btn-warning">Batal</button>
				<button class="btn btn-primary">Simpan</button>
			</div>
		</div>
    </div><!-- #row-fluid -->
</div><!-- #container-fluid -->