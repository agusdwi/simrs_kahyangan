<? 
    $this->load->view('gudang_farmasi/local/behav');
?>

<style type="text/css" media="screen">
    /*#ffb-input{
        width: 40%;
    }*/

    #fx_supplier_ctr.ffb{
        width:320px !important;
        top: 28px !important;
    }

    #fx_supplier_ctr .row .col1{
        float:left;
        /*width:50px;*/
    }
    #fx_supplier_ctr .row .col2{
        float:left;
        margin-left: 15px;
        width:100px;
    }
     #fx_supplier_ctr .row .col3{
        float:left;
        margin-left: 15px;
        /*width:150px;*/
    }

    #fx_item.ffb{
        width:320px !important;
        top: 28px !important;
    }

    #fx_item_ctr .row .col1{
        float:left;
        /*width:50px;*/
    }
    #fx_item_ctr .row .col2{
        float:left;
        margin-left: 15px;
        width:100px;
    }

    .frm_search input{margin-top:-1px;float:right;margin-left:2px}
    .frm_search .btn{float:right;margin-left:5px}
    .frm_search .btn span{padding:2px 10px;}
    .search_choice a{font-weight:bold}
    .search_choice a.active{color:#fb9338}
    .search_choice {float:right;margin-right:95px}
    #advance{display:none}
    #filter{border-bottom: 1px dashed #DDD;}
    #body_search{margin-top:10px}
    .tname{font-size:110%}
    #dyntable tbody tr td{border-right:none}
    #dyntable tbody tr td + td + td {border-right:1px solid #DDD}
    #dyntable tr:nth-child(even){
        background:#F7F7F7;
    }
    .dataTables_scrollHead{
        margin-bottom: -22px;
    }
    .dataTables_info{
        margin-top: 20px;
    }
</style>
<script type="text/javascript">
    $(function(){
        $('.datepicker').datepicker({
            minDate: 0,
        });

        var d_uri = "<?=base_url()?>gudang_farmasi/purchase_request/purchase";
        oTb = $('#tb_dokter').dataTable( {
            "bProcessing": true,
            "bServerSide": true,
            "bLengthChange": false,     
            "bFilter": true,
            "sPaginationType": "full_numbers",
            "aoColumns": [null,null,{ "bSortable": false }],
            "sAjaxSource": d_uri,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                var newArray = $.merge(aoData, [{ "name": "<?=$this->security->get_csrf_token_name()?>", "value": "<?=$this->security->get_csrf_hash()?>" }]);
                $.ajax( {
                    "dataType": 'json',
                    "type": "POST", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                } );
            }
        });
        $("#tb_dokter_filter").hide();  
        $(".chatsearch input").keyup(function(e){
            $("#tb_dokter_filter input").val($(".chatsearch input").val()).trigger('keyup');
        })
    });
    </script>

<div id="his">
<div class="pageheader notab">
    <h1 class="pagetitle">Tambah <?=$title?><a id="history" style="float:right" class="btn btn-success"><i class="icon-plus-sign icon-white"></i>History</a></h1>
</div>

<div id="field1">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-align-justify"></i>                                  
                    </span>
                    <h5>Langkah 1 : Masukkan Supplier</h5>
                </div>
                <div class="widget-content nopadding">
                    <?=form_open(cur_url().'create_pr',array('class' => 'form-horizontal form','id' => 'form_sup')); ?>
                        <div class="control-group">
                            <label class="control-label">No Request *</label>
                            <div class="controls">
                                <input type="text" value="<?=$id_po;?>" style="width:15%" id="ipr_id" readonly="readonly" name="ds[ipr_id]" >
                            </div>
                            <label class="control-label">Supplier *</label>
                            <div class="controls">
                                <div id="fx_supplier"></div>
                            </div>
                            <label class="control-label">Tanggal Request *</label>
                            <div class="controls">
                                <input type="text" class="mini datepicker" name="ipr_date" id="ipr_date" data-date-format="dd-mm-yyyy">
                            </div>
                            <label class="control-label">Petugas *</label>
                            <div class="controls">
                                <input type="text" placeholder="Petugas" class="mini" id="ipr_operator" name="ds[ipr_operator]" value="<?=get_user('username')?>">
                            </div>
                            <label class="control-label">Catatan</label>
                            <div class="controls">
                                <textarea  class="medium" style="width:40%" id="ipr_note" name="ds[ipr_note]"></textarea>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button style="margin-left:85%" id="reset" class="btn btn-warning" type="reset">Reset</button>
                            <button class="btn btn-primary" id="next" type="submit">Lanjut</button>
                        </div>
                    <?=form_close()?>
                </div>
            </div>                      
        </div>
    </div>
</div>
</div>
</div>

<div id="field2" class="hide">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-align-justify"></i>                                  
                    </span>
                    <h5 style="width:94%"><span>Langkah 2 : Masukkan Detail Pemesanan</span><span style="margin-left:70%"><?=$id_po;?></span></h5>
                </div>
                    <?=form_open(cur_url().'create_pr',array('class' => 'form-horizontal form','id' => 'form_det')); ?>
                        <div class="frm_trx" style="width:360px;padding:10px;height:155px;margin:0 auto;margin-top:20px;margin-bottom:20px;border:1px dashed #969696;color:#AAA">
                            <div class="section">
                                <label style="width:100px;margin-top:12px;padding-left:20px;float:left;">Item *</label>
                                <div>
                                    <div style="float:left;margin-top:8px" id="fx_item"></div>
                                    <button style="margin: 6px 0px;" id="lookup" class="btn btn-success"><i class="icon-search"></i></button> 
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <label class="control-label" style="margin-left:-30%">Pos *</label>
                            <div class="controls">
                                <select style="width:140px;margin-left:-56px" id="ipr_pos" >
                                    <?foreach ($pos->result() as $p): ?>
                                        <option name="ipr_pos" value="<?=$p->imp_id?>"><?=$p->imp_name?></option>
                                    <?endforeach ?>
                                </select>
                            </div>
                            <div class="section">
                                <label style="width:100px;margin-top: 12px;margin-left:20px;float:left">Jumlah</label>
                                <div>
                                    <input type="text" name="" style="width:50px;margin-top:6px" id="qty" autocomplete="off">
                                </div>
                            </div>
                            <div class="section">
                                <label style="width:100px;margin-top: 12px;float:left"></label>
                                <div>
                                    <button id="reset" class="btn" type="reset">reset</button>
                                    <a class="btn btn-info" id="tambah">tambah</a>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box">
                            <div class="widget-content nopadding">
                                <table class="table table-bordered table-striped" id="tbTambah">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Pos</th>
                                            <th style="width:100px">Jumlah</th>
                                            <th style="width:50px">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody><!-- 
                                        <tr>
                                            <td colspan="4">Data belum tersedia</td>
                                        </tr> -->
                                    </tbody>
                                </table>                            
                            </div>
                        </div>
                        <div class="form-actions">
                            <button href="javascript: history.go(-1)" style="margin-left:-60px" class="btn" id="back">Kembali</button>
                            <button style="margin-left:80%" id="reset" class="btn btn-warning" type="reset">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
            </div>                      
        </div>
    </div>
</div>
</div>

<div id="history1" class="hide">
<div class="pageheader notab">
    <h1 class="pagetitle">History <?=$title?><a id="add" style="float:right" class="btn btn-success"><i class="icon-plus-sign icon-white"></i>Tambah</a></h1>
</div>

    <div class="container-fluid">
    <div class="row-fluid">
        <div class="span5" >
            <div class="title"><h3><?=$title?></h3></div>
        </div>
        <div class="span5" style="float:right;">
            <div class="widgetbox" style="margin-top:20px;">
                <?=form_open('',array('class'=>'frm_search'));?>
                    <div id="basic">
                        <div class="chatsearch" >
                            <input type="text" name="" placeholder="Search" style="width:91%;margin:auto;">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br clear="all">
    <div class="row-fluid">
        <div class="widgetbox">
            <div class="span12">
                <table id='tb_dokter' class="table table-bordered">
                    <thead>
                        <th width="100px">Kode Request</th>
                        <th width="100px">Nama Supplier</th>
                        <th width="50px ">Tanggal</th>
                        <!-- <th width="50px ">Aksi</th> -->
                        <!-- <th width="100px">Tgl Pendaftaran</th> -->
                        <!-- <th width="50px">Detail</th> -->
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
var count = 0;
var isvalid;
    $(function(){
        $("#next").click(function(){
            $("#field1").fadeOut(function(){
                $("#field2").fadeIn();
            })
            return false;
        })
        $("#back").click(function(){
            $("#field2").fadeOut(function(){
                $("#field1").fadeIn();
            })
            return false;
        })
        $("#history").click(function(){
            $("#his").fadeOut(function(){
                $("#history1").fadeIn();
            })
            return false;
        })
        $("#add").click(function(){
            $("#history1").fadeOut(function(){
                $("#his").fadeIn();
            })
            return false;
        })

        $(".form").submit(function(){
            var url  = $(this).attr('action');
            var data = $("#form_sup").serialize()+"&"+$("#form_det").serialize();
            $.post(url,data, function(data) {
                $("#reset").trigger('click');
                $("#back").trigger('click');
            }); 
            return false;
        }) 

        $("#tambah").click(function(){
            count += 1;
                var nama    = $("#fx_item_input").val();
                var item    = $("#fx_item_hidden").val();
                var pos    = $("select#ipr_pos option:selected").text();
                var post    = $("select#ipr_pos option:selected").val();
                var qty     = $("#qty").val();
                $("<tr><td>"+nama+"</td><td>"+pos+"</td><td style='text-align:center'>"+qty+"</td><td style='width:5px;text-align:center' class='del'><a href='' class='deletes' style=''><b class='icon-remove'></b></a></td><input id='rows"+count+"' name='rows[]' value='"+item+"|"+post+"|"+qty+"' type='hidden'>").appendTo('#tbTambah tbody');
                $("#fx_item_input").val("");
                $("#qty").val("");
                return false;
        })   

        $(".deletes").die('click').live('click',function(){
            $(this).parent().parent().fadeOut(function(){
                $(this).remove();
            })
            return false;
        })  
    })
</script>