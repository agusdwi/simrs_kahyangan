 <script type="text/javascript" src='<?=base_url()?>assets/js/jquery.treeview.js'></script> 
<script type="text/javascript" src='<?=base_url()?>assets/js/valid.js'></script> 
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/PLUGINS/treeview.css">
<style>
	#hidden{
		width:14px;height:14px;display:block;margin-top:-200px;margin-right:-5px;float:right;
		background:url(<?=base_url()?>assets/images/prev.gif);cursor:pointer;
		}
	#hidden.next{background:url(<?=base_url()?>assets/images/next.gif)}
	.rg_all{width:96% !important}
	.datatables_processing{margin:13px 30px 0px 0px !important}	
</style>
<script>
	var d_uri;
		function init(){
			$("#hd").hide();
			$("#simpan , #reset , #batal,#detail,.edit, .delete").hide();
			$("#init,#tambah").show();
		}
		function view(){
			$("input ,textarea").addClass('view').attr("disabled","disabled");
		}
		function edit(){
			$("input ,textarea").removeClass('view').removeAttr('disabled');
		}
	$(function(){
		init();
		$("#tb_item_filter input").attr('placeholder','kode / nama item');
		$("#tb_item th").append('<span></span>');
		$('.view').die('click').live('click',function(){
			//oTb.fnReloadAjax(d_uri+$(this).attr('uid'));
			$("#b_pos").html($(this).html());
			return false;
		})

		$('.hitarea:eq(0)').trigger('click');
		$('.view:eq(0)').trigger('click');

		$("#tambah").click(function(){
			init();
			;edit();
			$("#batal").trigger('click');
			$("#simpan,#reset,#batal,#detail,#parent").show();
			$("#init,#tambah,#sprt").hide();
			$("#form").attr('action','pos_item/create');
			$('#nama').focus();
		})
		$(".view").die().live('click',function(){
			var url=$(this).attr('href');
			$("#b_pos").html($(this).html());
			$.getJSON(url,function(data){
				$("#tambah").trigger('click');
				$("#nama").val(data.nm);$("#dsc").val(data.dsc);$("#parent").val(data.pr);
				$("#tambah ,#parent,#simpan,#reset").hide();
				$(".edit,.delete,#sprt,#batal").show();
				$('.delete').attr('href','pos_item/delete/'+data.cd);
				$("#form").attr('action','pos_item/update/'+data.cd);
				$("#sprt").html($("#parent option:selected").text().replace('|','').replace(/-/g,'').replace(/^\s*/g, '').replace(/\s*$/g, ''));
				view();
			});
			return false;
		})
		$(".edit").die().live('click',function(){
			edit();
			$(".edit,#sprt,.delete").hide();
			$("#tambah,#parent,#simpan,#batal").show();
			$("#nama").focus();
		})

		$('.delete').die("click").live("click",function(){
			var url = $(this).attr('href');
			//alert(url);
			var obj = $(this);
			var a = confirm('yakin menghapus data ini ?');
			if(a){
				$.get(url,function(data){
					$("#msg_container").msg({  
						tipe:"success",
						txt:'data '+data+' berhasil dihapus'
					});
				});
					 location.reload();
			}
			return false;
		})
		$("#batal").click(function(){init()})
		$("ul#tree").treeview({collapsed: true,animated: "fast"});
		$(".hitarea:first").trigger("click");

		//tambah+validator
		$("#form").validate({
                               rules: {
                                  'parent':"required",
                                  'nama': "required",     
                                  'dsc' : "required",
                                 
                              },
                              Message:{
                                'parent' : "Pilih poli terlebih dulu",
                                'nama' : "Isikan nama terlebih dulu",
                              },
                              submitHandler: function(form) {
                                 $.ajax({
                                    url: $("#form").attr('action'),
                                    type: "POST",
                                    crossDomain: true,
                                    data: $('#form').serialize(),
                                    dataType: "json",
                                    success: function( data ) {
                                         //tbldokter.fnDraw();
                                         location.reload();
                                        
                                    },
                                    error:function(){
                                      
                                      //$('.error_submit').html('terjadi kesalahan saat menyimpan data, mohon ulangi sekali lagi ');
                                    }
                                  });
                                return false;
                            },
                            
                            errorPlacement: function(error, element) {
								error.appendTo( element.parent());
                             }
                            });



		$("#form").bind("onSuccess", function(e, els) {
			var numSucceeded = els.length,
				numExpected = $(this).data('validator').getInputs().length;
			if (numSucceeded === numExpected) {
				var a =$(this).serialize();
				var url = $(this).attr('action');
				$.post(url,a, function(data){
					$("#msg_container").msg({  
						tipe:"success",
						txt:data
					});
					$("#master_pos").trigger('click');
				});
				return false;//biar form gak jadi submit, karena submit sudah di handle oleh ajax
			}
		});
		//init
		init();
	})
</script>
<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>
<div id="container-fluid">
	<div class='row-fluid'>
		<div class="title" style="padding:0px"><h3>Pos</h3></div>
		<hr/>
		<div id="sidebar" class="span4" style="">
			<div class='widget-box'>
				<div class="widget-title">
											<span class="icon"><i class="icon-play-circle"></i></span>
											<h5>Daftar POS</h5>
				</div>
				 
				<div style="overflow:auto;height:300px;font-weight: bold !important;">
					<?=$tree?>
				</div>
			</div>
			<!-- <div id="hidden" title='sembunyikan'></div> -->
		</div>
		<div id="rg" class='span7' style="float:left;">
			<div class='widget-box'>
				<div class="widget-title">
											<span class="icon"><i class="icon-play-circle"></i></span>
											<h5>POS : <span id="b_pos"></span></b></h5>
				</div>
						 
					 <?=form_open(cur_url().'create',array('class' => 'form-horizontal form panel','id' => 'form', 'style'=>"padding:10px;")); ?> 	 
					<!-- <form  id="form" class="form panel" method="post" novalidate> -->
						<header>
							<b id="title_form">Detail POS</b>
							<a style='float:right;margin-left:5px' href="#tambah" class="btn btn-success btn-mini" id="tambah">Tambah Data</a>
							<a style='float:right ' href="#edit" class="btn btn-info btn-mini edit">edit</a>
							<a style='float:right;margin-right:5px' href="#" class="btn btn-danger btn-mini delete" val=''>delete</a>
							<div class='clear'></div>
						</header>
						<hr>
						<div id="ctn" style="">
						<div id="init"><center><b>Silahkan pilih POS di samping<br>atau klik tombol tambah untuk menambah data</b></center></div>
						<div id="detail">
						<fieldset>
							<div class="span8" style='margin-left:15px;'><label>Nama POS *</label><input type="text" required="required" name="nama" id="nama"></div>	
							<div class="span6"><label>Parent *</label>
								<span id="sprt"></span>
								<?=$parent?>
							</div>	
							<div class="span5">
								<label>Deskripsi *</label><textarea name="dsc" id="dsc" rows='4' cols='35' required='required' "></textarea>
							</div>
						</fieldset>
						</div>
						</div>
						<hr>
						<button class="btn btn-success" type="submit" id="simpan" style="float:right;margin-left:5px">Simpan</button>
						<button class="btn btn-info" type="reset" id="reset" style="float:right">Reset</button>
						<button class="btn btn-danger" type="reset" id="batal" >Batal</button>
					</form>
			</div>
		</div>
		</div>
		<div class='clear'></div>
	</div>
</div>