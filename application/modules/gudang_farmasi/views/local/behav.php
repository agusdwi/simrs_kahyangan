<script type="text/javascript">
	$(function(){
		$('#fx_supplier').flexbox(BASE+"/gudang_farmasi/master_data/get_supplier", {
			//method : 'POST',
			paging: false,
			showArrow: false ,
			maxVisibleRows : 10,
			width : 185,
			resultTemplate: '<div class="col1">{id}</div><div class="col2">{name}</div><div class="col3">{add}</div>',
			/*onSelect:function(){
				callback();
			}*/

		});

		$('#fx_suppliers').flexbox(BASE+"/gudang_farmasi/master_data/get_supplier", {
			//method : 'POST',
			paging: false,
			showArrow: false ,
			maxVisibleRows : 10,
			width : 214,
			resultTemplate: '<div class="col1">{id}</div><div class="col2">{name}</div><div class="col3">{add}</div>',
			onSelect:function(){
				$.getJSON(BASE+"gudang_farmasi/purchase_order/order/"+$("#fx_suppliers_hidden").val(), function(json) {
					$("#tbOrder tbody").load(url);
				})
			}
		});

		$('#fx_item').flexbox(BASE+"/gudang_farmasi/master_data/get_item", {
			//method : 'POST',
			paging: false,
			showArrow: false ,
			maxVisibleRows : 10,
			width : 180,
			resultTemplate: '<div class="col1">{id}</div><div class="col2">{name}</div>',
			onSelect:function(){
				$("#qty").focus();
				$.getJSON(BASE+"gudang_farmasi/master_data/get_item_harga/"+$("#fx_item_hidden").val(), function(json) {
					$("#satuan").val(json.harga);
					$("#unit").val(json.unit);
				})
			}

		});
	});
</script>