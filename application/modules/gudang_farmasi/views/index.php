<div class="pageheader notab">
	    <h1 class="pagetitle"><?=$title?></h1>
	</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span5 center" style="text-align: center;">                 
			<ul class="quickstats">
				<li>
					<a href="<?=base_url()?>gudang_farmasi/purchase_request">
						<img alt="" src="<?=base_url()?>assets/img/icons/32/Edit.png">
						<span>purchase<br> request</span>
					</a>
				</li>
				<li>
					<a href="<?=base_url()?>gudang_farmasi/purchase_order">
						<img alt="" src="<?=base_url()?>assets/img/icons/32/DefinedParameters.png">
						<span> purchase<br>order</span>
					</a>
				</li>
				<li>
					<a href="<?=base_url()?>gudang_farmasi/receive_item">
						<img alt="" src="<?=base_url()?>assets/img/icons/32/download.png">
						<span>receive<br>item</span>
					</a>
				</li>
				<li>
					<a href="<?=base_url()?>gudang_farmasi/retur">
						<img alt="" src="<?=base_url()?>assets/img/icons/32/Prescription.png">
						<span> retur</span>
					</a>
				</li>
				<li>
					<a href="<?=base_url()?>gudang_farmasi/stok">
						<img alt="" src="<?=base_url()?>assets/img/icons/32/cabinet.png">
						<span> stok</span>
					</a>
				</li>
				<li>
					<a href="<?=base_url()?>gudang_farmasi/transfer_item">
						<img alt="" src="<?=base_url()?>assets/img/icons/32/Archive.png">
						<span> transfer item</span>
					</a>
				</li>
			</ul>
 		</div>  
		<div class="span7 center">
			<br>
			<div class="title" style="padding:0px"><h3>Critical Inventory Stok</h3></div>
			<br>
			<div class="widget-box" >
										<div class="widget-title">
											<span class="icon"><i class="icon-time"></i></span>
											<h5>Monitor Stok</h5>
										</div>
										<div class="widget-content nopadding">
											<table class="table table-striped table-bordered">
			                                        <thead>
			                                            <tr>
			                                                <th>Nama Obat</th>
			                                                <th>Stok</th>
			                                                <th>Opts</th>
			                                            </tr>
			                                        </thead>
			                                        <tbody>
			                                            <tr>
			                                                <td class="taskDesc"><i class="icon-info-sign"></i> Amoxilin</td>
			                                                <td class="taskStatus"><span class="pending">10 bungkus</span></td>
			                                                <td class="taskOptions"><a href="#" class="tip-top" data-original-title="Update"><i class="icon-ok"></i></a> <a href="#" class="tip-top" data-original-title="Delete"><i class="icon-remove"></i></a></td>
			                                            </tr>
			                                            <tr>
			                                                <td class="taskDesc"><i class="icon-plus-sign"></i> Paracetamol 500mg</td>
			                                                <td class="taskStatus"><span class="pending">20 Tablet</span></td>
			                                                <td class="taskOptions"><a href="#" class="tip-top" data-original-title="Update"><i class="icon-ok"></i></a> <a href="#" class="tip-top" data-original-title="Delete"><i class="icon-remove"></i></a></td>
			                                            </tr>
			                                            <tr>
			                                                <td class="taskDesc"><i class="icon-ok-sign"></i> Yekaflu 300mg</td>
			                                                <td class="taskStatus"><span class="pending">25 Table</span></td>
			                                                <td class="taskOptions"><a href="#" class="tip-top" data-original-title="Update"><i class="icon-ok"></i></a> <a href="#" class="tip-top" data-original-title="Delete"><i class="icon-remove"></i></a></td>
			                                            </tr>
			                                        </tbody>
			                                    </table>
										</div>
									</div>
		</div>
    </div>
</div>