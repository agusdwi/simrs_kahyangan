<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_data extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 6
			);
	}

	public function index(){
		$data['main_view']	= 'master_data/index';
		$data['title']		= 'Master data ';
		$data['desc']		= 'Description. ';
		$data['cf']			=  $this->cf;
		$data['current']	= 24;
		$data['type'] 		= $this->db->get_where("inv_item_type",array('it_status'=>1));
		$data['master'] 	= $this->db->get_where("inv_item_master",array('im_status'=>1));
		$data['supplier'] 	= $this->db->get_where("mst_supplier",array('msup_status'=>1));
		$data['currency'] 	= $this->db->get_where("mst_currency",array('mc_status'=>1));
		$data['provinsi'] 	= $this->db->get("mst_province");
		$data['regency'] 	= $this->db->get("mst_regency");
		$this->load->view('template',$data);
	}

	function get_supplier(){
		$data = array();
		$q = strtolower($_GET['q']);
		$data = $this->db->select('msup_id as id,msup_name as name,msup_address as add')->like('lower(msup_id)', $q, 'both')->or_like('lower(msup_name)', $q, 'both')->or_like('lower(msup_address)', $q, 'both')->get('v_supplier',100,0)->result_array();
		echo json_encode(array('results'=> $data));
	}

	function get_item(){
		$data = array();
		$q = strtolower($_GET['q']);
		$data = $this->db->select('im_id as id,im_name as name')->like('lower(im_id)', $q, 'both')->or_like('lower(im_name)', $q, 'both')->get_where('inv_item_master',array('im_status'=>1),100,0)->result_array();
		echo json_encode(array('results'=> $data));
	}

	function get_item_harga($id){
		$item = $this->db->get_where('inv_item_master',array('im_id'=>$id));
		$data = array();
		$total = 0;
		foreach($item->result() as $d){
			// $total = $total+(($d->item_harga)*($d->jasa_qty));
			$data = array(
				'id'	=> $d->im_id,
				// 'name'	=> $d->item_nama,
				// 'qty'	=> $d->jasa_qty,
				'unit'	=> $d->im_unit,
				'harga'	=> $d->im_item_price,
				// 'total'	=> $total
			);
		}
		echo json_encode($data);
	}

	function add_type(){
		$data = $this->input->post('ds');
		$this->db->insert('inv_item_type', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		redirect(cur_url(-1));
	}

	function add_master(){
		$data = $this->input->post('ds');
		$this->db->insert('inv_item_master', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		redirect(cur_url(-1));
	}

	function add_supplier(){
		$data = $this->input->post('ds');
		$this->db->insert('mst_supplier', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		redirect(cur_url(-1));
	}

	function update($id){
		$data = $this->input->post('ds');	
		$this->db->where('', $id);
		$this->db->update('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('', $id);
		$this->db->update('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil dihapus'));
		redirect(cur_url(-2));
	}
}