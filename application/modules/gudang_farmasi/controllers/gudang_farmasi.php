<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gudang_farmasi extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 6
			);
	}

	public function index(){
		$data['main_view']	= 'gudang_farmasi/index';
		$data['title']		= 'Gudang Farmasi';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}
}