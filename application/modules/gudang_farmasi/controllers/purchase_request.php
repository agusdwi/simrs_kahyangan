<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_request extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 6
			);
	}

	public function index(){
		$data['main_view']	= 'purchase_request/index';
		$data['title']		= 'Purchase Request';
		$data['cf']			=  $this->cf;
		$data['id_po'] 		=  $this->get_id();
		$data['pos'] 		=  $this->db->get_where('inv_mst_pos',array('imp_status'=>1));
		$data['date'] 		=  new DateTime("now"); 
		$data['current']	= 16;
		$this->load->view('template',$data);
	}

	function create_pr(){
		$data = $this->input->post('ds');
		$data['msup_id'] = $this->input->post('fx_supplier');
		$dates = $this->input->post('ipr_date');
		$data['ipr_date'] = date('Y-m-d', strtotime ($dates));
		$this->db->insert('inv_purchase_req', $data); 
		$data = $this->input->post();
		$id = $this->get_id();
		foreach($data['rows'] as $r){
			$dt = explode('|',$r);
			$dt = array(
				'iprd_id'			=> $id,
				'iprd_item'			=> $dt[0],
				'iprd_pos'			=> $dt[1],
				'iprd_qty'			=> $dt[2]
			);
			$this->db->insert('inv_purchase_req_detail', $dt);
		}
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		redirect(cur_url(-1));
	}

	function history(){
		$data['main_view']	= 'purchase_request/history';
		$data['title']		= 'Purchase Request';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}

	function get_id(){
		$format = db_conv($this->db->get_where('com_code',array('id'=>1)))->value_1;
		$l = strlen($format)+1+4;
		$q = "SELECT substr(ipr_id, $l, 4) as n 
			  from inv_purchase_req where ipr_id like '$format%' order by n desc limit 1"; 
		$q = $this->db->query($q);
		if($q->num_rows() == 0){
			$no = '0001';
		}else{
			$nl = intval(db_conv($q)->n);
			$nl++;
			$no = rtrim(sprintf("%'04s\n",$nl));
		}
		return $format.DATE('y').DATE('m').$no;		
	}

	function detail($transaction_id){
		$data['main_view']	= 'gudang_farmasi/purchase_request/detail_request';
		$data['title']		= 'Purchase Request > Detail';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}

	function purchase(){
		$config['sTable'] 			= 'v_ipr';
		/*$config['join'][] 		= array('trx_dokter_spesialis b','a.dr_cd = b.dr_cd');
		$config['join'][] 			= array('trx_spesialis c','c.spesialis_cd = b.spesialis_cd');*/
		$config['aColumns'] 		= array('ipr_id','msup_name','ipr_date');
		$config['key'] 				= 'ipr_id';
		$config['aColumns_format'] 	= array("ipr_id","msup_name","ipr_date");
		$config['searchColumn'] 	= array('ipr_id','msup_name');
		$config['aksi'] = array(
								'stat'  	=> true,
								'key'		=> 'ipr_id',//
								/*'edit'   	=> base_url().'rawat_jalan/master__dokter/update/',
								'delete' 	=> base_url().'rawat_jalan/master__dokter/delete/dr/',*/
								// 'detail'	=> base_url().'pendaftaran/daftar_pasien/detail/',
								);
		init_datatable($config);
	}
}