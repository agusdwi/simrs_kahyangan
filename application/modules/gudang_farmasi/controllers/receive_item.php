<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receive_item extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 6
			);
	}

	public function index(){
		$data['main_view']	= 'receive_item/index';
		$data['title']		= 'Receive Item';
		$data['cf']			=  $this->cf;
		$data['id']			= $this->get_id();
		$data['invoice']	= $this->get_invoice();
		$data['currency']	= $this->db->get_where('mst_currency',array('mc_status'=>1));
		$data['current']	= 18;
		$this->load->view('template',$data);
	}

	function get_id(){
		$format = db_conv($this->db->get_where('com_code',array('id'=>6)))->value_1;
		$l = strlen($format)+1+4;
		$q = "SELECT substr(iri_id, $l, 4) as n 
			  from inv_recive_item where iri_id like '$format%' order by n desc limit 1"; 
		$q = $this->db->query($q);
		if($q->num_rows() == 0){
			$no = '0001';
		}else{
			$nl = intval(db_conv($q)->n);
			$nl++;
			$no = rtrim(sprintf("%'04s\n",$nl));
		}
		return $format.DATE('y').DATE('m').$no;		
	}

	function get_invoice(){
		$format = db_conv($this->db->get_where('com_code',array('id'=>7)))->value_1;
		$l = strlen($format)+1+4;
		$q = "SELECT substr(iri_invoice, $l, 4) as n 
			  from inv_recive_item where iri_invoice like '$format%' order by n desc limit 1"; 
		$q = $this->db->query($q);
		if($q->num_rows() == 0){
			$no = '0001';
		}else{
			$nl = intval(db_conv($q)->n);
			$nl++;
			$no = rtrim(sprintf("%'04s\n",$nl));
		}
		return $format.DATE('y').DATE('m').$no;		
	}

	function detail_rec($transaction_id){
		$data['main_view']	= 'gudang_farmasi/receive_item/detail_rec';
		$data['title']		= 'Receive Item > Detail';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);	
	}
}