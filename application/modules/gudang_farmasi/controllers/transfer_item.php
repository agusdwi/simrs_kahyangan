<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transfer_item extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 6
			);
	}

	public function index(){
		$data['main_view']	= 'transfer_item/index';
		$data['title']		= 'Transfer Item';
		$data['cf']			=  $this->cf;
		$data['current']	= 21;
		$this->load->view('template',$data);
	}

	function transfer(){
		$data['main_view']	= 'transfer_item/transfer';
		$data['title']		= 'Transfer Item > Transfer';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}
}