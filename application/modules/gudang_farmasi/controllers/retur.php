<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Retur extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 6
			);
	}

	public function index(){
		$data['main_view']	= 'retur/index';
		$data['title']		= 'Retur';
		$data['cf']			=  $this->cf;
		$data['current']	= 19;
		$this->load->view('template',$data);
	}

	function new_retur(){
		$data['main_view']	= 'retur/new_retur';
		$data['title']		= 'Retur > New Retur';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);	
	}
}