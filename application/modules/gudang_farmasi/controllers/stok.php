<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stok extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 6
			);
	}

	public function index(){
		$data['main_view']	= 'stok/index';
		$data['title']		= 'Stok';
		$data['cf']			=  $this->cf;
		$data['current']	= 20;
		$this->load->view('template',$data);
	}
}