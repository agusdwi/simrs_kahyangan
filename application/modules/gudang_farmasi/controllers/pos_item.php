<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pos_item extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 6
			);
	}

	public function index(){
		$data['main_view']	= 'pos_item/index';
		$data['title']		= 'Title ';
		$data['desc']		= 'Description. ';
		$data['tree']		=	$this->get_pos_all();
		$data['cf']			=  $this->cf;
		$data['parent'] = $this->get_parent();
		$data['current']	= 23;
		$this->load->view('template',$data);
	}

	function create(){
		$data = array(
			'imp_name' => $this->input->post('nama'), 
			'imp_parent' => $this->input->post('parent'),
			'imp_description' => $this->input->post('dsc'),
		);
		
		return $this->db->insert('inv_mst_pos', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		redirect(cur_url(-1));
	}

	function update($id){
		$data = array(
			'imp_name' => $this->input->post('nama'), 
			'imp_parent' => $this->input->post('parent'),
			'imp_description' => $this->input->post('dsc'),
		);
		$this->db->where('imp_id', $id);
		return $this->db->update('inv_mst_pos', $data);
		echo $this->db->last_query();  
		$this->session->set_flashdata('message',array('success','Data berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('imp_id', $id);
		$this->db->delete('inv_mst_pos');
		$this->session->set_flashdata('message',array('success','Data berhasil dihapus'));
		redirect(cur_url(-2));
	}

	function detail($id){
		$a = $this->db->get_where('inv_mst_pos',array('imp_id'=>$id));
		foreach($a->result() as $r){
			$d['cd'] = $r->imp_id;
			$d['nm'] = $r->imp_name;
			$d['pr'] = $r->imp_parent;
			$d['dsc'] = $r->imp_description;
		}
		echo json_encode($d);
	}


	function createTree($data, $parent = 0){
        static $i = 1;
        $tab = str_repeat("\t\t", $i);
        if (isset($data[$parent])) {
            $html = "\n$tab<ul id='tree'>";
            $i++;
            foreach ($data[$parent] as $v) {
                $child = $this->createTree($data, $v->imp_id);
                $html .= "\n\t$tab<li>";
                $html .= '<a uid="'.$v->imp_id.'" class="view" href="'.base_url()."gudang_farmasi/pos_item/detail/". $v->imp_id . '" class="ajax-links" >' . $v->imp_name . '</a>';
                if ($child) {
                    $i--;
                    $html .= $child;
                    $html .= "\n\t$tab";
                }
                $html .= '</li>';
            }
            $html .= "\n$tab</ul>";
            return $html;
        } else {
            return false;
        }
    }
	
	function createTreeParent($data, $parent = 0){
        static $i = 1;
        $tab = str_repeat("&nbsp;&nbsp;&nbsp;", $i);
        if (isset($data[$parent])) {
            //$html = "\n$tab<select>";
			$html='';
            $i++;
            foreach ($data[$parent] as $v) {
                $child = $this->createTreeParent($data, $v->imp_id);
                $html .= "<option value='{$v->imp_id}'>$tab|--{$v->imp_name}</option>";
                if ($child) {
                    $i--;
                    $html .= $child;
				}
			}
            return $html;
        } else {
            return false;
        }
    }
	
	function get_pos_all(){
		$a = $this->db->get('inv_mst_pos');
		$datas='';
		foreach ($a->result() as $row) {
			$data[$row->imp_parent][] = $row;
		}
        $tree = $this->createTree($data);
		return $tree;
	}
	
	function get_parent($r=""){
		$r = ($r=="" ? "<option value='0'>root</option>" : "");
		$a = $this->db->get('inv_mst_pos');
		foreach ($a->result() as $row) {
			$data[$row->imp_parent][] = $row;
		}
		$tree = $this->createTreeParent($data);
		$tree = "<select class='multiple' style='width:270px' required='required' id='parent' name='parent' size='5'>$r".$tree;
		$tree .= "</select>";
		return $tree;
	}

}