<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_order extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 6
			);
	}

	public function index(){
		$data['main_view']	= 'purchase_order/index';
		$data['title']		= 'Purchase Order';
		$data['cf']			=  $this->cf;
		$data['id']			= $this->get_id();
		$data['invoice']	= $this->get_invoice();
		$data['currency']	= $this->db->get_where('mst_currency',array('mc_status'=>1));
		$data['current']	= 17;
		$this->load->view('template',$data);
	}

	function add_order(){
		$data = $this->input->post('ds');
		$data['ipo_supplier'] = $this->input->post('fx_supplier');
		$dates = $this->input->post('ipo_date_req');
		$data['ipo_date_req'] = date('Y-m-d', strtotime ($dates));
		$this->db->insert('inv_purchase_order', $data); 
		$data = $this->input->post();
		$id = $this->get_id();
		$sub = $this->input->post('tot');
		$total = $this->input->post('grand');
		foreach($data['rows'] as $r){
			$dt = explode('|',$r);
			$dt = array(
				'ipod_id'			=> $id,
				'ipod_item'			=> $dt[0],
				'ipod_qty'			=> $dt[1],
				'ipod_sub'			=> $sub,
				'ipod_total'		=> $total
			);
			$this->db->insert('inv_purchase_order_detail', $dt);
		}
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		redirect(cur_url(-1));
	}

	function get_id(){
		$format = db_conv($this->db->get_where('com_code',array('id'=>4)))->value_1;
		$l = strlen($format)+1+4;
		$q = "SELECT substr(ipo_id, $l, 4) as n 
			  from inv_purchase_order where ipo_id like '$format%' order by n desc limit 1"; 
		$q = $this->db->query($q);
		if($q->num_rows() == 0){
			$no = '0001';
		}else{
			$nl = intval(db_conv($q)->n);
			$nl++;
			$no = rtrim(sprintf("%'04s\n",$nl));
		}
		return $format.DATE('y').DATE('m').$no;		
	}

	function order($id){
		$order = $this->db->get_where('view_detail_jasa',array('jasa_id'=>$id));
		$data = array();
		$total = 0;
		foreach($jasa->result() as $d){
			$total = $total+(($d->item_harga)*($d->jasa_qty));
			$data = array(
				'id'	=> $d->jasa_detail_id,
				'name'	=> $d->item_nama,
				'qty'	=> $d->jasa_qty,
				'harga'	=> $d->item_harga,
				'total'	=> $total
			);
		}
		echo json_encode($data);
	}

	function get_invoice(){
		$format = db_conv($this->db->get_where('com_code',array('id'=>5)))->value_1;
		$l = strlen($format)+1+4;
		$q = "SELECT substr(ipo_invoice, $l, 4) as n 
			  from inv_purchase_order where ipo_invoice like '$format%' order by n desc limit 1"; 
		$q = $this->db->query($q);
		if($q->num_rows() == 0){
			$no = '0001';
		}else{
			$nl = intval(db_conv($q)->n);
			$nl++;
			$no = rtrim(sprintf("%'04s\n",$nl));
		}
		return $format.DATE('y').DATE('m').$no;		
	}

	function new_order(){
		$data['main_view']	= 'gudang_farmasi/purchase_order/new_order';
		$data['title']		= 'Purchase Order > New Order';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}

	function edit(){
		$data['main_view']	= 'gudang_farmasi/purchase_order/new_order';
		$data['title']		= 'Purchase Order > Edit Order';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);	
	}

	function data_order(){
		$config['sTable'] 			= 'v_ipo';
		/*$config['join'][] 		= array('trx_dokter_spesialis b','a.dr_cd = b.dr_cd');
		$config['join'][] 			= array('trx_spesialis c','c.spesialis_cd = b.spesialis_cd');*/
		$config['aColumns'] 		= array('ipo_id','ipo_supplier','ipo_req','ipo_order','ipo_status');
		$config['key'] 				= 'ipo_id';
		$config['aColumns_format'] 	= array("ipo_id","ipo_supplier","ipo_req","ipo_order","ipo_status");
		$config['searchColumn'] 	= array('ipo_id','ipo_supplier');
		$config['aksi'] = array(
								'stat'  	=> true,
								'key'		=> 'ipo_id',//
								/*'edit'   	=> base_url().'rawat_jalan/master__dokter/update/',
								'delete' 	=> base_url().'rawat_jalan/master__dokter/delete/dr/',*/
								// 'detail'	=> base_url().'pendaftaran/daftar_pasien/detail/',
								);
		init_datatable($config);
	}
}