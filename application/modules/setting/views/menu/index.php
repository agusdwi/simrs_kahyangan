<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>       
</div><!--pageheader-->
<div id="contentwrapper" class="contentwrapper">
    <div class="one_half">
      <div id="basicform" class="subcontent">
        <div class="contenttitle2">
            <h3>Form menu </h3>
        </div><!--contenttitle-->

        <?=form_open(cur_url().'create',array('class' => 'stdform','id' => 'form')); ?>
            <p>
              <label>Nama menu</label>
              <span class="field"><input type="text" name="ds[menu_name]" id="menu_name" class="mediuminput" placeholder="Nama Menu" /></span>
            </p>
            <p>
                <label>Modul</label>
                <span class="field">
                        <select name="ds[modul_id]" class="uniformselect">
                            <? foreach ($dm->result() as $d):?>
                                <option value="<?=$d->modul_id?>"><?=$d->modul_nama?></option>
                            <?endforeach?>
                        </select>
                </span>
            </p>
              <p>
                  <label>File Upload</label>
                  <span class="field">
                      <input type="file" name="filename"/>
                  </span>
              </p>
              <p class="stdformbutton">
                  <button class="submit radius2">Submit</button>
                  <input type="reset" class="reset radius2" value="Clear">
              </p>
          </form>
        </div>
    </div>
    <div class="one_half last">
        <div class="widgetbox">
          <div class="contenttitle2">
                <h3>Daftar Group Menu</h3>
            </div><!--contenttitle-->

            <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
                <colgroup>
                    <col class="con0" />
                    <col class="con1" />
                    <col class="con0" />
                    <col class="con1" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head0">#</th>
                        <th class="head1">Nama Menu</th>
                        <th class="head0">URL Menu</th>
                        <th class="head1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?$i=0;foreach ($dt->result() as $d): $i++;?>
                    <tr>
                        <td><?=$i?></td>
                        <td><?=$d->menu_nama?></td>
                        <td><?=$d->menu_url?></td>
                        <td class="center"><a href="#" class="edit"
                            uid     ="<?=$d->menu_id?>"
                            >Edit</a> &nbsp; <a href="<?=cur_url()?>delete/<?=$d->menu_id?>" class="delete">Delete</a></td>
                    </tr>
                    <?endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>