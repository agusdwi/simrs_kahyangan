<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
	<span class="pagedesc"><?=(isset($desc)) ? $desc : '';?></span>        
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper widgetpage">
 	<div class="one_half">
 		<div id="basicform" class="subcontent">
			<div class="contenttitle2">
		        <h3>Form User </h3>
		    </div><!--contenttitle-->

		    <?=form_open(cur_url().'create',array('class' => 'stdform','id' => 'form')); ?>
		    	<p>
	                <label>Username</label>
	                <span class="field"><input type="text" name="ds[username]" id="username" class="mediuminput" placeholder="Username" /></span>
	            </p>
	            <p>
	                <label>Password</label>
	                <span class="field"><input type="password" name="ds[password]" id="password" class="mediuminput" placeholder="Password" /></span>
	            </p>
	            <p>
	                <label>User tipe</label>
                    <span class="field">
                        <div class="selector" id="uniform-undefined">
                            <span>Choose One</span>
                            <select name="ds[tipe_id]" class="uniformselect" style="opacity: 0; ">
                                <?foreach ($dt as $d): ?>
                                    <option value="<?=$d->tipe_id?>"><?=$d->tipe_nama?></option>
                                <?endforeach?>
                            </select>
                        </div>
                    </span>
	            </p>
	            <p class="stdformbutton">
	                <button class="submit radius2">Submit Button</button>
	                <input type="reset" class="reset radius2" value="Reset Button">
	            </p>
			</form>
		</div>
 	</div>
 	<div class="one_half last">
 		<div class="widgetbox">
 			<div class="contenttitle2">
               	<h3>Daftar User</h3>
            </div><!--contenttitle-->

            <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
            	<colgroup>
                    <col class="con0" />
                    <col class="con1" />
                    <col class="con0" />
                    <col class="con1" />
                </colgroup>
            	<thead>
                    <tr>
                        <th class="head0">#</th>
                        <th class="head1">Nama User</th>
                        <th class="head0">User tipe</th>
                        <th class="head1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?$i=0;foreach ($ds as $d): $i++;?>
                    <tr>
                        <td><?=$i?></td>
                        <td><?=$d->username?></td>
                        <td><?=$d->user_tipe?></td>
                        <td class="center"><a href="#" class="edit" uid="<?=$d->user_id?>">Edit</a> &nbsp; <a href="<?=cur_url()?>delete/<?=$d->user_id?>" class="delete">Delete</a></td>
                    </tr>
                    <?endforeach ?>
                </tbody>
            </table>
 		</div>
 	</div>
 </div>

<script type="text/javascript" charset="utf-8">
    $(function(){
        $(".edit").click(function(){
            $("#modul_nama").val($(this).parent().parent().find('td:eq(1)').html()).focus();
            $("#form").attr('action','<?=cur_url()?>update/'+$(this).attr('uid'));
            return false;
        })
    })
</script>