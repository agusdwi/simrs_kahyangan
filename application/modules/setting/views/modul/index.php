<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
    <span class="pagedesc"><?=(isset($desc)) ? $desc : '';?></span>        
</div><!--pageheader-->
<div id="contentwrapper" class="contentwrapper widgetpage">
 	<div class="one_half">
 		<div id="basicform" class="subcontent">
			<div class="contenttitle2"> 
		        <h3>Form modul </h3>
		    </div><!--contenttitle-->

		    <?=form_open_multipart(cur_url().'create',array('class' => 'stdform','id' => 'form')); ?>
		    	<p>
	                <label>Nama modul</label>
	                <span class="field"><input type="text" name="ds[modul_nama]" id="modul_nama" class="mediuminput" placeholder="Nama Modul" /></span>
	            </p>
	            <p>
	                <label>File Upload</label>
	                <span class="field">
	                    <input type="file" name="filename" size="20" />
	                </span>
	            </p>
	            <p class="stdformbutton">
	                <button class="submit radius2">Submit</button>
	                <input type="reset" class="reset radius2" value="Clear">
	            </p>
			</form>
		</div>
 	</div>
 	<div class="one_half last">
 		<div class="widgetbox">
 			<div class="contenttitle2">
               	<h3>Daftar Modul</h3>
            </div><!--contenttitle-->

            <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
            	<colgroup>
                    <col class="con0" />
                    <col class="con1" />
                    <col class="con0" />
                    <col class="con1" />
                </colgroup>
            	<thead>
                    <tr>
                        <th class="head0">#</th>
                        <th class="head1">Nama Modul</th>
                        <th class="head0">URL</th>
                        <th class="head1">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?$i=0;foreach ($dm->result() as $d): $i++;?>
                    <tr>
                        <td><?=$i?></td>
                        <td><?=$d->modul_nama?></td>
                        <td><?=$d->modul_url?></td>
                        <td class="center"><a href="#" class="edit" uid ="<?=$d->modul_id?>">Edit</a> &nbsp; <a href="<?=cur_url()?>delete/<?=$d->modul_id?>" class="delete">Delete</a></td>
                    </tr>
                    <?endforeach ?>
                </tbody>
            </table>
 		</div>
 	</div>
</div>