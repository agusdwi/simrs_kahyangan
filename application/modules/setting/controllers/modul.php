<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modul extends CI_Controller {

	public function index(){
		$data['main_view']	= 'modul/index';
		$data['title']		= 'Manajemen Group Modul ';
		$data['desc']		= 'Manajemen Group Modul. ';
		$data['dm']			= $this->db->get_where('com_modul',array('status'=>1));
		$this->load->view('template',$data);
	}
 
	function create(){
		$data = $this->input->post('ds');
		$mdl 	= str_replace(' ','_', $data['modul_nama']);
		$data['modul_url'] = $mdl;
		$data['modul_icon'] = $this->do_upload();
		$this->create_modul($data['modul_url']);
		$this->db->insert('com_modul', $data); 
		
		$this->session->set_flashdata('message',array('success','Modul berhasil di buat'));
		redirect(cur_url(-1));
	}

	function do_upload()
	{
			$config = array(
				'allowed_types' => 'jpg|png|jpeg|bmp|gif',
				'upload_path' 	=> realpath('./assets/images/icons/'),
				'encrypt_name'  => true);

			$this->load->library('upload', $config);
			$this->upload->initialize($config);				
			if (!$this->upload->do_upload('filename'))
			{
				$error = array('error' => $this->upload->display_errors());
			}
			
			$nama = $this->upload->data();
			$config2 = array(
				'image_library' =>'gd2' , 
				'source_image' => $nama['full_path'],
				'width' => 16,
				'height' => 13
				);
			$this->load->library('image_lib', $config2); 
			$this->image_lib->resize();
			return $nama['file_name'];
	}

	function update($id){
		$data = $this->input->post('ds');	
		$this->db->where('modul_id', $id);
		$this->db->update('com_modul', $data); 
		$this->session->set_flashdata('message',array('success','Modul berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('modul_id', $id);
		$this->db->update('com_modul', $data);
		$this->session->set_flashdata('message',array('success','Modul berhasil dihapus'));
		redirect(cur_url(-2));
	}

	function create_modul($mdl){

		// create directory
		mkdir("application/modules/$mdl", 0777);
		mkdir("application/modules/$mdl/controllers", 0777);
		mkdir("application/modules/$mdl/views", 0777);
		
		//baca def_controller
		$myFile = "files/template/def_controller.txt";
		$fh = fopen($myFile, 'r');
		$theData = fread($fh, filesize($myFile));
		$theData = str_replace('user_define_controller',ucfirst($mdl),$theData);	
		fclose($fh);		
		
		// create file controller
		$ourFileName = "application/modules/$mdl/controllers/$mdl.php";
		$ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
		fwrite($ourFileHandle, $theData);
		fclose($ourFileHandle);
		
		// create view
		$ourFileName = "application/modules/$mdl/views/index.php";
		$ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
		fwrite($ourFileHandle, "standart view of controller $mdl");
		fclose($ourFileHandle);
	}
}