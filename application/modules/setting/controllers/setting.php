<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function index(){
		$data['main_view']	= 'index';
		$data['title']		= 'Setting ';
		$data['desc']		= 'Modul setting. ';
		$data['dm']			= $this->db->get_where('com_modul',array('status'=>1));
		$this->load->view('template',$data);
	}
}