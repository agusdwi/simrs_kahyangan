<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Com_user extends CI_Controller {

	public function index(){
		$data['main_view']	= 'com_user/index';
		$data['title']		= 'Manajemen User ';
		$data['desc']		= 'Manajemen User. ';
		$data['ds']			= $this->db->get_where('com_user',array('status'=>1))->result();
		$data['dt']			= $this->db->get_where('com_user_tipe',array('status'=>1))->result();
		$this->load->view('template',$data);
	}

	function create(){
		$data = $this->input->post('ds');
		$this->db->insert('com_modul', $data); 
		$this->session->set_flashdata('message',array('success','Modul berhasil di buat'));
		redirect(cur_url(-1));
	}

	function update($id){
		$data = $this->input->post('ds');	
		$this->db->where('modul_id', $id);
		$this->db->update('com_modul', $data); 
		$this->session->set_flashdata('message',array('success','Modul berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('modul_id', $id);
		$this->db->update('com_modul', $data);
		$this->session->set_flashdata('message',array('success','Modul berhasil dihapus'));
		redirect(cur_url(-2));
	}
}