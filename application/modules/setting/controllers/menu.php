<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function index(){
		$data['main_view']	= 'menu/index';
		$data['title']		= 'Manajemen Group Menu ';
		$data['desc']		= 'Manajemen Group Menu. ';
		$data['dm']			= $this->db->get_where('com_modul',array('status'=>1));
		$data['dt']			= $this->db->get_where('sys_menu',array('menu_status'=>1));
		$this->load->view('template',$data);
	}

	function create(){
		$data 	= $this->input->post('ds');
		$mod 	= db_conv($this->db->get_where('com_modul',array('modul_id'=>$data['modul_id'])));
		$mdl 	= str_replace(' ','_', $mod->modul_nama);
		$menu 	= str_replace(' ','_', $data['menu_name']);
		$data['menu_url'] 	 = $mdl."/".$menu;
		$this->create_menu($mdl,$menu);
		$this->db->insert('sys_menu', $data); 
		$this->session->set_flashdata('message',array('success','Menu berhasil di buat'));
		redirect(cur_url(-1));
	}

	function update($id){
		$data = $this->input->post('ds');	
		$this->db->where('modul_id', $id);
		$this->db->update('com_modul', $data); 
		$this->session->set_flashdata('message',array('success','Menu berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('modul_id', $id);
		$this->db->update('com_modul', $data);
		$this->session->set_flashdata('message',array('success','Menu berhasil dihapus'));
		redirect(cur_url(-2));
	}

	function create_menu($mdl, $menu){
		// create directory
		mkdir("application/modules/$mdl", 0777);
		mkdir("application/modules/$mdl/controllers", 0777);
		mkdir("application/modules/$mdl/views/$menu", 0777);
		
		// baca def_controller
		$myFile = "files/template/def_menu_controller.txt";
		$fh = fopen($myFile, 'r');
		$theData = fread($fh, filesize($myFile));
		$theData = str_replace($mdl,$menu,$theData);	
		$theData = str_replace('user_define_controller',ucfirst($menu),$theData);
		$theData = str_replace('menu',$menu,$theData);
		fclose($fh);		
		
		// create file controller
		$ourFileName = "application/modules/$mdl/controllers/$menu.php";
		$ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
		fwrite($ourFileHandle, $theData);
		fclose($ourFileHandle);
		
		// create view
		$ourFileName = "application/modules/$mdl/views/$menu/index.php";
		$ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
		fwrite($ourFileHandle, "standart view of controller $mdl");
		fclose($ourFileHandle);
	}
}