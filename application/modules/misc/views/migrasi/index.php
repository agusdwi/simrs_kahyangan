<style type="text/css">
	select:focus
	{ 
		background-color:yellow;
	}
	.chosen-row{
		background: rgba(243,219,107,0.4);
	}
	.not-suggest{
		background: rgba(158,39,46,0.4);
	}
	.database{
		background: rgba(0,123,202,0.3);
	}
</style>
<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5">
			<div class="title">
				<h3>Migrasi Pasien</h3>
			</div>
		</div>
	</div>
	<div style="width:200px">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;keterangan : 
		<ul>
			<li class="database">data tersimpan</li>
			<li class="chosen-row">terdeteksi dari alamat</li>
			<li class="not-suggest">tidak terdeteksi</li>
		</ul>
	</div>
	<?=form_open('',array('id'=>'bigsearch-form'))?>
		<div class="row-fluid">
			<div class="widgetbox">
				<div id="big-container">
					<div class="span12" style="padding:15px" id="bigsearch">
						<a href="<?=base_url()?>misc/migrasi/simpan" class="btn btn-success pull-left save-bulk">
							Simpan Perubahan
						</a>
						<div class="pagin">
							<?=$pagin;?>
						</div>
						<br>
						<table class="table std table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Pasien</th>
									<th>Alamat</th>
									<th>Provinsi</th>
									<th>Kab/kota</th>
								</tr>
							</thead>
							<tbody>
								<?$i=$start;foreach ($ds->result() as $key): $i++;?>
								<tr data-suggest='<?=get_sugest($key->sd_address)?>' class="parent-row <?=$key->sd_reg_prov != ''? 'database' : 'chosen-row' ;?>">
									<td class="tb_no"><?=$i;?></td>
									<td><?=$key->sd_name;?></td>
									<td><?=$key->sd_address;?></td>
									<td>
										<select 
											style="width:200px" 
											name="ds[<?=$key->sd_rekmed?>][sd_reg_prov]" 
											data-prov = <?=$key->sd_reg_prov;?> 
											data-kota = <?=$key->sd_reg_kab;?>
											class="provinsi <?=$key->sd_reg_prov != ''? 'chosen' : 'not-chosen' ;?>"
											>
												<option value="">pilih provinsi</option>
												<?foreach ($province->result() as $k): ?>
													<option 
															<?=$key->sd_reg_prov == $k->mpr_id ? 'selected="selected"' : '' ;?> 
															value="<?=$k->mpr_id?>">
																<?=$k->mpr_name?>
													</option>
												<?endforeach ?>
										</select>
									</td>
									<td>
										<select style="width:250px"  name="ds[<?=$key->sd_rekmed?>][sd_reg_kab]" class="kota"></select>
									</td>
								</tr>
								<?endforeach;?>
							</tbody>
						</table>
						<a href="<?=base_url()?>misc/migrasi/simpan" class="btn btn-success pull-left save-bulk">
							Simpan Perubahan
						</a>
						<div class="pagin">
							<?=$pagin;?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	$(function(){
		var link = '<?=cur_url()?>';
		$('body').on('click','.pagin a',function (e) {
			e.preventDefault();
			link = $(this).attr('href');
			history.pushState(null, null, link);
			$("#big-container").load(link+" #bigsearch",function(){
				set_prov();
			});
		});
		$('body').on('click','.save-bulk',function (e) {
			e.preventDefault();
			var url = $(this).attr('href');
			$.post(url, $("#bigsearch-form").serialize(),function(){
				alert('berhasil disimpan');
				$("#big-container").load(link+" #bigsearch",function(){
					set_prov();
				});	
			});
		});
		$(".provinsi:first").focus();

		$('body').on('change','.provinsi',function (e) {
			e.preventDefault();
			var kota = $(this).parent().parent().find('.kota');
			kota.html('');
			var html = "";
			$.each(str_kota[$(this).val()], function( index, value ) {
				html += '<option value="'+value[0]+'">'+value[1]+'</option>';
			});
			kota.append(html);
		});

		set_prov();
	})

	<?
		$str = array();
		foreach ($regency->result() as $key) {
			$str[$key->mpr_id][] = array($key->mre_id,$key->mre_name);
		}
		$str_kota = json_encode($str);
	?>
	var str_kota = <?=$str_kota?>;
	var rmLst   = "";

	function set_prov(){
		$("select.chosen").each(function(index) {
			$(this).trigger('change');
			$(this).parent().parent().find('.kota').val($(this).data('kota'));
		});
		$("select.not-chosen").each(function(index) {
			$(this).val('14')
				.trigger('change');
			var sgt = $(this).parents('.parent-row').data('suggest');
			var kota = $(this).parents('.parent-row').find('.kota');
			if (sgt == 'bantul') {
				kota.val(200);
			}else if(sgt == 'sleman'){
				kota.val(202);
			}else if(sgt == 'yogyakarta'){
				kota.val(203);
			}else{
				kota.val(200);
				$(this).parents('.parent-row').addClass('not-suggest');
			}
		});
		$(".provinsi:first").focus();
	}
</script>

<?
	function get_sugest($string){
		$string = strtolower($string);
		if (stripos($string, "sleman") !== false) {
			return "sleman";
		}else if (stripos($string, "bantul") !== false) {
			return "bantul";
		}else if (stripos($string, "yogyakarta") !== false) {
			return "yogyakarta";
		}else if (stripos($string, "kulon progo") !== false) {
			return 'kulon progo';
		}else if (stripos($string, "gunung kidul") !== false) {
			return 'gunung kidul';
		}
	}
?>