<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Migrasi extends CI_Controller {
	function __construct() {
		parent::__construct();
	}

	public function index($page=0) {
		$per_page = 50;
		$all = $this->db->get_where('ptn_social_data',array('sd_status'=>1));

		$this->load->library('pagination');
		
		$config['base_url'] 	= base_url('misc/migrasi/index');
		$config['total_rows'] 	= $all->num_rows();
		$config['per_page'] 	= $per_page; 
		$config['uri_segment'] 	= 4;
		$this->pagination->initialize($config); 
		
		
		$data['regency']	= $this->db->get('mst_regency');
		$data['province']	= $this->db->get('mst_province');	
		$data['main_view'] 	= 'migrasi/index';
		$data['pagin'] 		= $this->pagination->create_links();
		$data['ds'] 		= $this->db->limit($per_page,$page)->get_where('ptn_social_data',array('sd_status'=>1));
		$data['start'] 		= $page;
		$this->load->view('template', $data);
	}

	function simpan(){
		$data = $this->input->post();
		foreach ($data['ds'] as $key => $value) {
			$data = array(
			               'sd_reg_prov' 	=>  $value['sd_reg_prov'],
			               'sd_reg_kab' 	=>  $value['sd_reg_kab']
			            );
			$this->db->where('sd_rekmed', $key);
			$this->db->update('ptn_social_data', $data);
		}
	}
}