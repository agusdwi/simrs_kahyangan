<style type="text/css" media="screen">
	.frm_search input {
		margin-top: -1px;
		float: right;
		margin-left: 2px
	}
	.frm_search .btn {
		float: right;
		margin-left: 5px
	}
	.frm_search .btn span {
		padding: 2px 10px;
	}
	.search_choice a {
		font-weight: bold
	}
	.search_choice a.active {
		color: #fb9338
	}
	.search_choice {
		float: right;
		margin-right: 95px
	}
	#advance {
		display: none
	}
	#filter {
		border-bottom: 1px dashed #DDD;
	}
	#body_search {
		margin-top: 10px
	}
	.tname {
		font-size: 110%
	}
	#dyntable tbody tr td {
		border-right: none
	}
	#dyntable tbody tr td + td + td + td + td {
		border-right: 1px solid #DDD
	}
	#dyntable tr:nth-child(even) {
		background: #F7F7F7;
	}
	.dataTables_scrollHead {
		margin-bottom: -22px;
	}
	.dataTables_info {
		margin-top: 20px;
	}
</style>
<script type="text/javascript" charset="utf-8">
		$(function(){
	var d_uri = "<?=base_url() ?>master/dokter/dt_doctor";
		oTb = $('#tb_dokter').dataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"bLengthChange": false,
		"bFilter": true,
		"sPaginationType": "full_numbers",
		"aoColumns": [null,null,null],
		"sAjaxSource": d_uri,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
		var newArray = $.merge(aoData, [{ "name": "<?=$this -> security -> get_csrf_token_name() ?>", "value": "<?=$this -> security -> get_csrf_hash() ?>" }]);
		$.ajax( {
		"dataType": 'json',
		"type": "POST",
		"url": sSource,
		"data": aoData,
		"success": fnCallback
		} );
		}
		});
		$("#tb_dokter_filter").hide();
		$(".chatsearch input").keyup(function(e){
		$("#tb_dokter_filter input").val($(".chatsearch input").val()).trigger('keyup');
		})
		// buat init sort
		oTb.fnSort( [ [0,'desc']] );
		$('.modals').click(function(){
		url = "<?=base_url() ?>master/dokter/get_pop";
		$.get(url,function(data){
			$('.modal-body').children().remove();
			$('.modal-body').append(data);
		});
	})
		})
</script>

<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?><a href="#tbhmodal" class="btn btn-success pull-right modals" data-toggle="modal" ><i class="icon-plus-sign icon-white"></i> Tambah dokter</a></h1>
	<div id="tbhmodal" class="modal hide">
		<div class="modal-header">
			<button data-dismiss="modal" class="close" type="button">
				x
			</button>
			<h3>Tambah Data Dokter</h3>
		</div>
		<div class="modal-body">

		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5" >
			<div class="title">
				<h3>Data Dokter</h3>
			</div>
		</div>
		<div class="span5" style="float:right;">
			<div class="widgetbox" style="margin-top:20px;">
				<?=form_open('', array('class' => 'frm_search')); ?>
				<div id="basic">
					<div class="chatsearch" >
						<input type="text" name="" placeholder="Search" style="width:91%;margin:auto;">
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12">
				<table id='tb_dokter' class="table table-bordered">
					<thead>
						<th width="70px">ID Dokter</th>
						<th>Nama dokter</th>
						<th width="120px">Aksi</th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
