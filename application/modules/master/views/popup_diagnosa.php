<script src="<?=base_url() ?>assets/js/valid.js"></script>
<link rel="stylesheet" href="<?=base_url() ?>assets/css/jquery.gritter.css" />
<script type="text/javascript" src='<?=base_url() ?>assets/js/jquery.gritter.min.js'></script>
<script>
	$(function() {
		$(".add_doctor").validate({
			rules : {
				'nama icd' : "required",
				'nama diagnosa':"required",
			},
			Message : {
				'nama icd' : "Isikan nama icd terlebih dahulu",
				'nama diagnosa' : "Isikan nama diagnosa terlebih dahulu",
			},
			submitHandler : function(form) {
				$.ajax({
					url : 'diagnosa/add_diagnosa',
					type : "POST",
					crossDomain : true,
					data : $('.add_diagnosa').serialize(),
					dataType : "json",
					success : function(data) {
						$('.close').click();
						$.gritter.add({
							title : 'Success',
							text : 'Data berhasil disimpan',
							sticky : true
						});
						tbldokter.fnDraw();

					},
					error : function() {
						$('.error_submit').html('terjadi kesalahan saat menyimpan data, mohon ulangi sekali lagi ');
					}
				});
				return false;
			},

			errorPlacement : function(error, element) {
				error.appendTo(element.parents(".controls"));
			}
		});
	})
</script>
<style type="text/css" media="screen">
	.alert {
		background-color: transparent;
		border: 0px;
	}

	#gritter-notice-wrapper {
		right: 13%;
		top: 100px;
	}
	.form-horizontal .controls {
		padding: 5px 0;
	}
</style>

<div id="gritter-notice-wrapper" class="alert hide" style="width:750px;position:fixed">
	<div id="gritter-item-1" class="gritter-item-wrapper" style="margin:0 -17px 5px 0">
		<div class="gritter-top"></div>
		<div class="gritter-item">
			<div class="gritter-close" style="display: none; width:50px "></div>
			<img src="<?=base_url() ?>assets/img/demo/envelope.png" class="gritter-image">
			<div class="gritter-with-image" style="width:448px">
				<span class="gritter-title" style="margin-left:36px">Message</span>
				<p>
					Data Berhasil Disimpan
				</p>
			</div>
			<div style="clear:both"></div>
		</div>
		<div class="gritter-bottom"></div>
	</div>
</div>

<?=form_open(cur_url() . 'add_diagnosa', array('class' => 'form-horizontal add_diagnosa stdform', 'id' => 'add_schdl2 form basic_validate', 'name' => 'add_schdl2', 'novalidate' => "novalidate")); ?>
<div id="advances" style="margin-top:10px;">
	<div class='span5' >
		<div class="control-group" style="border:none;">
			<label class="control-label" style="width:30%;">Nama diagnosis ICD</label>
			<div class="controls" >
				<input type="text" name="namaicd" />
			</div>
		</div>
		<div class="control-group" style="border:none;">
			<label class="control-label" style="width:30%;">Nama diagnosis</label>
			<div class="controls" >
				<input type="text" name="namadiag" />
			</div>
		</div>
		<br clear="all">
		<label class='error_submit' style='background: url("../assets/img/caution.png") no-repeat 2px 4px;
		padding-left: 19px;
		font-weight: bold;
		color: #C00 !important;
		clear: both;'></label>
		<input type="submit" value="Simpan" style='float:right' class="btn tambah_schdl btn-success">
	</div>
</div>
</form>