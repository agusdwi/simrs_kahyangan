<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5" >
			<div class="title">
				<h3>Edit Data Diagnosa</h3>
			</div>
		</div>
	</div>
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12">
				<form action="" method="post">
				<table>
					<tr>
						<td><b>ID Diagnosa</b></td>
						<td><?=$diagnosa->diag_id?></td>
					</tr>
					<tr>
						<td><b>ICD Diagnosa</b></td>
						<td><input type="text" value="<?=$diagnosa->diag_icd?>" /></td>
					</tr>
					<tr>
						<td><b>Nama Diagnosa</b></td>
						<td><input type="text" value="<?=$diagnosa->diag_name?>" /></td>
					</tr>
				</table>
				<button type="submit" class="btn btn-warning">Simpan</button>
				</form>
			</div>
		</div>
	</div>
</div>
