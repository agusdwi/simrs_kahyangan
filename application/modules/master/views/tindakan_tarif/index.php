<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5">
			<div class="title">
				<h3>Tarif Tindakan</h3>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12" style="padding:15px" id="bigsearch">
				<table class="table std100 table-bordered data-table">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Tindakan</th>
							<?foreach ($cls->result() as $key): ?>
							<th><?=$key->cls_name;?></th>	
							<?endforeach;?>
						</tr>
					</thead>
					<tbody>
						<?$i=0;foreach ($ds->result() as $key): $i++;?>
						<tr>
							<td><?=$i;?></td>
							<td>
								<?=$key->treat_name;?>
								<a class="label label-info onhover" href="<?=cur_url().'proses/'.$key->treat_id;?>">edit</a>
							</td>	
							<?foreach ($cls->result() as $k): ?>
								<td class="text-right"><?=get_ds($key->treat_id,$k->cls_id,$tarif);?></td>	
							<?endforeach;?>
						</tr>
						<?endforeach;?>
					</tbody>
				</table>  
			</div>
		</div>
	</div>
</div>

<?
	function get_ds($m,$i,$ds){
		$sum = '';
		if (isset($ds[$m][$i])) {
			foreach ($ds[$m][$i] as $key) {
				$sum += array_sum($key);
			}
		}
		return $sum;
	}
?>