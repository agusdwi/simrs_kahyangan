<script type="text/javascript">
	var irow = 1;
</script>
<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5">
			<div class="title">
				<h3>Tarif Tindakan</h3>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12" style="padding:15px" id="bigsearch">
				<div class="well">
					<b>Nama tindakan :</b><br>
					<h4><?=$ds->treat_name;?>, (<?=$ds->treat_code;?>)</h4>
				</div>
				<?=form_open()?>
				<b>Komponen Harga</b>
				<table class="table table-bordered data-table tb-fixed" id="tb_main">
					<col width="270px" />
					<thead>
						<tr>
							<th>Role</th>
							<?$tot=0;foreach ($cls->result() as $key): $tot++;?>
								<th><?=$key->cls_name;?></th>	
							<?endforeach;?>
						</tr>
					</thead>
					<tbody>
						<?$i=0;foreach ($tarif as $k => $val):?>
							<?foreach ($val as $k3 => $v3): $i++;?>
								<tr>
									<td>
										<select name="r[<?=$i;?>]">
											<?foreach ($role->result() as $key): ?>
												<option value="<?=$key->role_id;?>" 
													<?=(($key->role_id == $k3)?'selected="selected"':'');?>>
														<?=$key->role_name;?>
												</option>
											<?endforeach;?>
										</select>
										<?if ($i>1): ?>
											<a class="label label-important onhover del" href="#">x</a>
										<?endif;?>
									</td>
									<?foreach ($cls->result() as $k2): ?>
										<td class="text-center">
											<input name="t[<?=$i;?>][<?=$k2->cls_id;?>]" class="tx_small" type="text" value="<?=(isset($v3[$k2->cls_id])?$v3[$k2->cls_id]:'');?>">
										</td>	
									<?endforeach;?>
								</tr>
							<?endforeach;?>
						<?endforeach;?>
						<?if (empty($tarif)): ?>
							<?$i=1;?>
							<tr>
								<td>
									<select name="r[1]">
										<?foreach ($role->result() as $key): ?>
										<option value="<?=$key->role_id;?>"><?=$key->role_name;?></option>
										<?endforeach;?>
									</select>
								</td>
								<?foreach ($cls->result() as $k): ?>
								<td class="text-center">
									<input class="tx_small" type="text" name="t[1][<?=$k->cls_id;?>]">
								</td>	
								<?endforeach;?>
							</tr>
						<?endif;?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="<?=$tot+1;?>">
								<center><a href="" id="tambah_row" class="label label-info">tambah</a></center>
							</td>
						</tr>
					</tfoot>
				</table>  
				<div class="form-actions" style="margin-top: 10px;margin-bottom: 0px;">
					<button type="submit" name="savenext" style="float:right;margin-left:5px" class="btn btn-info">Simpan</button>
					<a style="float:right;margin-left:5px" class="btn btn-danger" href="<?=cur_url('-2');?>">kembali</a>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?
	function get_ds($m,$i,$ds){
		return (isset($ds[$m][$i]))? $ds[$m][$i] : '';
	}
?>

<table class="hide" id="pre">
	<tr>
		<td>
			<select class="pre_s">
				<?foreach ($role->result() as $key): ?>
					<option value="<?=$key->role_id;?>"><?=$key->role_name;?></option>
				<?endforeach;?>
			</select>
			<a class="label label-important onhover del" href="#">x</a>
		</td>
		<?foreach ($cls->result() as $k): ?>
		<td class="text-center">
			<input class="tx_small pre_i" type="text" cls_id='<?=$k->cls_id;?>'>
		</td>	
		<?endforeach;?>
	</tr>
</table>

<script type="text/javascript">
	$(function(){
		irow += <?=$i;?>;
		$("body").on('click','#tambah_row',function(e){
			e.preventDefault();
			$("#pre tbody").find('.pre_s').attr('name','r['+irow+']');
			var a = $("#pre tbody .pre_i");
			$.each(a,function(i,v){
				$(v).attr('name','t['+irow+']['+$(v).attr('cls_id')+']')
			})
			$("#tb_main tbody").append($("#pre tbody").html());
			irow++;
		})
		$("body").on('click','.del',function(e){
			e.preventDefault();
			$(this).parent().parent().remove();
		})
	})	
</script>