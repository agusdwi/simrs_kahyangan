<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5" >
			<div class="title">
				<h3>Edit Data Dokter</h3>
			</div>
		</div>
	</div>
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12">
				<input type="hidden" class='crsf' name="<?php echo $this->security->get_csrf_token_name()?>" value="<?php echo $this->security->get_csrf_hash()?>" >
				<?=form_open('master/dokter/update/'.$dokter->dr_id,array('class' => 'form-horizontal frm_search stdform','id' => 'form')); ?>
				<table>
					<tr>
						<td><b>ID Dokter</b></td>
						<td><?=$dokter->dr_id?></td>
					</tr>
					<tr>
						<td><b>Nama Dokter</b></td>
						<td><input type="text" value="<?=$dokter->dr_name?>" name="nama" /></td>
					</tr>
				</table>
				<button type="submit" class="btn btn-warning">Simpan</button>
				</form>
			</div>
		</div>
	</div>
</div>
