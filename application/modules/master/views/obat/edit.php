<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/obat" class="btn btn-success pull-right"><i class="icon-chevron-left icon-white"></i> Daftar Obat</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-align-justify"></i>                                  
					</span>
					<h5>Form Obat</h5>
				</div>
				<div class="widget-content nopadding">
					<?=form_open('',array('class' => 'form-horizontal form','id' => 'form_obat')); ?>
						<div class="control-group">
							<label class="control-label">Kode Obat *</label>
							<div class="controls">
								<input class="mini" type="text" id="mdcn_code" name="ds[mdcn_code]" value="<?=$ds->mdcn_code;?>"  autofocus="autofocus">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama Obat *</label>
							<div class="controls">
								<input class="medium" type="text" id="mdcn_name" name="ds[mdcn_name]" value="<?=$ds->mdcn_name;?>">
							</div>
						</div>
						<div class="form-actions">
							<button id="reset" class="btn btn-warning" type="reset">Reset</button>
							<button class="btn btn-primary" id="next" type="submit">Simpan</button>
						</div>
					<?=form_close()?>
				</div>
			</div>                      
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#form_obat").validate({
			rules: {
				'ds[mdcn_code]': "required",
				'ds[mdcn_name]': "required"
			},
			submitHandler: function(form) {
				var url  = jQuery(form).attr('action')+"/cek";
				var data = jQuery(form).serialize();
				$.post(url,data, function(data) {
					if (data.result == "true") {
						form.submit();
					}else{
						alert('kode sudah digunakan, silahkan pilih yang lain');
						$("#mdcn_code").focus();
						return false;
					}
				},'json');
			}
		});
	})
</script>