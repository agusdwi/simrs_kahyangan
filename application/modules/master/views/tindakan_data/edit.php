<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/tindakan_data" class="btn btn-success pull-right"><i class="icon-chevron-left icon-white"></i> Daftar Tindakan</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-align-justify"></i>                                  
					</span>
					<h5>Form Tindakan</h5>
				</div>
				<div class="widget-content nopadding">
					<?=form_open('',array('class' => 'form-horizontal form','id' => 'form_tindakan')); ?>
						<div class="control-group">
							<label class="control-label">Kode Tindakan *</label>
							<div class="controls">
								<input class="small" type="text" id="treat_code" name="ds[treat_code]" autofocus="autofocus" value="<?=$ds->treat_code;?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama Tindakan *</label>
							<div class="controls">
								<input class="medium" type="text" id="treat_name" name="ds[treat_name]" value="<?=$ds->treat_name;?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Jenis Tindakan *</label>
							<div class="controls">
								<select name="ds[treat_jenis]">
									<option <?=($ds->treat_jenis == 'OK (kamar operasi)')? 'selected':'';?>>OK (kamar operasi)</option>
									<option <?=($ds->treat_jenis == 'VK (kamar persalinan)')? 'selected':'';?>>VK (kamar persalinan)</option>
								</select>
							</div>
						</div>
						<div class="form-actions">
							<button id="reset" class="btn btn-warning" type="reset">Reset</button>
							<button class="btn btn-primary" id="next" type="submit">Simpan</button>
						</div>
					<?=form_close()?>
				</div>
			</div>                      
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#form_tindakan").validate({
			rules: {
				'ds[treat_code]': "required",
				'ds[treat_name]': "required"
			}
		});
	})
</script>