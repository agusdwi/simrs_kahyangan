<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/diagnosa" class="btn btn-success pull-right"><i class="icon-chevron-left icon-white"></i> Daftar Diagnosa</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-align-justify"></i>                                  
					</span>
					<h5>Form Diagnosa</h5>
				</div>
				<div class="widget-content nopadding">
					<?=form_open('',array('class' => 'form-horizontal form','id' => 'form_dokter')); ?>
						<?$prt = 0;if (!empty($item)): $prt=$item->diag_id?>
							<div class="control-group">
								<b style="margin:20px;display:block">Data Master : (<?=$item->diag_code;?>)<?=$item->diag_name;?></b>
							</div>
						<?endif;?>
						<input type="hidden" name="ds[diag_parent]" value="<?=$prt;?>">
						<div class="control-group">
							<label class="control-label">Kode ICD10 *</label>
							<div class="controls">
								<input class="small" type="text" id="diag_code" name="ds[diag_code]" value="" autofocus="autofocus">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama ICD10 *</label>
							<div class="controls">
								<input class="medium" type="text" id="diag_name" name="ds[diag_name]" value="">
							</div>
						</div>
						<div class="form-actions">
							<button id="reset" class="btn btn-warning" type="reset">Reset</button>
							<button class="btn btn-primary" id="next" type="submit">Simpan</button>
						</div>
					<?=form_close()?>
				</div>
			</div>                      
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#form_dokter").validate({
			rules: {
				'ds[diag_code]': "required",
				'ds[diag_name]': "required"
			}
		});
	})
</script>