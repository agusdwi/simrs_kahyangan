<script type="text/javascript" src="<?=base_url()?>assets/js/plugins/dyna/src/jquery.dynatree.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/plugins/dyna/src/skin/ui.dynatree.css">
<style type="text/css">
.error{
	color: red;
}
</style>
<script type="text/javascript">
var load = 0;
var dyna_params = {};
var csrf_name = "<?=$this->security->get_csrf_token_name();?>"
var csrf_hash = "<?=$this->security->get_csrf_hash();?>"
var selected="";
$(function(){
	$("#tree").dynatree({
		onLazyRead: function(node){
			$.ajax({
				url: BASE+"master/diagnosa/get/"+node.data.key,
				dataType: 'json',
				success: function(data, textStatus){
					res = [];
					$.each(data,function(i,v){
						res.push({title: v.title, key: v.key, isFolder: v.isFolder, isLazy: v.isLazy});
					})
					node.setLazyNodeStatus(DTNodeStatus_Ok);
					node.addChild(res);
				}
			});
		},onActivate: function(node) {
			pilih(node.data);
		},

		children: <?=$a;?>
	});

	$("#a").click(function(){
		load += 10;
		var rootNode = $("#tree").dynatree("getRoot");
		$.ajax({
			url: BASE+"master/diagnosa/ajaxGetInit/"+load,
			dataType: 'json',
			success: function(data, textStatus){
				res = [];
				$.each(data,function(i,v){
					res.push({title: v.title, key: v.key, isFolder: v.isFolder, isLazy: v.isLazy});
				})
				rootNode.addChild(res);
			}
		});
	});
	$("#reset").click(function(){
		$("#tree").dynatree("getTree").reload();
	})

	$("#q").keyup(function(e){
		$(".error").html('');
		var qq = $(this).val();
		if(e.which == $.ui.keyCode.ENTER){
			if($(this).val().length > 2){
				var rootNode = $("#tree").dynatree("getRoot");
				rootNode.removeChildren();        
				dyna_params['q'] = qq;
				dyna_params[csrf_name] = csrf_hash;
				$.ajax({
					url: BASE+"master/diagnosa/search/",
					dataType: 'json',
					type: 'post',
					data:dyna_params,
					success: function(data, textStatus){
						res = [];
						$.each(data,function(i,v){
							res.push({title: v.title, key: v.key, isFolder: v.isFolder, isLazy: v.isLazy});
						})
						rootNode.addChild(res);
					}
				});
			}else{
				$(".error").html('please provide at least 3 char');
			}
		}	
	})

	$('body').on('click','#add',function (e) {
		var url = $(this).attr('href');
		$(this).attr('href',url + selected);
	});

	$('body').on('click','#edit',function (e) {
		var url = $(this).attr('href');
		if (selected == "") {
			alert('Silahkan pilih diagnosa yang akan di edit');
			e.preventDefault();
		};
		$(this).attr('href',url + selected);
	});

	$('body').on('click','#delete',function (e) {
		var url = $(this).attr('url');
		if (selected == "") {
			alert('Silahkan pilih diagnosa yang akan di edit');
			e.preventDefault();
		}else{
			if (confirm('apakah anda akan menghapus data ini \nData yang ada dibawahnya akan ikut terhapus')) {
				window.location = url + selected;	
			};
		}
	});
})

function pilih(obj){
	selected = obj.key;
}
</script>
<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5" >
			<div class="title">
				<h3>Data Diagnosa</h3>
			</div>
		</div>
		<div class="span5" style="float:right;">
			<div class="widgetbox" style="margin-top:20px;">
				<div id="basic">
					<div class="chatsearch" >
						<input type="text" id="q" name="" placeholder="Search" style="width:91%;margin:auto;">
					</div>
					<p class="error"></p>
				</div>
		</div>
	</div>
</div>
<br clear="all">
<div class="row-fluid">
	<div class="widgetbox">
		<div class="span12">
			<div style="float:right">
				<a id="add" href="<?=base_url()?>master/diagnosa/add/">
					<span class="label label-success">add</span>
				</a>
				<a id="edit" href="<?=base_url()?>master/diagnosa/edit/">
					<span class="label label-info">edit</span>
				</a>
				<a id="delete" style="cursor:pointer" url="<?=base_url()?>master/diagnosa/delete/">
					<span class="label label-important">delete</span>
				</a>
			</div>
			<p>*Pilih diagnosa untuk tambah detail / edit / delete</p>
			<div id="tree">

			</div>
			<br>
			<button id="a">loadmore</button>
		</div>
	</div>
</div>