<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5">
			<div class="title">
				<h3>Update Tarif Obat</h3>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12" style="padding:15px" id="bigsearch">
				<?=form_open()?>
					<table class="table std100 table-bordered data-table">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama Obat</th>
								<?foreach ($ins->result() as $key): ?>
									<th><?=$key->ins_name;?></th>	
								<?endforeach;?>
							</tr>
						</thead>
						<tbody>
							<?$i=0;foreach ($mdcn->result() as $key): $i++;?>
								<tr>
									<td><?=$i;?></td>
									<td>(<?=$key->mdcn_code;?>) <?=$key->mdcn_name;?></td>	
									<?foreach ($ins->result() as $k): ?>
										<td>
											<input type="text" class="tx_small" name="prc[<?=$key->mdcn_id?>][<?=$k->ins_id?>]"
												value="<?=get_ds($key->mdcn_id,$k->ins_id,$ds);?>"
											>
										</td>	
									<?endforeach;?>
								</tr>
							<?endforeach;?>
						</tbody>
					</table>  
					<div class="form-actions" style="margin-top: 10px;margin-bottom: 0px;">
						<button type="submit" name="savenext" style="float:right;margin-left:5px" class="btn btn-info">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?
	function get_ds($m,$i,$ds){
		return (isset($ds[$m][$i]))? $ds[$m][$i] : '';
	}
?>