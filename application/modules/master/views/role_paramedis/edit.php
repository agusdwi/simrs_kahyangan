<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/role_paramedis" class="btn btn-success pull-right"><i class="icon-chevron-left icon-white"></i> Daftar Spesialis Paramedis</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-align-justify"></i>                                  
					</span>
					<h5>Form Spesialis Paramedis</h5>
				</div>
				<div class="widget-content nopadding">
					<?=form_open('',array('class' => 'form-horizontal form','id' => 'form_role_paramedis')); ?>
						<div class="control-group">
							<label class="control-label">Nama Spesialis Paramedis *</label>
							<div class="controls">
								<input class="medium" type="text" id="role_name" name="ds[role_name]" value="<?=$ds->role_name;?>" autofocus="autofocus">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Jenis Paramedis *</label>
							<div class="controls">
								<select id="jenis_id" name="ds[jenis_id]">
									<? $jp = $this->db->get_where('mst_jenis_paramedis'); ?>
									<? foreach($jp->result() as $j){ ?>
									<option value="<?=$j->id;?>"<?=($j->id==$ds->jenis_id) ? ' selected' : '';?>><?=$j->nama;?></option>
									<? } ?>
								</select>
							</div>
						</div>
						<div class="form-actions">
							<button id="reset" class="btn btn-warning" type="reset">Reset</button>
							<button class="btn btn-primary" id="next" type="submit">Simpan</button>
						</div>
					<?=form_close()?>
				</div>
			</div>                      
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#form_role_paramedis").validate({
			rules: {
				'ds[role_name]': "required",
				'ds[jenis_id]': "required"
			}
		});
	})
</script>