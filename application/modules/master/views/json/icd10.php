<html>
<head>
    <title></title>
    <script type="text/javascript" src="<?=base_url()?>assets/js/plugins/dyna/jquery/jquery.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/plugins/dyna/jquery/jquery-ui.custom.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/plugins/dyna/src/jquery.dynatree.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/plugins/dyna/src/skin/ui.dynatree.css">
    <style type="text/css">
        .error{
            color: red;
        }
    </style>
    <script type="text/javascript">
        var load = 0;
        var csrf_name = "<?=$this->security->get_csrf_token_name();?>"
        var csrf_hash = "<?=$this->security->get_csrf_hash();?>"
        var dyna_params = {};
        var BASE = "<?=base_url()?>";
        $(function(){
            $("#tree").dynatree({
                onLazyRead: function(node){
                    $.ajax({
                        url: BASE+"master/json/icd10/get/"+node.data.key,
                        dataType: 'json',
                        success: function(data, textStatus){
                            res = [];
                            $.each(data,function(i,v){
                                res.push({title: v.title, key: v.key, isFolder: v.isFolder, isLazy: v.isLazy});
                            })
                            node.setLazyNodeStatus(DTNodeStatus_Ok);
                            node.addChild(res);
                        }
                    });
                },onActivate: function(node) {
                    pilih(node.data);
                },

                children: <?=$a;?>
            });

            $("#a").click(function(){
                load += 10;
                var rootNode = $("#tree").dynatree("getRoot");
                $.ajax({
                    url: BASE+"master/json/icd10/ajaxGetInit/"+load,
                    dataType: 'json',
                    success: function(data, textStatus){
                        res = [];
                        $.each(data,function(i,v){
                            res.push({title: v.title, key: v.key, isFolder: v.isFolder, isLazy: v.isLazy});
                        })
                        rootNode.addChild(res);
                    }
                });
            });
            $("#reset").click(function(){
                $("#tree").dynatree("getTree").reload();
            })

            $("#q").keyup(function(e){
                $(".error").html('');
                var qq = $(this).val();
                if(e.which == $.ui.keyCode.ENTER){
                    if($(this).val().length > 2){
                        var rootNode = $("#tree").dynatree("getRoot");
                        rootNode.removeChildren();        
                        dyna_params['q'] = qq;
                        dyna_params[csrf_name] = csrf_hash;
                        $.ajax({
                            url: BASE+"master/json/icd10/search/",
                            dataType: 'json',
                            type: 'post',
                            data:dyna_params,
                            success: function(data, textStatus){
                                res = [];
                                $.each(data,function(i,v){
                                    res.push({title: v.title, key: v.key, isFolder: v.isFolder, isLazy: v.isLazy});
                                })
                                rootNode.addChild(res);
                            }
                        });
                    }else{
                        $(".error").html('please provide at least 3 char');
                    }
                }
                    
            })
        });

        function pilih(obj){
            parent.$.fn.colorbox.close();
            parent.pilih(obj);
        }
    </script>
</head>
<body>
    <button id="a">loadmore</button>
    <button id="reset">reset</button>
    <input type="text" name="q" id="q" autocomplete="off">
    <p class="error"></p>
    <div id="tree">

    </div>
</body>
</html>