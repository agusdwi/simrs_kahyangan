<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/poli/add" class="btn btn-success pull-right modals"><i class="icon-plus-sign icon-white"></i> Tambah Poli</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5">
			<div class="title">
				<h3>Data Poli</h3>
			</div>
		</div>
		<div class="span5" style="float:right;">
			<div class="widgetbox" style="margin-top:20px;">
				<div id="basic">
					<div class="chatsearch" >
						<input type="text" name="" id="styleSearch" placeholder="Search" style="width:91%;margin:auto;">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12" style="padding:15px" id="bigsearch">
				<table class="table std table-bordered data-table" >
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Poli</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?$i=0;foreach ($ds->result() as $key): $i++;?>
						<tr>
							<td class="tb_no"><?=$i;?></td>
							<td>
								<?if (!empty($key->pl_img)): ?>
									<img src="<?=base_url()?>files/poli/<?=$key->pl_img;?>" class="thumb_table">
								<?endif;?>
								<b><?=$key->pl_name;?></b>
							</td>
							<td class="tb_aksi2">
								<a href="<?=base_url()?>master/poli/edit/<?=$key->pl_id;?>">
									<span class="label label-info">edit</span>
								</a>
								<a class="delete" href="<?=base_url()?>master/poli/delete/<?=$key->pl_id;?>">
									<span class="label label-important">delete</span>
								</a>
							</td>
						</tr>
						<?endforeach;?>
					</tbody>
				</table>  
			</div>
		</div>
	</div>
</div>