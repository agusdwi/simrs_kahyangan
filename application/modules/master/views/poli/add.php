<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/poli" class="btn btn-success pull-right"><i class="icon-chevron-left icon-white"></i> Daftar Poli</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-align-justify"></i>                                  
					</span>
					<h5>Form Poli</h5>
				</div>
				<div class="widget-content nopadding">
					<?=form_open_multipart('',array('class' => 'form-horizontal form','id' => 'form_poli')); ?>
						<div class="control-group">
							<label class="control-label">Nama Poli *</label>
							<div class="controls">
								<input class="mini" type="text" id="pl_name" name="ds[pl_name]" autofocus="autofocus">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Gambar Poli (ikon) *</label>
							<div class="controls">
								<input class="mini" type="file" id="userfile" name="userfile"  >
							</div>
						</div>
						<div class="form-actions">
							<button id="reset" class="btn btn-warning" type="reset">Reset</button>
							<button class="btn btn-primary" id="next" type="submit">Simpan</button>
						</div>
					<?=form_close()?>
				</div>
			</div>                      
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#form_poli").validate({
			rules: {
				'ds[pl_name]': "required",
				'userfile': "required"
			}
		});
	})
</script>