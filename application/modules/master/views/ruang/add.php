<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/ruang" class="btn btn-success pull-right"><i class="icon-chevron-left icon-white"></i> Daftar Ruang</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-align-justify"></i>                                  
					</span>
					<h5>Form Ruang</h5>
				</div>
				<div class="widget-content nopadding">
					<?=form_open('',array('class' => 'form-horizontal form','id' => 'form_ruang')); ?>
						<div class="control-group">
							<label class="control-label">Nama Ruang *</label>
							<div class="controls">
								<input class="medium" type="text" id="r_nama" name="ds[r_nama]" autofocus="autofocus">
							</div>
						</div>
						<div class="form-actions">
							<button id="reset" class="btn btn-warning" type="reset">Reset</button>
							<button class="btn btn-primary" id="next" type="submit">Simpan</button>
						</div>
					<?=form_close()?>
				</div>
			</div>                      
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#form_ruang").validate({
			rules: {
				'ds[r_nama]': "required"
			}
		});
	})
</script>