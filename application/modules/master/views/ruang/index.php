<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/ruang/add" class="btn btn-success pull-right modals" style="margin-left:10px">
			<i class="icon-plus-sign icon-white"></i> Tambah Ruang
		</a>
		<a href="<?=base_url()?>master/ruang/add_kamar" class="btn btn-success pull-right modals">
			<i class="icon-plus-sign icon-white"></i> Tambah Kamar
		</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5">
			<div class="title">
				<h3>Data Ruang & Kamar</h3>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12" style="padding:15px" id="bigsearch">
				<table class="table std table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Kamar</th>
							<th>Kapasitas</th>
							<th>Harga /hari</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<? $r=0;foreach ($ds[0]->result() as $key): $r++;?>
							<tr>
								<td style="width:30px"><?=$r;?></td>
								<td colspan="3"><b><?=$key->r_nama;?></b></td>
								<td class="tb_aksi2">
									<a href="<?=base_url()?>master/ruang/edit/<?=$key->id;?>">
										<span class="label label-info">edit</span>
									</a>
									<a class="delete" href="<?=base_url()?>master/ruang/delete/<?=$key->id;?>">
										<span class="label label-important">delete</span>
									</a>
								</td>
							</tr>
							<? if (isset($ds[1][$key->id])): ?>
								<? $j=0;foreach ($ds[1][$key->id] as $k): $j++;?>
									<tr>
										<td><?=$r;?>.<?=$j;?></td>	
										<td><?=$k->k_nama;?></td>
										<td><?=$k->k_kapasitas;?></td>
										<td class="money"><?=int_to_money($k->k_harga);?></td>
										<td class="tb_aksi2">
											<a href="<?=base_url()?>master/ruang/edit_kamar/<?=$k->id;?>">
												<span class="label label-info">edit</span>
											</a>
										<a class="delete" href="<?=base_url()?>master/ruang/delete_kamar/<?=$k->id;?>">
												<span class="label label-important">delete</span>
											</a>
										</td>
									</tr>
								<?endforeach ?>
							<? endif ?>
						<?endforeach ?>
					</tbody>
				</table>  
			</div>
		</div>
	</div>
</div>