<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/ruang" class="btn btn-success pull-right"><i class="icon-chevron-left icon-white"></i> Daftar Ruang</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-align-justify"></i>                                  
					</span>
					<h5>Form Kamar</h5>
				</div>
				<div class="widget-content nopadding">
					<?=form_open('',array('class' => 'form-horizontal form','id' => 'form_ruang')); ?>
						<div class="control-group">
							<label class="control-label">Ruang </label>
							<div class="controls">
								<select name="ds[ruang_id]">
									<?foreach ($ruang->result() as $key): ?>
										<option <?=($key->id==$ds->ruang_id)?'selected="selected"':"";?> value="<?=$key->id;?>"><?=$key->r_nama;?></option>
									<?endforeach ?>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama Kamar *</label>
							<div class="controls">
								<input class="medium" type="text" id="k_nama" name="ds[k_nama]" autofocus="autofocus" value="<?=$ds->k_nama;?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kapasitas Pasien</label>
							<div class="controls">
								<input class="medium" type="text" id="k_kapasitas" name="ds[k_kapasitas]" value="<?=$ds->k_kapasitas;?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Harga / malam</label>
							<div class="controls">
								<input class="medium" type="text" id="k_harga" name="ds[k_harga]" value="<?=$ds->k_harga;?>">
							</div>
						</div>
						<div class="form-actions">
							<button id="reset" class="btn btn-warning" type="reset">Reset</button>
							<button class="btn btn-primary" id="next" type="submit">Simpan</button>
						</div>
					<?=form_close()?>
				</div>
			</div>                      
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#form_ruang").validate({
			rules: {
				'ds[k_nama]'		: "required",
				'ds[k_kapasitas]'	: {required:true,number:true},
				'ds[k_harga]'		: {required:true,number:true}
			}
		});
	})
</script>