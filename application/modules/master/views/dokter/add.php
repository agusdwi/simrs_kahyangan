<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/dokter" class="btn btn-success pull-right"><i class="icon-chevron-left icon-white"></i> Daftar Dokter</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-align-justify"></i>                                  
					</span>
					<h5>Form Dokter</h5>
				</div>
				<div class="widget-content nopadding">
					<?=form_open('',array('class' => 'form-horizontal form','id' => 'form_dokter')); ?>
						<div class="control-group">
							<label class="control-label">Nama Paramedis *</label>
							<div class="controls">
								<input class="medium" type="text" id="dr_name" name="ds[dr_name]" autofocus="autofocus">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Jenis Paramedis *</label>
							<div class="controls">
								<select name="ds[role_id]">
									<?foreach ($role->result() as $key): ?>
										<option value="<?=$key->role_id;?>"><?=$key->role_name;?></option>
									<?endforeach;?>
								</select>
							</div>
						</div>
						<div class="form-actions">
							<button id="reset" class="btn btn-warning" type="reset">Reset</button>
							<button class="btn btn-primary" id="next" type="submit">Simpan</button>
						</div>
					<?=form_close()?>
				</div>
			</div>                      
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#form_dokter").validate({
			rules: {
				'ds[dr_name]': "required"
			}
		});
	})
</script>