<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/dokter/add" class="btn btn-success pull-right modals"><i class="icon-plus-sign icon-white"></i> Tambah Dokter</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5">
			<div class="title">
				<h3>Data Dokter</h3>
			</div>
		</div>
		<div class="span5" style="float:right;">
			<div class="widgetbox" style="margin-top:20px;">
				<div id="basic">
					<div class="chatsearch" >
						<input type="text" name="" id="styleSearch" placeholder="Search" style="width:91%;margin:auto;">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<ul class="nav nav-tabs">
			<? $i=0; foreach ($jp->result() as $j) { ?>
			<li<?=($j->id==$this->uri->segment(4) OR ($this->uri->segment(4)=='' && $i==0)) ? ' class="active"': '';?>><a href="<?=base_url().'master/dokter/index/'.$j->id;?>"><?=$j->nama;?></a></li>
			<? $i++;} ?>
		</ul>
	</div>

	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12" style="padding:15px" id="bigsearch">
				<table class="table std table-bordered data-table">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Dokter</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?$i=0;foreach ($ds->result() as $key): $i++;?>
						<tr>
							<td class="tb_no"><?=$i;?></td>
							<td><?=$key->dr_name;?></td>
							<td class="tb_aksi2">
								<a href="<?=base_url()?>master/dokter/edit/<?=$key->dr_id;?>">
									<span class="label label-info">edit</span>
								</a>
								<a class="delete" href="<?=base_url()?>master/dokter/delete/<?=$key->dr_id;?>">
									<span class="label label-important">delete</span>
								</a>
							</td>
						</tr>
						<?endforeach;?>
					</tbody>
				</table>  
			</div>
		</div>
	</div>
</div>