<div class="pageheader notab">
	<h1 class="pagetitle">
		<?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>master/dokter/add" class="btn btn-success pull-right modals"><i class="icon-plus-sign icon-white"></i> Tambah Dokter</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5">
			<div class="title">
				<h3>Update Prosentase Tarif Paramedis</h3>				
			</div>
		</div>		
	</div>

	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12" style="padding:15px" id="bigsearch">
				<?=form_open()?>
					<table class="table std100 table-bordered data-table">
						<thead>
							<tr>
								<th>No</th>
								<th>Jenis Paramedis</th>
								<th>Untuk Paramedis</th>
								<th>Untuk Rumah Sakit</th>
							</tr>
						</thead>
						<tbody>
							<?$i=0;foreach ($mdcn->result() as $key): $i++;?>
								<tr>
									<td><?=$i;?></td>
									<td><?=$key->role_name;?></td>
									<td><input type="text" class="tx_small pro_paramedis pro_paramedis-<?=$key->role_id;?>" name="role_prosentase[<?=$key->role_id;?>]" value="<?=$key->role_prosentase;?>" id="<?=$key->role_id;?>"></td>
									<td><input type="text" class="tx_small pro_rs pro_rs-<?=$key->role_id;?>" value="<?=$key->role_prosentase <> '' ? 100-$key->role_prosentase : '';?>" id="<?=$key->role_id;?>"></td>
								</tr>
							<?endforeach;?>
						</tbody>
					</table>  
					<div class="form-actions" style="margin-top: 10px;margin-bottom: 0px;">
						<button type="submit" name="savenext" style="float:right;margin-left:5px" class="btn btn-info">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?
	function get_ds($m,$i,$ds){
		return (isset($ds[$m][$i]))? $ds[$m][$i] : '';
	}
?>
<script type="text/javascript">
	$(document).ready(function(){
    	$(".pro_paramedis").blur(function(){
    		var id = $(this).attr('id');
    		var val = 100 - $(this).val();
    		$(".pro_rs-"+id).val(val);
    	});
    	$(".pro_rs").blur(function(){
    		var id = $(this).attr('id');
    		var val = 100 - $(this).val();
    		$(".pro_paramedis-"+id).val(val);
    	});
    });
</script>