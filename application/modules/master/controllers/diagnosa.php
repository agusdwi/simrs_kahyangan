<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Diagnosa extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' => 7);
		$this->table = "mst_diagnosa";
	}

	public function index() {
		$data['main_view'] 	= 'diagnosa/diagnosa_index';
		$data['title'] 		= 'Pengelolaan Data Diagnosa';
		$data['cf'] 		= $this -> cf;
		$data['current'] 	= 29;
		$data['a'] 			= $this->getInit();
		$this -> load -> view('template', $data);
	}

	function ajaxGetInit($i=0){
		echo $this->getInit($i);
	}

	function getInit($i=0){
		$d = $this->db->order_by('diag_code')->limit(10,$i)->get_where('mst_diagnosa',array('diag_parent'=>0));
		$js = $this->toJson($d);
		return json_encode($js);
	}

	function get($id){
		$d = $this->db->get_where('mst_diagnosa',array('diag_parent'=>$id));
		$js = $this->toJson($d);
		echo json_encode($js);
	}

	function search(){
		$name = $this->input->post('q');
		$d = $this->db->like('diag_name',$name)->get_where('mst_diagnosa',array('diag_parent'=>0));
		$js = $this->toJson($d);
		echo json_encode($js);
	}

	function getJsonSelect(){
		$call = $_GET['callback'];
		$name = $this->input->post('q');
		$d = $this->db->like('diag_name',$name)->get_where('mst_diagnosa',array('diag_parent'=>0));
		$js = $this->toJson($d);
		echo $call."(".json_encode(array('icd'=>$js)).")";
	}

	function toJson($obj){
		$js = array();
		foreach ($obj->result() as $key) {
			$js[] = array(
				'title'			=> "<b>$key->diag_code</b>"."  ".$key->diag_name,
				'isFolder'		=> false,
				'isLazy'		=> true,
				'key'			=> $key->diag_id
			);
		}
		return $js;
	}

	// crud
	function add($id=""){
		if(is_post()){
			$data = $this->input->post('ds');
			$this->db->insert($this->table,$data);
			$this->session->set_flashdata('message','diagnosa berhasil ditambahkan');
			redirect(base_url('master/diagnosa'));
		}else{
			$data['main_view'] 	= 'diagnosa/diagnosa_add';
			$data['title'] 		= 'Tambah Data Diagnosa';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 29;
			$data['item'] 		= (!empty($id))? $this->db->get_where('mst_diagnosa',array('diag_id'=>$id))->row() : array() ;
			$this->load->view('template', $data);
		}
	}

	function edit($id,$act=""){
		if(is_post()){
			$data = $this->input->post('ds');
			$this->db->where('diag_id', $id);
			$this->db->update($this->table, $data); 
			$this->session->set_flashdata('message','data dokter berhasil di perbarui');
			redirect(base_url('master/diagnosa'));
		}else{
			$data['main_view'] 	= 'diagnosa/diagnosa_edit';
			$data['title'] 		= 'Edit Diagnosa';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 26;
			$data['ds'] 		= $this->db->get_where($this->table,array('diag_id'=>$id))->row();
			$data['item'] 		= ($data['ds']->diag_parent == 0)? array() : $this->db->get_where('mst_diagnosa',array('diag_id'=>$data['ds']->diag_parent))->row();
			$this->load->view('template', $data);
		}	
	}

	function delete($id){
		$this->db->delete($this->table, array('diag_id' => $id)); 
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(cur_url(-2));
	}
}
