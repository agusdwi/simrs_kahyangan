<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Paramedis_prosentase extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' => 7);
	}

	public function index() {
		if (is_post()) {
			$input = $this->input->post();
			$this->save($input);
			$this->session->set_flashdata('message','data berhasil doupdate');
			redirect(cur_url());
		}
		$data['main_view'] 	= 'paramedis_prosentase';
		$data['title'] 		= 'Pengelolaan Prosentase Tarif Paramedis';
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 41;
		$data['mdcn'] 		= $this->db->get('mst_ref_role_paramedis');
		$this->load->view('template', $data);
	}

	function save($in){
		$d = array();
		foreach ($in['role_prosentase'] as $key => $value) {
			$d = "UPDATE mst_ref_role_paramedis SET role_prosentase = '$value' WHERE role_id = '$key' ";
			$this->db->query($d);
		}
	}

}