<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Tindakan_tarif extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' => 7);
	}

	public function index() {
		$data['main_view'] 	= 'tindakan_tarif/index';
		$data['title'] 		= 'Pengelolaan Tarif Tindakan';
		$data['cf'] 		= $this -> cf;
		$data['current'] 	= 31;
		$data['ds'] 		= $this->db->get_where('mst_treathment',array('status'=>1));
		$data['cls'] 		= $this->db->get('mst_ref_class');
		$data['tarif'] 		= $this->get_tarif();
		
		$this->load->view('template', $data);
	}

	function get_tarif(){
		$d = $this->db->get_where('mst_treathment_tarif');
		$r = array();
		foreach ($d->result() as $key) {
			$r[$key->treat_id][$key->cls_id][$key->role_id][] = $key->treat_tarif_harga;
		}
		return $r;
	}

	function get_tarif_detail($id){
		$d = $this->db->get_where('mst_treathment_tarif',array('treat_id'=>$id));
		$r = array();
		foreach ($d->result() as $key) {
			$r[$key->group_treat_id][$key->role_id][$key->cls_id] = $key->treat_tarif_harga;
		}
		return $r;
	}

	function get_tarif_dasar($id){
		$d = $this->db->get_where('mst_treathment_tarif_dasar',array('treat_id'=>$id));
		$r = array();
		foreach ($d->result() as $key) {
			$r[$key->cls_id] = $key->treat_tarif_harga;
		}
		return $r;
	}

	function proses($id){
		if (is_post()) {
			$this->db->delete('mst_treathment_tarif', array('treat_id' => $id)); 
			$this->db->delete('mst_treathment_tarif_dasar', array('treat_id' => $id)); 
			$input = $this->input->post();

			// tarif dasar komponen
			foreach ($input['r'] as $key => $val) {
				foreach ($input['t'][$key] as $k => $v) {
					if(!empty($v)){
						$d = array(
							'group_treat_id' 	=> $key,
							'treat_id'			=> $id,
							'cls_id'			=> $k,
							'role_id'			=> $val,
							'treat_tarif_harga' => $v
						);
						$this->db->insert('mst_treathment_tarif',$d);
					}
				}
			}
		}
		$data['main_view'] 	= 'tindakan_tarif/proses';
		$data['title'] 		= 'Pengelolaan Tarif Tindakan';
		$data['ds'] 		= $this->db->get_where('mst_treathment',array('treat_id'=>$id))->row();
		$data['cls'] 		= $this->db->get('mst_ref_class');
		$data['role'] 		= $this->db->get('mst_ref_role_paramedis');
		$data['tarif'] 		= $this->get_tarif_detail($id);
		$data['td'] 		= $this->get_tarif_dasar($id);
		$data['cf'] 		= $this -> cf;
		$data['current'] 	= 31;
		$this->load->view('template', $data);
	}
}
