<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
			'modul_id'	=> 7
		);
	}

	public function index(){
		redirect(cur_url().'dokter');
	}
	
	function contoh_iframe(){
		$data['main_view']	= 'contoh/iframe';
		$this->load->view('iframe',$data);
	}
}