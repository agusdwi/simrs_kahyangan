<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Role_paramedis extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' => 7);
		$this->table = 'mst_ref_role_paramedis';
	}

	public function index() {
		$data['main_view'] 	= 'role_paramedis/index';
		$data['title'] 		= 'Pengelolaan Spesialis Paramedis';
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 42;
		$data['ds'] 		= $this->db->order_by('role_id')->get_where($this->table);
		$this->load->view('template', $data);
	}

	function add($act=""){
		if(is_post()){
			$data = $this->input->post('ds');
			$this->db->insert($this->table,$data);
			$this->session->set_flashdata('message','spesialis paramedis berhasil ditambahkan');
			redirect(cur_url(-1));
		}else{
			$data['main_view'] 	= 'role_paramedis/add';
			$data['title'] 		= 'Tambah Data Spesialis Paramedis';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 42;
			$this->load->view('template', $data);
		}
	}

	function edit($id,$act=""){
		if(is_post()){
			$data = $this->input->post('ds');
			$this->db->where('role_id', $id);
			$this->db->update($this->table, $data); 
			$this->session->set_flashdata('message','data spesialis paramedis berhasil di perbarui');
			redirect(cur_url(-2));
		}else{
			$data['main_view'] 	= 'role_paramedis/edit';
			$data['title'] 		= 'Edit Spesialis Paramedis';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 42;
			$data['ds'] 		= $this->db->get_where($this->table,array('role_id'=>$id))->row();
			$this->load->view('template', $data);
		}	
	}

	function delete($id){
		$this->db->where('role_id', $id);
		$this->db->delete($this->table, $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(cur_url(-2));
	}
}
