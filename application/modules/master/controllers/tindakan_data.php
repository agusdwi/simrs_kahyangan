<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tindakan_data extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' => 7);
		$this->table = 'mst_treathment';
	}

	public function index() {
		$data['main_view'] 	= 'tindakan_data/index';
		$data['title'] 		= 'Pengelolaan Data Tindakan';
		$data['cf'] 		= $this -> cf;
		$data['current'] 	= 35;
		$data['ds'] 		= $this->db->get_where($this->table,array('status'=>1));
		$this -> load -> view('template', $data);
	}

	function add(){
		if(is_post()){
			$data = $this->input->post('ds');
			$this->db->insert($this->table,$data);
			$this->session->set_flashdata('message','data berhasil di simpan');
			redirect(cur_url(-1));
		}else{
			$data['main_view'] 	= 'tindakan_data/add';
			$data['title'] 		= 'Tambah Data Tindakan';
			$data['cf'] 		= $this -> cf;
			$data['current'] 	= 35;
			$this -> load -> view('template', $data);
		}
	}

	function edit($id){
		if (is_post()) {
			$data = $this->input->post('ds');
			$this->db->where('treat_id', $id);
			$this->db->update($this->table, $data);
			$this->session->set_flashdata('message','data berhasil di update');
			redirect(cur_url(-2));
		}else{
			$data['main_view'] 	= 'tindakan_data/edit';
			$data['title'] 		= 'Edit Data Tindakan';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 35;
			$data['ds'] 		= $this->db->get_where($this->table,array('treat_id'=>$id))->row();
			$this->load->view('template', $data);
		}
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('treat_id', $id);
		$this->db->update($this->table, $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(cur_url(-2));
	}
}