<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Obat_tarif extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' => 7);
	}

	public function index() {
		if (is_post()) {
			$input = $this->input->post();
			$this->save($input);
			$this->session->set_flashdata('message','data berhasil doupdate');
			redirect(cur_url());
		}
		$data['main_view'] 	= 'obat_tarif';
		$data['title'] 		= 'Pengelolaan Tarif Obat';
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 32;
		$data['ins'] 		= $this->db->get('mst_ref_insurance');
		$data['mdcn'] 		= $this->db->get_where('mst_medicine',array('status'=>1));
		$data['ds'] 		= $this->format($this->db->get('mst_medicine_insurance'));
		$this->load->view('template', $data);
	}

	function save($in){
		$d = array();
		$this->db->truncate('mst_medicine_insurance');
		foreach ($in['prc'] as $key => $value) {
			foreach ($value as $k => $v) {
				if (!empty($v)) {
					$d = "INSERT INTO mst_medicine_insurance (ins_id, mdcn_id, mi_price) VALUES ($k,$key,$v)";
					$this->db->query($d);
				}
			}
		}
	}

	function format($d){
		$s = array();
		foreach ($d->result() as $key) {
			$s[$key->mdcn_id][$key->ins_id] = $key->mi_price;
		}
		return $s;
	}
}