<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Obat extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' => 7);
		$this->table = 'mst_medicine';
	}

	public function index() {
		$data['main_view'] 	= 'obat/index';
		$data['title'] 		= 'Pengelolaan Data Obat';
		$data['cf'] 		= $this -> cf;
		$data['current'] 	= 27;
		$data['ds'] 		= $this->db->get_where($this->table,array('status'=>1));
		$this->load->view('template', $data);
	}

	function add($act=""){
		if($act==""){
			if(is_post()){
				$data = $this->input->post('ds');
				$this->db->insert($this->table,$data);
				$this->session->set_flashdata('message','obat berhasil ditambahkan');
				redirect(cur_url(-1));
			}else{
				$data['main_view'] 	= 'obat/add';
				$data['title'] 		= 'Tambah Data Obat';
				$data['cf'] 		= $this->cf;
				$data['current'] 	= 27;
				$this->load->view('template', $data);
			}	
		}else{
			$input = $this->input->post('ds');
			$ds = $this->db->get_where($this->table,array('mdcn_code'=>$input['mdcn_code'],'status'=>1));
			if ($ds->num_rows() > 0) {
				echo json_encode(array('result'=>'false'));
			}else echo json_encode(array('result'=>'true'));
		}
	}

	function edit($id,$act=""){
		if($act==""){
			if(is_post()){
				$data = $this->input->post('ds');
				$this->db->where('mdcn_id', $id);
				$this->db->update($this->table, $data); 
				$this->session->set_flashdata('message','obat berhasil di perbarui');
				redirect(cur_url(-2));
			}else{
				$data['main_view'] 	= 'obat/edit';
				$data['title'] 		= 'Edit Obat';
				$data['cf'] 		= $this->cf;
				$data['current'] 	= 27;
				$data['ds'] 		= $this->db->get_where($this->table,array('mdcn_id'=>$id))->row();
				$this->load->view('template', $data);
			}	
		}else{
			$input = $this->input->post('ds');
			$old = $this->db->get_where($this->table,array('mdcn_id'=>$id))->row();
			if($input['mdcn_code'] == $old->mdcn_code){
				echo json_encode(array('result'=>'true'));
			}else{
				$ds = $this->db->get_where($this->table,array('mdcn_code'=>$input['mdcn_code'],'status'=>1));
				if ($ds->num_rows() > 0) {
					echo json_encode(array('result'=>'false'));
				}else echo json_encode(array('result'=>'true'));							
			}
		}
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('mdcn_id', $id);
		$this->db->update($this->table, $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(cur_url(-2));
	}
}
