<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Dokter extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' => 7);
		$this->table = 'trx_doctor';
	}

	public function index($jenis_id='') {
		$data['main_view'] 	= 'dokter/index';
		$data['title'] 		= 'Pengelolaan Data Dokter';
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 26;
		if($jenis_id=='')
			$data['ds'] 	= $this->db->order_by('dr_name')->get_where('v_paramedis_jenis',array('dr_status'=>1));
		else
			$data['ds'] 	= $this->db->order_by('dr_name')->select('*')->where('jenis_id',$jenis_id)->where('dr_status',1)->get('v_paramedis_jenis');

		$data['jp'] 		= $this->db->order_by('id')->get_where('mst_jenis_paramedis');
		$this->load->view('template', $data);
	}

	function add($act=""){
		if(is_post()){
			$data = $this->input->post('ds');
			$this->db->insert($this->table,$data);
			$this->session->set_flashdata('message','dokter berhasil ditambahkan');
			redirect(cur_url(-1));
		}else{
			$data['main_view'] 	= 'dokter/add';
			$data['title'] 		= 'Tambah Data Dokter';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 26;
			$data['role'] 		= $this->db->get_where('mst_ref_role_paramedis');
			$this->load->view('template', $data);
		}
	}

	function edit($id,$act=""){
		if(is_post()){
			$data = $this->input->post('ds');
			$this->db->where('dr_id', $id);
			$this->db->update($this->table, $data); 
			$this->session->set_flashdata('message','data dokter berhasil di perbarui');
			redirect(cur_url(-2));
		}else{
			$data['main_view'] 	= 'dokter/edit';
			$data['title'] 		= 'Edit Dokter';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 26;
			$data['ds'] 		= $this->db->get_where($this->table,array('dr_id'=>$id))->row();
			$data['role'] 		= $this->db->get_where('mst_ref_role_paramedis');
			$this->load->view('template', $data);
		}	
	}

	function delete($id){
		$data = array('dr_status' => 0);
		$this->db->where('dr_id', $id);
		$this->db->update($this->table, $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(cur_url(-2));
	}
}
