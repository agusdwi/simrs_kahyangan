<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Poli extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' => 7);
		$this->table = 'trx_poly';
	}

	public function index() {
		$data['main_view'] = 'poli/index';
		$data['title'] = 'Pengelolaan Data Poli';
		$data['cf'] = $this -> cf;
		$data['current'] = 30;
		$data['ds'] 		= $this->db->get_where($this->table,array('pl_status'=>1));
		$this -> load -> view('template', $data);
	}

	function add(){
		if(is_post()){
			$data = $this->input->post('ds');
			$img = $this->upload();
			if($img[0]){
				$data['pl_img'] = $img[1];
				$this->db->insert($this->table,$data);
				$this->session->set_flashdata('message','data berhasil di simpan');
				redirect(cur_url(-1));
			}else{
				$this->session->set_flashdata('message','data tidak berhasil dimasukkan, gambar tidak sesuai');
				redirect(cur_url());
			}
		}else{
			$data['main_view'] = 'poli/add';
			$data['title'] = 'Tambah Data Poli';
			$data['cf'] = $this -> cf;
			$data['current'] = 30;
			$this -> load -> view('template', $data);
		}
	}

	function edit($id){
		if (is_post()) {
			$data = $this->input->post('ds');
			if(!empty($_FILES['userfile']['name'])){
				$img = $this->upload();
				if($img[0]){
					$data['pl_img'] = $img[1];
				}else{
					$this->session->set_flashdata('message','data tidak berhasil dimasukkan, gambar tidak sesuai');
					redirect(cur_url());		
				}
			}
			$this->db->where('pl_id', $id);
			$this->db->update($this->table, $data); 
			$this->session->set_flashdata('message','data berhasil di update');
			redirect(cur_url(-2));
		}else{
			$data['main_view'] = 'poli/edit';
			$data['title'] = 'Edit Data Poli';
			$data['cf'] = $this -> cf;
			$data['current'] = 30;
			$data['ds'] = $this->db->get_where($this->table,array('pl_id'=>$id))->row();
			$this -> load -> view('template', $data);
		}
	}

	function upload(){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']		= 'files/poli';
		$config['allowed_types']	= 'png';
		$config['max_size']			= '10000';
		$config['max_width']		= '2000';
		$config['max_height']		= '2000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			return array(false,$this->upload->display_errors());
		}else{
			$a = $this->upload->data();
			return array(true,$a['file_name']);
		}
	}

	function delete($id){
		$data = array('pl_status' => 0);
		$this->db->where('pl_id', $id);
		$this->db->update($this->table, $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(cur_url(-2));
	}

}