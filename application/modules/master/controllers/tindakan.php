<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Tindakan extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' => 7);
		$this->table = 'mst_treathment';
	}

	public function index() {
		$data['main_view'] 	= 'tindakan/index';
		$data['title'] 		= 'Pengelolaan Data Tindakan';
		$data['cf'] 		= $this -> cf;
		$data['current'] 	= 28;
		$data['ds'] 		= $this->db->get_where($this->table,array('status'=>1));
		$this->load->view('template', $data);
	}

	function add($act=""){
		if($act==""){
			if(is_post()){
				$data = $this->input->post('ds');
				$this->db->insert($this->table,$data);
				$this->session->set_flashdata('message','tindakan berhasil ditambahkan');
				redirect(cur_url(-1));
			}else{
				$data['main_view'] 	= 'tindakan/add';
				$data['title'] 		= 'Tambah Data Tindakan';
				$data['cf'] 		= $this->cf;
				$data['current'] 	= 28;
				$this->load->view('template', $data);
			}	
		}else{
			$input = $this->input->post('ds');
			$ds = $this->db->get_where($this->table,array('treat_code'=>$input['treat_code'],'status'=>1));
			if ($ds->num_rows() > 0) {
				echo json_encode(array('result'=>'false'));
			}else echo json_encode(array('result'=>'true'));
		}
	}

	function edit($id,$act=""){
		if($act==""){
			if(is_post()){
				$data = $this->input->post('ds');
				$this->db->where('treat_id', $id);
				$this->db->update($this->table, $data); 
				$this->session->set_flashdata('message','tindakan berhasil di perbarui');
				redirect(cur_url(-2));
			}else{
				$data['main_view'] 	= 'tindakan/edit';
				$data['title'] 		= 'Edit Tindakan';
				$data['cf'] 		= $this->cf;
				$data['current'] 	= 28;
				$data['ds'] 		= $this->db->get_where($this->table,array('treat_id'=>$id))->row();
				$this->load->view('template', $data);
			}	
		}else{
			$input = $this->input->post('ds');
			$old = $this->db->get_where($this->table,array('treat_id'=>$id))->row();
			if($input['treat_code'] == $old->treat_code){
				echo json_encode(array('result'=>'true'));
			}else{
				$ds = $this->db->get_where($this->table,array('treat_code'=>$input['treat_code'],'status'=>1));
				if ($ds->num_rows() > 0) {
					echo json_encode(array('result'=>'false'));
				}else echo json_encode(array('result'=>'true'));							
			}
		}
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('treat_id', $id);
		$this->db->update($this->table, $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(cur_url(-2));
	}
}
