<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ruang extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' => 7);
	}

	public function index() {
		$data['main_view'] 	= 'ruang/index';
		$data['title'] 		= 'Pengelolaan Data Ruang';
		$data['cf'] 		= $this -> cf;
		$data['current'] 	= 36;
		$data['ds'] 		= $this->get_data();
		$this -> load -> view('template', $data);
	}

	private function get_data(){
		$ruang = $this->db->get_where('mst_ruang',array('status'=>1));
		$kamar = $this->db->get_where('v_ruang_kamar',array('status'=>1));
		$ds = array();
		foreach ($kamar->result() as $key) {
			$ds[$key->ruang_id][] = $key;
		}
		return array($ruang,$ds);
	}

	function add(){
		if(is_post()){
			$data = $this->input->post('ds');
			$this->db->insert('mst_ruang',$data);
			$this->session->set_flashdata('message','data berhasil di simpan');
			redirect(cur_url(-1));
		}else{
			$data['main_view'] 	= 'ruang/add';
			$data['title'] 		= 'Tambah Data Ruang';
			$data['cf'] 		= $this -> cf;
			$data['current'] 	= 36;
			$this -> load -> view('template', $data);
		}
	}

	function edit($id){
		if (is_post()) {
			$data = $this->input->post('ds');
			$this->db->where('id', $id);
			$this->db->update('mst_ruang', $data);
			$this->session->set_flashdata('message','data berhasil di update');
			redirect(cur_url(-2));
		}else{
			$data['main_view'] 	= 'ruang/edit';
			$data['title'] 		= 'Edit Data Ruang';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 36;
			$data['ds'] 		= $this->db->get_where('mst_ruang',array('id'=>$id))->row();
			$this->load->view('template', $data);
		}
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('id', $id);
		$this->db->update('mst_ruang', $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(cur_url(-2));
	}

	function add_kamar(){
		if(is_post()){
			$data = $this->input->post('ds');
			$this->db->insert('mst_kamar',$data);
			$this->session->set_flashdata('message','data berhasil di simpan');
			redirect(cur_url(-1));
		}else{
			$data['main_view'] 	= 'ruang/add_kamar';
			$data['title'] 		= 'Tambah Data Kamar';
			$data['cf'] 		= $this -> cf;
			$data['current'] 	= 36;
			$data['ruang'] 		= $this->db->get_where('mst_ruang',array('status'=>1));
			$this -> load -> view('template', $data);
		}
	}

	function edit_kamar($id){
		if (is_post()) {
			$data = $this->input->post('ds');
			$this->db->where('id', $id);
			$this->db->update('mst_kamar', $data);
			$this->session->set_flashdata('message','data berhasil di update');
			redirect(cur_url(-2));
		}else{
			$data['main_view'] 	= 'ruang/edit_kamar';
			$data['title'] 		= 'Edit Data Kamar';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 36;
			$data['ds'] 		= $this->db->get_where('mst_kamar',array('id'=>$id))->row();
			$data['ruang'] 		= $this->db->get_where('mst_ruang',array('status'=>1));
			$this->load->view('template', $data);
		}
	}

	function delete_kamar($id){
		$data = array('status' => 0);
		$this->db->where('id', $id);
		$this->db->update('mst_kamar', $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(cur_url(-2));
	}
}