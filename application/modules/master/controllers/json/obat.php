<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Obat extends CI_Controller {
	function __construct() {
		parent::__construct();
	}

	function getJsonSelect(){
		$call = $_GET['callback'];
		$name = $this->input->post('q');
		$sql = "SELECT * FROM mst_medicine WHERE status =  1 AND ( mdcn_code  LIKE '%$name%' ESCAPE '!' OR  mdcn_name  LIKE '%$name%' ESCAPE '!' )";
		$d = $this->db->query($sql);

		$js = $this->toJson($d);
		echo $call."(".json_encode(array('icd'=>$js)).")";
	}

	function toJson($obj){
		$js = array();
		foreach ($obj->result() as $key) {
			$js[] = array(
				'title'			=> $key->mdcn_name,
				'key'			=> $key->mdcn_id,
				'code'			=> $key->mdcn_code
			);
		}
		return $js;
	}
}