<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Icd10 extends CI_Controller {
	function __construct() {
		parent::__construct();
	}

	function index(){
		$init = $this->getInit();
		$this->load->view('json/icd10',array('a'=>$init));
	}

	function ajaxGetInit($i=0){
		echo $this->getInit($i);
	}

	function getInit($i=0){
		$d = $this->db->limit(10,$i)->get_where('mst_diagnosa',array('diag_parent'=>0));
		$js = $this->toJson($d);
		return json_encode($js);
	}

	function get($id){
		$d = $this->db->get_where('mst_diagnosa',array('diag_parent'=>$id));
		$js = $this->toJson($d);
		echo json_encode($js);
	}

	function search(){
		$name = $this->input->post('q');
		$d = $this->db->like('diag_name',$name)->get_where('mst_diagnosa',array('diag_parent'=>0));
		$js = $this->toJson($d);
		echo json_encode($js);
	}

	function getJsonSelect(){
		$call = $_GET['callback'];
		$name = $this->input->post('q');
		$d = $this->db->like('diag_name',$name)->get_where('mst_diagnosa',array('diag_parent'=>0));
		$js = $this->toJson($d);
		echo $call."(".json_encode(array('icd'=>$js)).")";
	}

	function toJson($obj){
		$js = array();
		foreach ($obj->result() as $key) {
			$js[] = array(
				'title'			=> "<b>$key->diag_code</b>"."  ".$key->diag_name,
				'isFolder'		=> false,
				'isLazy'		=> true,
				'key'			=> $key->diag_id
			);
		}
		return $js;
	}
}
