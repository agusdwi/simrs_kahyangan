<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Tindakan extends CI_Controller {
	function __construct() {
		parent::__construct();
	}

	function getJsonSelect(){
		$call = $_GET['callback'];
		$name = $this->input->post('q');
		$sql = "SELECT * FROM mst_treathment WHERE status =  1 AND ( treat_code  LIKE '%$name%' ESCAPE '!' OR  treat_name  LIKE '%$name%' ESCAPE '!' )";
		$d = $this->db->query($sql);

		$js = $this->toJson($d);
		echo $call."(".json_encode(array('icd'=>$js)).")";
	}

	function toJson($obj){
		$js = array();
		foreach ($obj->result() as $key) {
			$js[] = array(
				'title'			=> $key->treat_name,
				'key'			=> $key->treat_id,
				'code'			=> $key->treat_code
			);
		}
		return $js;
	}
}
