<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rawat_jalan extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 3
			);
	}

	public function index(){
		$data['main_view']	= 'index';
		$data['title']		= 'Rawat Jalan';
		$data['cf']			=  $this->cf;
		$data['current']	= '';
		$data['desc']		= 'Description. ';
		$data['poli']		= $this->db->get_where('trx_poly',array('pl_status'=> 1));
		$data['left_sidebar'] = "left_rawat_jalan";
		$this->load->view('template',$data);
	}

	function create(){
		$rm = "1";
		$data = $this->input->post('ds');
		// $this->db->insert('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		echo cur_url(-1)."cetak/$rm";
	} 

	function update($id){
		$data = $this->input->post('ds');	
		$this->db->where('', $id);
		$this->db->update('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('', $id);
		$this->db->update('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil dihapus'));
		redirect(cur_url(-2));
	}

	function cetak($rm){
		$data['main_view']	= 'poli_tulang/cetak';
		$this->load->view('template_print',$data);
	}
}