<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller { 
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 3
			);
	}

	public function index(){
		$data['cf']			=  $this->cf;
		$data['title']		= 'Laporan Rawat Jalan';
		$data['main_view']	= 'laporan/index';
		$this->load->view('template',$data);
	}
	
	function pasien(){
		$data['cf']			=  $this->cf;
		$data['title']		= 'Laporan Rawat Jalan';
		$data['main_view']	= 'laporan/pasien';
		$this->load->view('template',$data);
	}	

	function get_ten(){
		$data['cf']			=  $this->cf;
		$data['title']		= 'Laporan 10 Besar Penyakit';
		$data['data']		= $this->db->get('v_lap_10_penyakit');
		//$data['title_laporan']	= 'Laporan 10 Besar Penyakit';
		$data['main_view']	= 'laporan/ajax/pasien_ten';
		$this->load->view('template',$data);	
	}

	function kunjungan(){
		$data['cf']			=  $this->cf;
		$data['title']		= 'Laporan Kunjungan Pasien';
		$data['data']		= $this->db->get('v_kunjungan_pasien');
		//$data['title_laporan']	= 'Laporan 10 Besar Penyakit';
		$data['main_view']	= 'laporan/kunjungan';
		$this->load->view('template',$data);
	}
}