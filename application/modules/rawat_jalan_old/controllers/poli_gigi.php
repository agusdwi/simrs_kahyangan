<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poli_gigi extends CI_Controller { 
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 3
			);
	}

	public function index(){
		$data['cf']			=  $this->cf;
		$data['current']	= '';
		$data['title']		= 'Pasien Poli Gigi';
		$data['desc']		= 'Poliklinik Gigi';
		$data['main_view']	= 'poli_gigi/index';
		//$data['main_view']	= 'poli_gigi/rekam_medis';
		$this->load->view('template',$data);
	}
	
	function proses(){
		$data['cf']			=  $this->cf;
		$data['title']		= 'Tindakan Pasien Poli Gigi';
		$data['desc']		= 'Poliklinik Gigi';
		$data['main_view']	= 'poli_gigi/proses';
		$this->load->view('template',$data);
	}

	function create(){
		$data = $this->input->post('ds');
		$this->db->insert('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		redirect(cur_url(-1));
	}

	function update($id){
		$data = $this->input->post('ds');	
		$this->db->where('', $id);
		$this->db->update('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('', $id);
		$this->db->update('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil dihapus'));
		redirect(cur_url(-2));
	}
	// kajian
	function kajian(){
		$this->load->view('poli_gigi/kajian');
	}
	// rekam medis
	function rekam_medis(){
		$this->load->view('poli_gigi/rekam_medis');
	}
	// diagnosis
	function diagnosis(){
		$this->load->view('poli_gigi/diagnosis');
	}

	// resep dan obat
	function resep(){
		$this->load->view('poli_gigi/resep');
	}
	
	//json
	function get_diagnosa(){
		$data = array();
		for($i=0;$i<=10;$i++){
			$data = array(
				'id'	=> 'KODE',
				'name'	=> 'NAMA'
			);
		}
		echo json_encode($data);
	}
}