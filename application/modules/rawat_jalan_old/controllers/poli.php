<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poli extends CI_Controller { 
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 3
			);
	}

	public function index(){
		$data['main_view']	= 'index';
		$data['title']		= 'Rawat Jalan';
		$data['cf']			=  $this->cf;
		$data['current']	= '';
		$data['desc']		= 'Description. ';
		$data['poli']		= $this->db->get_where('trx_poly',array('pl_status'=> 1));
		$data['left_sidebar'] = "left_rawat_jalan";
		$this->load->view('template',$data);
	}
	
	function antrian($id){
		$poli = $this->db->get_where('trx_poly',array('pl_id'=> $id))->row();

		$data['cf']			=  $this->cf;
		$data['title']		= $poli->pl_name;
		$data['main_view']	= 'poli/antrian';
		$data['cur_poli']	= $poli;
		$data['left_sidebar'] = "left_rawat_jalan";
		$data['antrian']	= $this->db->get_where('v_queue_poli',array('queo_status'=>1,'pl_id'=>$id));
		debug_array($data['antrian']);
		$this->load->view('template',$data);
	}

	function proses(){

	}

	
}