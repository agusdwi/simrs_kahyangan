<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poli_kebidanan extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 3
			);
	}

	public function index(){
		$data['main_view']	= 'poli_kebidanan/index';
		$data['title']		= 'Pasien Poli Kebidanan';
		$data['cf']			=  $this->cf;
		$data['current']	= '';
		$data['desc']		= 'Description. ';
		$data['antrian']	= $this->db->get_where('v_queue_poli',array('queo_status'=>1,'pl_id'=>2));
		// debug_array($data['antrian']->result());
		$this->load->view('template',$data);
	}
	
	function proses(){
		$data['prev']		= $this->input->post('ds');
		// debug_array($data['prev']);
		if(isset($data['prev']['sd_rekmed'])){
			$cek = $this->db->get_where('trx_medical',array('mdc_id'=>$data['prev']['queo_id']));
			if( $cek->num_rows() == 0){
				$dt['sd_rekmed'] = $data['prev']['sd_rekmed'];
				$dt['mdc_id']	 = $data['prev']['queo_id'];
				$dt['pl_id']	 = $data['prev']['pl_id'];
				$dt['dr_id']	 = $data['prev']['dr_id'];
				$dt['mdc_in']	 = date('Y-m-d h:i:s');
				$dt['mdc_status']= 1;
				$this->db->insert('trx_medical',$dt);
			}
		}
		$data['main_view']	= 'poli_kebidanan/proses';
		$data['title']		= 'Pasien Poli Kebidanan';
		$data['cf']			=  $this->cf;
		$data['current']	= '';
		$data['desc']		= 'Description. ';
		$data['mdc_id']		= $data['prev']['queo_id'];
		$this->load->view('template',$data);
	}
	
	function single_rekmed($id){
		$data['id'] = str_replace('#', '', $id);
		$this->load->view('rawat_jalan/poli_kebidanan/single_rekmed',$data);
	}

	function create(){
		$data = $this->input->post('ds');
		$this->db->insert('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		redirect(cur_url(-1));
	}

	function update($id){
		$data = $this->input->post('ds');	
		$this->db->where('', $id);
		$this->db->update('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('', $id);
		$this->db->update('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil dihapus'));
		redirect(cur_url(-2));
	}
	
	
	//kajian objective (ko)
	function kajian($mdc_id){
		$data['mdc_id'] = $mdc_id;
		$this->load->view('poli_kebidanan/kajian',$data);
	}

	function kajian_objective(){
		$data['ko']		= $this->db->get_where('mst_medical_objective',array('pl_id'=>2));
		$this->load->view('poli_kebidanan/kajian_objective',$data);
	}

	function add_kajian_objective(){
		$data = $this->input->post('ds');
		$this->db->insert('mst_medical_objective',$data);
	}

	//kajian subjective (ks)
	function kajian_subjective(){
		$data['ks']		= $this->db->get_where('mst_medical_subjective',array('pl_id'=>2));
		$this->load->view('poli_kebidanan/kajian_subjective',$data);
	}

	function add_kajian_subjective(){
		$data = $this->input->post('ds');
		$this->db->insert('mst_medical_subjective',$data);
	}

	function simpan_kajian(){
		$data = $this->input->post('ds');
		$this->db->insert('trx_medical_subjective',$data);
		$dt = $this->input->post('do');
		$this->db->insert('trx_medical_objective',$dt);
	}
	//kajian medical
	

	// rekam medis
	function rekam_medis($act = "index",$pasien_id = ''){
		if($act == "index"){
			$this->load->view('poli_kebidanan/rekam_medis');	
		}
		else if($act == "load_more"){
			//get  rekam medis where pasien $pasien_id
			$data = array( 0 => array('medical_id' => 900, 'medical_date_time' => "12-10-2012 15:30"), 1 => array('medical_id' => 673, 'medical_date_time' => "12-10-2012 14:00"));
			echo json_encode($data,true);
		}
	}

	// diagnosis
	function diagnosis($id){
		$data['mdc_id']	= $id;
		$data['diagnosa'] = $this->db->get_where('v_detail_diagnosa',array('mdc_id'=>$id));
		$this->load->view('poli_kebidanan/diagnosis',$data);
	}

	function simpan_diagnosis(){
		$data 		= $this->input->post();
		$dt = array();
		$mdc_id = $data['mdc_id'];

		$data_diag = $this->db->get_where('trx_diagnosa_treathment',array('mdc_id'=>$mdc_id));
		if($data_diag->num_rows() >= 1){

			

			$this->db->where('mdc_id',$mdc_id);
			$this->db->delete('trx_diagnosa_treathment_detail');

			$dt_diag  = $data_diag->result();
			$dat_id = $dt_diag[0]->dat_id;

			$i = 0;
			foreach ($data['diagnosa'] as $key => $value) {
				if(! empty($data['diagnosa'][$i])){
					$dt[] = array(
						'mdc_id'	=> $mdc_id,
						'dat_id'	=> $dat_id,
						'dat_diag'	=> $value,
						'dat_treat'	=> $data['tindakan'][$i],
						'dat_case'	=> $data['rj_case_'.$i],
						'dat_paramedic_price'	=> $data['harga_jasa'][$i],
						'dat_item_price'	=> $data['harga'][$i]
					);
				}
				$i++;
			}
			$this->db->insert_batch('trx_diagnosa_treathment_detail',$dt);

		}else{
			

			$dtd['mdc_id'] = $mdc_id;
			$dtd['dat_id'] = $this->gen_id_diagnosa();
			$this->db->insert('trx_diagnosa_treathment',$dtd);

			$i = 0;
			foreach ($data['diagnosa'] as $key => $value) {
				if(! empty($data['diagnosa'][$i])){
					$dt[] = array(
						'mdc_id'	=> $mdc_id,
						'dat_id'	=> $dtd['dat_id'],
						'dat_diag'	=> $value,
						'dat_treat'	=> $data['tindakan'][$i],
						'dat_case'	=> $data['rj_case_'.$i],
						'dat_paramedic_price' => $data['harga_jasa'][$i],
						'dat_item_price'	=> $data['harga'][$i]
					);
				}
				$i++;
			}
		$this->db->insert_batch('trx_diagnosa_treathment_detail',$dt);
		}
	}

	//membuat id resep
	function gen_id_diagnosa(){
		$kd = $this->db->get_where('com_code',array('title'=>'format_diagnosa'));
		if($kd->num_rows() == 1){
			$kode = db_conv($kd);
		}
		$format = $kode->value_1.DATE('ym');

		$q = $this->db->query("SELECT substr(dat_id, 9, 4) as n 
			  from trx_diagnosa_treathment where dat_id like '$format%' order by n desc limit 1"); 
		
		if($q->num_rows() == 0){
			$no = '0001';
		}else{
			$nl = intval(db_conv($q)->n);
			$nl++; 
			$no = rtrim(sprintf("%'04s\n",$nl));
		}
		return $format.$no;		
	}

	// resep dan obat
	function resep($id){
		$data['resep']	= $this->db->get_where('v_detail_resep',array('mdc_id'=>$id));
		$data['mdc_id'] = $id;
		$this->load->view('poli_kebidanan/resep',$data);
	}

	function simpan_resep(){
		$dt 		= $this->input->post();
		$mdc_id 	= $dt['mdc_id'];
		$harga_jasa = 0;
		$data_resep = $this->db->get_where('trx_recipe',array('mdc_id'=>$mdc_id));
		$dt_com 	= $this->db->get_where('com_code',array('title'=>'jasa_resep'));
		if($dt_com->num_rows() >= 1){
			$hg_jasa = db_conv($dt_com);
			$harga_jasa = $hg_jasa->value_1;
		}
		//variabel :
		$dt_resep 	= array();
		$i = 0;
		$total = 0;

		if($data_resep->num_rows() >= 1){

			//mengupdate total recipe
			$dtr['recipe_paramedic_price'] = $harga_jasa;
			$this->db->where('mdc_id',$mdc_id);
			$this->db->update('trx_recipe',$dtr);

			//menghapus detail
			$this->db->where('mdc_id',$mdc_id);
			$this->db->delete('trx_recipe_detail');

			//insert detail :
			$dt_res = $data_resep->result();
			$id 	= $dt_res[0]->recipe_id;
			$i = 0;
			foreach ($dt['resep'] as $key => $value) {
				if(! empty($dt['resep'][$i])){
					$dt_resep[] = array(
						'recipe_medicine'	=> $value,
						'mdc_id'			=> $dt['mdc_id'],
						'recipe_rule'		=> $dt['use'][$i],
						'recipe_qty'		=> $dt['qty'][$i],
						'recipe_item_price'	=> $dt['harga'][$i] * $dt['qty'][$i],
						'recipe_id'			=> $id,
						'recipe_number'		=> $i+1
					);
				}
				$i++;
			}
			$this->db->insert_batch('trx_recipe_detail',$dt_resep);

			$dt_stat['recipe_status'] = 1;
			$this->db->where('mdc_id',$mdc_id);
			$this->db->update('trx_medical',$dt_stat);
		}else{
			
			
			$dtr['mdc_id']			= $mdc_id;
			$dtr['recipe_id']		= $this->gen_id();
			$dtr['recipe_paramedic_price'] = $harga_jasa;
			$this->db->insert('trx_recipe',$dtr);
			

			$id = $dtr['recipe_id'];
			$i = 0;
			foreach ($dt['resep'] as $key => $value) {
				if(! empty($dt['resep'][$i])){
					$dt_resep[] = array(
						'recipe_medicine'	=> $value,
						'mdc_id'			=> $dt['mdc_id'],
						'recipe_rule'		=> $dt['use'][$i],
						'recipe_qty'		=> $dt['qty'][$i],
						'recipe_item_price'	=> $dt['harga'][$i] * $dt['qty'][$i],
						'recipe_id'			=> $id,
						'recipe_number'		=> $i+1
					);
				}
				$i++;
			}
			$this->db->insert_batch('trx_recipe_detail',$dt_resep);

			$dt_stat['recipe_status'] = 1;
			$this->db->where('mdc_id',$mdc_id);
			$this->db->update('trx_medical',$dt_stat);
		}
	}

	//membuat id resep
	function gen_id(){
		$kd = $this->db->get_where('com_code',array('title'=>'format_resep'));
		if($kd->num_rows() == 1){
			$kode = db_conv($kd);
		}
		$format = $kode->value_1.DATE('ym');

		$q = $this->db->query("SELECT substr(recipe_id, 9, 4) as n 
			  from trx_recipe where recipe_id like '$format%' order by n desc limit 1"); 
		
		if($q->num_rows() == 0){
			$no = '0001';
		}else{
			$nl = intval(db_conv($q)->n);
			$nl++; 
			$no = rtrim(sprintf("%'04s\n",$nl));
		}
		return $format.$no;		
	}


	//keterangan
	function keterangan(){
		$this->load->view('poli_kebidanan/keterangan');
	}
	//ringkasan
	function ringkasan($medical_id){
		$data['mdc_id']	= $medical_id;
		$ringkasan	= $this->db->get_where('v_ringkasan',array('mdc_id'=>$medical_id));
		if($ringkasan->num_rows() >= 1){
			$data['ringkasan'] = db_conv($this->db->get_where('v_ringkasan',array('mdc_id'=>$medical_id)));
		}
		$data['obat'] 	= $this->db->get_where('v_detail_resep',array('mdc_id'=>$medical_id));
		$this->load->view('poli_kebidanan/ringkasan',$data);
	}
	//Harga
	function harga($id){
		$data['billing'] 	= $this->db->get('mst_bill');
		$data['mdc_id']		= $id;
		$data['resep'] 		= $this->db->get_where('v_detail_resep',array('mdc_id'=>$id));
		$data['tindakan']	= $this->db->get_where('v_detail_diagnosa',array('mdc_id'=>$id));
		$this->load->view('poli_kebidanan/harga',$data);
	}

	function update_harga(){
		$dt = $this->input->post();
		// debug_array($dt);
		$i = 0;
		if(isset($dt['id_obat'])){
			foreach ($dt['id_obat'] as $key => $value) {
				$data['dokter_price'] = $dt['resep_dokter_price'][$i];
				$where = array('recipe_medicine'=>$value,'mdc_id'=>$dt['mdc_id']);
				$this->db->where($where);
				$this->db->update('trx_recipe_detail',$data);
				$i++;
			}	
		}
		


		$i = 0;
		if(isset($dt['id_tindakan'])){
			foreach ($dt['id_tindakan'] as $key => $value) {
				$data_tindakan['dokter_price'] = $dt['tindakan_dokter_price'][$i];
				$where_tindakan = array('dat_treat'=>$value,'mdc_id'=>$dt['mdc_id']);
				$this->db->where($where_tindakan);
				$this->db->update('trx_diagnosa_treathment_detail',$data_tindakan);
				// debug_array($this->db->last_query(),true);
				$i++;
			}
		}
		// debug_array($data);
		
	}

	function simpan_harga(){
		$mdc_id = $this->input->post('mdc_id');
		$dt['mdc_status'] = 1;
		$this->db->where('mdc_id',$mdc_id);
		$this->db->update('trx_medical',$dt);

	}

	function simpan_kirim(){
		$this->db->trans_start();
		$mdc_id = $this->input->post('mdc_id');
		$dt['mdc_status'] = 2;
		$this->db->where('mdc_id',$mdc_id);
		$this->db->update('trx_medical',$dt);
		$this->calculate_bill($mdc_id);
		$this->db->trans_complete();
	}

	//hanief
	function calculate_bill($mdc_id){
		//get doctor price
		$dr = $this->db->select('recipe_paramedic_price')->where('mdc_id',$mdc_id)->get('trx_recipe')->row();
		$dr_price = $dr->recipe_paramedic_price;
		//get data recipe and add it in trx_bill
		$aa = $this->db->select('mdc_id,recipe_medicine,mdcn_name,recipe_rule,recipe_qty,recipe_item_price')->join('mst_medicine mm','mm.mdcn_id = trd.recipe_medicine')->where('mdc_id',$mdc_id)->get('trx_recipe_detail trd')->result();
		foreach($aa as $a){
			//obat dan resep : 'bill_id' => 5
			$arr[] = array(
				'mdc_id' 	=> $mdc_id,
				'bill_id' 	=> 5,
				'desc' 	=> $a->mdcn_name.'  ('.$a->recipe_medicine.') x'.$a->recipe_qty.' ('.$a->recipe_rule.')',
				'amount'	=> $a->recipe_item_price + $dr_price,
				'tanggungan'	=> $a->recipe_item_price + $dr_price,
			);
		}
		$this->db->insert_batch('trx_bill',$arr);
		$arr = array();
		//get data tindakan and add it in trx_bill
		$this->db->select('dat_id,dat_case,dat_diag,diag_name,dat_treat,treat_name,dat_paramedic_price,dat_item_price');
		$this->db->join('mst_treathment mt','tdtd.dat_treat = mt.treat_id');
		$this->db->join('mst_diagnosa md','tdtd.dat_diag = md.diag_id');
		$aa = $this->db->where('mdc_id',$mdc_id)->get('trx_diagnosa_treathment_detail tdtd')->result();
		foreach($aa as $a){
			//obat dan resep : 'bill_id' => 5
			$arr[] = array(
				'mdc_id' 	=> $mdc_id,
				'bill_id' 	=> 6,
				'desc' 	=> $a->diag_name.' - '.$a->treat_name.' ('.$a->dat_case.')',
				'amount'	=> $a->dat_paramedic_price + $a->dat_item_price,
				'tanggungan'	=> $a->dat_paramedic_price + $a->dat_item_price,
			);
		}
		$this->db->insert_batch('trx_bill',$arr);
		//last_step
		$this->db->select('SUM(amount) AS total_amount, SUM(tanggungan) AS total_pay');
		$this->db->where('mdc_id', $mdc_id);
		$data = $this->db->get('trx_bill')->row();
		//SELECT bill_id, SUM(amount) AS total_amount, SUM(tanggungan) AS total_tanggungan FROM trx_bill WHERE mdc_id='12110001' GROUP BY bill_id
		
		$this->db->where('mdc_id',$mdc_id);
		$this->db->update('trx_medical',$data);
	}
	
	function get_diagnosa(){
		$query = $this->input->get('q'); 
		$this->db->select('*');
		$this->db->from('mst_diagnosa');
		if($query!=null){
			$this->db->like('lower(diag_name)', "$query",'both');
		}
		$data = $this->db->get()->result();
		
		foreach ($data as $datas) {
			$data['results'][] = array(
				'id'	=> "$datas->diag_id",
				'name'	=> "$datas->diag_name"
			);
		}

		echo json_encode($data);
	} 

	function json_get_tindakan($id){
		$tindakan = $this->db->get_where('mst_treathment',array('treat_id'=>$id));
		$data = array();
		$total = 0;
		foreach($tindakan->result() as $d){
			$data = array(
				'id'	=> $d->treat_id,
				'name'	=> $d->treat_name,
				'harga'	=> $d->treat_item_price,
				'harga_jasa' => $d->treat_paramedic_price
			);
		}
		echo json_encode($data);
	}

	function get_tindakan(){
		$query = $this->input->get('q'); 
		$this->db->select('*');
		$this->db->from('mst_treathment');
		if($query!=null){
			$this->db->like('lower(treat_name)', "$query",'both');
		}
		$data = $this->db->get()->result();
		
		foreach ($data as $datas) {
			$data['results'][] = array(
				'id'	=> "$datas->treat_id",
				'name'	=> "$datas->treat_name"
			);
		}

		echo json_encode($data);
	} 


	//resep dan obat
	function get_resep(){
		$query = $this->input->get('q'); 
		$this->db->select('*');
		$this->db->from('inv_item_master');
		if($query!=null){
			$this->db->like('lower(im_name)', "$query",'both');
		}
		$data = $this->db->get()->result();
		
		foreach ($data as $datas) {
			$data['results'][] = array(
				'id'	=> "$datas->im_id",
				'name'	=> "$datas->im_name"
			);
		}

		echo json_encode($data);
	} 

	function json_get_resep($id){
		$obat = $this->db->get_where('inv_item_master',array('im_id'=>$id));
		$data = array();
		$total = 0;
		foreach($obat->result() as $d){
			$data = array(
				'id'	=> $d->im_id,
				'name'	=> $d->im_name,
				'harga'	=> $d->im_item_price
			);
		}
		echo json_encode($data);
	}

	function ket_sakit(){
		$rm = "1";
		$data = $this->input->post('ds');
		// $this->db->insert('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		echo cur_url(-1)."cetak_ket/$rm";
	}

	function cetak_ket($rm){
		$data['main_view']	= 'poli_kebidanan/cetak_ket';
		$this->load->view('template_print',$data);
	}

	function sk_pengantar(){
		$rm = "1";
		$data = $this->input->post('ds');
		// $this->db->insert('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		echo cur_url(-1)."cetak_sk/$rm";
	}

	function cetak_sk($rm){
		$data['main_view']	= 'poli_kebidanan/cetak_sk';
		$this->load->view('template_print',$data);
	}

	function pengiriman(){
		$rm = "1";
		$data = $this->input->post('ds');
		// $this->db->insert('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		echo cur_url(-1)."cetak_kirim/$rm";
	}

	function cetak_kirim($rm){
		$data['main_view']	= 'poli_kebidanan/cetak_kirim';
		$this->load->view('template_print',$data);
	}

	function data_diri($rm){
		$data['main_view']	= 'poli_kebidanan/data_diri';
		$data_pasien		= $this->db->get_where('v_queue_poli',array('sd_rekmed'=>$rm));
		if($data_pasien->num_rows() >= 1){
			$data['pasien'] 	= db_conv($this->db->get_where('v_queue_poli',array('sd_rekmed'=>$rm)));
		}
		
		$this->load->view('poli_kebidanan/data_diri',$data);
	}
}