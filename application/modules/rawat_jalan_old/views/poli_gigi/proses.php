<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" style="background:#E5E5E5;">
            <div class="title" style="margin-bottom:0px;"><h3>Data Pasien</h3></div>
            <br clear="all">
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Nomor RM</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: 082312</b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Nama</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: Sigit Hanafi</b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Jenis Kelamin</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: laki-laki</b>
                    </div>
                </div>
            </div>

            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Umur</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: 32 Tahun</b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Gol. Darah</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: O</b>
                    </div>
                </div>
            </div>

            
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0px;margin-left:0px;" >
                <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" style="padding-left:0px;margin-left:0px;">
                    <li class="ui-state-default ui-corner-top"><a href="#" surl="<?=base_url('rawat_jalan/poli_gigi/rekam_medis')?>">Rekam Medis</a></li>
                    <li class="ui-state-default ui-corner-top"><a href="#" surl="<?=base_url('rawat_jalan/poli_gigi/kajian')?>">Kajian</a></li>
                    <li class="ui-state-default ui-corner-top"><a href="#" surl="<?=base_url('rawat_jalan/poli_gigi/diagnosis')?>">Diagnosis</a></li>
                    <li class="ui-state-default ui-corner-top"><a href="#" surl="<?=base_url('rawat_jalan/poli_gigi/resep')?>">Resep dan Obat</a></li>
                </ul>
                <div id="page">

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    $(function(){
        $("#tabs ul li a").click(function(){
            var surl =  $(this).attr("surl");
            $("#page").load(surl);
            $("#tabs ul li").each(function(){
                $(this).removeClass('ui-state-active ui-tabs-selected');
            })
            $(this).parent().addClass("ui-state-active ui-tabs-selected");
            return false;
        })
        $("#tabs ul li:eq(1) a").trigger('click');
    });
</script>