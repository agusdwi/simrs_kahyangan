<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
        	<div class="title"><h3>Rekam Medis</h3></div>
            <br clear="all" />
            <!--?=form_open(cur_url().'create',array('class' => 'stdform','id' => 'form')); ?-->
                <style>
                    table#diagnose_form td{
                        margin:0px;
                        border:1px #CCCCCC solid;
                        vertical-align: top;
                    }
                    table#diagnose_form td textarea{ 
                        overflow: scroll;
                        width: 100%;
                        padding:0px;
                    }
                    table#form_resep td,table#form_resep th{
                        padding:5px;
                        border:1px #CCCCCC solid;
                        vertical-align: top;   
                    }
                </style>
                <table id="diagnose_form" style="width:100%">
                    <tr><td style="padding:3px">Diagnosa</td><td style="padding:3px">Tindakan</td><td style="padding:3px">Kasus</td></tr>
                    <?for($i = 0; $i<5;$i++){?>
                        <tr>
                            <td><textarea id="rj_diagnose_<?=$i?>" name="rj_diagnose[]" rows="3" ></textarea></td>
                            <td><textarea id="rj_act_<?=$i?>" name="rj_act[]" rows="3"></textarea></td>
                            <td style="padding:5px"><input type="radio" name="rj_case_<?=$i?>" value="new">Kasus Baru<br/><input type="radio" name="rj_case_<?=$i?>" value="old">Kasus Lama</td>
                        </tr>
                    <?}?>
                </table>
        </div><!-- #div.span12-->
    </div>
</div>