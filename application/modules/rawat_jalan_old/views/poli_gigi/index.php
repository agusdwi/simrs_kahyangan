<style type="text/css" media="screen">
	.dataTables_scrollHead{
		margin-bottom:-25px;
	}
</style>
<script type="text/javascript" charset="utf-8">
	$(function(){

		//table scrol Yalert();
		var otb;
		otb = $('.def_table_y').dataTable( {
			"sPaginationType": "bootstrap",
			"sScrollY": "400px",
			"bPaginate": false
	    });
		$(".search_choice a").click(function(){
			$(".active").removeClass('active');
			$(this).addClass('active');
			if($(this).attr('atr') == 'bsc'){
				$("#advance").hide();
				$("#basic").show();
				$(".mediuminput").focus();
			}else{
				$("#advance").show();
				$("#basic").hide();
				$(".smallinput").focus();
			}
			return false;
		})
		$(".tb_scrol tbody tr").click(function(){
			$(".tb_scrol tbody tr.active").removeClass('active');
			$(".black_loader").fadeIn(300).delay(800).fadeOut(300);
			$(this).addClass('active');
		})
	})
</script>
<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>       
</div><!--pageheader-->
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span7">
			<div class="title">
				<h3>Antrian Poli Gigi  
					<span class="tgl-sekarang"></span> 
				</h3>
			</div>
			
			<table class="table table-bordered def_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
			<thead>
				<tr role="row">
					<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:15%" aria-label="Nama: activate to sort column ascending">Nomor Antrian</th>
					<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:40%" aria-label="Harga: activate to sort column ascending">Nama Pasien/ No. RM</th>
					<th class="sorting" role="columnheader" tabindex="0" aria-controls="src_table" rowspan="1" colspan="1" style="width:45%" aria-label="Aksi: activate to sort column ascending">Alamat</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="center">
						<b>1</b>
					</td>
					<td>
						<b>Sigit Hanafi</b>
						<br>
						<b><i>RM : 87878787</i></b>
					</td>
					<td>
						<i>Jalan Monjali Km 30 No 80</i>
					</td>
				</tr>
				<tr>
					<td class="center">
						<b>1</b>
					</td>
					<td>
						<b>Sigit Hanafi</b>
						<br>
						<b><i>RM : 87878787</i></b>
					</td>
					<td>
						<i>Jalan Monjali Km 30 No 80</i>
					</td>
				</tr>
				<tr>
					<td class="center">
						<b>1</b>
					</td>
					<td>
						<b>Sigit Hanafi</b>
						<br>
						<b><i>RM : 87878787</i></b>
					</td>
					<td>
						<i>Jalan Monjali Km 30 No 80</i>
					</td>
				</tr>
				<tr>
					<td class="center">
						<b>1</b>
					</td>
					<td>
						<b>Sigit Hanafi</b>
						<br>
						<b><i>RM : 87878787</i></b>
					</td>
					<td>
						<i>Jalan Monjali Km 30 No 80</i>
					</td>
				</tr>
				<tr>
					<td class="center">
						<b>2</b>
					</td>
					<td>
						<b>Sigit Hanafi</b>
						<br>
						<b>RM : 87878787</b>
					</td>
					<td>
						<i>Jalan Monjali Km 30 No 80</i>
					</td>
				</tr>
				<tr>
					<td class="center">
						<b>3</b>
					</td>
					<td>
						<b>Sigit Hanafi</b>
						<br>
						<b>RM : 87878787</b>
					</td>
					<td>
						<i>Jalan Monjali Km 30 No 80</i>
					</td>
				</tr><tr>
					<td class="center">
						<b>4</b>
					</td>
					<td>
						<b>Sigit Hanafi</b>
						<br>
						<b>RM : 87878787</b>
					</td>
					<td>
						<i>Jalan Monjali Km 30 No 80</i>
					</td>
				</tr>
			</tbody>
			</table>
		</div>
		<div class="span5">
			<div class="title">
				<h3>Data Diri Pasien Sigit Hanafi  
					<span class="tgl-sekarang"></span> 
				</h3>
			</div>
			<div style="width:100%;border:1px solid #DDD;position:relative;">
				<div class="black_loader">
					<img src="<?=get_loader(11)?>">
				</div>
				<?=form_open(cur_url().'proses',array('class' => 'stdform','id' => 'form')); ?>
					
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;height:40px;float:left;text-align:right;">
							<label>Nomor RM </label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b>8789789</b>
						</div>
					</div>
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;height:40px;float:left;text-align:right;">
							<label>Nama Lengkap</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b>Sigit Hanafi</b>
						</div>
					</div> 
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;float:left;height:40px;text-align:right;">
							<label>Jenis Kelamin</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b>Laki- laki</b>
						</div>
					</div> 
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;float:left;height:40px;text-align:right;">
							<label>Alamat</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b>Jalan Monjali Nomor 80 km 5</b>
						</div>
					</div> 
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;height:40px;float:left;text-align:right;">
							<label>Umur</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b>30 Tahun</b>
						</div>
					</div> 
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;float:left;height:40px;text-align:right;">
							<label>Gol. Darah</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b>AB</b>
						</div>
					</div> <br><br>
					<br clear="all">
					<div class="form-actions" style="margin:0px;vertical-align:bottom;">
						<button type="submit" class="btn btn-primary">Tindakan Lanjut</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
