<table class="table_keterangan">
	<tbody style="">
		<tr><td style="width:25%"></td><td></td></tr>
		<tr><td style="width:25%"></td><td></td></tr>
		<tr><td style="width:25%"></td><td></td></tr>
		<tr><td colspan=2><div class="title">Surat Keterangan Sakit</div></td></tr>
		<tr>
			<td>
				Hari, Mulai Tanggal
			</td>
			<td>09/10/2012<span> s/d </span>10/10/2012</td>
		</tr>
		<tr>
			<td>
				<div class="title">Surat Keterangan</div>
			</td>
			<td>
				<textarea style="width:75%"></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<label>Menerangkan</label>
			</td>
			<td>
				<textarea style="width:75%"></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<label>LIB</label>
			</td>
			<td>
				<input type="text" name="month_number" style="width:50px">
			</td>
		</tr>
		<tr><td colspan=2><div class="title">Surat Keterangan Cuti Persalinan (3 bulan)</div></td></tr>
		<tr>
			<td>
				Hari, Mulai Tanggal
			</td>
			<td>09/10/2012<span> s/d </span>10/10/2012</td>
		</tr>
		<tr>
			<td>
				<div class="title">Surat Keterangan</div>
			</td>
			<td>
				<textarea style="width:75%"></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<label>Menerangkan</label>
			</td>
			<td>
				<textarea style="width:75%"></textarea>
			</td>
		</tr>
		<tr><td><label>Surat ini digunakan untuk</label></td><td><textarea style="width:75%"></textarea></td></tr>
		<tr>
			<td><label>Tekanan Darah</label></td>
			<td>
				<input class="small" type="text" name="blood" style="width:50px"> Ma/Hg 
				Tinggi <input class="small" type="text" name="height" style="width:50px"> Cm
				Berat <input class="small" type="text" name="weight" style="width:50px"> Kg
			</td>
		</tr>
		<tr>
			<td colspan=2>
				<div class="title">Surat Keterangan Tidak Buta Warna</div>
			</td>
		</tr>
		<tr>
			<td>
				<label>Keadaan Mata</label>
			</td>
			<td>
				<input class="small" type="text" name="eye_condition">
			</td>
		</tr>
		<tr><td><label>Visus</label></td><td><input class="small" type="text" name="visus"></td></tr>
		<tr><td colspan=2><div class="title">Surat Keterangan Bebas Narkoba</div></td></tr>
		<tr><td><label>Pada pemeriksaan saat ini</label></td><td><textarea style="width:75%"></textarea></td></tr>
		<tr><td><label>Surat ini digunakan untuk</label></td><td><textarea style="width:75%"></textarea></td></tr>
	</tbody>
</table>