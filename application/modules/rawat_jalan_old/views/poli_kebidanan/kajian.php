<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.gritter.css" />
<style>
	.box-kajian{
		height:205px;
		background-color:#DDDDDD;
		padding:5px;
		overflow-y:scroll;
	}
	.table-kajian td{
		padding-left: 10px;
	}
    .check{
        margin-right: 2px;
    }

    .alert{
        background-color: transparent;
        border: 0px;
    }

    #gritter-notice-wrapper{
        right: 13%;
        top: 100px;
    }
    label{
        display: inline-block;
    }
</style>


<div id="gritter-notice-wrapper" class="alert hide" style="width:750px;position:fixed">
    <div id="gritter-item-1" class="gritter-item-wrapper" style="margin:0 -17px 5px 0">
        <div class="gritter-top"></div>
        <div class="gritter-item">
            <div class="gritter-close" style="display: none; width:50px "></div>
            <img src="<?=base_url()?>assets/img/demo/envelope.png" class="gritter-image">
            <div class="gritter-with-image" style="width:448px">
                <span class="gritter-title" style="margin-left:36px">Message</span>
                <p>Data Berhasil Disimpan   </p>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="gritter-bottom"></div>
    </div>
</div>

<div>
	<div class="title"><h3>Kajian Subjektif</h3></div>
	<br clear="all"/>
        	<div class="span8">
            		<div class="box-kajian" id="box_kajian_subjective">
            			
            		</div> <!-- #div.box-kajian -->
                
        		<div style="padding:5px">
        			<span>
                    <?=form_open(cur_url().'add_kajian',array('class' => 'form-horizontal','id' => 'form_ks')); ?>
                        <label style="float:left">ADD KAJIAN :  </label><input type="text" id="ms_indication" name="ds[ms_indication]" style="width:200px;margin:0px"/> 
                        <input type="radio" name="ds[ms_filter]" style="margin-top:-2px;" value="S"/>&nbsp;  S
                        <input type="radio" name="ds[ms_filter]" style="margin-top:-2px;" value="N"/>&nbsp;  N
                        <input type="radio" name="ds[ms_filter]" style="margin-top:-2px;" value="J"/>&nbsp;  J
                        <input type="submit"  class="btn btn-primary" style="float:right" value="Tambah Kajian"></input>
                    <?=form_close()?> 
                    </span>
        		</div><!-- #div add kajian -->
                <?=form_open(cur_url(),array('class' => 'form-horizontal','id' => 'form_kajian_su')); ?>
                    <input type="hidden" name="ds[mdc_id]" value="<?=$mdc_id?>">
                    <div>
                        <textarea id="des_kajian" style="width:100%" rows="4" placeholder="tuliskan deskripsi kajian anda disini" name="ds[ms_desc]" ></textarea>
                    </div>
                <?=form_close()?> 
        	</div><!-- #div.span8 -->
        	<div class="span3">
        		<div class="formwrapper" style="overflow:auto;height:125px;width:100%;border:1px #DDDDDD solid">
                    <label style="margin-left:4px"><input type="checkbox" name="subjstudy_ket[]" />&nbsp;Depan bawah</label>
                    <label style="margin-left:4px"><input type="checkbox" name="subjstudy_ket[]" />&nbsp;Kiri atas</label>
                    <label style="margin-left:4px"><input type="checkbox" name="subjstudy_ket[]" />&nbsp;Kiri bawah</label>
                    <label style="margin-left:4px"><input type="checkbox" name="subjstudy_ket[]" />&nbsp;Kanan atas</label>
                    <label style="margin-left:4px"><input type="checkbox" name="subjstudy_ket[]" />&nbsp;Kanan bawah</label>
                    <label style="margin-left:4px"><input type="checkbox" name="subjstudy_ket[]" />&nbsp;Sakit</label>
                    <label style="margin-left:4px"><input type="checkbox" name="subjstudy_ket[]" />&nbsp;Tidak sakit</label>
                </div>
                <div>
                    Nilai : <br/>
                    <textarea style="width:90%"></textarea>
                </div>
        	</div><!-- #div.span3 -->
        
        <br clear="all"/>
        <div>
            <div class="title">
                <h3>Kajian Objektif</h3>
            </div>
            <br clear="all"/>
            <div class="span8" >

                <div class="box-kajian" id="box_kajian_objective">
                        
                </div> <!-- #div.box-kajian -->
               
                <div style="padding:5px">
                    <span>
                    <?=form_open(cur_url().'add_kajian_objective',array('class' => 'form-horizontal','id' => 'form_ko')); ?>
                       <label style="float:left">ADD KAJIAN :  </label><input type="text" id="mo_indication" name="ds[mo_indication]" style="width:200px;margin:0px"/> 
                        <input type="radio" name="ds[mo_filter]" style="margin-top:-2px;" value="S"/>&nbsp;  S
                        <input type="radio" name="ds[mo_filter]" style="margin-top:-2px;" value="N"/>&nbsp;  N
                        <input type="radio" name="ds[mo_filter]" style="margin-top:-2px;" value="J"/>&nbsp;  J
                        <input type="submit" class="btn btn-primary" style="float:right" value="Tambah Kajian"></input>
                    <?=form_close()?> 
                    </span>
                </div><!-- #div add kajian -->
                <?=form_open(cur_url(),array('class' => 'form-horizontal','id' => 'form_kajian_ob')); ?>
                <input type="hidden" name="do[mdc_id]" value="<?=$mdc_id?>">
                <div>
                    <textarea name="do[mo_desc]"style="width:100%" rows="4" placeholder="tuliskan deskripsi kajian anda disini" id="des_kajian_ob" ></textarea>
                </div>
                 <?=form_close()?> 
            </div><!-- #div.span8 -->
            <div class="span3" >
                <div class="formwrapper" style="overflow:auto;height:125px;width:100%;border:1px #DDDDDD solid">
                    <label style="margin-left:4px"><input type="checkbox" name="subjstudy_ket[]" />&nbsp;Depan bawah</label>
                    <label style="margin-left:4px"><input type="checkbox" name="subjstudy_ket[]" />&nbsp;Kiri atas</label>
                    <label style="margin-left:4px"><input type="checkbox" name="subjstudy_ket[]" />&nbsp;Kiri bawah</label>
                </div>
                <div>
                    Nilai : <br/>
                    <textarea style="width:90%"></textarea>
                </div>
            </div><!-- #div.span3 -->
        </div><!-- #div kajian objektif -->
    
</div>
<div class="form-actions" style="margin-top: 35%;margin-bottom: 0px;height:20px">
    <button type="submit" style="float:right;" class="btn btn-primary simpan">Simpan</button>
</div>


<script>
    $(function(){

        isvalid = $("#form_ko").validate({
            rules: {
                'ds[mo_indication]': "required"
            },
            submitHandler: function(form) {
                var url  = "<?=base_url()?>rawat_jalan/poli_tulang/add_kajian_objective";
                var data = jQuery(form).serialize();
                $.post(url,data, function(data){
                    $(".alert").fadeIn().delay(500).fadeOut(function(){
                        $("#save").trigger('click'); 
                        var url = '<?=base_url()?>rawat_jalan/poli_tulang/kajian_objective';
                        $("#box_kajian_objective").load(url);
                        $("#mo_indication").val('');
                    });     
                }); 
                return false;
            }
        });

        isvalid_ks = $("#form_ks").validate({
            rules: {
                'ds[ms_indication]': "required"
            },
            submitHandler: function(form) {
                var url  = "<?=base_url()?>rawat_jalan/poli_tulang/add_kajian_subjective";
                var data = jQuery(form).serialize();
                $.post(url,data, function(data){
                    $(".alert").fadeIn().delay(500).fadeOut(function(){
                        $("#save").trigger('click'); 
                         var url  = "<?=base_url()?>rawat_jalan/poli_tulang/kajian_subjective";
                        $("#box_kajian_subjective").load(url);
                        $("#ms_indication").val('');
                    });     
                }); 
                return false;
            }
        });

        $(".simpan").click(function(){
            var url  = "<?=base_url()?>rawat_jalan/poli_tulang/simpan_kajian";
            var data = $('#form_kajian_su').serialize()+"&"+$('#form_kajian_ob').serialize();
            $.post(url,data, function(data){
                $(".alert").fadeIn().delay(500).fadeOut(function(){
                    $("#save").trigger('click'); 
                });     
            }); 
            return false;
        });
           


        var url = '<?=base_url()?>rawat_jalan/poli_tulang/kajian_objective';
        $("#box_kajian_objective").load(url);

         var url_ks = '<?=base_url()?>rawat_jalan/poli_tulang/kajian_subjective';
        $("#box_kajian_subjective").load(url_ks);

        $("#save").click(function(){})

    })
</script>