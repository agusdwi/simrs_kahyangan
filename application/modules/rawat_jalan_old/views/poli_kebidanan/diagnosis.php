<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.gritter.css" />
<script>
	var ags = 0;
	var i = 0;
$(function(){

	

	$(".tbh").click(function(){
		add();					
	})

	
	function add(){
		i++;
		var str = ("<tr>");
			str += ("<td style='text-align:center;vertical-align:middle;width:10%;border-left:none;'><b>"+i+"</b></td>");
			str += ("<td style='width:10%;'><div id='"+i+"_fx_diagnosa' class='fx_diagnosa' style='width:98%;border-bottom:1px dotted gray;''></div></td>");
			str += ("<td style='width:10%;'><div id='"+i+"_fx_tindakan' class='fx_tindakan' style='width:98%;border-bottom:1px dotted gray;''></div><input type='hidden' id='"+i+"_harga' name='harga[]' class='harga' ><input type='hidden' id='"+i+"_harga_jasa' name='harga_jasa[]' class='harga_jasa' ></td>");
			str += ("<td style='padding:5px;width:200px;'><input type='radio' id='"+i+"_case_new' name='rj_case_"+(i-1)+"' value='new'  > &nbsp;Kasus Baru<br/><input type='radio' id='"+i+"_case_old' name='rj_case_"+(i-1)+"' value='old'  > &nbsp;Kasus Lama</td>");
			str += ("</tr>");
			$("#diag tbody").append(str);
		$('#'+i+'_fx_diagnosa').flexbox(BASE+'rawat_jalan/poli_tulang/get_diagnosa',{
			paging: false,
			showArrow: false ,
			maxVisibleRows : 0,
			width : 300,
			resultTemplate: '{id} - {name}',
		});
		$('#'+i+'_fx_tindakan').flexbox(BASE+'rawat_jalan/poli_tulang/get_tindakan',{
			paging: false,
			showArrow: false ,
			maxVisibleRows : 0,
			width : 300,
			resultTemplate: '{id} - {name}',
			onSelect: function() {
				var id = $(this).parent().find('input:eq(0)').val();
				var id_harga = $(this).parent().parent().find('input:eq(2)').attr('id');
				var id_harga_jasa = $(this).parent().parent().find('input:eq(3)').attr('id');
				$.getJSON(BASE+"rawat_jalan/poli_tulang/json_get_tindakan/"+id, function(json) {
					$("#"+id_harga).val(json.harga);
					$("#"+id_harga_jasa).val(json.harga_jasa);
				})
			}
		});
		$(".fx_diagnosa").find('input:eq(0)').attr('name','diagnosa[]');
		$(".fx_tindakan").find('input:eq(0)').attr('name','tindakan[]');
		$(".fx_diagnosa").find('input:eq(1)').attr('name','');
		$(".fx_tindakan").find('input:eq(1)').attr('name','');
	}



	isvalid = $("#form_diagnosa").validate({
		rules: {
            
        },
        submitHandler: function(form) {
            var url  = "<?=base_url()?>rawat_jalan/poli_tulang/simpan_diagnosis";
            var data = jQuery(form).serialize();
            $.post(url,data, function(data){
                $(".alert").fadeIn().delay(500).fadeOut(function(){
                    $("#save").trigger('click'); 
                });     
            }); 
            return false;
        }
    });

    $("#save").click(function(){})

    for(var j = 1; j <= 5; j++) {
				$(".tbh").trigger('click');
			};

})

</script>

<style type="text/css">
	.tables thead tr th{
		height:28px;
		font-size:13px;
		vertical-align: middle;
	}

	.ffb-input{
		background: transparent;
		border: none;
		/*border-bottom: 1px dotted gray;*/
	}
	.table_tr thead th{
		height: 28px;
		vertical-align: middle;
		font-size: 13px;
	}
	.alert{
        background-color: transparent;
        border: 0px;
    }

    #gritter-notice-wrapper{
        right: 13%;
        top: 100px;
    }
</style>

<?
	$i = 0;
	if (isset($diagnosa)){
		foreach ($diagnosa->result() as $key => $value) {
			$i++;
			?>
			<script>
				$(".tbh").trigger('click');
				$("#"+<?=$i?>+"_fx_diagnosa_hidden").val('<?=$value->dat_diag?>');
				$("#"+<?=$i?>+"_fx_diagnosa_input").val('<?=$value->diag_name?>');
				$("#"+<?=$i?>+"_fx_tindakan_hidden").val('<?=$value->dat_treat?>');
				$("#"+<?=$i?>+"_fx_tindakan_input").val('<?=$value->treat_name?>');
				$("#"+<?=$i?>+"_harga_jasa").val('<?=$value->dat_paramedic_price?>');
				$("#"+<?=$i?>+"_harga").val('<?=$value->dat_item_price?>');
				$("#")
			</script>
			<?
			// debug_array($value->dat_case,true);
			if(($value->dat_case) == 'new'){
				?>
					<script>
						$("#"+<?=$i?>+"_case_new").attr('checked','checked');
					</script>
				<?
			}else{
				?>
					<script>
						$("#"+<?=$i?>+"_case_old").attr('checked','checked');
					</script>
				<?
			}
		}
	}else{
		?>
			<script>
			
			</script>
		<?
	}
?>

<div id="gritter-notice-wrapper" class="alert hide" style="width:750px;position:fixed">
    <div id="gritter-item-1" class="gritter-item-wrapper" style="margin:0 -17px 5px 0">
        <div class="gritter-top"></div>
        <div class="gritter-item">
            <div class="gritter-close" style="display: none; width:50px "></div>
            <img src="d<?=base_url()?>assets/img/demo/envelope.png" class="gritter-image">
            <div class="gritter-with-image" style="width:448px">
                <span class="gritter-title" style="margin-left:36px">Message</span>
                <p>Data Berhasil Disimpan   </p>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="gritter-bottom"></div>
    </div>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<?=form_open(base_url().'rawat_jalan/poli_tulang/simpan_diagnosis',array('class' => 'form-horizontal','id' => 'form_diagnosa')); ?>
				<input type="hidden" name="mdc_id" value=<?=$mdc_id?> >
				<div class="widget-content nopadding">
					<table id="diag" cellpadding="0" cellspacing="0" border="0" class="tabel-dokter table table-bordered table_tr" style="border-left:none;">
						<thead>
							<tr role="row">
								<th class="head0" style="border-left:none;">Nomor</th>
								<th class="head1">Diagnosa</th>
								<th class="head2">Tindakan</th>
								<th class="head3">Jenis kasus</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4" style="border-left:none;">
									<button type="button" class="btn btn-warning tbh" style="margin-left:45%;">Tambah</button>
								</td>
							</tr>
						</tfoot>
					</table>							
				</div>
				<div class="form-actions" style="margin-bottom:0px;margin-top:0px;">
					<button type="submit" class="btn btn-primary" style="float:right;">Simpan</button>
				</div>
				<?=form_close()?> 
			</div>
		</div>
	</div>
</div>
