<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" style="background:#E5E5E5;">
            <div class="title" style="margin-bottom:0px;"><h3>Data Pasien</h3></div>
            <br clear="all">
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Nomor Antrian</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: <?=$prev['queo_id']?></b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Nomor RM</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: <?=$prev['sd_rekmed']?></b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Nama</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: <?=$prev['sd_name']?></b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Jenis Kelamin</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: <?=strtolower($prev['sd_sex']) == 'l'? 'Laki-laki' : 'Perempuan'; ?></b>
                    </div>
                </div>
            </div>

            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Umur</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>:  <?=$prev['sd_age']?> Tahun</b>
                    </div>
                </div>
            </div>
            <div class="span1">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Gol. Darah</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>:  <?=$prev['sd_blood_tp']?></b>
                    </div>
                </div>
            </div>

            
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0px;margin-left:0px;" >
                <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" style="padding-left:0px;margin-left:0px;">
                    <li class="ui-state-default ui-corner-top"><a href="#" surl="<?=base_url('rawat_jalan/poli_tulang/rekam_medis')?>" id="tab_rekmed">Rekam Medis</a></li>
                    <li class="ui-state-default ui-corner-top"><a href="#" surl='<?=base_url("rawat_jalan/poli_tulang/kajian/$mdc_id")?>' id="tab_kajian">Kajian</a></li>
                    <li class="ui-state-default ui-corner-top"><a href="#" surl='<?=base_url("rawat_jalan/poli_tulang/diagnosis/$mdc_id")?>' id="tab_diagnosis">Diagnosis</a></li>
                    <li class="ui-state-default ui-corner-top"><a href="#" surl='<?=base_url("rawat_jalan/poli_tulang/resep/$mdc_id")?>' id="tab_obat">Resep dan Obat</a></li>
                    <li class="ui-state-default ui-corner-top"><a href="#" surl='<?=base_url("rawat_jalan/poli_tulang/keterangan/$mdc_id")?>' id="tab_keterangan">Keterangan</a></li>
                    <li class="ui-state-default ui-corner-top"><a href="#" surl='<?=base_url("rawat_jalan/poli_tulang/harga/$mdc_id")?>' id="tab_harga">Harga</a></li>
                    <li class="ui-state-default ui-corner-top"><a href="#" surl='<?=base_url("rawat_jalan/poli_tulang/ringkasan/$mdc_id")?>' id="tab_ringkasan">Ringkasan</a></li>
                </ul>
                <div id="page">
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(function(){
        $("#tabs ul li a").click(function(){
            var surl =  $(this).attr("surl");
            $("#page").load(surl);
            $("#tabs ul li").each(function(){
                $(this).removeClass('ui-state-active ui-tabs-selected');
            })
            $(this).parent().addClass("ui-state-active ui-tabs-selected");
            return false;
        })
        $("#tabs ul li:eq(1) a").trigger('click');

        $('#fx_diagnosa').flexbox(BASE+'rawat_jalan/poli_tulang/get_diagnosa', {
            paging: false,
            showArrow: false ,
            maxVisibleRows : 10,
            width : 250,
            resultTemplate: '<div class="col1">{id}</div><div class="col2">{name}</div>',
        })
    });
</script>