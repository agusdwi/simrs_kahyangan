<div class="container-fluid">
    <div class="row-fluid">
        <div class="tabbable tabs-left">
            <? $med_recs = array('1021' => '04-11-2012 05:15','1001' =>'01-11-2012 08:51','960' =>'31-10-2012 08:03', '0' => 'load_more');?>
            <ul class="nav nav-tabs" id="list_rekmed">
                <?foreach($med_recs as $i => $med_rec):?>
                    <li class="pilih"><a href="#<?=$i?>" data-toggle="tab"><?=$med_rec?></a></li>
                <?endforeach;?>
            </ul>
            <div class="tab-content" id="detail_rekmed">
                <?foreach($med_recs as $i => $med_rec):?>
                    <div class="tab-pane" id="<?=$i?>">...</div>
                <?endforeach;?>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        //combine ajax.get data from controller and tab
        $('ul#list_rekmed a[data-toggle="tab"]').live('click',function(e){
            href = $(this).attr('href');
            if(href != '#0'){
                $.get('<?=base_url()?>rawat_jalan/poli_tulang/single_rekmed/id', function(data) {
                    $(href).html(data);
                });
            }
            else{
                load_more = $("ul#list_rekmed li").eq(-1).html();
                $('ul#list_rekmed li').eq(-1).remove();
                //if load more
                $.get('<?=base_url()?>rawat_jalan/poli_tulang/rekam_medis/load_more/', function(data) {
                    batas = 0;
                    $(data).each(function(d){
                        $("ul#list_rekmed").append("<li><a href='#"+data[d].medical_id+"' data-toggle='tab' >"+data[d].medical_date_time+"</a></li>");
                        $('#detail_rekmed').append('<div class="tab-pane" id="'+data[d].medical_id+'">...</div>')
                        batas++;
                    })
                    $("ul#list_rekmed").append("<li>"+load_more+"</li>");
                },'json');
            }
            return false;
        })
        
        $('ul#list_rekmed a[data-toggle="tab"]:eq(0)').trigger('click');
    })
</script>