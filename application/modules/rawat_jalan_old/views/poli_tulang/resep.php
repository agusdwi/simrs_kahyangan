<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.gritter.css" />
<script>
var i = 0;
$(function(){
	
	$(".tbh").click(function(){
		add();					
		
	});

	function add(){
		i++;
			var str = ("<tr>");
			str += ("<td style='text-align:center'><b>"+i+"</b></td>");
			str += ("<td><div id='"+i+"_fx_resep' class='fx_resep' style='width:98%;border-bottom:1px dotted gray;''></div><input type='hidden' id='"+i+"_harga' name='harga[]' class='harga' ></td>");
			str += ("<td><input type='text' name='use[]' id='"+i+"_aturan' style='width:98%;background:transparent;border: none;border-bottom: 1px dotted gray;'></td>");
			str += ("<td><input type='text' name='qty[]' id='"+i+"_jumlah' style='width:98%;background:transparent;border: none;border-bottom: 1px dotted gray;text-align:center'></td>");
			str += ("</tr>");
			$("#rsp tbody").append(str);

			$('#'+i+'_fx_resep').flexbox(BASE+'rawat_jalan/poli_tulang/get_resep',{
				paging: false,
				showArrow: false ,
				maxVisibleRows : 0,
				width : 300,
				resultTemplate: '{id} - {name}',
				onSelect: function() {
					var hv = $(this).parent().find('input:eq(0)').val();
					var id_harga = $(this).parent().parent().find('input:eq(2)').attr('id');
					$.getJSON(BASE+"rawat_jalan/poli_tulang/json_get_resep/"+hv, function(json) {
						$("#"+id_harga).val(json.harga);
					})
				}
			});
			$(".fx_resep").find('input:eq(0)').attr('name','resep[]');
			$(".fx_resep").find('input:eq(1)').attr('name','');

	}

	for(var j = 1; j <= 5; j++) {
		$(".tbh").trigger('click');
	};

	isvalid = $("#form_resep").validate({
		rules: {
            
        },
        submitHandler: function(form) {
            var url  = "<?=base_url()?>rawat_jalan/poli_tulang/simpan_resep";
            var data = jQuery(form).serialize();
            $.post(url,data, function(data){
                $(".alert").fadeIn().delay(500).fadeOut(function(){
                    $("#save").trigger('click'); 
                });     
            }); 
            return false;
        }
    });


    $("#save").click(function(){})

});
</script>

<?
	$i = 0;
	if (isset($resep)){
		foreach ($resep->result() as $key => $value) {
			$i++;
			?>
			<script>
				$(".tbh").trigger('click');
				$("#"+<?=$i?>+"_fx_resep_hidden").val('<?=$value->recipe_medicine?>');
				$("#"+<?=$i?>+"_fx_resep_input").val('<?=$value->im_name?>');
				$("#"+<?=$i?>+"_aturan").val('<?=$value->recipe_rule?>');
				$("#"+<?=$i?>+"_jumlah").val('<?=$value->recipe_qty?>');
				$("#"+<?=$i?>+"_harga").val('<?=$value->im_item_price?>');
			</script>
			<?
		}
	}else{
		
	}
?>
<style type="text/css">
	.tables thead tr th{
		height:28px;
		font-size:13px;
		vertical-align: middle;
	}

	#resep[]_ctr.ffb{
		width:300px !important;
		top: 28px !important;
	}

	#resep[]_ctr .row .col1{
		float:left;
		width:100px;
	}
	#resep[]_ctr .row .col2{
		float:left;
		width:200px;
	}
	.ffb-input{
		background: transparent;
		border: none;
	}
	.table_tr thead th{
		height: 28px;
		vertical-align: middle;
		font-size: 13px;
	}
	.alert{
        background-color: transparent;
        border: 0px;
    }

    #gritter-notice-wrapper{
        right: 13%;
        top: 100px;
    }
</style>

<div id="gritter-notice-wrapper" class="alert hide" style="width:750px;position:fixed">
    <div id="gritter-item-1" class="gritter-item-wrapper" style="margin:0 -17px 5px 0">
        <div class="gritter-top"></div>
        <div class="gritter-item">
            <div class="gritter-close" style="display: none; width:50px "></div>
            <img src="<?=base_url()?>assets/img/demo/envelope.png" class="gritter-image">
            <div class="gritter-with-image" style="width:448px">
                <span class="gritter-title" style="margin-left:36px">Message</span>
                <p>Data Berhasil Disimpan   </p>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="gritter-bottom"></div>
    </div>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<?=form_open(base_url().'rawat_jalan/poli_tulang/simpan_resep',array('class' => 'form-horizontal','id' => 'form_resep')); ?>
			<input type="hidden" name="mdc_id" value=<?=$mdc_id?> > 
			<div class="widget-box">
				<div class="widget-content nopadding">
				<table id="rsp" class="table table-bordered def_table_y dataTable table_tr" align="center" style="margin-left:0px;width:100%;border-left:none;" id="table1">
				<thead>
					<tr role="row">
						<th style="width:2%;border-left:none;">Nomor</th>
						<th style="width:30%;">Obat</th>
						<th style="width:30%;">Aturan Pakai</th>	
						<th style="width:20%;">Jumlah</th>	
					</tr>
				</thead>
				<tbody>
					
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4" style="border-left:none;">
							<button type="button" id="" class="btn btn-warning tbh" style="margin-left:45%;">Tambah Obat</button>
						</td>
					</tr>
				</tfoot>
				</table>
				</div>
				<div class="form-actions" style="margin-bottom:0px;">
					<input type="submit"  class="btn btn-primary" style="float:right" value="Simpan"></a>
				</div>
			</div>
			<?=form_close()?> 
		</div>
	</div>
</div>	
