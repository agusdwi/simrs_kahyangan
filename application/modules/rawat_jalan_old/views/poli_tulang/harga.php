<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.gritter.css" />
<script>

$(function(){

	isvalid = $("#form_harga").validate({
		rules: {
            
        },
        submitHandler: function(form) {
            var url  = "<?=base_url()?>rawat_jalan/poli_tulang/update_harga";
            var data = jQuery(form).serialize();
            $.post(url,data, function(data){
                $(".alert").fadeIn().delay(500).fadeOut(function(){
                    $("#save").trigger('click'); 
                });     
            }); 
            return false;
        }
    });

    isvalid = $("#simpan_kirim").validate({
		rules: {
            
        },
        submitHandler: function(form) {
            var url  = "<?=base_url()?>rawat_jalan/poli_tulang/simpan_kirim";
            var data = jQuery(form).serialize();
            $.post(url,data, function(data){
                $(".alert").fadeIn().delay(500).fadeOut(function(){
                    $("#save").trigger('click'); 
                });     
            }); 
            return false;
        }
    });

    $('.simpan').click(function(){
    	$("#form_harga").submit();
    })

    $("#save").click(function(){})

})

</script>
<style>
	.harga-input{
		box-shadow: 0px 0px 3px 3px rgba(0, 0, 0.075, 0.075) inset;
	}
	.table_tr thead th{
		height: 28px;
		vertical-align: middle;
		font-size: 13px;
	}
	.table_tr tbody th{
		height: 28px;
		vertical-align: middle;
		font-size: 13px;
	}
	.alert{
        background-color: transparent;
        border: 0px;
    }

    #gritter-notice-wrapper{
        right: 13%;
        top: 100px;
    }
</style>
<?
	// debug_array($tindakan->result());
?>


<div id="gritter-notice-wrapper" class="alert hide" style="width:750px;position:fixed">
    <div id="gritter-item-1" class="gritter-item-wrapper" style="margin:0 -17px 5px 0">
        <div class="gritter-top"></div>
        <div class="gritter-item">
            <div class="gritter-close" style="display: none; width:50px "></div>
            <img src="d<?=base_url()?>assets/img/demo/envelope.png" class="gritter-image">
            <div class="gritter-with-image" style="width:448px">
                <span class="gritter-title" style="margin-left:36px">Message</span>
                <p>Data Berhasil Disimpan   </p>
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="gritter-bottom"></div>
    </div>
</div>


<div class='container-fluid'>
	<div class='row-fluid'>
		<div class='span12'>
			<div class="widget-box">
				<div class="widget-content nopadding">
					<?=form_open(base_url().'rawat_jalan/poli_tulang/simpan_harga',array('class' => 'form-horizontal','id' => 'form_harga')); ?>	
					<input type="hidden" name="mdc_id" value="<?=$mdc_id?>">				
						<table class="table table-bordered table-striped table_tr" style="border-left:none;margin-bottom:0px;">
							<thead>
								<tr>
									<th>No.</th>
									<th>Jenis</th>
									<th>Harga Item</th>
									<th>Harga Jasa</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
								<?
									if(isset($resep)){
										?>
										<tr>
											<td colspan="5" style="text-align:left;font-weight:bold;border-left:0px;">Pembelian Obat</td>
										</tr>
										<?
										$i = 1;
										$jasa = 0;
										$total = 0;
										$total_jasa = 0;
										$total_item = 0;
										foreach ($resep->result() as $key => $value) {
											if($i == 1){
												$jasa = $value->recipe_paramedic_price;
											}else{
												$jasa = 0;
											}
											?>
												<tr>
													<td style="border-left:0px;text-align:center;width:20px;"><?=$i?></td>
													<td>
														<!-- <input type="hidden" name="id_obat[]" value="<?=$value->recipe_medicine?>" > -->
														<?=$value->recipe_medicine."/".$value->im_name?> &nbsp;&nbsp;(<?=$value->recipe_rule?>) qty : <?=$value->recipe_qty?>
													</td>
													<td style="text-align:right;"><?=$value->recipe_item_price?></td>
													<td style="text-align:right;"><?=$jasa?></td>
													<!-- <td style="text-align:right;"><input type="text" name="resep_dokter_price[]" style="border:none;border-bottom:1px dotted gray;text-align:right" value="<?=$value->dokter_price?>" ></td> -->
													<td style="text-align:right;"><?=($value->recipe_item_price + $jasa)?></td>
												</tr>
											<?
											$i++;
											$total_jasa += $jasa;
											$total_item += $value->recipe_item_price;
										}
									}
								?>
								<?
									if(isset($tindakan)){
										?>
											<tr>
												<td colspan="5" style="text-align:left;font-weight:bold;border-left:0px;">Tindakan</td>
											</tr>
										<?
										$i = 1;
										foreach ($tindakan->result() as $key => $value) {
											?>
												<tr>
													<td style="border-left:0px;text-align:center;width:20px;"><?=$i?></td>
													<td style="text-align:left;">
														<!-- <input type="hidden" name="id_tindakan[]" value="<?=$value->dat_treat?>" > -->
														<?=$value->treat_name?> kasus : <?=$value->dat_case?>
													</td>
													<td style="text-align:right;"><?=$value->dat_item_price?></td>
													<td style="text-align:right;"><?=$value->dat_paramedic_price?></td>
													<!-- <td style="text-align:right;"><input type="text" name="tindakan_dokter_price[]" style="border:none;border-bottom:1px dotted gray;text-align:right" value="<?=$value->dokter_price?>" ></td> -->
													<td style="text-align:right;"><?=($value->dat_paramedic_price + $value->dat_item_price)?></td>
												</tr>
											<?
											$i++;
											$total_jasa += $value->dat_paramedic_price;
											$total_item += $value->dat_item_price;
										}
									}
									$total = $total_item + $total_jasa;
								?>
								<tr>
									<th colspan="2" style="text-align:left;">Total</th>
									<th style="text-align:right;"><?=$total_item?></th>
									<th style="text-align:right;"><?=$total_jasa?></th>
									<th style="text-align:right;"><?=$total?></th>
								</tr>
							</tbody>
							<tfoot>
					
							</tfoot>
						</table>
						<?=form_close()?>
						<?=form_open(base_url().'rawat_jalan/poli_tulang/simpan_kirim',array('class' => 'form-horizontal','id' => 'simpan_kirim')); ?>
					<div class='form-actions' style='text-align:right;margin-top:0px;'>
						<button class="btn btn-warning">Batal</button>
						<button type="button" class="btn btn-success simpan">Simpan</button>
						<input type="hidden" name="mdc_id" value="<?=$mdc_id?>">
						<button type="submit" class="btn btn-primary" style="float:right;">Simpan dan Kirim</button>
						<?=form_close()?>
					</div>
				</div>
				
			</div>
	</div>
</div>
</div>
