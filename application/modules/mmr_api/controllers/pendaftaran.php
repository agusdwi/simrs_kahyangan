<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// error code
// 0 => patient not exist
// 1 => patien already registered

class Pendaftaran extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->url_qrcode = base_url()."qrcode/?data=";
	}

	public function cek(){
		$id 	= $this->input->post('id');
		$regID 	= $this->input->post('regID');
		$api_out = array();

		// cek availibility
		$ptn = $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$id));
		if ($ptn->num_rows() == 1) {
			$user = $this->db->get_where('mmr_api_user',array('sd_rekmed'=>$id));
			if ($user->num_rows == 0) {
				$ptn 	= $ptn->row();
				$date 	= date("Y-m-d H:i:s");
				$token 	= md5($ptn->sd_rekmed.$date);

				$data = array(
					"sd_rekmed"		=> $ptn->sd_rekmed,
					"api_token"		=> $token,
					"api_reg_date"	=> $date,
					"regID"			=> $regID
				);

				$this->db->insert('mmr_api_user',$data);

				$api_out['success'] 		= 1;
				$api_out['token'] 			= $token;
				$api_out['info']			= "patient register success";

			}else{
				$api_out['success'] 		= 0;

				$user = $user->row();
				if($user->api_aktif == "0"){
					$api_out['error_code']		= 1;
					$api_out['info']			= "patient already resgitered but pending approval";
				}else{
					$api_out['error_code']		= 2;
					$api_out['info']			= "patient already resgitered";
				}
			}
		}else {
			$api_out['success'] 		= 0;
			$api_out['error_code']		= 0;
			$api_out['info']			= "patient not exist";
		}

		echo json_encode($api_out);
	}

	public function getInitData(){
		if (is_post()) {
			$token 		= $this->input->post('token');
			$ptn_api 	= $this->db->get_where('mmr_api_user',array('api_token'=>$token))->row();
			$ptn_data 	= $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$ptn_api->sd_rekmed))->row();
			$data = array(
				'ptn_api'	=> $ptn_api,
				'ptn_data'	=> $ptn_data,
				'qrcode'	=> $this->generate_qrcode($ptn_api->sd_rekmed)
				);
			echo json_encode(array('success'=>1,'data'=>$data));
		}else{
			echo json_encode(array('error_code'=>'400','info'=>'bad request'));
		}
	}

	public function cekaktif(){
		if (is_post()) {
			$rekmed 	= $this->input->post('rekmed');
			$ptn_api 	= $this->db->get_where('mmr_api_user',array('sd_rekmed'=>$rekmed))->row();
			if($ptn_api->api_aktif == 1)
				echo json_encode(array('success'=>1,'token'=>$ptn_api->api_token));
		}else{
			echo json_encode(array('error_code'=>'400','info'=>'bad request'));
		}		
	}

	function generate_qrcode($rekmed){
		$url = $this->url_qrcode.$rekmed;
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = json_decode(curl_exec($ch));

		return base_url().$result->image;
	}

	public function password(){
		$token 		= $this->input->post('token');
		$password 	= $this->input->post('password');
		$api_out = array();

		// cek availibility
		$ptn = $this->db->get_where('mmr_api_user',array('api_token'=>$token));
		if ($ptn->num_rows() == 1) {
			$ptn = $ptn->row();

			$data = array('api_password' =>  md5($password));
			$this->db->where('api_id', $ptn->api_id);
			$this->db->update('mmr_api_user', $data); 

			$api_out['success'] 		= 1;
			$api_out['info']			= "password register success";
		}else {
			$api_out['success'] 		= 0;
			$api_out['error_code']		= 0;
			$api_out['info']			= "token not exist";
		}

		echo json_encode($api_out);
	}

	public function upload_foto_profil($token){
		$user = $this->db->get_where('mmr_api_user',array('api_token'=>$token));
		if ($user->num_rows() == 1) {
			$user = $user->row();
			
			$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
			$config['upload_path']		= 'files/avatar/';
			$config['allowed_types']	= '*';
			$config['max_size']			= '100000';
			$config['max_width']		= '500000';
			$config['max_height']		= '500000';
			$config['encrypt_name']		= true;
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload()){
				$api_out['success'] 		= 0;
				$api_out['error_code']		= 0;
				$api_out['info']			= $this->upload->display_errors();
			}else{
				$a = $this->upload->data();

				$this->load->library('image_moo');
				$this->image_moo
					->load('files/avatar/'.$a['file_name'])
					->resize_crop(300,300)
					->save('files/avatar/'.$a['file_name'],true);

				$data = array(
				               'foto' =>  $a['file_name']
				            );
				$this->db->where('api_id', $user->api_id);
				$this->db->update('mmr_api_user', $data); 

				$api_out['success'] 		= 1;
				$api_out['info']			= "upload success";
				$api_out['filename']		= base_url('files/avatar')."/".$a['file_name'];
			}
		}else{
			$api_out['success'] 		= 0;
			$api_out['info']			= "not authorized";
			$api_out['filename']		= "";
		}

		echo json_encode($api_out);
	}
}