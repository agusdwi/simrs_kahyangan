<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>
<div class="container-fluid">
	<div class="row-fluid">
		<a href="<?=base_url();?>pendaftaran/daftar_pasien" style="float:right" class="btn btn-warning"></i>Kembali</a>
		<div class="span5" >
			<div class="title"><h3>Detail Pasien </h3></div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12">
				<table id='tb_dokter' class="table table-bordered">
					<tbody>
						<tr><td width="100px">No.REKMED</td><td><?=$pasien->sd_rekmed?></td></tr>
						<tr><td>Nama Pasien</td><td><?=$pasien->sd_name?></td></tr>
					</tbody>
				</table>

				<table class="table table-bordered">
					<thead>
						<th>No</th>
						<th>Nama Keluarga</th>
						<th>Status Keluarga</th>
						<th>Alamat</th>
						<th>No.Telp</th>
					</thead>
					<tbody>
						<?php $i=1; foreach ($detail->result() as $dt):?>
						<tr>
							<td width="30px"><?=$i++?></td>
							<td width="100px"><?=$dt->nama_fam?></td>
							<td width="100px"><?=$dt->hub?></td>
							<td width="100px"><?=$dt->alamat?></td>
							<td width="100px"><?=$dt->phone?>/<?=$dt->telp?></td>
						</tr>
						<?endforeach?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
