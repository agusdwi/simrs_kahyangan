<div class="pageheader notab">
    <h1 class="pagetitle">Pasien Rujukan Rawat Inap</h1>
</div>

<div style="width:100%;border:1px solid #DDD;position:relative;">
	<?=form_open(cur_url(),array('class' => 'stdform','id' => 'formAntrian')); ?>
		<br clear="all">
		<div style="width:100%;padding-left:30px;">
			<div style="width:100px;float:left;height:40px;text-align:right;">
				<label>Tanggal</label>
			</div>
			<div style="margin-left:30px;float:left;">
				<input type="text" name="date" class="datepicker" value="<?=DATE('d-m-Y');?>">
			</div>
		</div><br clear="all"> 
		<div style="width:100%;padding-left:30px;">
			<div style="width:100px;float:left;height:40px;text-align:right;">
				<label>Dokter</label>
			</div>
			<div style="margin-left:30px;float:left;">
				<?=get_dropdown_dokter('ds[dr_id]');?>
			</div>
		</div> 
		<br clear="all">
		<div style="width:100%;padding-left:30px;">
			<div style="width:100px;height:40px;float:left;text-align:right;">
				<label>No Rekmed</label>
			</div>
			<div style="margin-left:30px;float:left;">
				<b><?=$this->mdc->sd_rekmed;?></b>
			</div>
			<input type="hidden" name="ds[sd_rekmed]" value="<?=$this->mdc->sd_rekmed;?>">
		</div> 
		<br clear="all">
		<div style="width:100%;padding-left:30px;">
			<div style="width:100px;float:left;height:40px;text-align:right;">
				<label>Nama Pasien</label>
			</div>
			<div style="margin-left:30px;float:left;">
				<b id="name"><?=$this->ptn->sd_name;?></b>
			</div>
		</div> 
		<br clear="all">
		<div style="width:100%;padding-left:30px;">
			<div style="width:100px;height:40px;float:left;text-align:right;">
				<label>Asuransi</label>
			</div>
			<div style="margin-left:30px;float:left;">
				<div>
					<div class="controls">
						<select id="tp_insurance" style="width:140px" name="ds[tp_insurance]">
							<?foreach ($tp_insurance->result() as $key): ?>
							<option value="<?=$key->ins_id?>"><?=$key->ins_name?></option>	
							<?endforeach ?>
						</select>
					</div>
				</div>
			</div>
		</div>
		<br clear="all">
		<div style="width:100%;padding-left:30px;">
			<div style="width:100px;height:40px;float:left;text-align:right;">
				<label>Ruang</label>
			</div>
			<div style="margin-left:30px;float:left;">
				<div>
					<div class="controls">
						<select id="ruang" style="width:140px" name="ds[ruang_id]">
							<option value="" selected="selected">Choose One</option>
							<?foreach ($ruang->result() as $key): ?>
							<option value="<?=$key->id?>"><?=$key->r_nama?></option>	
							<?endforeach ?>
						</select>
					</div>
				</div>
			</div>
		</div>
		<br clear="all">
		<div style="width:100%;padding-left:30px;">
			<div style="width:100px;height:40px;float:left;text-align:right;">
				<label>Kamar</label>
			</div>
			<div style="margin-left:30px;float:left;">
				<div>
					<div class="controls">
						<select id="cls" style="width:140px" name="ds[cls_id]">
							<option value="" selected="selected">Choose One</option>
							<? for($i=1; $i<=count($ds[1]); $i++) { ?>
							<? $j=0;foreach ($ds[1][$i] as $k): $j++;?>
							<option value="<?=$k->id?>"><?=$k->k_nama?></option>
						<? endforeach ?>
						<? } ?>	
					</select>
				</div>
			</div>
		</div>
	</div>
	<br clear="all">
	<div class="form-actions" style="margin:0px;vertical-align:bottom;">
		<button type="submit" class="btn btn-primary">Simpan</button>
	</div>
</form>
</div>
<script type="text/javascript">
	<?
		$str = array();
		foreach ($kamar->result() as $key) {
			$str[$key->ruang_id][] = array($key->id,$key->k_nama);
		}
		$str_cls = json_encode($str);
	?>
	var str_cls = <?=$str_cls?>;
	var rmLst   = "";

	$(function(){
		$("#ruang").change(function(){
			$("#cls").html('<option value="">Choose One</option>');
			$.each(str_cls[$(this).val()], function( index, value ) {
				$("#cls").append('<option value="'+value[0]+'">'+value[1]+'</option>');
			});
		})
		$("#ruang").val(16);
		$("#ruang").trigger('change');
	})
</script>
