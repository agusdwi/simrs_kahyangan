<script type="text/javascript">
	var chart;
	$(document).ready(function() {
	chart = new Highcharts.Chart({
		chart: {
			renderTo: 'container',
			defaultSeriesType: 'column'
		},
		title: {
			text: '5 Penyakit Terbanyak'
		},
		subtitle: {
			text: 'bulan oktober 2012'
		},
		xAxis: {
			categories: [
				'penyakit'
			]
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Rainfall (mm)'
			}
		},
		legend: {
			layout: 'vertical',
			backgroundColor: '#FFFFFF',
			align: 'left',
			verticalAlign: 'top',
			x: 100,
			y: 70,
			floating: true,
			shadow: true
		},
		tooltip: {
			formatter: function() {
				return ''+
					this.x +': '+ this.y +'';
			}
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
			series: [{
			name: 'Influenza',
			data: [49.9]

		}, {
			name: 'Kolera',
			data: [83.6]

		}, {
			name: 'Malaria',
			data: [48.9]

		}, {
			name: 'Demam Berdarah',
			data: [42.4]

		},{
			name: 'Gigi berlubang',
			data: [30]

		}]
	});
	});
</script>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="pageheader notab">
		    <h1 class="pagetitle"><?=$title?></h1>
		</div>
	</div>
	<div class="row-fluid">
        <div class="span5 center" style="text-align: center;">                 
			<ul class="quickstats">
				<? $def_image = base_url("assets/img/icons/medicoicons/pregnancy.png")?>
				<?foreach ($poli->result() as $key): ?>
					<li>
						<a href="<?=base_url()?>rawat_jalan/poli/antrian/<?=$key->pl_id;?>">
							<!-- <img alt="" src="<?=(!empty($key->pl_img)? base_url('files/poli_icon/'.$key->pl_img) : $def_image);?>"> -->
							<img alt="" src="<?=base_url()?>assets/img/icons/medicoicons/medkit.png">
							<span><?=$key->pl_name;?></span>
						</a>
					</li>	
				<?endforeach ?>
				<li>
					<a href="<?=base_url()?>rawat_jalan/manual">
						<img alt="" src="<?=base_url()?>assets/img/icons/medicoicons/clipboard.png">
						<span>Input Manual</span>
					</a>
				</li>	
			</ul>
        </div>
  		<div class="span7" style="padding:20px 10px">
  			<div id="container"></div>
  		</div>
    </div>
</div>