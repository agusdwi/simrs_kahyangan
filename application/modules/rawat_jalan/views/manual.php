<style type="text/css" media="screen">
	.dataTables_scrollHead{
		margin-bottom:-25px;
		margin-top: 30px;
	}
	.dataTables_scrollBody table.tb_scrol{
		margin-top: 23px;
	}
	.dataTables_filter{
		position: absolute;
		top: 125px
	}
	#fx_pasien_ctr.ffb{
		width:300px !important;
		top: 28px !important;
	}

	#gritter-notice-wrapper{
		right: 13%;
		top: 100px;
	}
	#fx_pasien_ctr .row .col1{
		float:left;
		/*width:50px;*/
	}
	#fx_pasien_ctr .row .col2{
		float:left;
		margin-left: 10px;
		/*width:200px;*/
	}
	#fx_pasien_input{
		width: 207px !important;
	}
</style>
<script type="text/javascript" charset="utf-8">
	var objPasien = <?=json_encode($ds->result())?>;
	$(function(){
		$('.custom_table_y').dataTable( {
			"sPaginationType": "bootstrap",
			"sScrollY": "350px",
			"bPaginate": false,
			"bFilter":true,
			"bInfo":false,
			"bSort": false
		});
	})
</script>
<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>       
</div><!--pageheader-->
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span6">
			<div class="title">
				<h3>Pasien dalam proses pemeriksaan</h3>
			</div>
			
			<table class="table table-bordered custom_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
				<thead>
					<tr role="row">
						<th class="sorting" style="width:15%">Nomor</th>
						<th class="sorting" style="width:40%">Pasien</th>
						<th class="sorting" style="width:30%">Dokter</th>
						<th class="sorting">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?$i=0;foreach ($ds->result() as $key): $i++;?>
						<tr>
							<td class="center">
								<b><?=$i;?></b>
							</td>
							<td>
								<b><?=$key->sd_name;?> / <?=$key->sd_rekmed;?></b>
								<br>
								<b><?=$key->sd_address;?></b>
							</td>
							<td>
								<b><?=$key->dr_name;?></b>
							</td>
							<td style="text-align:center">
								<a href="<?=base_url('rawat_jalan/poli/tinfo')?>/<?=$key->mdc_id;?>">
									<span class="label label-success">proses</span>
								</a>
							</td>
						</tr>	
					<?endforeach;?>
				</tbody>
			</table>
		</div>
		<div class="span6">
			<div class="title">
				<h3>Input Pemeriksaan Pasien Baru</h3>
			</div>
			<div style="width:100%;border:1px solid #DDD;position:relative;">
				<div class="black_loader">
					<img src="<?=get_loader(11)?>">
				</div>
				<?=form_open(cur_url(),array('class' => 'stdform','id' => 'formAntrian')); ?>
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;float:left;height:40px;text-align:right;">
							<label>Tanggal</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<input type="text" name="ds[date]" class="datepicker" value="<?=DATE('d-m-Y');?>">
						</div>
					</div><br clear="all"> 
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;height:40px;float:left;text-align:right;">
							<label>Poli</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<div>
								<div class="controls">
									<select id="poli" style="width:140px" name="ds[poli_id]">
										<option value="" selected="selected">Choose One</option>
										<?foreach ($poli->result() as $key): ?>
										<option value="<?=$key->pl_id?>"><?=$key->pl_name?></option>	
										<?endforeach ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;float:left;height:40px;text-align:right;">
							<label>Dokter</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<?=get_dropdown_dokter('ds[dr_id]');?>
						</div>
					</div> 
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;height:40px;float:left;text-align:right;">
							<label>No Rekmed</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<div id="fx_pasien" name="fx_pasien"></div>	
						</div>
					</div> 
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;float:left;height:40px;text-align:right;">
							<label>Nama Pasien</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b id="name">-</b>
						</div>
					</div> 
					<br clear="all">
					<div class="form-actions" style="margin:0px;vertical-align:bottom;">
						<button type="submit" class="btn btn-primary">Tindakan Lanjut</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#fx_pasien').flexbox(BASE+"pendaftaran/get_pasien", {
			//method : 'POST',
			paging: false,
			showArrow: false ,
			maxVisibleRows : 10,
			width : 100,
			resultTemplate: '<div class="col1">{name}</div><div class="col2">{id}</div>',
			onSelect:function(){
				$("#name").html($("#fx_pasien_hidden").val());
			}

		});
		$("#fx_pasien_input").attr('placeholder','nama / rekmed pasien')
		$("#formAntrian").submit(function(){
			if ($("#poli").val() == '') {
				alert('silahkan memilih poli');
			}else if($("#dd_dokter").val() == ''){
				alert('silahkan memilih dokter');
			}else if($("#fx_pasien_hidden").val() == ''){
				alert('silahkan memilih pasien');
			}else{
				return true;
			}
			return false;
		})
	})
</script>