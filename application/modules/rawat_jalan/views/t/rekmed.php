<style type="text/css">
	#detail_rekmed,#list_rekmed{
		min-height: 200px;
		overflow:hidden;
	}
</style>
<?=$this->load->view('t/a_info');?>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0px;margin-left:0px;" >
                <?=$this->load->view('t/a_menu_tab');?>
                <div id="page">
                	<div class="container-fluid">
                		<div class="row-fluid">
                			<?if (count($date) == 0): ?>
                				<center><h4>pasien belum memiliki riwayat medis</h4></center>
                			<?else: ?>	
	                			<div id="rekmed_tabbable" class="tabbable tabs-left ">
	                				<ul class="nav nav-tabs" id="list_rekmed">
	                					<?foreach($date as $key => $d): $v = (object)$d;?>
	                						<li class="pilih">
	                							<a data-tipe="<?=$v->tipe?>" href="<?=$v->id;?>">
                                                    <?=format_date_time($key,false);?>
                                                    <?if ($v->tipe == 'ranap'): ?>
                                                    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---
                                                    <br> <?=format_date_time($v->out,false);?>
                                                    <?endif;?>
                                                </a>
	                						</li>
	                					<?endforeach;?>
	                				</ul>
	                				<div class="tab-content" id="detail_rekmed">
	                					...
	                				</div>
	                			</div>
                			<?endif ?>
                		</div>
                	</div>	
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(function(){
    	$(".pilih").on('click','a',function(event){
    		event.preventDefault();
            $(".pilih.active").removeClass('active');
    		$(this).parent().addClass('active');
    		$("#detail_rekmed").html('...');
    		$("#detail_rekmed").load(CUR+$(this).attr('href')+'/'+$(this).data('tipe'));
            return false;
    	})
		$('ul#list_rekmed a:eq(0)').trigger('click')
    })
</script>