<?
    $tbs = 'ui-state-active ui-tabs-selected';
    $t3  = $this->uri->segment(3);
?>

<ul id="menu-tab" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" style="padding-left:0px;margin-left:0px;">
    <li class="<?=($t3=='tinfo')? $tbs : '' ;?>" ><a href="<?=cur_url(-2);?>tinfo/<?=$this->mdc->mdc_id?>" id="tab_rekmed">Info Umum</a></li>
    <li class="<?=($t3=='trekmed')? $tbs : '' ;?>" ><a href="<?=cur_url(-2);?>trekmed/<?=$this->mdc->mdc_id?>" id="tab_rekmed">Rekam Medis</a></li>
    <li class="<?=($t3=='tdiagnosa')? $tbs : '' ;?>" ><a href="<?=cur_url(-2);?>tdiagnosa/<?=$this->mdc->mdc_id?>" id="tab_diagnosa">Diagnosa</a></li>
    <li class="<?=($t3=='ttindakan')? $tbs : '' ;?>" ><a href="<?=cur_url(-2);?>ttindakan/<?=$this->mdc->mdc_id?>" id="tab_tindakan">Tindakan</a></li>
    <li class="<?=($t3=='tresep')? $tbs : '' ;?>" ><a href="<?=cur_url(-2);?>tresep/<?=$this->mdc->mdc_id?>" id="tab_obat">Resep & Obat</a></li>
    <li class="<?=($t3=='tparamedis')? $tbs : '' ;?>" ><a href="<?=cur_url(-2);?>tparamedis/<?=$this->mdc->mdc_id?>" id="tab_obat">Paramedis</a></li>
    <li class="<?=($t3=='tharga')? $tbs : '' ;?>" ><a href="<?=cur_url(-2);?>tharga/<?=$this->mdc->mdc_id?>" id="tab_harga">Harga</a></li>
    <li class="<?=($t3=='tringkasan')? $tbs : '' ;?>" ><a href="<?=cur_url(-2);?>tringkasan/<?=$this->mdc->mdc_id?>" id="tab_ringkasan">Ringkasan</a></li>
</ul>
<div class="clear"></div>
<? if (empty($this->mdc->dr_id)): ?>
    <script type="text/javascript">
        $(function(){
           $("#menu-tab").on('click','a',function(){
                alert('silahkan memilih dokter terlebih dahulu dan simpan !');
                return false;
           })
        })
    </script>
<? endif ?>