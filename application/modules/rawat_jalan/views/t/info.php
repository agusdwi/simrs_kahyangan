<style type="text/css">
	.chzn-container .chzn-results{
		max-height: 100px;
	}
</style>
<?=$this->load->view('t/a_info');?>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
        	<?=form_open('',array('class' => 'form-horizontal form','id' => 'form_info')); ?>
	            <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0px;margin-left:0px;" >
	                <?=$this->load->view('t/a_menu_tab');?>
	                <div id="page">
	                	<div class="control-group">
	                		<label class="control-label">No RM</label>
	                		<div class="controls">
	                			<b><?=$this->ptn->sd_rekmed;?></b>
	                		</div>
	                	</div>
	                	<div class="control-group">
	                		<label class="control-label">Nama Pasien</label>
	                		<div class="controls">
	                			<b><?=$this->ptn->sd_name;?></b>
	                		</div>
	                	</div>
	                	<div class="control-group">
	                		<label class="control-label">Tanggal Lahir</label>
	                		<div class="controls">
	                			<b><?=format_date_time($this->ptn->sd_date_of_birth,false);?></b>
	                		</div>
	                	</div>
	                	<div class="control-group">
	                		<label class="control-label">Tanggal Masuk</label>
	                		<div class="controls">
	                			<input type="text" name="ds[mdc_in]" class="datepicker" value="<?=format_date_time($this->mdc->mdc_in,false);?>" style="width:205px">
	                		</div>
	                	</div>
	                	<div class="control-group">
	                		<label class="control-label">Poli</label>
	                		<div class="controls">
	                			<b><?=$this->poli->pl_name;?></b>
	                		</div>
	                	</div>
	                	<div class="control-group">
	                		<label class="control-label">Dokter</label>
	                		<div class="controls">
	                			<?=get_dropdown_dokter('ds[dr_id]',$this->mdc->dr_id);?>
	                		</div>
	                	</div>
	                	<div class="control-group">
	                		<label class="control-label">Asuransi</label>
	                		<div class="controls">
	                			<select name="ds[tp_insurance]" class="chzn">
	                				<?foreach ($insurance->result() as $key): ?>
	                					<option <?=($key->ins_id == $this->mdc->tp_insurance)? 'selected="seleted"' : '';?> value="<?=$key->ins_id;?>"><?=$key->ins_name;?></option>
	                				<?endforeach;?>
	                			</select>
	                		</div>
	                	</div>
	                	<div class="control-group" style="background:rgba(228,181,89,0.3)">
	                		<label class="control-label">Alergi</label>
	                		<div class="controls">
	                			<input type="text" class="small" name="ptn[alergi]" value="<?=$alergi;?>">
	                		</div>
	                	</div>
	                	<div class="control-group">
	                		<label class="control-label">Catatan</label>
	                		<div class="controls">
	                			<textarea class="medium" name="ds[note]"><?=$this->mdc->note;?></textarea>
	                		</div>
	                	</div>

	                </div>
	                <?=$this->load->view('t/a_bottom');?>
	            </div>
        	</form>
        </div>
    </div>
</div>
<? if (empty($this->mdc->dr_id)): ?>
    <script type="text/javascript">
        $(function(){
        	$("#form_info").submit(function(){
        		if ($("#dd_dokter").val() == '') {
        			alert('Dokter wajib di isi !!!');
        			return false;
        		}
        	})
        })
    </script>
<? endif ?>