<?
	if ($bill->num_rows() == 1) {
		$b = $bill->row();
		$bl_note 		= $b->bl_note;
		$bl_discount 	= $b->bl_discount;
	}else{
		$bl_note 		= "";
		$bl_discount 	= "";
	}
?>
<style type="text/css">
	.table td:first-child{
		border-left: 1px solid #dddddd !important;
	}
</style>
<?=$this->load->view('t/a_info');?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">			
			<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0px;margin-left:0px;" >
				<?=form_open('',array('id' => 'form_bill')); ?>
					<?=$this->load->view('t/a_menu_tab');?>
						<div style="padding:0px 10px 0px 10px">
							<table class="table table-bordered table-striped table_tr" style="border-left:none;margin-bottom:0px;">
								<thead>
									<tr>
										<th>No.</th>
										<th>Jenis</th>
										<th>Harga Item</th>
										<th>Qty</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="5" style="text-align:left;font-weight:bold;border-left:0px;">Pembelian Obat</td>
									</tr>
									<?$i=$ztot=0;foreach ($obat as $key): $i++?>
										<tr>
											<td style="border-left:0px;text-align:center;width:20px;"><?=$i;?></td>
											<td>
												<?if ($key->is_racik == 1): ?>
												 	<b>(racik)</b> <?=$key->recipe_racik;?>
												<?else:?>
													<b>(<?=$key->mdcn_code;?>)</b><?=$key->mdcn_name;?>
												<?endif;?> 
											</td>		 	
											<td class="money"> <?=int_to_money($key->harga);?></td>
											<td class="money"> <?=$key->recipe_qty;?></td>
											<td class="money"> 
												<?
													$itot = $key->recipe_qty*$key->harga;
													$ztot+= $itot;
													echo int_to_money($itot);
												?>
											</td>
										</tr>	
									<?endforeach;?>
									<tr>
										<td colspan="5" style="text-align:left;font-weight:bold;border-left:0px;">Tindakan / jasa medis</td>
									</tr>
									<?foreach ($treat->result() as $key): $i++;?>
										<tr>
											<td><?=$i;?></td>
											<td><b>(<?=$key->treat_code;?>)</b> <?=$key->treat_name;?></td>
											<td class="money"><?=int_to_money($key->total);?></td>
											<td class="money"><?=$key->jumlah;?></td>
											<td class="money"> 
												<?
													$itot = $key->total*$key->jumlah;
													$ztot+= $itot;
													echo int_to_money($itot);
												?>
											</td>
										</tr>
									<?endforeach;?>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Total</td>
										<td class="money"><?=int_to_money($ztot);?></td>
									</tr>
									<tr>
										<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Diskon</td>
										<td class="money"><?=int_to_money(empty($bl_discount)?0:$bl_discount);?></td>
									</tr>
									<tr>
										<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Grand Total</td>
										<td class="money"><?=int_to_money($ztot - $bl_discount);?></td>
									</tr>

								</tfoot>
							</table>
							<div id="dform" class="well" style="width:365px;margin-left:10px;margin-top:10px;padding:10px">
								<input type="hidden" name="bill[bl_total]" value="<?=$ztot;?>">
								<b>Catatan</b>
								<textarea style="margin-bottom:0px;width:350px" rows="2" name="bill[bl_note]" placeholder="catatan pembayaran" ><?=$bl_note;?></textarea><br>
								<br>
								<p><b>Diskon / potongan harga</b> Rp.<input type="text" class="tx_small" name="bill[bl_discount]" value="<?=$bl_discount;?>"></p>
							</div>
						</div>
					<?=$this->load->view('t/a_bottom');?>
				<?=form_close()?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$.post('',$("#form_bill").serialize(),function(){

		});
	})	
</script>