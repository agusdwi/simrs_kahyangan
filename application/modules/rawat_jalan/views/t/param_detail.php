<script type="text/javascript">
	var irow = 1;
</script>
<?=form_open('',array('id'=>'frmtarif'))?>
<table class="table table-bordered data-table" id="tb_main">
	<thead>
		<tr>
			<th>Role</th>
			<th>Paramedis</th>
			<th>Biaya</th>
		</tr>
	</thead>
	<tbody>
		<?if ($tarif->num_rows()>0): ?>
			<?$i=0;$tot=0;foreach ($tarif->result() as $key):$i++;?>
				<tr>
					<td>
						<select name="r[<?=$i;?>]" class="sl_role">
							<?foreach ($role->result() as $k): ?>
								<option value="<?=$k->role_id;?>" 
									<?=(($k->role_id == $key->role_id)?'selected="selected"':'');?>>
									<?=$k->role_name;?>
								</option>
							<?endforeach;?>
						</select>
						<?if ($i>1): ?>
							<a class="label label-important onhover del" href="#">x</a>
						<?endif;?>
					</td>
					<td>
						<select class="ajchzn" name="p[<?=$i;?>]">
							<?foreach ($dr->result() as $d): ?>
								<option value="<?=$d->dr_id;?>"
									<?=(($d->dr_id == $key->param_id)?'selected="selected"':'');?>>
									<?=$d->dr_name;?></option>
							<?endforeach;?>
						</select>
					</td>	
					<td>
						<input type="text" class="tx_small" name="t[<?=$i;?>]" value="<?=$key->treat_tarif_harga;?>">
						<?$tot+=$key->treat_tarif_harga;?>
					</td>	
				</tr>
			<?endforeach;?>
		<?else:?>
			<?$i=0;$tot=0;foreach ($ds->result() as $key):$i++;?>
				<tr>
					<td>
						<select name="r[<?=$i;?>]" class="sl_role">
							<?foreach ($role->result() as $k): ?>
								<option value="<?=$k->role_id;?>" 
									<?=(($k->role_id == $key->role_id)?'selected="selected"':'');?>>
									<?=$k->role_name;?>
								</option>
							<?endforeach;?>
						</select>
						<?if ($i>1): ?>
							<a class="label label-important onhover del" href="#">x</a>
						<?endif;?>
					</td>
					<td>
						<select id="<?=($i==1)?'def_dr':''?>" class="ajchzn" name="p[<?=$i;?>]">
							<?foreach ($dr->result() as $d): ?>
								<option value="<?=$d->dr_id;?>"><?=$d->dr_name;?></option>
							<?endforeach;?>
						</select>
					</td>	
					<td>
						<input type="text" class="tx_small" name="t[<?=$i;?>]" value="<?=$key->treat_tarif_harga;?>">
						<?$tot+=$key->treat_tarif_harga;?>
					</td>	
				</tr>
			<?endforeach;?>
			<?if ($ds->num_rows() == 0): ?>
				<?$i=1;?>
				<tr>
					<td>
						<select name="r[<?=$i;?>]" class="sl_role">
							<?foreach ($role->result() as $key): ?>
								<option value="<?=$key->role_id;?>"><?=$key->role_name;?></option>
							<?endforeach;?>
						</select>
					</td>
					<td>
						<select id="def_dr" class="ajchzn" name="p[<?=$i;?>]">
							<?foreach ($dr->result() as $d): ?>
								<option value="<?=$d->dr_id;?>"><?=$d->dr_name;?></option>
							<?endforeach;?>
						</select>
					</td>	
					<td>
						<input class="tx_small" name="t[<?=$i;?>]" value="" type="text">
					</td>	
				</tr>
				<?$i++;?>
			<?endif;?>
		<?endif;?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2">
				<b>Total</b>
			</td>
			<td><?=$tot;?></td>
		</tr>
		<tr>
			<td colspan="3">
				<center><a href="" id="tambah_row" class="label label-info">tambah</a></center>
			</td>
		</tr>
	</tfoot>
</table> 
<button type="button" name="save" style="float:right;" id="btnSimpan" class="btn btn-primary simpan btn-small">Simpan</button>
</form>
<table class="hide" id="pre">
	<tr>
		<td>
			<select name="r[<?=$i;?>]" class="sl_role">
				<?foreach ($role->result() as $key): ?>
					<option value="<?=$key->role_id;?>"><?=$key->role_name;?></option>
				<?endforeach;?>
			</select>
		</td>
		<td>
			<select class="tp" name="p[<?=$i;?>]">
				<?foreach ($dr->result() as $d): ?>
				<option value="<?=$d->dr_id;?>"><?=$d->dr_name;?></option>
				<?endforeach;?>
			</select>
		</td>	
		<td>
			<input class="tx_small th" value="" type="text">
		</td>	
	</tr>
</table> 
<script type="text/javascript">
	$(function(){
		irow += <?=$i;?>;
		$("#def_dr").val(def_dr_id);
		$('.ajchzn').chosen();
	})
</script>