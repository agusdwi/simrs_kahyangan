<style type="text/css">
    #fl_rujukan{
        display: none;
    }
</style>
<?=$this->load->view('t/a_info');?>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0px;margin-left:0px;" >
                <?=form_open('',array('id' => 'form_diag')); ?>
                    <?=$this->load->view('t/a_menu_tab');?>
                    <div id="page">
                    	<div class="container-fluid">
                    		<div class="row-fluid">
                                <div class="span12" style="padding:0px 1%">
                                    <table id="rekmed" style="width:100%" class="table table-bordered tb_scrol">
                                        <tr>
                                            <td style="width:30%">Tanggal & Jam</td><td><?=pretty_date($ds['medical']->mdc_in);?></td>
                                        </tr>
                                        <tr>
                                            <td>Poli / unit</td><td><?=$ds['medical']->pl_name;?></td>
                                        </tr>
                                        <tr>
                                            <td>Dokter</td><td><?=$ds['medical']->dr_name;?></td>
                                        </tr>
                                        <tr>
                                            <td>Diagnosa</td>
                                            <td>
                                                <div class="td_container">                                                
                                                    <?foreach ($ds['diagnosis'] as $key => $value): ?>
                                                        <b><?=$key;?></b><br>
                                                        <?foreach ($value as $k): ?>
                                                            <p>- <?=$k?></p>
                                                        <?endforeach ?>
                                                    <a href="<?=base_url()?>rawat_jalan/poli/tdiagnosa/<?=$this->uri->segment(4);?>" class="btn btn-warning btn-mini onhover ringk-edit">edit</a>
                                                    <?endforeach ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tindakan</td>
                                            <td>
                                                <div class="td_container">                                                
                                                    <?foreach ($ds['tindakan']->result() as $key): ?>
                                                        <p>- <?=$key->jumlah;?>x <?=$key->treat_code;?>, <?=$key->treat_name;?></p> 
                                                    <?endforeach ?>
                                                    <a href="<?=base_url()?>rawat_jalan/poli/ttindakan/<?=$this->uri->segment(4);?>" class="btn btn-warning btn-mini onhover ringk-edit">edit</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Obat yang digunakan</td>
                                            <td>
                                                <div class="td_container">                                                
                                                    <table>
                                                        <?foreach ($ds['obat']->result() as $key): ?>
                                                            <tr>
                                                                <td>
                                                                    <?=($key->is_racik == 1)? '- racik,' : '-' ;?>
                                                                    <?if ($key->is_racik == 1): ?>
                                                                        <?=$key->recipe_racik;?>
                                                                    <? else: ?>
                                                                        <?=$key->mdcn_code;?>, <?=$key->mdcn_name;?>
                                                                    <?endif ?>
                                                                </td>
                                                                <td><?=$key->recipe_rule;?></td>
                                                                <td>
                                                                    <?=$key->recipe_qty;?>
                                                                </td>
                                                            </tr>
                                                        <?endforeach?>
                                                    </table>
                                                    <a href="<?=base_url()?>rawat_jalan/poli/tresep/<?=$this->uri->segment(4);?>" class="btn btn-warning btn-mini onhover ringk-edit">edit</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Status Pulang</td>
                                            <td>
                                                <?
                                                    $stp = explode(',', $ds['medical']->status_pulang);
                                                ?>
                                                <select id="st_pulang" name="ds[status_pulang]">
                                                    <option <?=($stp[0] == 'pulang')?'selected="selected"':'';?>>pulang</option>
                                                    <option <?=($stp[0] == 'rujukan')?'selected="selected"':'';?>>rujukan</option>
                                                    <option <?=($stp[0] == 'rawat inap')?'selected="selected"':'';?>>rawat inap</option>
                                                </select>   
                                                <input type="text" name="rujukan" class="mini" id="fl_rujukan" value="<?=($stp[0] == 'rujukan')? $stp[1]:'';?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Catatan</td>
                                            <td>
                                                <textarea name="ds[note]" id="hd_diag_desc" style="margin-bottom:0px;width:350px" rows="4" placeholder="masukkan catatan"><?=$ds['medical']->note;?></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Kontrol</td>
                                            <td>
                                                <input type="text" class="small datepicker" name="date_control" value="<?=$ds['medical']->date_control;?>">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                    		</div>
                    	</div>	
                    </div>
                    <?=$this->load->view('t/a_bottom',array('finish'=>true));?>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#st_pulang").change(function(){
            if($(this).val() == 'rujukan'){
                $("#fl_rujukan").show().focus();
            }else{
                $("#fl_rujukan").hide();
            }
        })
        $("#st_pulang").trigger('change');
    })
</script>