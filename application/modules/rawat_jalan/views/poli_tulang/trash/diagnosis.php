<? 
    $this->load->view('rawat_jalan/local/behav');
?>


<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-th"></i>
					</span>
					<h5>Diagnosis Dan Tindakan</h5>
				</div>
				<div class="widget-content nopadding">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="width:5%" align="center">Nomor</th>
								<th>Diagnosa</th>
								<th>Tindakan</th>
								<th style="width:20%">Jenis kasus</th>
							</tr>
						</thead>
						<tbody>
							<?for($i=1;$i<=5;$i++):?>
								<tr>
									<td style="text-align:center;"><b><?=$i?></b></td>
									<td style="width:25%">
										<div id="fx_diagnosa"></div>
									</td>
									<td style="width:25%">
										<div class="tindakan" style="width:300px" id="tindakan[]"></div><!-- the flexbox!-->
									</td>
									<td style="padding:5px">
										<input type="radio" name="rj_case_<?=$i?>" value="new"> &nbsp;Kasus Baru<br/>
										<input type="radio" name="rj_case_<?=$i?>" value="old"> &nbsp;Kasus Lama
									</td>
								</tr>
							<?endfor;?>
						</tbody>
					</table>							
				</div>
				<div class="form-actions" style="margin-bottom:0px">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>
		</div>
	</div>
</div>
