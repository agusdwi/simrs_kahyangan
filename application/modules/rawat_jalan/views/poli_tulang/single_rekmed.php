<div class="span12" style="padding:0px 1%">
	<?=form_open(base_url().'rawat_jalan/rawat_jalan/create',array('class' => 'form-horizontal','id' => 'form')); ?>
	<table style="width:100%" class="table table-bordered tb_scrol">
		<tr>
			<td></td>
			<td style="border:1px;float:right">
				<button class="btn btn-info"><i class="icon-print icon-white"></i> Print</button>
				<button class="btn btn-info"><i class="icon-print icon-white"></i> Print All</button>
				<a href="#myModal" id="cetak" role="button" class="btn hide" data-toggle="modal">Launch demo modal</a>
			</td>
		</tr>
	</table>
	<table style="width:100%" class="table table-bordered tb_scrol">
		<tr>
			<td style="width:30%">Tanggal & Jam</td><td>7 November 2012 14:15</td>
		</tr>
		<tr>
			<td>Dokter</td><td>Dr. Fikri</td>
		</tr>
		<tr>
			<td>Kajian Subjektif</td><td>Tulang retak</td>
		</tr>
		<tr>
			<td>Kajian Objektif</td><td>Tulang retak disertai memar</td>
		</tr>
		<tr>
			<td>Diagnosis</td>
			<td>
				- H33.2 Uterus<br> - A 46, Erysipelas
			</td>
		</tr>
		<tr>
			<td>Tindakan</td>
			<td>
				- 104.0 nonvenereal endemic syphilis<br>
				- 131.0 urogenital trichomoniasis
			</td>
		</tr>
		<tr>
			<td>Obat yang digunakan</td>
			<td>
				Amoxilin 50gr 3emplek 2x3 sehari<br>
				Paracetamol 50gr 3emplek 2x3 sehari
			</td>
		</tr>
		<tr>
			<td>Rujukan</td><td>-</td>
		</tr>
		<tr>
			<td>Surat keterangan yang dikeluarkan</td><td>-</td>
		</tr>
	</table>
	<?=form_close()?>
</div>

<!-- Modal -->
<div id="myModal" style="height:500px;width:700px;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Data Rekam Medis Pasien</h3>
  </div>
  <div class="modal-body">
    <iframe src="" name="ifr" id="ifr" style="margin:0px;padding:0px;border:none;width:98%;height:110%"></iframe>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Keluar</button>
    <button id="cetakIframe" class="btn btn-primary">Cetak</button>
  </div>
</div>

<script type="text/javascript">
	$("#form").submit(function(){
	    var url  = $(this).attr('action');
	    var data = $(this).serialize();
	    $.post(url,data, function(data){
	        $("#cetak").trigger('click');
			$("#ifr").attr('src',data);
	    }); 
	    return false;
	})

	$("#cetakIframe").click(function(){
	    ifr.print();
	    window.location = "<?=base_url()?>rawat_jalan/poli_tulang/proses";
	})
</script>