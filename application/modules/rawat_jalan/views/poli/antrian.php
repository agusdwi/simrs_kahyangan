<style type="text/css" media="screen">
	.dataTables_scrollHead{
		margin-bottom:-25px;
	}
	.dataTables_scrollBody table.tb_scrol{
		margin-top: 23px;
	}
	.dataTables_filter{
		position: absolute;
		top: 80px;
		left: 300px;
	}
</style>
<script type="text/javascript" charset="utf-8">
	var objPasien = <?=json_encode($ds->result())?>;
	$(function(){
		$(".tb_scrol tbody tr").click(function(){
			var index = $( "tr" ).index(this) - 2;
			var p = objPasien[index];
			$(".tb_scrol tbody tr.active").removeClass('active');
			$(".black_loader").fadeIn('fast').fadeOut('fast',function(){	
				$("#b_nm_t").html(p.sd_name);
				$("#b_rm").html(p.sd_rekmed);
				$("#b_nm").html(p.sd_name);
				$("#b_sex").html(p.sd_sex);
				$("#b_ad").html(p.sd_address);
				$("#sd_rekmed").val(p.sd_rekmed);
				$("#dr_id").val(p.dr_id);
				$("#queue_id").val(p.queo_id);
			});
			$(this).addClass('active');
		})
		$("#formAntrian").submit(function(){
			if($("#b_nm_t").html() == "-"){
				alert('silahkan memilih pasien di sebelah kiri');
				return false;
			}
		})
		$('.custom_table_y').dataTable( {
			"sPaginationType": "bootstrap",
			"sScrollY": "350px",
			"bPaginate": false,
			"bFilter":true,
			"bInfo":false,
			"bSort": false
		});
	})
</script>
<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>       
</div><!--pageheader-->
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span7">
			<div class="title">
				<h3>Antrian <?=$title;?></h3>
			</div>
			
			<table class="table table-bordered custom_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
				<thead>
					<tr role="row">
						<th class="sorting" style="width:15%">Nomor</th>
						<th class="sorting" style="width:40%">Pasien</th>
						<th class="sorting" style="width:45%">Dokter</th>
					</tr>
				</thead>
				<tbody>
					<?$i=0;foreach ($ds->result() as $key): $i++;?>
						<tr>
							<td class="center">
								<b><?=$i;?></b>
							</td>
							<td>
								<b><?=$key->sd_name;?> / <?=$key->sd_rekmed;?></b>
								<br>
								<b><?=$key->sd_address;?></b>
							</td>
							<td>
								<b><?=$key->dr_name;?></b>
							</td>
						</tr>	
					<?endforeach;?>
				</tbody>
			</table>
		</div>
		<div class="span5">
			<div class="title">
				<h3>Data Diri Pasien <span id="b_nm_t">-</span>
					<span class="tgl-sekarang"></span> 
				</h3>
			</div>
			<div style="width:100%;border:1px solid #DDD;position:relative;">
				<div class="black_loader">
					<img src="<?=get_loader(11)?>">
				</div>
				<?=form_open(cur_url(),array('class' => 'stdform','id' => 'formAntrian')); ?>
					<input type="hidden" name="pl_id" value="<?=$cur_poli->pl_id?>">
					<input type="hidden" id="sd_rekmed" name="sd_rekmed" value="">
					<input type="hidden" id="dr_id" name="dr_id" value="">
					<input type="hidden" id="queue_id" name="queue_id" value="">
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;height:40px;float:left;text-align:right;">
							<label>Nomor RM</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b id="b_rm">-</b>
						</div>
					</div>
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;height:40px;float:left;text-align:right;">
							<label>Nama Lengkap</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b id="b_nm">-</b>
						</div>
					</div> 
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;float:left;height:40px;text-align:right;">
							<label>Jenis Kelamin</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b id="b_sex">-</b>
						</div>
					</div> 
					<br clear="all">
					<div style="width:100%;padding-left:30px;">
						<div style="width:100px;float:left;height:40px;text-align:right;">
							<label>Alamat</label>
						</div>
						<div style="margin-left:30px;float:left;">
							<b id="b_ad">-</b>
						</div>
					</div> 
					<br clear="all">
					<div class="form-actions" style="margin:0px;vertical-align:bottom;">
						<button type="submit" class="btn btn-primary">Tindakan Lanjut</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>