<style>
	.bg_widget{
		background-color: #efefef;
		background-image: -webkit-gradient(linear, 0 0%, 0 100%, from(#fdfdfd), to(#eaeaea));
		background-image: -webkit-linear-gradient(top, #fdfdfd 0%, #eaeaea 100%);
	    background-image: -moz-linear-gradient(top, #fdfdfd 0%, #eaeaea 100%);
	    background-image: -ms-linear-gradient(top, #fdfdfd 0%, #eaeaea 100%);
	    background-image: -o-linear-gradient(top, #fdfdfd 0%, #eaeaea 100%);
	    background-image: -linear-gradient(top, #fdfdfd 0%, #eaeaea 100%);
	}
	.label_surat{
		width:100px;
		float:left;
	}
	.input_surat{
		width:auto;
		float:left;
		padding:0px;
	}
</style>
<div class="container-fluid">
    <div class="row-fluid">
    	<div class="span12">
    		<!--membuat Nav untuk keterangan apa saja -->
    		<div class="span12">
    			<div class="tabbable tabs-left">
					<ul class="nav nav-tabs" id="list_keterangan">
						<li class="active"><a href="#surat_ket" data-toggle="tab">Surat Keterangan</a></li>
						<li class=""><a href="#pengantar" data-toggle="tab">Surat Pengantar Dirawat</a></li>
		                <li class=""><a href="#pengiriman" data-toggle="tab">Pengiriman Penderita</a></li>
		                <li class=""><a href="#lab" data-toggle="tab">Surat Pengantar Laboratorium</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="surat_ket">
							<style>
								.table_keterangan td{
									vertical-align: middle;
								}
								.table_keterangan .title{
									margin-left: 0px;
									margin-bottom: 0px;
								}
								.table_keterangan label{
									margin-right: 10px;
									float:right;
								}
							</style>
							<div class="span11">
								<div class="widget-box">
									<div class="widget-title">
										<!-- Membuat nav untuk surat keterangan-->
										<ul class="nav nav-tabs bg_widget">
											<li class="active"><a href="#1" data-toggle="tab">Keterangan Sakit</a></li>
											<li class=""><a href="#2" data-toggle="tab">Surat Keterangan</a></li>
											<li class=""><a href="#3" data-toggle="tab">Cuti Persalinan</a></li>
											<li class=""><a href="#4" data-toggle="tab">Surat Keterangan Sehat</a></li>
											<li class=""><a href="#5" data-toggle="tab">Keterangan Tidak Buta Warna</a></li>
											<li class=""><a href="#6" data-toggle="tab">Keterangan Bebas Narkoba</a></li>
										</ul>
										<br clear="all">
									</div>
									<br clear="all">
									<div class="tab-content">
										<div class="tab-pane active" id="1">
											<div class="span12">
												<div class="title"><h3>Surat Keterangan Sakit</h3></div>
												<br clear ="all">
												<?=form_open(base_url().'rawat_jalan/poli_tulang/sk_pengantar',array('class' => 'form-horizontal','id' => 'form')); ?>	
												<div style="margin-left:20px;">
													<input type="hidden" name="ds[ref_category]" value="Keterangan Sakit">
													<div class="label_surat">
														Nomor Surat
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="ds[ref_date]" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														
													</div>
												</div>
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Jumlah hari
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="ds[ref_date]" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														Hari,
													</div>
												</div>
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Mulai
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="ds[ref_date_start]" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														sampai dengan
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="ds[ref_date_end]" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
												
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Keterangan
													</div>
													<div class="input_surat">
														<textarea style="width:500px" name="ds[ref_description]"></textarea>
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div class="form-actions">
													<button style="float:right;" class="btn btn-primary" >Simpan</button>
												</div>
												<?=form_close()?>
											</div>
										</div>
										<div class="tab-pane" id="2">
											<div class="span12">
												<div class="title"><h3>Surat Keterangan</h3></div>
												<br clear="all">

												<?=form_open(base_url().'rawat_jalan/poli_tulang/sk_pengantar',array('class' => 'form-horizontal','id' => 'form')); ?>	
												<div style="margin-left:20px;">
													<input type="hidden" name="ds[ref_category]" value="Keterangan Sakit">
													<div class="label_surat">
														Nomor Surat
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="ds[ref_date]" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														
													</div>
												</div>
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Tanggal
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Keterangan
													</div>
													<div class="input_surat">
														<textarea style="width:500px"></textarea>
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div class="form-actions">
													<button style="float:right;" class="btn btn-primary" >Simpan</button>
												</div>
												<?=form_close()?>
											</div>
										</div>
										<div class="tab-pane" id="3">
											<div class="span12">
												<div class="title"><h3>Surat Cuti Persalinan</h3></div>
												<br clear="all">

												<?=form_open(base_url().'rawat_jalan/poli_tulang/sk_pengantar',array('class' => 'form-horizontal','id' => 'form')); ?>	
												<div style="margin-left:20px;">
													<input type="hidden" name="ds[ref_category]" value="Keterangan Sakit">
													<div class="label_surat">
														Nomor Surat
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="ds[ref_date]" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														
													</div>
												</div>
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Tanggal
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Keterangan
													</div>
													<div class="input_surat">
														<textarea style="width:500px"></textarea>
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div class="form-actions">
													<button style="float:right;" class="btn btn-primary" >Simpan</button>
												</div>
												<?=form_close()?>
											</div>
										</div>
										<div class="tab-pane" id="4">
											<div class="span12">
												<div class="title"><h3>Surat Keterangan Sehat</h3></div>
												<br clear="all">

												<?=form_open(base_url().'rawat_jalan/poli_tulang/sk_pengantar',array('class' => 'form-horizontal','id' => 'form')); ?>	
												<div style="margin-left:20px;">
													<input type="hidden" name="ds[ref_category]" value="Keterangan Sakit">
													<div class="label_surat">
														Nomor Surat
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="ds[ref_date]" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														
													</div>
												</div>
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Tanggal
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Keterangan
													</div>
													<div class="input_surat">
														<textarea style="width:500px"></textarea>
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div class="form-actions">
													<button style="float:right;" class="btn btn-primary" >Simpan</button>
												</div>
												<?=form_close()?>
											</div>
										</div>
										<div class="tab-pane" id="5">
											<div class="span12">
												<div class="title"><h3>Surat Keterangan Tidak Buta Warna</h3></div>
												<br clear="all">

												<?=form_open(base_url().'rawat_jalan/poli_tulang/sk_pengantar',array('class' => 'form-horizontal','id' => 'form')); ?>	
												<div style="margin-left:20px;">
													<input type="hidden" name="ds[ref_category]" value="Keterangan Sakit">
													<div class="label_surat">
														Nomor Surat
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="ds[ref_date]" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														
													</div>
												</div>
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Tanggal
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Keterangan
													</div>
													<div class="input_surat">
														<textarea style="width:500px"></textarea>
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div class="form-actions">
													<button style="float:right;" class="btn btn-primary" >Simpan</button>
												</div>
												<?=form_close()?>
											</div>
										</div>
										<div class="tab-pane" id="6">
											<div class="span12">
												<div class="title"><h3>Surat Keterangan Bebas Narkoba</h3></div>
												<br clear="all">

												<?=form_open(base_url().'rawat_jalan/poli_tulang/sk_pengantar',array('class' => 'form-horizontal','id' => 'form')); ?>	
												<div style="margin-left:20px;">
													<input type="hidden" name="ds[ref_category]" value="Keterangan Sakit">
													<div class="label_surat">
														Nomor Surat
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="ds[ref_date]" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														
													</div>
												</div>
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Tanggal
													</div>
													<div class="input_surat">
														<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
														
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div style="margin-left:20px;">
													<div class="label_surat">
														Keterangan
													</div>
													<div class="input_surat">
														<textarea style="width:500px"></textarea>
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<div class="form-actions">
													<button style="float:right;" class="btn btn-primary" >Simpan</button>
												</div>
												<?=form_close()?>
											</div>
										</div>
									</div>
										<!--selesai embuat nav-->
								</div>

							</div>
		                </div>
		                <div class="tab-pane" id="pengantar">
		                  <div class="span12">
			                  	<div class="widget-box">
			                  		<div class="title"><h3>Surat Pengantar Dirawat</h3></div>
			                  		<?=form_open(base_url().'rawat_jalan/poli_tulang/sk_pengantar',array('class' => 'form-horizontal','id' => 'form')); ?>
			                  			<div style="margin-left:20px;">
											<input type="hidden" name="ds[ref_category]" value="Keterangan Sakit">
											<div class="label_surat">
												Nomor Surat
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="ds[ref_date]" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
												
											</div>
										</div>
										<br clear="all">
			                  			<div style="float:right;margin-right:450px;">
											<div class="label_surat">
												Tanggal
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
												
											</div>
										</div>
										<div style="margin-left:20px;">
											<div class="label_surat">
												Kepada Yth,
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
												<br>di tempat
											</div>
										</div>
										<br clear="all">
										<div style="margin-left:20px;">
											<div class="label_surat">
												Diagnosa sementara
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
											</div>
										</div>
										<br clear="all">
										<div style="margin-left:20px;">
											<div class="label_surat">
												Indikasi Rawat Inap
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
											</div>
										</div><br clear="all">
										<div style="margin-left:20px;">
											<div class="label_surat">
												Pemeriksaan laborat
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
											</div>
										</div>
										<br clear="all">
										<div style="margin-left:20px;">
											<div class="label_surat">
												EKG/Radiologi/ISG/CT-SCAN
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
											</div>
										</div>
										<br clear="all">
										<div style="margin-left:20px;">
											<div class="label_surat">
												Tindakan/Terapi Sementara	
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
											</div>
										</div>
										<br clear="all">
										<br clear="all">
										<div style="margin-left:20px;">
											<div class="label_surat">
												Keterangan
											</div>
											<div class="input_surat">
												<textarea style="width:500px"></textarea>
											</div>
										</div>
										<br clear="all">
										<br clear="all">
										<div class="form-actions">
											<button style="float:right;" class="btn btn-primary" >Simpan</button>
										</div>
			                  		<?=form_close()?>
			                  	</div>
							
							</div>
		                </div>
		                <div class="tab-pane" id="pengiriman">
		                  <div class="span12">
		                  	<div class="widget-box">
			                  		<div class="title"><h3>Surat Pengantar Dirawat</h3></div>
			                  		<?=form_open(base_url().'rawat_jalan/poli_tulang/sk_pengantar',array('class' => 'form-horizontal','id' => 'form')); ?>
			                  			<div style="margin-left:20px;">
											<input type="hidden" name="ds[ref_category]" value="Keterangan Sakit">
											<div class="label_surat">
												Nomor Surat
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="ds[ref_date]" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
												
											</div>
										</div>
										<br clear="all">
			                  			<div style="float:right;margin-right:450px;">
											<div class="label_surat">
												Tanggal
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
												
											</div>
										</div>
										<div style="margin-left:20px;">
											<div class="label_surat">
												Kepada Yth,
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
												<br>di tempat
											</div>
										</div>
										<br clear="all">
										<br clear="all">
										<div style="margin-left:20px;">
											<div class="label_surat">
												&nbsp;
											</div>
											<div class="input_surat">
												Dengan ini kami kirimkan penderita untuk dilakukan<br>
												<textarea style="width:500px"></textarea>
											</div>
										</div>
										<br clear="all">
										<div style="margin-left:20px;">
											<div class="label_surat">
												Diagnosa
											</div>
											<div class="input_surat">
												<input class="small" type="text" name="" style="width:100px;border:none;border-bottom:1px dotted gray;text-align:left;" value="">
											</div>
										</div><br clear="all">
										<div style="margin-left:20px;">
											<div class="label_surat">
												&nbsp;
											</div>
											<div class="input_surat">
												Hasil pemeriksaan<br>
												<textarea style="width:500px"></textarea>
											</div>
										</div>
										<br clear="all">
										<div style="margin-left:20px;">
											<div class="label_surat">
												&nbsp;
											</div>
											<div class="input_surat">
												Pertolongan yan telah diberikan<br>
												<textarea style="width:500px"></textarea>
											</div>
										</div>
										<br clear="all">
										<br clear="all">
										<div class="form-actions">
											<button style="float:right;" class="btn btn-primary" >Simpan</button>
										</div>
			                  		<?=form_close()?>
			                  	</div>
								
							</div>
		                </div>
		                <div class="tab-pane" id="lab">
		                  <div class="span12">
		                  	<div class="widget-box">
			                  	<div class="title"><h3>Surat Pengantar Laboratorium</h3></div>
			                  		<?=form_open('#',array('class' => 'form-horizontal','id' => 'form')); ?>
										<div style="margin-left:20px;">
											<div class="label_surat">
												&nbsp;
											</div>
											<div class="input_surat">
												Laboratorium : <br>
												<select>
													<option>Radiologi</option>
													<option>USG</option>
												</select><br>

												Diagnosa / keterangan lain<br>
												<textarea style="width:500px"></textarea>
											</div>
										</div>
										<br clear="all">
										<br clear="all">
										<div class="form-actions">
											<button style="float:right;" class="btn btn-primary" >Simpan</button>
										</div>
			                  		<?=form_close()?>
			                  	</div>
								
							</div>
		                </div>
						<a href="#myModal" id="cetak" role="button" class="btn hide" data-toggle="modal">Launch demo modal</a>
					</div>
				</div>
    		</div>

		</div>

	</div>
</div>

<div id="myModal" style="height:500px;width:720px;margin-left:-320px" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Cetak Surat Keterangan</h3>
  </div>
  <div class="modal-body">
    <iframe src="" name="ifr" id="ifr" style="margin:0px;padding:0px;border:none;width:100%;height:90%"></iframe>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Keluar</button>
    <button id="cetakIframe" class="btn btn-primary">Cetak</button>
  </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(function(){
        $(".form-horizontal").submit(function(){
            var url  = $(this).attr('action');
            var data = $(this).serialize();
            $.post(url,data, function(data){
                $("#cetak").trigger('click');
				$("#ifr").attr('src',data);
            }); 
            return false;
        })

        $("#cetakIframe").click(function(){
            ifr.print();
            window.location = "<?=base_url()?>rawat_jalan/poli_tulang/proses";
        })
    })
</script>
