
<div class="span12" style="padding:0px 1%">
	<style>
		tr#subjstudy a,tr#objstudy a,tr#diagnose a,tr#medicine a,tr#rujukan a,tr#keterangan a{display: none;}
		tr#subjstudy:hover a,tr#objstudy:hover a,tr#diagnose:hover a,tr#medicine:hover a,tr#rujukan:hover a,tr#keterangan:hover a{display: inline;}
		.btn-mini{margin:0px;line-height: 15px !important;}
	</style>
	<script type="text/javascript">
		$(function(){
			$('#table1 a').click(function(){
				id = $(this).attr('href');
				$('#tab_'+id).click();
				return false;
			})
		})
	</script>
	<?=form_open(base_url().'rawat_jalan/poli_tulang/simpan_kirim',array('class' => 'form-horizontal','id' => 'form')); ?>
	<input type="hidden" name="mdc_id" value=<?=$mdc_id?> >
	<table style="width:100%" class="table table-bordered tb_scrol" id="table1">
		<tr>
			<td style="width:30%">Tanggal & Jam Masuk</td>
			<td>

				<? if(isset($medical)){
					foreach ($medical->result() as $key => $value) {
						echo pretty_date($value->mdc_in,false);
					}
				}?>
			</td>
		</tr>
		<tr>
			<td>Dokter</td>
			<td>
				<? if(! empty($ringkasan)){
					echo $ringkasan->dr_name;
				}?>
				
			</td>
		</tr>
		<tr id="subjstudy">
			<td>Kajian Subjektif</td>
			<td>
				
				<? if(! empty($kajian_sub)){
					foreach ($kajian_sub->result() as $key => $value) {
						echo $value->ms_desc;
					}
				}?>
				<a href="kajian" class="btn btn-warning btn-mini" style="float:right;">edit</a>
			</td>
		</tr>
		<tr id="objstudy">
			<td>Kajian Objektif</td>
			<td>
				<? if(! empty($kajian_ob)){
					foreach ($kajian_ob->result() as $key => $value) {
						echo $value->mo_desc;
					}
				}?>
				<a href="kajian" class="btn btn-warning btn-mini" style="float:right;">edit</a>
			</td>
		</tr>
		<tr id="diagnose">
			<td>Diagnosis dan Tindakan</td>
			<td>
				<a href="diagnosis" class="btn btn-warning btn-mini" style="float:right;">edit</a>
				<? if(! empty($diagnosa)){
					foreach ($diagnosa->result() as $key => $value) {
						echo "Diagnosa : ".$value->diag_name."  tindakan :".$value->treat_name;
						echo "<br>";
					}
				}?>
				
			</td>
		</tr>
		<tr id="medicine">
			<td>
				Obat
			</td>
			<td>
			<?
			if(isset($obat)){
				foreach ($obat->result() as $key => $value) {
					 echo $value->recipe_medicine."&nbsp;&nbsp;&nbsp;&nbsp;".$value->im_name."&nbsp;&nbsp;&nbsp;&nbsp;".$value->recipe_qty."&nbsp;&nbsp;&nbsp;".$value->recipe_rule;
					 echo "<br>";
				}
			}
			
			?>
			</td>
		</tr>
		<tr id="rujukan">
			<td>Rujukan</td><td><a href="keterangan" class="btn btn-warning btn-mini" style="float:right;">edit</a></td>
		</tr>
		<tr id="keterangan">
			<td>Surat keterangan yang dikeluarkan</td><td><a href="keterangan" class="btn btn-warning btn-mini" style="float:right;">edit</a></td>
		</tr>
	</table>
	<button type="submit" class="btn btn-primary" style="float:right;margin:0px 5px 10px 0px">Finish Pemeriksaan</button>	
	<?=form_close()?>
</div>

<style type="text/css">
.alert{
		background-color: transparent;
		border: 0px;
	}
	
	#gritter-notice-wrapper{
		right: 13%;
		top: 100px;
	}
</style>

<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.gritter.css" />
<div id="gritter-notice-wrapper" class="alert hide" style="width:750px;position:fixed;right:13%;top:100px">
		<div id="gritter-item-1" class="gritter-item-wrapper" style="margin:0 -17px 5px 0">
			<div class="gritter-top"></div>
			<div class="gritter-item">
				<div class="gritter-close" style="display: none; width:50px "></div>
				<img src="<?=base_url()?>assets/img/demo/envelope.png" class="gritter-image">
				<div class="gritter-with-image" style="width:448px">
					<span class="gritter-title" style="margin-left:36px">Message</span>
					<p>Data Berhasil Disimpan 	</p>
				</div>
				<div style="clear:both"></div>
			</div>
			<div class="gritter-bottom"></div>
		</div>
	</div>

<!-- Modal -->
<div id="myModal" style="height:500px;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Data Rekam Medis Pasien</h3>
  </div>
  <div class="modal-body">
    <iframe src="" name="ifr" id="ifr" style="margin:0px;padding:0px;border:none;width:100%;height:90%"></iframe>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Keluar</button>
    <button id="cetakIframe" class="btn btn-primary">Cetak</button>
  </div>
</div>

<script type="text/javascript">

	isvalid = $("#form").validate({
		rules: {
            
        },
        submitHandler: function(form) {
            var url  = "<?=base_url()?>rawat_jalan/poli_tulang/simpan_kirim";
            var data = jQuery(form).serialize();
            $.post(url,data, function(data){
                $(".alert").fadeIn().delay(500).fadeOut(function(){
                    $("#save").trigger('click'); 
                });     
            }); 
            return false;
        }
    });

    $("#save").click(function(){});

	$("#cetakIframe").click(function(){
	    ifr.print();
	    window.location = "<?=base_url()?>rawat_jalan/poli_tulang/proses";
	})
</script>