<div class="title">
	<h4>Data Rekam Medis Pasien</h4>
	<h6>Sigit Hanafi | 082312</h6>
</div>
<table style="width:100%" class="table table-bordered tb_scrol">
	<tr>
		<td style="width:30%">Tanggal & Jam</td><td>7 November 2012 14:15</td>
	</tr>
	<tr>
		<td>Dokter</td><td>Dr. Fikri</td>
	</tr>
	<tr>
		<td>Kajian Subjektif</td><td>Tulang retak</td>
	</tr>
	<tr>
		<td>Kajian Objektif</td><td>Tulang retak disertai memar</td>
	</tr>
	<tr>
		<td>Diagnosis</td>
		<td>
			- H33.2 Uterus<br> - A 46, Erysipelas
		</td>
	</tr>
	<tr>
		<td>Tindakan</td>
		<td>
			- 104.0 nonvenereal endemic syphilis<br>
			- 131.0 urogenital trichomoniasis
		</td>
	</tr>
	<tr>
		<td>Obat yang digunakan</td>
		<td>
			Amoxilin 50gr 3emplek 2x3 sehari<br>
			Paracetamol 50gr 3emplek 2x3 sehari
		</td>
	</tr>
	<tr>
		<td>Rujukan</td><td>-</td>
	</tr>
	<tr>
		<td>Surat keterangan yang dikeluarkan</td><td>-</td>
	</tr>
</table>