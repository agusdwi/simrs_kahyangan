<div class='container-fluid'>
	<div class='row-fluid'>
		<div class='span12'>
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-th"></i>
					</span>
					<h5>Diagnosis Dan Tindakan</h5>
				</div>
				<div class="widget-content nopadding">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nomor</th>
								<th>Diagnosa</th>
								<th>Tindakan</th>
								<th>Jenis kasus</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Row 1</td>
								<td>Row 2</td>
								<td>Row 3</td>
								<td>Row 4</td>
							</tr>
							<tr>
								<td>Row 1</td>
								<td>Row 2</td>
								<td>Row 3</td>
								<td>Row 4</td>
							</tr>
							<tr>
								<td>Row 1</td>
								<td>Row 2</td>
								<td>Row 3</td>
								<td>Row 4</td>
							</tr>
						</tbody>
					</table>							
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</div>

		</div>
	</div>

</div>