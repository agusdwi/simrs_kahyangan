<style>
	.box-kajian{
		height:205px;
		background-color:#DDDDDD;
		padding:5px;
		overflow-x:scroll;
	}
	.table-kajian td{
		padding-left: 10px;
	}
</style>
<script>
    $(function(){
        $('#selectsubj').hide();
        $('#selectobj').hide();
        //kajian subjektif
        $('button#show_selectsubj').click(function(){
            $('div#kajiansubj').hide(400,function(){
                $('div#selectsubj').show();    
            });
        })
        $('button#add_subj').click(function(){
            $('div#selectsubj').hide(400,function(){
                $('div#kajiansubj').show();    
            });
        })
        //end kajian subjektif
        //kajian subjektif
        $('button#show_selectobj').click(function(){
            $('div#kajianobj').hide(400,function(){
                $('div#selectobj').show();    
            });
        })
        $('button#add_obj').click(function(){
            $('div#selectobj').hide(400,function(){
                $('div#kajianobj').show();    
            });
        })
        //end kajian subjektif
    })
</script>
<div>
    <!-- KAJIAN SUBJEKTIF -->
	<div class="title"><h3>Kajian Subjektif</h3></div>
	<br clear="all"/>
    <div id="kajiansubj" class="span11">
        <textarea rows=3 class="span12"></textarea>
        <br clear="all"/>
        <button style="float:right" class="btn btn-primary" id="show_selectsubj">Tambah Kajian</button>
    </div>
    <div class="span11" id="selectsubj">
    	<div class="span9">
    		<div class="box-kajian">
    			<table class="table-kajian">
    				<tr>
    					<td><label><div class="checker"><span><input type="checkbox" name="subjstudy[]" style="opacity: 0; "></span></div>Ada benjolan di bibir/lidah</label></td>
    					<td><label><div class="checker"><span><input type="checkbox" name="subjstudy[]" style="opacity: 0; "></span></div>Gigi banyak yang hilang, akan dibuatkan gigi palsu</label></td>
    				</tr>
    				<tr>
    					<td><label><div class="checker"><span><input type="checkbox" name="subjstudy[]" style="opacity: 0; "></span></div>Bibir kering dan pecah-pecah</label></td>
    					<td><label><div class="checker"><span><input type="checkbox" name="subjstudy[]" style="opacity: 0; "></span></div>Gigi bertumbuh dan akan menutupi gingsul</label></td>
    				</tr>
    				<tr>
    					<td><label><div class="checker"><span><input type="checkbox" name="subjstudy[]" style="opacity: 0; "></span></div>Check Gigi</label></td>
    					<td><label><div class="checker"><span><input type="checkbox" name="subjstudy[]" style="opacity: 0; "></span></div>Gigi gingsul dan akan di kawat</label></td>
    				</tr>
    				<tr>
    					<td><label><div class="checker"><span><input type="checkbox" name="subjstudy[]" style="opacity: 0; "></span></div>Gigi akan dilanjutkan untuk ditambal</label></td>
    				</tr>
    				<tr>
    					<td><label><div class="checker"><span><input type="checkbox" name="subjstudy[]" style="opacity: 0; "></span></div>Gigi akan ditambal</label></td>
    				</tr>
    				<tr>
    					<td><label><div class="checker"><span><input type="checkbox" name="subjstudy[]" style="opacity: 0; "></span></div>Gigi banyak yang gigis, akan di cabut</label></td>
    				</tr>
    				<tr>
    					<td><label><div class="checker"><span><input type="checkbox" name="subjstudy[]" style="opacity: 0; "></span></div>Gigi berlubang</label></td>
    				</tr>
    			</table>
    		</div> <!-- #div.box-kajian -->
    		<div style="padding:5px">
    			<span>
                	ADD KAJIAN :  <input type="text" id="add_kajian_field" style="width:200px;margin:0px"/> 
                	<div class="radio" ><span><input type="radio" name="radios" style="opacity: 0;"></span></div> S
    				<div class="radio" id="uniform-undefined"><span><input type="radio" name="radios" style="opacity: 0;"></span></div> N
    				<div class="radio" id="uniform-undefined"><span><input type="radio" name="radios" style="opacity: 0;"></span></div> J
                	<button class="btn btn-primary" style="float:right" id="add_subj"> + </button>
                </span>
    		</div><!-- #div add kajian -->
        </div><!-- #div.span8 -->
    	<div class="span3">
    		<div class="formwrapper" style="overflow:auto;height:125px;width:100%;border:1px #DDDDDD solid">
                <label><div class="checker"><span><input type="checkbox" name="subjstudy_ket[]" style="opacity: 0; "></span></div>Depan bawah</label>
                <label><div class="checker"><span><input type="checkbox" name="subjstudy_ket[]" style="opacity: 0; "></span></div>Kiri atas</label>
                <label><div class="checker"><span><input type="checkbox" name="subjstudy_ket[]" style="opacity: 0; "></span></div>Kiri bawah</label>
                <label><div class="checker"><span><input type="checkbox" name="subjstudy_ket[]" style="opacity: 0; "></span></div>Kanan atas</label>
                <label><div class="checker"><span><input type="checkbox" name="subjstudy_ket[]" style="opacity: 0; "></span></div>Kanan bawah</label>
                <label><div class="checker"><span><input type="checkbox" name="subjstudy_ket[]" style="opacity: 0; "></span></div>Sakit</label>
                <label><div class="checker"><span><input type="checkbox" name="subjstudy_ket[]" style="opacity: 0; "></span></div>Tidak sakit</label>
            </div>
            <div>
                Nilai : <br/>
                <textarea style="width:90%"></textarea>
            </div>
    	</div><!-- #div.span3 -->
    </div>
    <br clear="all"/>
    <!-- END OF KAJIAN SUBJEKTIF -->
    <!-- KAJIAN OBJEKTIF -->
    <div class="title">
        <h3>Kajian Objektif</h3>
    </div>
    <br clear="all"/>
    <div id="kajianobj" class="span11">
        <textarea rows=3 class="span12"></textarea>
        <br clear="all"/>
        <button style="float:right" class="btn btn-primary" id="show_selectobj">Tambah Kajian</button>
    </div>
    <div class="span11" id="selectobj">
        <div class="span9" >
            <div class="box-kajian">
                <table class="table-kajian">
                    <tr>
                        <td><label><div class="checker"><span><input type="checkbox" name="objstudy[]" style="opacity: 0; "></span></div>Caries Dentis</td>
                        <td><label><div class="checker"><span><input type="checkbox" name="objstudy[]" style="opacity: 0; "></span></div>Gigi missing</label></td>
                    </tr>
                    <tr>
                        <td><label><div class="checker"><span><input type="checkbox" name="objstudy[]" style="opacity: 0; "></span></div>Caries Media</label></td>
                    </tr>
                    <tr>
                        <td><label><div class="checker"><span><input type="checkbox" name="objstudy[]" style="opacity: 0; "></span></div>Caries Profunda</label></td>
                    </tr>
                    <tr>
                        <td><label><div class="checker"><span><input type="checkbox" name="objstudy[]" style="opacity: 0; "></span></div>Caries Sekunder</label></td>
                    </tr>
                    <tr>
                        <td><label><div class="checker"><span><input type="checkbox" name="objstudy[]" style="opacity: 0; "></span></div>Dingin</label></td>
                    </tr>
                    <tr>
                        <td><label><div class="checker"><span><input type="checkbox" name="objstudy[]" style="opacity: 0; "></span></div>Gigi dewasa sudah tampak sebagian, tapi gigi susu belum tanggal</label></td>
                    </tr>
                    <tr>
                        <td><label><div class="checker"><span><input type="checkbox" name="objstudy[]" style="opacity: 0; "></span></div>Gigi memanjang</label></td>
                    </tr>
                </table>
            </div> <!-- #div.box-kajian -->
            <div style="padding:5px">
                <span>
                    ADD KAJIAN :  <input type="text" id="add_kajian_field" style="width:200px;margin:0px"/> 
                    <div class="radio" ><span><input type="radio" name="radios" style="opacity: 0;"></span></div> S
                    <div class="radio" id="uniform-undefined"><span><input type="radio" name="radios" style="opacity: 0;"></span></div> N
                    <div class="radio" id="uniform-undefined"><span><input type="radio" name="radios" style="opacity: 0;"></span></div> J
                    <button class="btn btn-primary" style="float:right" id="add_obj"> + </button>
                </span>
            </div><!-- #div add kajian -->
        </div><!-- #div.span8 -->
        <div class="span3" >
            <div class="formwrapper" style="overflow:auto;height:125px;width:100%;border:1px #DDDDDD solid">
                <label><div class="checker"><span><input type="checkbox" name="objstudy_ket[]" style="opacity: 0; "></span></div>Depan bawah</label>
                <label><div class="checker"><span><input type="checkbox" name="objstudy_ket[]" style="opacity: 0; "></span></div>Kiri atas</label>
                <label><div class="checker"><span><input type="checkbox" name="objstudy_ket[]" style="opacity: 0; "></span></div>Kiri bawah</label>
            </div>
            <div>
                Nilai : <br/>
                <textarea style="width:90%"></textarea>
            </div>
        </div><!-- #div.span3 -->
    </div>
    <!-- END OF KAJIAN OBJEKTIF -->
</div>