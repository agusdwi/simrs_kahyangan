<script type="text/javascript" src="<?=base_url()?>assets/js/flexbox.js"></script>
<script>
var medicine = {};
medicine.results = [
	{id:'1',name:'A-B VASK TABLET 10 MG'},
	{id:'2',name:'3TC-HBV TABLET'},
	{id:'3',name:'A-B VASK TABLET 5 MG'},
	{id:'4',name:'ABBOTIC XL'},
	{id:'5',name:'ACCOLATE TABLET'},
	{id:'6',name:'ACCUPRIL TABLET 10 MG'},
	{id:'7',name:'ACCUPRIL TABLET 20 MG'},
	{id:'8',name:'ACCUPRIL TABLET 5 MG'},
	{id:'9',name:'ACEPRESS TABLET 12,5 MG'},
	{id:'10',name:'ACEPRESS TABLET 25 MG'},
	{id:'11',name:'ACETENSA TABLET'},
	{id:'12',name:'ACIFAR CAPLET 200 MG'},
	{id:'13',name:'ACIFAR CAPLET 400 MG'},
	{id:'14',name:'ACIFAR CREAM'},
	{id:'15',name:'ACITRAL TABLET'},
];
medicine.total = medicine.results.length;
$(function(){
	max = 5;
	$('.medicine').flexbox(medicine);
	$('#add_medicine').click(function(){
		tr = $("table#table1 tr:eq(-1)").html();
		$('table#table1').append('<tr>'+tr+'</tr>');
		$("table#table1 tbody tr:eq("+max+") td:eq(0)").html(max+1);
		$("table#table1 tbody tr:eq("+max+") td:eq(1)").html('<div class="medicine"></div>');
		$(".medicine:eq("+max+")").flexbox(medicine);
		max++;
		return false;
	})
})
</script>
<style type="text/css">
	.tables thead tr th{
		height:28px;
		font-size:13px;
		vertical-align: middle;
	}
</style>	
<div class="span9" style="margin-left:0px;padding:10px;">
	<table class="table table-bordered def_table_y dataTable tables" align="center" style="margin-left:0px;width:100%;" id="table1">
	<thead>
		<tr role="row">
			<th style="width:2%;">Nomor</th>
			<th style="width:30%;">Obat</th>
			<th style="width:30%;">Aturan Pakai</th>	
			<th style="width:20%;">Jumlah</th>	
		</tr>
	</thead>
	<tbody>
		<?for($i=1;$i<=5;$i++):?>
			<tr>
				<td style="text-align:center;"><b><?=$i?></b></td>
				<td>
					<div class="medicine" id="medicine[]"></div><!-- the flexbox!-->
				</td>
				<td>
					<input type="text" name="use[]">
				</td>
				<td>
					<input type="text" name="amount[]">
				</td>
			</tr>
		<?endfor;?>
	</tbody>
	</table>
	<button type="submit" class="btn btn-primary">Tindakan Lanjut</button>
	<a href="#" id="add_medicine" class="btn btn-success">Tambah Obat</a>
</div>
