<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rawat_jalan extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 3
			);
		$this->load->model('Mpasien','mp');
	}

	public function index(){
		$data['main_view']	= 'index';
		$data['title']		= 'Rawat Jalan';
		$data['cf']			=  $this->cf;
		$data['current']	= '';
		$data['poli']		= $this->db->get_where('trx_poly',array('pl_status'=> 1));
		$data['left_sidebar'] = "left_rawat_jalan";
		$this->load->view('template',$data);
	}
}