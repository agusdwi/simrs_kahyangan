<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
	status medical
	1 => open medical
	2 => close medical, not yet pay
	3 => pay, complete medical, go home/ switch to rawat inap	
*/
class Poli extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
			'modul_id'	=> 3
		);
		$this->date = DATE('Y-m-d');
		$this->load->model('Mpasien');
		$this->ptn_class = 1; // rawat jalan tipe kelas pasien 1 = standart
	}

	function antrian($pl_id){
		if(is_post()){
			$input = $this->input->post();
			redirect(cur_url(-2).'proses/'.$input['queue_id']);
		}
		$data['cur_poli']	= $this->db->get_where('trx_poly',array('pl_id'=>$pl_id))->row();
		$data['ds']			= $this->db->get_where('v_antrian_rajal',array('queo_status'=>1,'pl_id'=>$pl_id));
		$data['main_view']	= 'poli/antrian';
		$data['title']		= "Pasien ".$data['cur_poli']->pl_name;
		$data['cf']			=  $this->cf;
		$data['current']	= $pl_id;
		$data['left_sidebar'] = "left_rawat_jalan";
		$this->load->view('template',$data);	
	}

	function proses($queue_id){
		$que = $this->db->get_where('trx_queue_outpatient',array('queo_id'=>$queue_id))->row();
		$cek = $this->db->get_where('trx_medical',array('mdc_id'=>$queue_id));
		if ($cek->num_rows()>0) {
			redirect(cur_url(-2).'tinfo/'.$cek->row()->mdc_id);
		}else{
			$data = array(
				"mdc_id"	=> $que->queo_id,
				"sd_rekmed"	=> $que->sd_rekmed,
				"mdc_in"	=> date_now(),
				"pl_id"		=> $que->pl_id,
				"dr_id"		=> $que->dr_id
			);
			$this->db->insert('trx_medical',$data);
			redirect(cur_url(-2).'tinfo/'.$que->queo_id);
		}
	}

	private function tdata($id,&$data){
		$this->mdc 		= $this->db->get_where('trx_medical',array('mdc_id'=>$id))->row();
		$this->poli 	= $this->db->get_where('trx_poly',array('pl_id'=>$this->mdc->pl_id))->row();
		$this->ptn 		= $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$this->mdc->sd_rekmed))->row();
		$data['left_sidebar'] = "left_rawat_jalan";
		$data['cf']			=  $this->cf;
		$data['title']		= 'Proses Pemeriksaan '.$this->poli->pl_name;
		$data['cur_poli']	= $this->poli;
	}

	function tinfo($id){
		$this->tdata($id,$data);
		if (is_post()) {
			$input 				= $this->input->post();
			$insert 			= $input['ds'];
			$insert['mdc_in'] 	= date_to_sql($insert['mdc_in']);
			$this->db->where('mdc_id', $id);
			$this->db->update('trx_medical', $insert); 
			$this->Mpasien->set_alergi($this->mdc->sd_rekmed,$input['ptn']['alergi']);
			if(isset($input['savenext'])){
				redirect(cur_url(-2).'trekmed/'.$id);
			}
		}
		$this->tdata($id,$data);
		if (empty($this->mdc->dr_id)) {
			$antri = $this->db->get_where('trx_queue_outpatient',array('queo_id'=>$id));
			if ($antri->num_rows() > 0) {
				$dr_antri = $antri->row()->dr_id;
				$dt = array('dr_id' =>  $dr_antri);
				$this->db->where('mdc_id', $id);
				$this->db->update('trx_medical', $dt); 
				$this->mdc->dr_id = $dr_antri;
			}
		}
		
		$data['main_view']	= 't/info';
		$data['alergi']		= $this->Mpasien->get_alergi($this->mdc->sd_rekmed);
		$data['insurance']	= $this->db->get('mst_ref_insurance');
		$this->load->view('template',$data);
	}

	function trekmed($id,$mdc="",$tipe=""){
		if ($this->input->is_ajax_request()) {
			if ($tipe=='rajal') {
				$data = $this->Mpasien->get_rekmed($mdc);
				$this->load->view('t/rekmed_single',$data);
			}else{
				$data = $this->Mpasien->get_rekmed_ranap($mdc);
				$this->load->view('t/rekmed_single_ranap',$data);
			}
		}else{
			$this->tdata($id,$data);	
			$data['main_view']	= 't/rekmed';
			$data['date']		= $this->Mpasien->get_rekmed_date($id);
			$this->load->view('template',$data);
		}
	}

	function tdiagnosa($id,$diagid=""){
		if ($this->input->is_ajax_request()) {
			if($diagid!=""){
				$data = array('is_delete' =>  1);
				$this->db->where('trx_diag_id', $diagid);
				$this->db->update('trx_diagnosa', $data); 
				echo "success";
			}
			if (is_post()) {
				$input = $this->input->post();
				$insert = $input['ds'];
				$insert['mdc_id'] = $id;
				$this->db->insert('trx_diagnosa',$insert);
				$id = $this->db->insert_id();
				$row = $this->db->get_where('v_trx_diag ',array('trx_diag_id'=>$id))->row();
				echo json_encode(array('result'=>'success','data'=>$row));
			}
		}else{
			if (is_post()) {
				$input = $this->input->post();
				if(isset($input['savenext'])){
					redirect(cur_url(-2).'ttindakan/'.$id);
				}
			}
			$this->tdata($id,$data);	
			$data['main_view']	= 't/diagnosa';
			$data['ds']			= $this->db->get_where('v_trx_diag ',array('mdc_id'=>$id,'is_delete'=>0));
			$this->load->view('template',$data);			
		}
	}

	function ttindakan($id,$tdk_id=""){
		if ($this->input->is_ajax_request()) {
			if($tdk_id!=""){
				$data = array('is_delete' =>  1);
				$this->db->where('trx_treat_id', $tdk_id);
				$this->db->update('trx_treathment', $data); 
				echo "success";
			}
			if (is_post()) {
				$input = $this->input->post();
				$insert = $input['ds'];
				$insert['mdc_id'] = $id;
				$this->db->insert('trx_treathment',$insert);
				$id = $this->db->insert_id();
				$row = $this->db->get_where('v_trx_treat ',array('trx_treat_id'=>$id))->row();
				echo json_encode(array('result'=>'success','data'=>$row));
			}
		}else{
			if (is_post()) {
				$input = $this->input->post();
				if(isset($input['savenext'])){
					redirect(cur_url(-2).'tresep/'.$id);
				}
			}
			$this->tdata($id,$data);	
			$data['main_view']	= 't/tindakan';
			$data['ds']			= $this->db->get_where('v_trx_treat ',array('mdc_id'=>$id,'is_delete'=>0));
			$this->load->view('template',$data);			
		}
	}

	function tresep($id,$mdc_id=""){
		if ($this->input->is_ajax_request()) {
			if($mdc_id!=""){
				$data = array('is_delete' =>  1);
				$this->db->where('trx_recipe_id', $mdc_id);
				$this->db->update('trx_recipe', $data); 
				echo "success";
			}
			if (is_post()) {
				$input = $this->input->post();
				$insert = $input['ds'];
				$insert['mdc_id'] = $id;
				$this->db->insert('trx_recipe',$insert);
				$id = $this->db->insert_id();
				$row = $this->db->get_where('v_trx_recipe ',array('trx_recipe_id'=>$id))->row();
				echo json_encode(array('result'=>'success','data'=>$row));
			}
		}else{
			if (is_post()) {
				$input = $this->input->post();
				if(isset($input['savenext'])){
					redirect(cur_url(-2).'tparamedis/'.$id);
				}
			}
			$this->tdata($id,$data);	
			$data['main_view']	= 't/resep';
			$data['ds']			= $this->db->get_where('v_trx_recipe ',array('mdc_id'=>$id,'is_delete'=>0));
			$this->load->view('template',$data);			
		}
	}

	function tparamedis($id,$tid=""){
		if ($this->input->is_ajax_request()) {
			if (is_post()) {
				$this->db->delete('trx_param_tarif', array('mdc_id'=>$id,'treat_id'=>$tid)); 
				$input = $this->input->post();
				foreach ($input['r'] as $key => $value) {
					$in = array(
						'mdc_id'			=> $id,
						'treat_id'			=> $tid,
						'role_id'			=> $value,
						'param_id'			=> $input['p'][$key],
						'treat_tarif_harga'	=> $input['t'][$key]
					);	
					$this->db->insert('trx_param_tarif',$in);
				}
			}else{
				$data['ds']			= $this->db->get_where('mst_treathment_tarif',array('treat_id'=>$tid,'cls_id'=>$this->ptn_class));
				$data['role'] 		= $this->db->get('mst_ref_role_paramedis');
				$data['tarif'] 		= $this->db->get_where('trx_param_tarif',array('mdc_id'=>$id,'treat_id'=>$tid));
				$data['dr'] 		= $this->db->get_where('trx_doctor',array('dr_status'=>1));
				$this->load->view('t/param_detail',$data);
			}
		}else{
			$input = $this->input->post();
			if(isset($input['savenext'])){
				redirect(cur_url(-2).'tharga/'.$id);
			}

			$this->tdata($id,$data);	
			$data['main_view']	= 't/paramedis';
			$data['ds']			= $this->db->get_where('v_trx_treat ',array('mdc_id'=>$id,'is_delete'=>0));
			$this->load->view('template',$data);
		}
	}

	function cek_saved_tread($id){
		echo json_encode($this->db->group_by('treat_id')->get_where('trx_param_tarif',array('mdc_id'=>$id))->result());
	}

	function tharga($id){
		if ($this->input->is_ajax_request()) {
			if (is_post()) {
				$this->update_bill($id);				
			}
		}else{
			if (is_post()) {
				$this->update_bill($id);
				$input = $this->input->post();
				if(isset($input['savenext'])){
					redirect(cur_url(-2).'tringkasan/'.$id);
				}
			}
			$this->tdata($id,$data);	
			$data['main_view']	= 't/harga';
			$data['treat']		= $this->db->get_where('v_treat_param_name',array('mdc_id'=>$id,'is_delete'=>0));
			
			$data['obat']		= $this->Mpasien->get_harga_obat($id,$this->mdc->tp_insurance);
			$data['bill']		= $this->db->get_where('trx_bill',array('mdc_id'=>$id));
			$this->load->view('template',$data);			
		}
	}

	function tringkasan($id){
		$this->tdata($id,$data);
		if (is_post()) {
			$in = $this->input->post();
			$manual = $this->db->get_where('trx_medical',array('mdc_id'=>$id))->row()->is_manual;
			$status = ($manual == 0)? 2 : 3;
			if(isset($in['savenext'])){

				// update status medical
				$ipt = $this->input->post('ds');
				$data = array(
								'mdc_status' 	=>  $status,
								'modi_datetime'	=>getMicroTime(),
								'status_pulang'	=> $ipt['status_pulang']
							);
				$this->db->where('mdc_id', $id);
				$this->db->update('trx_medical', $data);

				// update antrian
				$data = array('queo_status' =>0 );
				$this->db->where('queo_id', $id);
				$this->db->update('trx_queue_outpatient', $data); 

				if($ipt['status_pulang'] == 'rawat inap'){
					redirect(base_url('rawat_jalan/poli/rujukan/'.$id));
				}else{
					$this->session->set_flashdata('message','pemeriksaan selesai');

					if ($manual == 0) {
						redirect(cur_url(-2).'antrian/'.$this->poli->pl_id);
					}else{
						redirect(base_url('rawat_jalan/manual'));
					}
				}

				
			}else{
				$input = $this->input->post('ds');
				$rujuk = $this->input->post('rujukan');
				if($input['status_pulang'] == 'rujukan')
					$input['status_pulang'] .= ','.$rujuk;
				$input['date_control'] = date_to_sql($this->input->post('date_control'));
				$this->db->where('mdc_id', $id);
				$this->db->update('trx_medical', $input); 
			}
		}
		$data['main_view']	= 't/ringkasan';
		$data['ds']			= $this->Mpasien->get_rekmed($id);
		$this->load->view('template',$data);			
	}

	function update_bill($id){
		$input = $this->input->post();
		$input['bill']['mdc_id'] = $id;

		$d = $this->db->get_where('trx_bill',array('mdc_id'=>$id));
		if($d->num_rows() == 1){
			$this->db->where('mdc_id', $id);
			$this->db->update('trx_bill', $input['bill']); 
		}else{
			$this->db->insert('trx_bill', $input['bill']); 
		}
	}

	function rujukan($id){
		$this->tdata($id,$data);
		if (is_post()) {
			$manual = $this->db->get_where('trx_medical',array('mdc_id'=>$id))->row()->is_manual;

			$data = $this->input->post('ds');
			$data['rnp_id'] = $this->Mpasien->get_rnp_id();
			$data['rnp_in'] = $this->input->post('date').' '.date("h:i:s");
			$data['rnp_status'] = 1;
			$this->db->insert('rnp_medical',$data);
			
			$this->session->set_flashdata('message','pasien berhasil didaftarkan rawat inap');

			if ($manual == 0) {
				redirect(cur_url(-2).'antrian/'.$this->poli->pl_id);
			}else{
				redirect(base_url('rawat_jalan/manual'));
			}
		}
		$data['main_view']	= 'rujukan';
		$data['tp_insurance'] = $this->db->get('mst_ref_insurance');
		$data['kamar']		= $this->db->get_where('mst_kamar',array('status'=>1));
		$data['ruang']		= $this->db->get_where('mst_ruang',array('status'=>1));
		$this->load->view('template',$data);					
	}
}