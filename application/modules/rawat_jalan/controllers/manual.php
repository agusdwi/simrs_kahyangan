<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
	status medical
	1 => open medical
	2 => close medical, not yet pay
	3 => pay, complete medical, go home/ switch to rawat inap	
*/
class Manual extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
			'modul_id'	=> 3
		);
		$this->date = DATE('Y-m-d');
		$this->load->model('Mpasien');
		$this->ptn_class = 1; // rawat jalan tipe kelas pasien 1 = standart
	}

	function index(){
		if(is_post()){
			$input = $this->input->post();
			$ds = $this->input->post('ds');
			if ($this->db->get_where('trx_medical',array('mdc_status'=>1,'sd_rekmed'=>$input['fx_pasien_input']))->num_rows() > 0) {
				$this->session->set_flashdata('message','pasien masih terdaftar pemeriksaan');
				redirect(cur_url());
			}if($this->db->get_where('trx_queue_outpatient',array('queo_status'=>1,'sd_rekmed'=>$input['fx_pasien_input']))->num_rows() > 0){
				$this->session->set_flashdata('message','pasien masih terdaftar dalam antrian');
				redirect(cur_url());
			}else{
				$mdc = $this->get_mdc_id();	
				$data = array(
					"mdc_id"	=> $mdc,
					"sd_rekmed"	=> $input['fx_pasien_input'],
					"mdc_in"	=> date_now(),
					"pl_id"		=> $ds['poli_id'],
					"dr_id"		=> $ds['dr_id'],
					"mdc_in"	=> date_to_sql($ds['date']).' '.DATE('H:i:s'),
					"is_manual"	=> 1
				);
				$this->db->insert('trx_medical',$data);
				redirect(cur_url(-1).'poli/tinfo/'.$mdc);
			}
		}
		$mnpl = array('pl_id'=>99);
		$data['cur_poli']	= (object)$mnpl;
		$data['ds']			= $this->db->get('v_manual_input');
		$data['main_view']	= 'manual';
		$data['title']		= 'Manual Input Medical';
		$data['cf']			=  $this->cf;
		$data['current']	= 99;
		$data['left_sidebar'] = "left_rawat_jalan";
		$data['poli'] 		= $this->db->get('trx_poly');
		$this->load->view('template',$data);
	}

	function get_mdc_id(){
		$ym = date('ym');
		$a = $this->db->like('queo_id',$ym,'after')->order_by('queo_id','DESC')->get('trx_queue_outpatient',1,0)->row();
		$queo = count($a) == 0 ?date('ym')."0001" : $a->queo_id+1;
		$no = count($a) == 0 ? 1 : $a->queo_no+1;
		$user = get_user();
		$data = array('queo_id'	=> $queo,'queo_status'=>0);		
		$this->db->insert('trx_queue_outpatient',$data);
		return $queo;
	}
}