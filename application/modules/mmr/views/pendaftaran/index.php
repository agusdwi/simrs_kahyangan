<script type="text/javascript">
	$(function(){
		$('.data-table').dataTable({
			"bJQueryUI": true,
			"bFilter": false,
			"bLengthChange": false,
			"sPaginationType": "full_numbers",
			"sDom": '<""l>t<"F"fp>'
		});
	})
</script>

<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>

<div class="mmr container-fluid">
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12" style="padding:15px">
				<ul id="myTab" class="nav nav-tabs">
					<li class="active"><a href="#home" data-toggle="tab">Pengguna Aktif</a></li>
					<li class=""><a href="#profile" data-toggle="tab">Pengguna Pending Approval</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane fade active in" id="home">
						<table class="table table-bordered data-table">
							<thead>
								<tr>
									<th>No</th>
									<th>Pasien</th>
									<th>Tgl Aktivasi</th>
									<th>Token</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?$i=0;foreach ($aktif->result() as $key): $i++;?>
									<tr>
										<td><?=$i;?></td>
										<td>
											<?if ($key->foto != ''): ?>
												<img style="width:100px;float:left;margin-right:10px" src="<?=base_url()?>files/avatar/<?=$key->foto;?>">
											<?endif;?>
											<div>
												<b>Rm : <?=$key->ptn_rekmed;?></b><br>
												<b style="font-size:150%"><?=$key->ptn_name;?></b>
											</div>
										</td>
										<td><?=format_date_time($key->api_reg_date)?></td>
										<td><?=$key->api_token;?></td>
										<td class="center">
											<a href="<?=base_url()?>mmr/pendaftaran/inaktif/<?=$key->ptn_rekmed;?>">
												<span class="label label-important">block</span>
											</a>
										</td>
									</tr>
								<?endforeach;?>
							</tbody>
						</table>  
					</div>
					<div class="tab-pane fade" id="profile">
						<table class="table table-bordered data-table">
							<thead>
								<tr>
									<th>No</th>
									<th>No Rekmed</th>
									<th>Nama Pasien</th>
									<th>Tgl Aktivasi</th>
									<th>Token</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?$i=0;foreach ($pending->result() as $key): $i++;?>
									<tr>
										<td><?=$i;?></td>
										<td><?=$key->ptn_rekmed;?></td>
										<td><?=$key->ptn_name;?></td>
										<td><?=format_date_time($key->api_reg_date)?></td>
										<td><?=$key->api_token;?></td>
										<td class="center">
											<a href="<?=base_url()?>mmr/pendaftaran/aktif/<?=$key->ptn_rekmed;?>">
												<span class="label label-success">approve</span>
											</a>
											<a href="<?=base_url()?>mmr/pendaftaran/tolak/<?=$key->ptn_rekmed;?>">
												<span class="label label-important">reject</span>
											</a>
										</td>
									</tr>
								<?endforeach;?>
							</tbody>
						</table>  
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
