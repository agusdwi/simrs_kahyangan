<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>

<div class="mmr container-fluid">
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12" style="padding:15px">
				<?=form_open()?>
					<b style="font-size:14px"><?=$text;?></b><br>
					<br>
					<table>
						<tr>
							<td>Nama</td>
							<td>:</td>
							<td><?=$ds->sd_name;?></td>
						</tr>
						<tr>
							<td>No Rekam Medis</td>
							<td>:</td>
							<td><?=$ds->sd_rekmed;?></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td>:</td>
							<td><?=$ds->sd_address;?></td>
						</tr>
					</table>
					<br><br>
					<a href="../" class="btn btn-danger btn-large">batal</a>
					<button class="btn btn-success btn-large">iya</button>
				</form>
				<br>
			</div>
		</div>
	</div>
</div>