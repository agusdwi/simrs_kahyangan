<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gcmdemo extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->api_key = "AIzaSyAgmgVzI4GS5QwqjHy9yDZq0l7IOkwbEZ4";
	}

	public function send_notification($registatoin_ids="", $message="") {
 		$registatoin_ids = "APA91bEW5QhlwrIjIBKpQS_56ys6mfAwjeO27of0czDaDw0KCg-mK-QSkNBZ4dLTks0Jr2HZ9uouQWmSDqEzjvC0jC8MWv7cZRsND9oQozXwZFNcu5VYu0z4n83ujwzLG13FKBcIdaYCHbSBEcD0UDl_eWmqPtRY8g";
        $url = 'https://android.googleapis.com/gcm/send';
        
        $fields = array(
            'registration_ids' 	=> $registatoin_ids,
            'data' 				=> "halo ini adalah pesan"
      	);
        
        $headers = array(
            'Authorization: key=' . $this->api_key,
            'Content-Type: application/json'
      	);
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $url);
        
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        
        curl_close($ch);
        echo $result;
        return $result;
    }
}