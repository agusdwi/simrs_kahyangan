<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
			'modul_id'	=> 1
		);
		$this->api_key = "AIzaSyBfeSRh8mglzQL4yj0zQmFEjwyOonVZnrU";
		$this->url_qrcode = base_url()."qrcode/?data=";
	}

	public function index(){
		$data['main_view']	= 'pendaftaran/index';
		$data['title']		= 'Daftar Pasien pada Aplikasi Mobile';
		$data['cf']			=  $this->cf;
		$data['current']	= 25;
		$data['aktif']		= $this->db->get_where('v_patient_api',array('api_aktif'=> 1));
		$data['pending']	= $this->db->get_where('v_patient_api',array('api_aktif'=> 0));
		$this->load->view('template',$data);
	}

	function aktif($rekmed){
		if (is_post()) {
			$user = $this->db->get_where('mmr_api_user',array('sd_rekmed'=>$rekmed))->row();
			$qrcode = $this->generate_qrcode($rekmed);
			$msg = array(
				"notif_type"=> "activation_success",
				"message" 	=> "akun anda telah diaktifkan",
				"rekmed" 	=> $rekmed,
				"token"		=> $user->api_token,
				"qrcode"	=> $qrcode
			);
			
			$res = $this->send_notification(array($user->regID),$msg);
			$res = json_decode($res);
			if($res->success){
				// update database
				$data = array('api_aktif' =>  1);
				$this->db->where('api_id', $user->api_id);
				$this->db->update('mmr_api_user', $data);

				$this->session->set_flashdata('message','user berhasil diaktifkan');
			}else{
				$this->session->set_flashdata('message','Google cloud message error, silahkan ulangi');
			}
			redirect(base_url('mmr/pendaftaran'));
		}else{
			$data['main_view']	= 'pendaftaran/detail';
			$data['title']		= 'Approval pendaftaran mobile app';
			$data['cf']			= $this->cf;
			$data['current']	= 25;
			$data['text']		= "Apakah anda ingin mengaktifkan pasien dibawah ini :";
			$data['ds']			= $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$rekmed))->row();
			$this->load->view('template',$data);
		}
	}

	function inaktif($rekmed){
		if (is_post()) {
			debug_array("sadasd");
		}else{
			$data['main_view']	= 'pendaftaran/detail';
			$data['title']		= 'Approval pendaftaran mobile app';
			$data['cf']			= $this->cf;
			$data['current']	= 25;
			$data['text']		= "Apakah ingin mem-blok pengguna ini";
			$data['ds']			= $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$rekmed))->row();
			$this->load->view('template',$data);
		}
	}

	function tolak($rekmed){
		if (is_post()) {
			debug_array("sadasd");
		}else{
			$data['main_view']	= 'pendaftaran/detail';
			$data['title']		= 'Approval pendaftaran mobile app';
			$data['cf']			= $this->cf;
			$data['current']	= 25;
			$data['text']		= "Apakah anda ingin menolak pengguna ini";
			$data['ds']			= $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$rekmed))->row();
			$this->load->view('template',$data);
		}
	}

	public function send_notification($registatoin_ids, $message) {
 
        $url = 'https://android.googleapis.com/gcm/send';
        
        $fields = array(
            'registration_ids' 	=> $registatoin_ids,
            'data' 				=> $message
      	);
        
        $headers = array(
            'Authorization: key=' . $this->api_key,
            'Content-Type: application/json'
      	);
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $url);
        
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        
        curl_close($ch);
        return $result;
    }

    function generate_qrcode($rekmed){
    	$url = $this->url_qrcode.$rekmed;
    	$ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $result = json_decode(curl_exec($ch));

    	return $result->image;
    }

    function control_notif(){
		$mmr = $this->db->get_where('v_skripsi_control_send',array('send_notif_control'=>0));

		$this->new_file('cron control notif');

		foreach ($mmr->result() as $key) {
			$msg = array(
				"notif_type"=> "control",
				"message" 	=> "Pengingat Kontrol Tanggal ".pretty_date($key->date_control,false),
				"dokter" 	=> $key->dr_name,
				"poli" 		=> $key->pl_name,
				"tgl" 		=> $key->date_control
			);

			$res = $this->send_notification(array($key->regID),$msg);
			$data = array(
			               'send_notif_control' =>  1
			            );
			$this->db->where('sd_rekmed', $key->sd_rekmed);
			$this->db->update('trx_medical', $data); 
		}
    }

    function new_file($str){
    	$myFile = "testFile.txt";
		$fh = fopen($myFile, 'a') or die("can't open file");
		$stringData = DATE("y-m-d h:i:s => $str
");
		fwrite($fh, $stringData);
		fclose($fh);
    }
}