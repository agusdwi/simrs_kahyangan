<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contoh extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
			'modul_id'	=> 1
		);
	}

	public function index(){
		$data['main_view']	= 'contoh/index';
		$data['title']		= 'Ini untuk contoh user interface';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}
	
	function contoh_iframe(){
		$data['main_view']	= 'contoh/iframe';
		$this->load->view('iframe',$data);
	}

	function cek_insert(){
		$data = array(
			"mci_code"	=> "jajal iki cobo",
			"mci_name"	=> "jajal iki cobo"
		);
		$this->db->insert('mst_city',$data);
		debug_array($this->db->last_query());
		debug_array($this->db->order_by('mci_id','desc')->get('mst_city',1)->row());
	}
}