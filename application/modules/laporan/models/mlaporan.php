<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mlaporan extends CI_Model { 
	function __construct() {
		parent::__construct();
	}

	function get_income_rajal($mode,$value){
		$sql = "select *,sum(bill) as total from v_report_income_rajal";

		if($mode == 'tahun')
			$sql .= " where tahun = '$value' GROUP BY bulan";
		else if($mode == 'bulan')
			$sql .= " where bulan = '$value' GROUP BY tanggal";
		else $sql = " select * from v_report_income_rajal where tanggal = '$value'";

		return $this->db->query($sql);
	}

	function get_income_ranap($mode,$value){
		$sql = "select *,sum(bill) as total from v_report_income_ranap";
		
		if($mode == 'tahun')
			$sql .= " where tahun = '$value' GROUP BY bulan";
		else if($mode == 'bulan')
			$sql .= " where bulan = '$value' GROUP BY tanggal";
		else $sql = " select * from v_report_income_ranap where tanggal = '$value'";

		return $this->db->query($sql);
	}

	function get_jasa_medis_rajal($bln,$prm){
		$ds = $this->db->get_where('v_report_jasa_medis_rajal',array('bulan'=>$bln,'param_id'=>$prm));
		// debug_array($ds->result());
		$r = array();
		$oldc = '';
		foreach ($ds->result() as $key) {
			if($oldc != $key->tanggal){
				$oldc = $key->tanggal;
			}
			$r[$oldc][] = $key;
		}
		return $r;
	}

	function get_jasa_medis_ranap($bln,$prm){
		$ds = $this->db->get_where('v_report_jasa_medis_ranap',array('bulan'=>$bln,'param_id'=>$prm));
		// debug_array($ds->result());
		$r = array();
		$oldc = '';
		foreach ($ds->result() as $key) {
			if($oldc != $key->tanggal){
				$oldc = $key->tanggal;
			}
			$r[$oldc][] = $key;
		}
		return $r;
	}

	function get_all_poli(){
		$sql = "select * from trx_poly where pl_status='1'";

		return $this->db->query($sql);
	}

	function get_pasien_per_poli($pl_id,$value){
		$sql = "select * from v_report_pasienperpoli where pl_id='$pl_id' and mdc_in = '$value'";

		return $this->db->query($sql);
	}

	function get_jumlah_pasien_per_poli($pl_id,$mode,$value){
		$sql = "select * from v_report_pasienperpoli";

		if($mode == 'tahun')
			$sql .= " where tahun = '$value' and pl_id='$pl_id'";
		else if($mode == 'bulan')
			$sql .= " where bulan = '$value' and pl_id='$pl_id'";
		else $sql = " select * from v_report_pasienperpoli where tanggal = '$value' and pl_id='$pl_id'";

		return $this->db->query($sql);
	}

	function get_all_dokter(){
		$sql = "select * from v_paramedis_jenis where dr_status='1'";

		return $this->db->query($sql);
	}

	function get_pasien_per_dokter($dr_id,$value){
		$sql = "select * from v_report_pasienperdokter where dr_id='$dr_id' and mdc_in = '$value'";

		return $this->db->query($sql);
	}

	function get_all_kelas(){
		$sql = "select * from mst_ref_class";

		return $this->db->query($sql);
	}

	function get_pasien_per_kelas($cls_id,$value,$value2){
		$sql = "select * from v_report_pasienperkelas where cls_id='$cls_id' and date_in >= '$value' and date_out <= '$value2'";

		return $this->db->query($sql);
	}

	function get_all_ranap($tgl){
		$sql = "select p.*,k.k_nama as k_nama,r.r_nama as r_nama from v_rnp_aktif p
					join mst_kamar k on p.cls_id = k.id
						join mst_ruang r on p.ruang_id= r.id
							where rnp_in >= '$tgl' ";

		return $this->db->query($sql);
	}
	
}