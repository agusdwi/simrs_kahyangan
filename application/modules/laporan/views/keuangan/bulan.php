
<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox" id="main-ganti">
			<div class="span12" id="ganti" style="padding:10px">
				<ul class="nav nav-pills">
					<li class="">
						<a href="<?=base_url('laporan/keuangan/hari')?>">Harian</a>
					</li>
					<li class="active">
						<a href="<?=base_url('laporan/keuangan/bulan')?>">Bulanan</a>
					</li>
					<li>
						<a href="<?=base_url('laporan/keuangan/tahun')?>">Tahunan</a>
					</li>
				</ul>
				<div class="clear"></div>
				<br>
				<div>
					<b>Pilih Tahun : </b>
					<select id="year" style="width:75px;margin-top:10px">
						<?for ($i=$this->start;$i<$this->year+1;$i++): ?>
							<option <?=($i == $cur_year)? 'selected="selected"':'';?> value="<?=$i;?>"><?=$i;?></option>
						<?endfor;?>
					</select>
					&nbsp;
					<b>Pilih Bulan : </b>
					<select id="month" style="width:150px;margin-top:10px">
						<?$i=0;foreach ($this->bulan as $key): $i++;?>
							<option <?=($i == $cur_month)? 'selected="selected"':'';?> value="<?=$i;?>"><?=$this->bulan[$i-1];?></option>
						<?endforeach;?>
					</select>
					<button id="apply" type="button" name="save" class="btn btn-mini btn-primary simpan">Submit</button>
				</div>
				<div>
					<h4>Rawat Jalan</h4>
					<table class="table table-bordered table-striped table_tr">
						<thead>
							<th>No</th>
							<th>Tanggal</th>
							<th>Total Transaksi</th>
						</thead>
						<tbody>
							<?$i=0;$ztotal=0;foreach ($rajal->result() as $key):$i++; ?>
								<?if ($key->total != 0): ?>
									<tr>
										<td style="width:100px"><?=$i;?></td>
										<td><?=pretty_date($key->tanggal);?></td>
										<td class="money"><?=int_to_money($key->total);?></td>
										<?
											$ztotal += $key->total;
										?>
									</tr>
								<?endif;?>
							<?endforeach;?>
						</tbody>
						<tfoot>
							<td style="text-align:right;font-weight:bold" colspan="2">Total</td>
							<td class="money" style="font-weight:bold"><?=int_to_money($ztotal);?></td>
						</tfoot>
					</table>
					<br>
					<h4>Rawat Inap</h4>
					<table class="table table-bordered table-striped table_tr">
						<thead>
							<th>No</th>
							<th>Tanggal</th>
							<th>Total Transaksi</th>
						</thead>
						<tbody>
							<?$i=0;$ztotal=0;foreach ($ranap->result() as $key):$i++; ?>
								<?if ($key->total != 0): ?>
									<tr>
										<td style="width:100px"><?=$i;?></td>
										<td><?=pretty_date($key->tanggal);?></td>
										<td class="money"><?=int_to_money($key->total);?></td>
										<?
											$ztotal += $key->total;
										?>
									</tr>
								<?endif?>
							<?endforeach;?>
						</tbody>
						<tfoot>
							<td style="text-align:right;font-weight:bold" colspan="2">Total</td>
							<td class="money" style="font-weight:bold"><?=int_to_money($ztotal);?></td>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('body').on('click','#apply',function (e) {
			e.preventDefault();
			var url = BASE+'laporan/keuangan/bulan/'+$("#month").val()+"/"+$("#year").val();
			window.location = url;
		});
	})
</script>