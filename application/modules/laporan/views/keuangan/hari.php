<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox" id="main-ganti">
			<div class="span12" id="ganti" style="padding:10px">
				<ul class="nav nav-pills">
					<li class="active">
						<a href="<?=base_url('laporan/keuangan/hari')?>">Harian</a>
					</li>
					<li>
						<a href="<?=base_url('laporan/keuangan/bulan')?>">Bulanan</a>
					</li>
					<li>
						<a href="<?=base_url('laporan/keuangan/tahun')?>">Tahunan</a>
					</li>
				</ul>
				<div class="clear"></div>
				<br>
				<div>
					<b>Pilih Tanggal : </b>
					<input type="text" style="width:100px;margin-top:8px" id="tgl" class="datepicker" value="<?=$cur_date;?>">
					<button id="apply" type="button" name="save" class="btn btn-mini btn-primary simpan">Submit</button>
				</div>
				<br>
				<div>
					<h4>Rawat Jalan</h4>

					<table class="table table-bordered table-striped table_tr">
						<thead>
							<th>No</th>
							<th>Tanggal</th>
							<th>Total Transaksi</th>
						</thead>
						<tbody>
							<?$i=0;$ztotal=0;foreach ($rajal->result() as $key):$i++; ?>
								<?if ($key->bill != 0): ?>
									<tr>
										<td style="width:100px"><?=$i;?></td>
										<td>(<?=$key->sd_rekmed?>) <?=$key->sd_name?></td>
										<td class="money"><?=int_to_money($key->bill);?></td>
										<?
											$ztotal += $key->bill;
										?>
									</tr>
								<?endif;?>
							<?endforeach;?>
						</tbody>
						<tfoot>
							<td style="text-align:right;font-weight:bold" colspan="2">Total</td>
							<td class="money" style="font-weight:bold"><?=int_to_money($ztotal);?></td>
						</tfoot>
					</table>

					<br>
					<h4>Rawat Inap</h4>
					<table class="table table-bordered table-striped table_tr">
						<thead>
							<th>No</th>
							<th>Tanggal</th>
							<th>Total Transaksi</th>
						</thead>
						<tbody>
							<?$i=0;$ztotal=0;foreach ($ranap->result() as $key):$i++; ?>
								<?if ($key->bill != 0): ?>
									<tr>
										<td style="width:100px"><?=$i;?></td>
										<td>(<?=$key->sd_rekmed?>) <?=$key->sd_name?></td>
										<td class="money"><?=int_to_money($key->bill);?></td>
										<?
											$ztotal += $key->bill;
										?>
									</tr>
								<?endif;?>
							<?endforeach;?>
						</tbody>
						<tfoot>
							<td style="text-align:right;font-weight:bold" colspan="2">Total</td>
							<td class="money" style="font-weight:bold"><?=int_to_money($ztotal);?></td>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?
	function get_month($id){
		$id = (int) substr($id, 5,2);
		return $id-1;
	}

?>

<script type="text/javascript">
	$(function(){
		$('body').on('click','#apply',function (e) {
			e.preventDefault();
			var url = BASE+'laporan/keuangan/hari/'+$("#tgl").val();
			window.location = url;
		});
	})
</script>