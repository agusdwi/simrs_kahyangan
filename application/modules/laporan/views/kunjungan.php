<style type="text/css">
	.sec-h{
		display: none;
	}
</style>
<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox" id="main-ganti">
			<div class="span12" id="ganti">
				<div class="pull-left">
					<b>Pilih Tahun : </b>
					<select id="prov">
						<?for ($i=$this->start;$i<$this->year+1;$i++): ?>
							<option <?=($i == $cur_year)? 'selected="selected"':'';?> value="<?=$i;?>">Tahun - <?=$i;?></option>
						<?endfor;?>
					</select>
				</div>
				<div class="pull-right" style="padding-left:0px;margin-right:40px;">
					<ul class="nav nav-pills">
						<li>
							<a data-section="#table-render" href="#">Table View</a>
						</li>
						<li>
							<a data-section="#chart-render" href="#">Chart View</a>
						</li>
					</ul>
				</div>
				<div class="clear"></div>
				<br><br>
				<table id='table-render' class="table table-bordered sec-h">
					<thead>
						<th width="70px">No</th>
						<th>Bulan</th>
						<th>Jumlah Pasien</th>
					</thead>
					<tbody>
						<?$i=0;foreach ($ds as $key): $i++;?>
							<tr>
								<td><?=$i;?></td>
								<td><?=$key['nama'];?></td>
								<td><?=$key['jum'];?></td>
							</tr>
						<?endforeach;?>
					</tbody>
				</table>
				<div id="chart-render" class='sec-h' style="height:400px">
					chart
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var chart_data = <?=$cur_chart;?>;
	$(function(){
		$('body').on('change','#prov',function (e) {
			e.preventDefault();
			var url = "<?=base_url()?>laporan/kunjungan/index/"+$(this).val();
			var url_json = "<?=base_url()?>laporan/kunjungan/json/"+$(this).val();

			$.getJSON(url_json, function(data) {
				chart_data = data;
				$("#main-ganti").load(url+' #ganti',function(){
					load_setup();
				});
			});
			history.pushState(null, null, url);
		});

		$('body').on('click','.nav-pills a',function (e) {
			e.preventDefault();
			$('.sec-h').hide();
			$($(this).data('section')).fadeIn();
			$('.nav-pills li.active').removeClass('active');
			$(this).parent().addClass('active');
			set_up_chart();
		});
		load_setup();		
	})

	function load_setup(){
		$('.nav-pills li:first a').trigger('click');
	}

	function set_up_chart(){
		chart = new Highcharts.Chart({
			chart: {
				renderTo: 'chart-render',
				defaultSeriesType: 'column'
			},
			title: {
				text: 'Laporan Kunjungan Pasien Tahun <?=$cur_year?>'
			},
			xAxis: {
				categories: [
				'Jumlah Pasien'
				]
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Pasien'
				}
			},
			legend: {
				layout: 'vertical',
				backgroundColor: '#FFFFFF',
				align: 'left',
				verticalAlign: 'top',
				x: 100,
				y: 70,
				floating: true,
				shadow: true
			},
			tooltip: {
				formatter: function() {
					return ''+
					this.x +': '+ this.y +'';
				}
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: chart_data
		});
	}
</script>