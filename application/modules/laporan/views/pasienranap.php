<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox" id="main-ganti">
			<div class="span12" id="ganti" style="padding:10px">
				<div>
					<b>Pilih Tanggal : </b>
					<input type="text" style="width:100px;margin-top:8px" id="tgl" class="datepicker" value="<?=$cur_date;?>">
					<button id="apply" type="button" name="save" class="btn btn-mini btn-primary simpan">Submit</button>
				</div>
				<br>
				<div>
					<?$this->uri->segment(4)<>'' ? $date = $this->uri->segment(4) : $date = $cur_date; ?>
					<table class="table table-bordered table-striped table_tr">
						<thead>
							<th>No</th>
							<th>Nama</th>
							<th>Ruang</th>
							<th>Alamat</th>
						</thead>
						<tbody>
							<?if($ds->num_rows() > 0){ ?>
							<?$i=0;foreach ($ds->result() as $ptn):$i++; ?>
									<tr>
										<td style="width:100px"><?=$i;?></td>
										<td><?=$ptn->sd_name?></td>
										<td><?=$ptn->r_nama?> / <?=$ptn->k_nama;?></td>
										<td><?=$ptn->sd_address?></td>
									</tr>
							<?endforeach;?>
							<? }else{ ?>
									<tr>
										<td>Tidak ada data</td>
									</tr>
							<? } ?>
						</tbody>
					</table>
					<br>
				</div>
			</div>
		</div>
	</div>
</div>

<?
	function get_month($id){
		$id = (int) substr($id, 5,2);
		return $id-1;
	}

?>

<script type="text/javascript">
	$(function(){
		$('body').on('click','#apply',function (e) {
			e.preventDefault();
			var url = BASE+'laporan/pasienrawatinap/index/'+$("#tgl").val();
			window.location = url;
		});
	})
</script>