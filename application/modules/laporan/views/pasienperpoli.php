<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox" id="main-ganti">
			<div class="span12" id="ganti" style="padding:10px">
				<div>
					<b>Pilih Tanggal : </b>
					<input type="text" style="width:100px;margin-top:8px" id="tgl" class="datepicker" value="<?=$cur_date;?>">
					<button id="apply" type="button" name="save" class="btn btn-mini btn-primary simpan">Submit</button>
				</div>
				<br>
				<div>
					<?if(count($poli->result()) > 0){ ?>
					<?foreach ($poli->result() as $pl):?>
					<h4><?=$pl->pl_name?></h4>
					<?$this->uri->segment(4)<>'' ? $date = $this->uri->segment(4) : $date = $cur_date; ?>
					<?$pasien	= $this->mlaporan->get_pasien_per_poli($pl->pl_id,date_to_sql($date));?>
					<span class="pull-right"><? echo count($pasien->result()); ?> pasien</span>
					<table class="table table-bordered table-striped table_tr">
						<thead>
							<th>No</th>
							<th>Nama</th>
							<th>Alamat</th>
						</thead>
						<tbody>
							<?if(count($pasien->result()) > 0){ ?>
							<?$i=0;$ztotal=0;foreach ($pasien->result() as $ptn):$i++; ?>
									<tr>
										<td style="width:100px"><?=$i;?></td>
										<td><?=$ptn->sd_name?></td>
										<td></td>
									</tr>
							<?endforeach;?>
							<? }else{ ?>
									<tr>
										<td>Tidak ada data</td>
									</tr>
							<? } ?>
						</tbody>
					</table>
					<br>
					<?endforeach;?>
					<? } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?
	function get_month($id){
		$id = (int) substr($id, 5,2);
		return $id-1;
	}

?>

<script type="text/javascript">
	$(function(){
		$('body').on('click','#apply',function (e) {
			e.preventDefault();
			var url = BASE+'laporan/pasienperpoli/index/'+$("#tgl").val();
			window.location = url;
		});
	})
</script>