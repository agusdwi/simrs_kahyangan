<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox" id="main-ganti">
			<div class="span12" id="ganti" style="padding:10px">
				<ul class="nav nav-pills">
					<li class="active">
						<a href="<?=base_url('laporan/jumlahpasienperpoli/hari')?>">Harian</a>
					</li>
					<li>
						<a href="<?=base_url('laporan/jumlahpasienperpoli/bulan')?>">Bulanan</a>
					</li>
					<li>
						<a href="<?=base_url('laporan/jumlahpasienperpoli/tahun')?>">Tahunan</a>
					</li>
				</ul>
				<div class="clear"></div>
				<br>
				<div>
					<b>Pilih Tanggal : </b>
					<input type="text" style="width:100px;margin-top:8px" id="tgl" class="datepicker" value="<?=$cur_date;?>">
					<button id="apply" type="button" name="save" class="btn btn-mini btn-primary simpan">Submit</button>
				</div>
				<br>
				<div>
					<?if(count($poli->result()) > 0){ ?>
					<?foreach ($poli->result() as $pl):?>
					<h4><?=$pl->pl_name?></h4>
					<?$this->uri->segment(4)<>'' ? $date = $this->uri->segment(4) : $date = $cur_date; ?>
					<?$pasien	= $this->mlaporan->get_jumlah_pasien_per_poli($pl->pl_id,'hari',date_to_sql($date));?>
					<span class=""><? echo count($pasien->result()); ?> pasien</span>
					<br>
					<?endforeach;?>
					<? } ?>

				</div>
			</div>
		</div>
	</div>
</div>

<?
	function get_month($id){
		$id = (int) substr($id, 5,2);
		return $id-1;
	}

?>

<script type="text/javascript">
	$(function(){
		$('body').on('click','#apply',function (e) {
			e.preventDefault();
			var url = BASE+'laporan/jumlahpasienperpoli/hari/'+$("#tgl").val();
			window.location = url;
		});
	})
</script>