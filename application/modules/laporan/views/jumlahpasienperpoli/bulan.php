
<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox" id="main-ganti">
			<div class="span12" id="ganti" style="padding:10px">
				<ul class="nav nav-pills">
					<li class="">
						<a href="<?=base_url('laporan/jumlahpasienperpoli/hari')?>">Harian</a>
					</li>
					<li class="active">
						<a href="<?=base_url('laporan/jumlahpasienperpoli/bulan')?>">Bulanan</a>
					</li>
					<li>
						<a href="<?=base_url('laporan/jumlahpasienperpoli/tahun')?>">Tahunan</a>
					</li>
				</ul>
				<div class="clear"></div>
				<br>
				<div>
					<b>Pilih Tahun : </b>
					<select id="year" style="width:75px;margin-top:10px">
						<?for ($i=$this->start;$i<$this->year+1;$i++): ?>
							<option <?=($i == $cur_year)? 'selected="selected"':'';?> value="<?=$i;?>"><?=$i;?></option>
						<?endfor;?>
					</select>
					&nbsp;
					<b>Pilih Bulan : </b>
					<select id="month" style="width:150px;margin-top:10px">
						<?$i=0;foreach ($this->bulan as $key): $i++;?>
							<option <?=($i == $cur_month)? 'selected="selected"':'';?> value="<?=$i;?>"><?=$this->bulan[$i-1];?></option>
						<?endforeach;?>
					</select>
					<button id="apply" type="button" name="save" class="btn btn-mini btn-primary simpan">Submit</button>
				</div>
				<div>
					<?if(count($poli->result()) > 0){ ?>
					<?foreach ($poli->result() as $pl):?>
					<h4><?=$pl->pl_name?></h4>
					<?$this->uri->segment(4)<>'' ? $month = $this->uri->segment(4) : $month = $cur_month; ?>
					<?$this->uri->segment(5)<>'' ? $year = $this->uri->segment(5) : $year = $cur_year; ?>
					<?$pasien	= $this->mlaporan->get_jumlah_pasien_per_poli($pl->pl_id,'bulan',$year.'-'.convert_month_digit($month));?>
					<span class=""><? echo count($pasien->result()); ?> pasien</span>
					<br>
					<?endforeach;?>
					<? } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('body').on('click','#apply',function (e) {
			e.preventDefault();
			var url = BASE+'laporan/jumlahpasienperpoli/bulan/'+$("#month").val()+"/"+$("#year").val();
			window.location = url;
		});
	})
</script>