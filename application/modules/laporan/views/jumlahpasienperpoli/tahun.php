<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox" id="main-ganti">
			<div class="span12" id="ganti" style="padding:10px">
				<ul class="nav nav-pills">
					<li class="">
						<a href="<?=base_url('laporan/jumlahpasienperpoli/hari')?>">Harian</a>
					</li>
					<li>
						<a href="<?=base_url('laporan/jumlahpasienperpoli/bulan')?>">Bulanan</a>
					</li>
					<li class="active">
						<a href="<?=base_url('laporan/jumlahpasienperpoli/tahun')?>">Tahunan</a>
					</li>
				</ul>
				<div class="clear"></div>
				<br>
				<div>
					<b>Pilih Tahun : </b>
					<select id="year" style="width:75px;margin-top:10px">
						<?for ($i=$this->start;$i<$this->year+1;$i++): ?>
							<option <?=($i == $cur_year)? 'selected="selected"':'';?> value="<?=$i;?>"><?=$i;?></option>
						<?endfor;?>
					</select>
					<button id="apply" type="button" name="save" class="btn btn-mini btn-primary simpan">Submit</button>
				</div>
				<br>
				<div>
					<?if(count($poli->result()) > 0){ ?>
					<?foreach ($poli->result() as $pl):?>
					<h4><?=$pl->pl_name?></h4>
					<?$this->uri->segment(4)<>'' ? $year = $this->uri->segment(4) : $year = $cur_year; ?>
					<?$pasien	= $this->mlaporan->get_jumlah_pasien_per_poli($pl->pl_id,'tahun',$cur_year);?>
					<span class=""><? echo count($pasien->result()); ?> pasien</span>
					<br>
					<?endforeach;?>
					<? } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?
	function get_month($id){
		$id = (int) substr($id, 5,2);
		return $id-1;
	}

?>

<script type="text/javascript">
	$(function(){
		$('body').on('click','#apply',function (e) {
			e.preventDefault();
			var url = BASE+'laporan/jumlahpasienperpoli/tahun/'+$("#year").val();
			window.location = url;
		});
	})
</script>