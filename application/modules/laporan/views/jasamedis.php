<style type="text/css">
	#dd_dokter_chzn{
		top: 13px !important;
	}
</style>
<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>

<div class="container-fluid">
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox" id="main-ganti">
			<div class="span12" id="ganti" style="padding:10px">
				<div>
					<b>Pilih Tahun : </b>
					<select id="year" style="width:75px;margin-top:10px">
						<?for ($i=$this->start;$i<$this->year+1;$i++): ?>
							<option <?=($i == $cur_year)? 'selected="selected"':'';?> value="<?=$i;?>"><?=$i;?></option>
						<?endfor;?>
					</select>
					&nbsp;
					<b>Pilih Bulan : </b>
					<select id="month" style="width:150px;margin-top:10px">
						<?$i=0;foreach ($this->bulan as $key): $i++;?>
							<option <?=($i == $cur_month)? 'selected="selected"':'';?> value="<?=$i;?>"><?=$this->bulan[$i-1];?></option>
						<?endforeach;?>
					</select>
					<b>Pilih paramedis : </b>
					<?=get_dropdown_dokter('dokter',$cur_ptn);?>
					<button id="apply" type="button" name="save" class="btn btn-mini btn-primary simpan">Submit</button>
				</div>
				<hr>
				<div>
					<h4>Laporan Jasa Medis Rawat Jalan</h4>
					<br>
					<table class="table table-bordered table-striped table_tr">
						<thead>
							<th>No</th>
							<th>Tindakan</th>
							<th>Posisi</th>
							<th>Harga</th>
							<th>Fee</th>
						</thead>
						<tbody>
							<?$i=$ztotal=0;foreach ($rajal as $key => $value): ?>
								<tr>
									<td colspan="5"><b><?=pretty_date($key);?></b></td>
								</tr>
								<?foreach ($value as $v): $i++;?>
									<tr>
										<td><?=$i;?></td>
										<td>(<?=$v->sd_rekmed;?>) <?=$v->sd_name;?><br>
											<b><?=$v->treat_name;?> (<?=$v->treat_code;?>)</b>
										</td>
										<td><?=$v->role_name;?></td>
										<td class="money"><?=int_to_money($v->treat_tarif_harga*$v->jumlah);?></td>
										<td class="money"><?=int_to_money($v->fee);?></td>
										<?$ztotal+=$v->fee?>
									</tr>
								<?endforeach;?>
							<?endforeach;?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4" style="font-weight:bold;text-align:right">Total</td>
								<td class="money" style="font-weight:bold"><?=int_to_money($ztotal);?></td>
							</tr>
						</tfoot>
					</table>			
					<br>
					<h4>Laporan Jasa Medis Rawat Inap</h4>
					<br>
					<table class="table table-bordered table-striped table_tr">
						<thead>
							<th>No</th>
							<th>Tindakan</th>
							<th>Posisi</th>
							<th>Harga</th>
							<th>Fee</th>
						</thead>
						<tbody>
							<?$i=$ztotal=0;foreach ($ranap as $key => $value): ?>
								<tr>
									<td colspan="5"><b><?=pretty_date($key);?></b></td>
								</tr>
								<?foreach ($value as $v): $i++;?>
									<tr>
										<td><?=$i;?></td>
										<td>(<?=$v->sd_rekmed;?>) <?=$v->sd_name;?><br>
											<b><?=$v->treat_name;?> (<?=$v->treat_code;?>)</b>
										</td>
										<td><?=$v->role_name;?></td>
										<td class="money"><?=int_to_money($v->treat_tarif_harga*$v->jumlah);?></td>
										<td class="money"><?=int_to_money($v->fee);?></td>
										<?$ztotal+=$v->fee?>
									</tr>
								<?endforeach;?>
							<?endforeach;?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4" style="font-weight:bold;text-align:right">Total</td>
								<td class="money" style="font-weight:bold"><?=int_to_money($ztotal);?></td>
							</tr>
						</tfoot>
					</table>			
					<br>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('body').on('click','#apply',function (e) {
			e.preventDefault();
			var url = BASE+'laporan/jasamedis/index/'+$("#month").val()+"/"+$("#year").val()+'/'+$("#dd_dokter").val();
			window.location = url;
		});
	})
</script>