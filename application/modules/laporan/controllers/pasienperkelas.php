<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pasienperkelas extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' =>11);
		$this->year = date('Y');
		$this->tgl = date('Y-m-d');
		$this->month = (int) date('m');
		$this->bulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$this->start = 2009;
		$this->load->model('mlaporan');
	}

	function index($tgl='',$tgl2=''){
		$tgl 	= ($tgl == '')? $this->tgl : date_to_sql($tgl);
		$tgl2 	= ($tgl2 == '')? $this->tgl : date_to_sql($tgl2);
		$data['main_view'] 	= 'pasienperkelas';
		$data['title'] 		= "Laporan Pasien Per Kelas: ".pretty_date($tgl)." - ".pretty_date($tgl2);
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 50;
		
		$data['kelas']	= $this->mlaporan->get_all_kelas();
		//$data['pasien']	= $this->mlaporan->get_pasien_per_dokter(108,$tgl);
		//debug_array($data['poli_kebidanan']);
		$data['cur_date']	= format_date_time($tgl,false);
		$data['cur_date2']	= format_date_time($tgl2,false);

		$this ->load->view('template', $data);
	}
}