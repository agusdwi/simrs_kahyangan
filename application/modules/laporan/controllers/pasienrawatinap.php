<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pasienrawatinap extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' =>11);
		$this->year = date('Y');
		$this->tgl = date('Y-m-d');
		$this->month = (int) date('m');
		$this->start = 2009;
		$this->load->model('mlaporan');
	}

	function index($tgl=''){
		$tgl 	= ($tgl == '')? $this->tgl : date_to_sql($tgl);
		$data['main_view'] 	= 'pasienranap';
		$data['title'] 		= "Laporan Pasien Rawat Inap ".pretty_date($tgl);
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 52;
		
		$data['ds']			= $this->mlaporan->get_all_ranap($tgl);
		$data['cur_date']	= format_date_time($tgl,false);

		$this ->load->view('template', $data);
	}
}