<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Jasamedis extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' =>11);
		$this->year = date('Y');
		$this->month = (int) date('m');
		$this->bulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$this->start = 2009;
		$this->load->model('mlaporan');
	}

	function index($id='',$tahun='',$prm=''){
		$id 	= ($id == '')? $this->month : $id;
		$tahun 	= ($tahun == '')? $this->year : $tahun;

		$data['rajal'] = $data['ranap'] = array();

		if(!$prm==""){
			$data['rajal'] = $this->mlaporan->get_jasa_medis_rajal($tahun.'-'.convert_month_digit($id),$prm);
			$data['ranap'] = $this->mlaporan->get_jasa_medis_ranap($tahun.'-'.convert_month_digit($id),$prm);
		}

		$data['state']	='get';

		$data['main_view'] 	= 'jasamedis';
		$data['title'] 		= "Laporan Jasa Medis ";
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 47;

		$data['cur_year']	= $tahun;
		$data['cur_month']	= $id;
		$data['cur_ptn']	= $prm;

		$this ->load->view('template', $data);
	}
}