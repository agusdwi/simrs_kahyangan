<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pasien extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' =>11);
	}

	public function index($id=14) {
		$data['main_view'] 	= 'pasien';
		$data['title'] 		= 'Laporan Jumlah Pasien';
		$data['cf'] 		= $this -> cf;
		$data['current'] 	= 44;

		$data['province']	= $this->db->get('mst_province');
		$data['ds']			= $this->populate_report($id);
		$data['cur_prov']	= $id;
		$data['cur_chart']	= $this->json($id);

		$this ->load->view('template', $data);
	}	

	function populate_report($id){
		$reg = $this->db->get_where('mst_regency',array('mpr_id'=>$id));
		$ds = array();
		foreach ($reg->result() as $key) {
			$ds[] = array(
				'id'	=> $key->mre_id,
				'nama'	=> $key->mre_name,
				'jum'	=> $this->db->get_where('ptn_social_data',array('sd_status'=>1,'sd_reg_kab'=>$key->mre_id))->num_rows(),
			);
		}
		return $ds;
	}

	function json($id){
		$reg = $this->db->get_where('mst_regency',array('mpr_id'=>$id));
		$ds = array();
		foreach ($reg->result() as $key) {
			$ds[] = array(
				'name'	=> $key->mre_name,
				'data'	=> array($this->db->get_where('ptn_social_data',array('sd_status'=>1,'sd_reg_kab'=>$key->mre_id))->num_rows())
			);
		}

		if ($this->input->is_ajax_request()) {
			echo json_encode($ds);
		}else return json_encode($ds);
		
	}
}