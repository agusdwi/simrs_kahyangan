<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Keuangan extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' =>11);
		$this->year = date('Y');
		$this->tgl = date('Y-m-d');
		$this->month = (int) date('m');
		$this->bulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$this->start = 2009;
		$this->load->model('mlaporan');
	}

	function hari($tgl=''){
		$tgl 	= ($tgl == '')? $this->tgl : date_to_sql($tgl);
		$data['main_view'] 	= 'keuangan/hari';
		$data['title'] 		= "Laporan Keuangan Hari ".pretty_date($tgl);
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 46;
		
		$data['rajal'] 		= $this->mlaporan->get_income_rajal('hari',$tgl);
		$data['ranap'] 		= $this->mlaporan->get_income_ranap('hari',$tgl);

		$data['cur_date']	= format_date_time($tgl,false);

		$this ->load->view('template', $data);
	}

	function bulan($id='',$tahun=''){
		$id 	= ($id == '')? $this->month : $id;
		$tahun 	= ($tahun == '')? $this->year : $tahun;
		$data['main_view'] 	= 'keuangan/bulan';
		$data['title'] 		= "Laporan Keuangan Bulan ".$this->bulan[$id-1].", $tahun";
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 46;
		
		$data['rajal'] 		= $this->mlaporan->get_income_rajal('bulan',$tahun.'-'.convert_month_digit($id));
		$data['ranap'] 		= $this->mlaporan->get_income_ranap('bulan',$tahun.'-'.convert_month_digit($id));

		$data['cur_year']	= $tahun;
		$data['cur_month']	= $id;

		$this ->load->view('template', $data);
	}

	function tahun($id=''){
		$tahun = ($id == '')? $this->year : $id;
		$data['main_view'] 	= 'keuangan/tahun';
		$data['title'] 		= "Laporan Keuangan Tahun ".$tahun;
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 46;

		$data['rajal'] 		= $this->mlaporan->get_income_rajal('tahun',$tahun);
		$data['ranap'] 		= $this->mlaporan->get_income_ranap('tahun',$tahun);

		$data['cur_year']	= $tahun;

		$this ->load->view('template', $data);
	}
}