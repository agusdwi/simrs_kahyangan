<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pasienperpoli extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' =>11);
		$this->year = date('Y');
		$this->tgl = date('Y-m-d');
		$this->month = (int) date('m');
		$this->bulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$this->start = 2009;
		$this->load->model('mlaporan');
	}

	function index($tgl=''){
		$tgl 	= ($tgl == '')? $this->tgl : date_to_sql($tgl);
		$data['main_view'] 	= 'pasienperpoli';
		$data['title'] 		= "Laporan Pasien Per Poliklinik ".pretty_date($tgl);
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 48;
		
		$data['poli']	= $this->mlaporan->get_all_poli();
		//$data['poli_kebidanan']	= $this->mlaporan->get_pasien_per_poli(1,$tgl);
		//$data['poli_anak']	= $this->mlaporan->get_pasien_per_poli(2,$tgl);
		//$data['poli_gigi']	= $this->mlaporan->get_pasien_per_poli(3,$tgl);
		//debug_array($data['poli_kebidanan']);
		$data['cur_date']	= format_date_time($tgl,false);

		$this ->load->view('template', $data);
	}
}