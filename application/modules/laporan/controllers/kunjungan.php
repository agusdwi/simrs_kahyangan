<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Kunjungan extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this -> cf = array('modul_id' =>11);
		$this->year = date('Y');
		$this->bulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$this->start = 2009;
	}

	public function index($id="") {
		$id = ($id == '')? $this->year : $id;
		$data['main_view'] 	= 'kunjungan';
		$data['title'] 		= "Laporan Jumlah Kunjungan Tahun $id";
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 45;

		$data['ds']			= $this->populate_report($id);
		$data['cur_year']	= $id;
		$data['cur_chart']	= $this->json($id);

		$this ->load->view('template', $data);
	}	

	function populate_report($id){
		$ds = array();
		for($i=0;$i<12;$i++){
			$j = $i+1;
			$j = ($j<10)? '0'.$j: $j;
			$ds[] = array(
				'nama'	=> $this->bulan[$i],
				'jum'	=> $this->db->like('mdc_in', "$id-$j", 'after')
									->get('trx_medical')->num_rows(),
			);
		}
		return $ds;
	}

	function json($id){
		$ds = array();
		for($i=0;$i<12;$i++){
			$j = $i+1;
			$j = ($j<10)? '0'.$j: $j;
			$ds[] = array(
				'name'	=> $this->bulan[$i],
				'data'	=> array($this->db->like('mdc_in', "$id-$j", 'after')
									->get('trx_medical')->num_rows()),
			);
		}

		if ($this->input->is_ajax_request()) {
			echo json_encode($ds);
		}else return json_encode($ds);
		
	}
}