<script type="text/javascript" charset="utf-8">
	$(function(){
		$('.tabel-dokter').dataTable( {
             
               "sPaginationType": "bootstrap",
               "sScrollY": "300px",
               "bPaginate": false
               
           });
			$('#DataTables_Table_0_filter').hide();
	})
</script>
<style>
    .dataTables_scrollHead{
        margin-bottom: -22px;
    }
    .dataTables_info{
        margin-top: 20px;
    }
</style>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="pageheader notab">
		    <h1 class="pagetitle"><?=$title?></h1>
		</div>
	</div>
	<div class="row-fluid">
        <div class="span5 center" style="text-align: center;">                 
			<ul class="quickstats">
				<li>
					<a href="<?=base_url()?>pelayanan_informasi/informasi_pasien"><img alt="" src="<?=base_url()?>assets/img/icons/32/people.png"><span>informasi<br> pasien</span></a>
				</li>
				<li>
					<a href="<?=base_url()?>pelayanan_informasi/jadwal_dokter"><img alt="" src="<?=base_url()?>assets/img/icons/medicoicons/nurse.png"><span>jadwal<br>dokter</span></a>
				</li>
				<li>
					<a href="<?=base_url()?>pelayanan_informasi/informasi_kamar"><img alt="" src="<?=base_url()?>assets/img/icons/medicoicons/hospital.png"><span>informasi<br>kamar</span></a>
				</li>
				<li>
					<a href="<?=base_url()?>pelayanan_informasi/informasi_paket"><img alt="" src="<?=base_url()?>assets/img/icons/medicoicons/clipboard.png"><span>informasi<br>paket</span></a>
				</li>
			</ul>
        </div>  
		<div class="span7" style="padding:5px 10px">
			<div class="title" style="padding:0px"><h3>Jadwal Dokter Hari ini</h3></div>
			<table cellpadding="0" cellspacing="0" border="0" class="tabel-dokter table table-bordered def_table_y dataTable tb_scrol">

	            <thead>
	                <tr  role="row">
	                    <th class="head0">No.</th>
	                    <th class="head1">Nama Dokter</th>
	                    <th class="head0">Poli</th>
	                    <th class="head1">Jam</th>
	                    <th class="head0">Ruang</th>
	                </tr>
	            </thead>

	            <tbody>
	                <tr>
	                    <td>1</td>
	                    <td>dr. B. Sidarto, Sp.PD</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                    
	                </tr>
	                <tr>
	                    <td>2</td>
	                     <td>dr. Budi Pranowo, L. Sp.PD</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                     
	                </tr>
	                <tr>
	                    <td>3</td>
	                    <td>dr. Doni Priambodo, Sp.PD-KPTI</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                     
	                </tr>
	                <tr>
	                    <td>4</td>
	                    <td>dr. Putut Bayupurnama, Sp.PD, KGEH</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                     
	                </tr>
	                <tr>
	                    <td>5</td>
	                     <td>Prof. dr. Bambang Irawan, Sp.PD. KKV, Sp.JP</td>
	                    <td>Gigi dan Mulut</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>

	                </tr>
	                 <tr>
	                    <td>6</td>
	                    <td>dr. B. Sidarto, Sp.PD</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                    
	                </tr>
	                <tr>
	                    <td>7</td>
	                     <td>dr. Budi Pranowo, L. Sp.PD</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                     
	                </tr>
	                <tr>
	                    <td>8</td>
	                    <td>dr. Doni Priambodo, Sp.PD-KPTI</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                     
	                </tr>
	                <tr>
	                    <td>9</td>
	                    <td>dr. Putut Bayupurnama, Sp.PD, KGEH</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                     
	                </tr>
	                <tr>
	                    <td>10</td>
	                     <td>Prof. dr. Bambang Irawan, Sp.PD. KKV, Sp.JP</td>
	                    <td>Gigi dan Mulut</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>

	                </tr>
	                 <tr>
	                    <td>11</td>
	                    <td>dr. B. Sidarto, Sp.PD</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                    
	                </tr>
	                <tr>
	                    <td>2</td>
	                     <td>dr. Budi Pranowo, L. Sp.PD</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                     
	                </tr>
	                <tr>
	                    <td>3</td>
	                    <td>dr. Doni Priambodo, Sp.PD-KPTI</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                     
	                </tr>
	                <tr>
	                    <td>4</td>
	                    <td>dr. Putut Bayupurnama, Sp.PD, KGEH</td>
	                    <td>Penyakit Dalam</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                     
	                </tr>
	                <tr>
	                    <td>5</td>
	                     <td>Prof. dr. Bambang Irawan, Sp.PD. KKV, Sp.JP</td>
	                    <td>Gigi dan Mulut</td>
	                    <td class="center">12.00 - 20.00</td>
	                    <td class="center">w12</td>
	                </tr>
	            </tbody>
	            <tfoot>
	            </tfoot>
	        </table>
		</div>
    </div>
</div>