<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MedicalModel extends CI_Model { 

	function __construct() {
		parent::__construct();
	}

	function getDokter(){
		$dokter = $this->db->get_where('v_paramedis_jenis',array('jenis_id'=>1));
		foreach ($dokter->result() as $key) {
			$result[] = array(
				'nama'	=> $key->dr_name,
				'ahli'	=> $key->role_name,
				'foto'	=> ""
			);
		}

		return $result;
	}

	function getPoli(){
		$pl = $this->db->get_where('trx_poly',array('pl_status'=>1));
		$result = array();
		foreach ($pl->result() as $key) {
			$result[] = array(
				'id'	=> $key->pl_id,
				'nama'	=> $key->pl_name,
				'img'	=> base_url('files/poli/'.$key->pl_img)
			);
		}
		return $result;
	}

	function getSchedule(){
		$month = DATE('m-Y');
		$month = "04-2014";
		$sch = $this->db->group_by('distday')->order_by('pl_name')->get_where('v_api_schedule',array('month'=>$month));
		$last_time = 0;
		$result = array();
		foreach ($sch->result() as $key) {
			$result[indo_day_int($key->weekday)][] = array(
					"doctor"	=>  $key->dr_name,
					"poli"		=>  $key->pl_name,
					"jam"		=>  $key->hour
				);
			$last_time = ($last_time < $key->insert_time)? $key->insert_time : $last_time;
		}
		if (isset($result['minggu'])) {
			$temp = $result['minggu'];
			unset($result['minggu']);
			$result['minggu'] = $temp;
		}
		
		return (object)array(
			'data'		=> $result,
			'month'		=> $month,
			'last_time'	=> $last_time
		);
	}

	function registerOutpatient($token){
		$ym = date('ym');
		$a = $this->db->like('queo_id',$ym,'after')->order_by('queo_id','DESC')->get('trx_queue_outpatient',1,0)->row();

		$ptn = $this->db->get_where('mmr_api_user',array('api_token'=>$token))->row();
		$queo = count($a) == 0 ?date('ym')."0001" : $a->queo_id+1;
		$no = count($a) == 0 ? 1 : $a->queo_no+1;

		$dr_id	= $this->db->get_where('trx_doctor',array('dr_name'=>$this->input->post('dokter')));
		$dr_id	= ($dr_id->num_rows() == 1)? $dr_id->row()->dr_id : '';
		$pl_id	= $this->db->get_where('trx_poly',array('pl_name'=>$this->input->post('poli')));
		$pl_id	= ($pl_id->num_rows() == 1)? $pl_id->row()->pl_id : die(json_encode(
			array(
				'status'	=>200,
				'success'	=>0,
				'message'	=>'Poli not found',
			)
		));

		//waktu
		$timei 	= $this->input->post('waktu');
		$time 	= $this->db->get_where('trx_dr_schedule',array('sch_time_start'=>$timei,'dr_id'=>$dr_id,'pl_id'=>$pl_id));
		$time  	= ($time->num_rows() == 1)? $time->row()->sch_id : '' ;

		$data = array(
			"queo_id"		=> $queo,
			"sd_rekmed"		=> $ptn->sd_rekmed,
			"queo_no"		=> $no,
			"queo_datetime"	=> $timei,
			"dr_id"			=> $dr_id,
			"pl_id"			=> $pl_id,
			"sch_id"		=> $time
		);
		$this->db->insert('trx_queue_outpatient',$data);
	}

	function getRekmed($token){
		$this->load->model('Mpasien');

		$last_time = $this->input->post('last_time');
		$ds = $this->db->get_where('mmr_api_user',array('api_token'=>$token))->row();
		$sd_rekmed = $ds->sd_rekmed;


		$this->db->where('modi_datetime >', $last_time); 
		$medical = $this->db->order_by('mdc_id','desc')->get_where('v_rekmed_medical',array('sd_rekmed'=>$sd_rekmed));


		$result = array();
		foreach ($medical->result() as $key) {
			$result[] = array(
				'id'				=> $key->mdc_id,
				'date'				=> $key->mdc_in,
				'poly'				=> $key->pl_name,
				'doctor'			=> $key->dr_name,
				'riwayat'			=> array(
										'diagnosa'	=> $this->formaterDiagnosa($this->Mpasien->get_rekmed_diag($key->mdc_id)),
										'obat'		=> $this->formaterObat($key->mdc_id)
					),
				'catatan'			=> $key->note,
				'jadwal_kontrol'	=> $key->date_control
			);

			$last_time = ($last_time < $key->modi_datetime)? $key->modi_datetime : $last_time;
		}

		return (object)array(
				'last_time'	=> $last_time,
				'data'		=> $result
		);
	}

	function formaterDiagnosa($str){
		$rst = "";
		foreach ($str as $key => $value) {
			$rst .= "<b>".$key."</b><br>";
			foreach ($value as $k) {
			 	$rst .= "- $k <br>";
			 }
			 $rst .= "<br>";
		}
		return $rst;

	}

	function formaterObat($id){
		$ds = $this->db->get_where('v_trx_recipe',array('mdc_id'=>$id,'is_delete'=>0));
		$obat = array();
		foreach ($ds->result() as $key) {
			$obat[] = array(
				'nama'		=> $key->is_racik == 1 ? $key->recipe_racik : $key->mdcn_name,
				'jumlah'	=> $key->recipe_qty,
				'aturan'	=> $key->recipe_rule
			);
		}
		return $obat;
	}
}