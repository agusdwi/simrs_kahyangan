<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AuthModel extends CI_Model { 

	function __construct() {
		parent::__construct();
	}

	public function getToken(){		
		list($usec, $sec) = explode(" ",microtime());
		$time = ((float)$usec + (float)$sec);
		$token = base64_encode($time);
		$data = array(
			'time'	=> $time,
			'token'	=> $token
		);
		$this->db->insert('api_token',$data);
		return $token;
	}

	public function authToken($token){
		$ds = $this->db->get_where('api_token',array('token'=>$token));
		if ($ds->num_rows() == 1) {
			$ds = $ds->row();
			list($usec, $sec) = explode(" ",microtime()); 
			$now = ((float)$usec + (float)$sec);
			if (($now - $ds->time) <= 60) {
				$data = array(
					'time'			=> $ds->time,
					'token'			=> $token,
					'acces_time' 	=> $now
				);	
				$this->db->insert('api_token_history',$data);
				$this->db->delete('api_token', array('time' => $ds->time)); 
				return true;
			}else $this->unAuthError();
		}else $this->unAuthError();
	}

	public function authPrivateToken($token){
		$ptn = $this->db->get_where('mmr_api_user',array('api_token'=>$token));
		if ($ptn->num_rows() == 1) {
			$ptn = $ptn->row();
			#jika pasien di blok
			if ($ptn->api_aktif == -1) {
				return false;
			}else{
				return true;
			}
		}else{
			$this->unAuthError();
		}
	}

	private function unAuthError(){
		echo json_encode(array(
				'status'	=> 401,
				'success'	=> 0,
				'message'	=> 'unauthorized access'
			));
		die();
	}
}