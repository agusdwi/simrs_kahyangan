<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PasienModel extends CI_Model { 

	function __construct() {
		parent::__construct();
		$this->url_qrcode = base_url()."qrcode/?data=";
		$this->api_key = "AIzaSyBfeSRh8mglzQL4yj0zQmFEjwyOonVZnrU";
	}

	public function authPasien(){
		// error code
		// 0 => patient not exist
		// 1 => patien already registered, pending aproval
		// 2 => patien already registered, please login
		// 3 => patien already registered, but blocked

		$no_rekmed 	= $this->input->post('no_rekmed');
		$regID 		= $this->input->post('regID');
		$api_out 	= array();

		// cek availibility
		$ptn = $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$no_rekmed));
		if ($ptn->num_rows() == 1) {
			$user = $this->db->get_where('mmr_api_user',array('sd_rekmed'=>$no_rekmed));
			if ($user->num_rows == 0) {
				$ptn 	= $ptn->row();
				$date 	= date("Y-m-d H:i:s");

				$data = array(
					"sd_rekmed"		=> $ptn->sd_rekmed,
					"api_reg_date"	=> $date,
					"regID"			=> $regID
				);

				$this->db->insert('mmr_api_user',$data);

				return (object) array(
					'success'	=> true,
					'message'	=> 'patient register success'
				);

			}else{
				$user = $user->row();
				if($user->api_aktif == "0"){
					return (object) array(
						'success'	=> false,
						'code'		=> 1,
						'message'	=> 'patient already resgitered but pending approval'
					);
				}else if($user->api_aktif == "-1"){
					return (object) array(
						'success'	=> false,
						'code'		=> 3,
						'message'	=> 'patient has been blocked'
					);
				}else{
					return (object) array(
						'success'	=> false,
						'code'		=> 2,
						'message'	=> 'patient has been already registered, please login or forgot password'
					);
				}
			}
		}else {
			return (object) array(
				'success'	=> false,
				'code'		=> 0,
				'message'	=> 'patient not exist'
			);
		}
	}

	public function getPasien($rekmed){
		$ptn_api 	= $this->db->get_where('mmr_api_user',array('sd_rekmed'=>$rekmed))->row();
		$ptn_data 	= $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$ptn_api->sd_rekmed))->row();

		$data = array(
			"no_rekmed"			=> $ptn_data->sd_rekmed,
			"jenis_kelamin"		=> ($ptn_data->sd_sex == 'p' ? 'Perempuan':'Laki-laki'),
			"nama"				=> $ptn_data->sd_name,
			"alamat"			=> $ptn_data->sd_address,
			"qrcode"			=> $this->generateQrcode($ptn_data->sd_rekmed)
			);
		return $data;
	}

	public function authRegister(){
		$no_rekmed	= $this->input->post('no_rekmed');
		$password	= $this->input->post('password');
		$ptn = $this->db->get_where('mmr_api_user',array('sd_rekmed'=>$no_rekmed,'api_aktif'=>1));
		if ($ptn->num_rows() == 1) {
			$ptn = $ptn->row();

			list($usec, $sec) = explode(" ",microtime());
			$time = ((float)$usec + (float)$sec);
			$token = base64_encode($time);

			$data = array('api_password' =>  md5($password),'api_token' =>  $token);
			$this->db->where('api_id', $ptn->api_id);
			$this->db->update('mmr_api_user', $data);
			return array(true,$token);
		}else{
			return array(false);
		}
	}

	public function authLogin(){
		$no_rekmed	= $this->input->post('no_rekmed');
		$password	= $this->input->post('password');
		$regId		= $this->input->post('regId');

		$ptn 		= $this->db->get_where('mmr_api_user',array('sd_rekmed'=>$no_rekmed,'api_aktif'=>1));
		$ptn_data 	= $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$no_rekmed))->row();
		if ($ptn->num_rows() == 1) {
			$ptn = $ptn->row();

			if (($ptn->regID != $regId) && ($ptn->regID != "")) {
				$msg = array(
					"notif_type"=> "logout",
					"message" 	=> "anda telah login di handphone lain"
					);
				$this->send_notification(array($ptn->regID),$msg);	
			}
			
			if (md5($password) == $ptn->api_password) {

				list($usec, $sec) = explode(" ",microtime());
				$time = ((float)$usec + (float)$sec);
				$token = base64_encode($time);

				$data = array('api_token' =>  $token,'regID'=>$regId);
				$this->db->where('sd_rekmed', $no_rekmed);
				$this->db->update('mmr_api_user', $data); 

				return (object) array(
					'success'	=> true,
					'message'	=> 'login successfull',
					'token'		=> $token,
					'qrcode'	=> $this->generateQrcode($no_rekmed),
					'alamat'	=> $ptn_data->sd_address,
					'foto'		=> base_url('files/avatar')."/".$ptn->foto,
					'nama'		=> $ptn_data->sd_name
					);
			}else{
				return (object) array(
					'success'	=> false,
					'message'	=> 'login unsuccessfull, invalid password',
					'code'		=> 1
					);
			}
			
		}else{
			return (object) array(
					'success'	=> false,
					'message'	=> 'login unsuccessfull, invalid rekmed',
					'code'		=> 2
				);
		}
	}

	public function authLogout(){
		$no_rekmed	= $this->input->post('no_rekmed');
		$data = array('api_token' =>  '');
		$this->db->where('sd_rekmed', $no_rekmed);
		$this->db->update('mmr_api_user', $data); 
	}

	function generateQrcode($rekmed){
		$url = $this->url_qrcode.$rekmed;
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = json_decode(curl_exec($ch));

		return base_url().$result->image;
	}

	public function uploadFotoProfil($token){
		$user = $this->db->get_where('mmr_api_user',array('api_token'=>$token))->row();

		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']		= 'files/avatar/';
		$config['allowed_types']	= '*';
		$config['max_size']			= '100000';
		$config['max_width']		= '500000';
		$config['max_height']		= '500000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			return (object) array(
				'success'	=> false,
				'code'		=> 1,
				'message'	=> $this->upload->display_errors()
			);
		}else{
			$a = $this->upload->data();

			$this->load->library('image_moo');
			$this->image_moo
				->load('files/avatar/'.$a['file_name'])
				->resize_crop(300,300)
				->save('files/avatar/'.$a['file_name'],true);

			$data = array(
				'foto' =>  $a['file_name']
			);
			$this->db->where('api_id', $user->api_id);
			$this->db->update('mmr_api_user', $data); 

			return (object) array(
				'success'	=> true,
				'filename'	=> base_url('files/avatar')."/".$a['file_name']
			);
		}

		echo json_encode($api_out);
	}

	public function updateProfil(){
		$token	= $this->input->post('token');

		$id = $this->db->get_where('mmr_api_user',array('api_token'=>$token))->row();

		$data = array(
			'sd_name'		=> $this->input->post('nama'),
			'sd_address'	=> $this->input->post('alamat'),
			'sd_telp'		=> $this->input->post('no_hp')
		);
		$this->db->where('sd_rekmed', $id->sd_rekmed);
		$this->db->update('ptn_social_data', $data); 
		return true;
	}

	public function send_notification($registatoin_ids, $message) {
 
        $url = 'https://android.googleapis.com/gcm/send';
        
        $fields = array(
            'registration_ids' 	=> $registatoin_ids,
            'data' 				=> $message
      	);
        
        $headers = array(
            'Authorization: key=' . $this->api_key,
            'Content-Type: application/json'
      	);
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $url);
        
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        
        curl_close($ch);
        return $result;
    }
}