<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	/*
    * start auth system, check private key
   	*/

	function __construct() {
		parent::__construct();

		$this->load->model('authModel');

		$headers = apache_request_headers();
		if(isset($headers['private-key'])){
			if ($headers['private-key'] != "agusdwi") {
				$this->unauthAccess();
			}
		}else{
			$this->unauthAccess();
		}

	}

	private function unauthAccess(){
		echo json_encode(array(
				'status'	=> 401,
				'success'	=> 0,
				'message'	=> 'bad private key'
			));
		die();
	}

	/*
    * end auth system, check private key
   	*/

	/* ==================================================================== */
   	
   	/*
    * start auth user, check rekmed, & activation
   	*/

	public function token(){
		$token = $this->authModel->getToken();;
		echo json_encode(array(
				'status'	=> 200,
				'success'	=> 1,
				'token'		=> $token
			));
	}

	public function auth(){
		$_token = $this->input->post('_token');
		if ($this->authModel->authToken($_token)) {
			$this->load->model('PasienModel');

			$authPasien = $this->PasienModel->authPasien();

			if ($authPasien->success) {
				echo json_encode(array(
					'status'	=> 201,
					'success'	=> 1,
					'message'	=> $authPasien->message
				));
			}else{
				echo json_encode(array(
					'status'		=> 200,
					'success'		=> 0,
					'error_code'	=> $authPasien->code,
					'message'		=> $authPasien->message
				));
			}
		}
	}

	public function getInitData(){
		$_token = $this->input->post('_token');
		if ($this->authModel->authToken($_token)) {
			$this->load->model('PasienModel');
			$rekmed = $this->input->post('no_rekmed');
			$authPasien = $this->PasienModel->getPasien($rekmed);
			echo json_encode(array(
				'status'		=> 200,
				'success'		=> 1,
				'message'		=> $authPasien
			));
		}
	}

	function register(){
		$_token = $this->input->post('_token');
		if ($this->authModel->authToken($_token)) {

			$this->load->model('PasienModel');
			$authRegister = $this->PasienModel->authRegister();

			if ($authRegister[0]) {
				echo json_encode(array(
					'status'	=> 201,
					'success'	=> 1,
					'message'	=> 'created successfully',
					'token'		=> $authRegister[1]
				));
			}else{
				echo json_encode(array(
					'status'	=> 200,
					'success'	=> 0,
					'message'	=> 'error submit password, probably user account blocked, not active, or duplicate. please contact admin'
				));
			}
		}
	}

	function login(){
		$_token = $this->input->post('_token');
		if ($this->authModel->authToken($_token)) {

			$this->load->model('PasienModel');
			$authLogin = $this->PasienModel->authLogin();

			if ($authLogin->success) {
				echo json_encode(array(
					'status'	=> 200,
					'success'	=> 1,
					'message'	=> 'login successfully',
					'token'		=> $authLogin->token,
					'qrcode'	=> $authLogin->qrcode,
					'alamat'	=> $authLogin->alamat,
					'foto'		=> $authLogin->foto,
					'nama'		=> $authLogin->nama
				));
			}else{
				echo json_encode(array(
					'status'		=> 200,
					'success'		=> 0,
					'error_code'	=> $authLogin->code,
					'message'		=> $authLogin->message
				));
			}

		}
	}

	function logout(){
		$token = $this->input->post('token');
		if ($this->authModel->authPrivateToken($token)) {
			$this->load->model('PasienModel');
			$authLogin = $this->PasienModel->authLogout();			
			echo json_encode(array(
				'status'	=> 200,
				'success'	=> 1
			));
		}
	}

	/*
    * end auth user, check rekmed, & activation
   	*/	

	/* ==================================================================== */

	/*
    * start update profile
   	*/	

	function upload($token){
		if ($this->authModel->authPrivateToken($token)) {
			$this->load->model('PasienModel');
			$uploadFotoProfil = $this->PasienModel->uploadFotoProfil($token);
			if ($uploadFotoProfil->success) {
				echo json_encode(array(
					'status'	=> 200,
					'success'	=> 1,
					'message'	=> 'upload foto profile successfully',
					'filename'	=> $uploadFotoProfil->filename
				));
			}else{
				echo json_encode(array(
					'status'		=> 200,
					'success'		=> 0,
					'error_code'	=> $uploadFotoProfil->code,
					'message'		=> $uploadFotoProfil->message
				));
			}
		}	
	}

	function profil(){
		$token = $this->input->post('token');
		if ($this->authModel->authPrivateToken($token)) {
			$this->load->model('PasienModel');
			$updateProfil = $this->PasienModel->updateProfil();
			if ($updateProfil) {
				echo json_encode(array(
					'status'	=> 200,
					'success'	=> 1,
					'message'	=> 'profil updated successfully',
				));
			}else{
				echo json_encode(array(
					'status'		=> 200,
					'success'		=> 0,
					'message'		=> 'unknown error'
				));
			}
		}	
	}

	/*
    * end update profile
   	*/	

	/* ==================================================================== */

	/*
    * start medical section, rekmed, register poli services, 
   	*/

   	public function rekmed(){
   		$token = $this->input->post('token');
   		if ($this->authModel->authPrivateToken($token)) {
			$this->load->model('MedicalModel');
			$data = $this->MedicalModel->getRekmed($token);

			echo json_encode(array(
					"status"		=> 200,
					"success"		=> 1,
					"last_time"		=> $data->last_time,
					"data"			=> $data->data
				));
   		}
   	}

	public function jadwal($token="",$last_time="0"){
		if ($this->authModel->authPrivateToken($token)) {

			$this->load->model('MedicalModel');
			$data = $this->MedicalModel->getSchedule();

			echo json_encode(array(
				"status"        => 200,
				"success"       => 1,
				"last_time"     => $data->last_time,
				"updated"       => ($last_time < $data->last_time)? 'true' : 'false',
				"month"         => $data->month,
				"data"          => $data->data
			));
		}
   	}

	public function dokter($token=""){
		if ($this->authModel->authPrivateToken($token)) {
			
			$this->load->model('MedicalModel');
			$data = $this->MedicalModel->getDokter();
			echo json_encode(array(
				'status'	=> 200,
				'success'	=> 1,
				'data'		=> $data,
			));	
		}
   	}

	public function daftar_poli(){
		$token = $this->input->post('token');
		if ($this->authModel->authPrivateToken($token)) {

			$this->load->model('MedicalModel');
			$data = $this->MedicalModel->registerOutpatient($token);

			echo json_encode(array(
				"status"	=>  201,
				"success"	=> 1,
				"message"	=>  "data saved successfully"
			));	
		}
	}

	public function get_poli($token=""){
		if ($this->authModel->authPrivateToken($token)) {

			$this->load->model('MedicalModel');
			$data = $this->MedicalModel->getPoli();

			echo json_encode(array(
				'status'	=> 200,
				'success'	=> 1,
				'data'		=> $data,
			));	
		}
	}
}