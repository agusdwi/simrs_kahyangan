<div class="container-fluid">
	<div class="row-fluid">
		<div class="pageheader notab">
		    <h1 class="pagetitle"><?=$title?></h1>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span6" >
			<div class="title"><h3>Kasir Rawat Inap</h3></div>
			<!-- <div class="chatsearch" style="width: 180px;float: right;">
				<input id="q" type="text" name="" placeholder="Search" style="width:135px;margin:auto;">
			</div> -->
		</div>
		<div class="span3" style="float:right;">
			<br>
			<ul class="nav nav-pills" style="float:right;">
				<li class="active">
					<a href="<?=base_url('kasir/rawat_inap/aktif')?>">Tagihan Aktif</a>
				</li>
				<li class="">
					<a href="<?=base_url('kasir/rawat_inap/arsip')?>">Arsip</a>
				</li>
			</ul>
		</div>
	</div>
	<br clear="all">
	<div class="row-fluid">
		<div class="span6">
			<table id="tb-pending" class="table table-bordered custom_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
				<thead>
					<tr role="row">
						<th class="sorting" style="width:15%">No</th>
						<th class="sorting" style="width:15%">Tgl Masuk</th>
						<th class="sorting" style="width:40%">Pasien</th>
						<th class="sorting" style="width:45%">Dokter</th>
						<th class="sorting" style="width:45%">Ruang</th>
					</tr>
				</thead>
				<tbody>
					<?$i=0;foreach ($ds->result() as $k): $i++;?>
						<tr data-id="<?=$k->rnp_id;?>" class="hover">
							<td><?=$i;?></td>
							<td><?=format_date_time($k->rnp_in,false);?></td>
							<td><?=$k->sd_name;?></td>
							<td><?=$k->dr_name;?></td>
							<td><?=$k->r_nama;?>, <?=$k->k_nama;?></td>
						</tr>
					<?endforeach;?>
				</tbody>
			</table>	
			<i>* click to show detail</i>
		</div>
		<div class="span6">
			<b>Detail Transaksi</b>
			<button class="pull-right btn btn-primary btn-mini" id="bayar">bayar & tutup transaksi</button>
			<button class="pull-right btn btn-mini" style="margin-right:5px" id="cetak">cetak</button>
			<hr>
			<iframe data-url="<?=base_url()?>kasir/rawat_inap/detail/" id="iframe" style="width:98.5%;border:1px solid grey;height:400px" src="<?=base_url()?>kasir/rawat_inap/detail"></iframe>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('body').on('click','.hover',function (e) {
			e.preventDefault();
			var id = $(this).data('id');
			$('tr.active').removeClass('active');
			$(this).addClass('active');
			var url = $("#iframe").data('url')+id;
			$("#iframe").attr('src',url);
		});
		$('body').on('click','#cetak',function (e) {
			e.preventDefault();
			document.getElementById("iframe").contentWindow.cetak();
		});
		$('body').on('click','#bayar',function (e) {
			e.preventDefault();
			if($("#tb-pending .active").length == 0 ){
				alert('anda belum memilih transaksi');
			}else{
				var uid = $("#tb-pending .active").closest('tr').data('id');
				var data = {};
				data[CSRF_NAME] = CSRF_HASH;
				data['id']		= uid;
				$.post("<?=base_url('kasir/rawat_inap/tutup')?>", data ,function(){
					alert('transaksi berhasil ditutup');	
					location.reload();
				});
			}
		});
	})
</script>