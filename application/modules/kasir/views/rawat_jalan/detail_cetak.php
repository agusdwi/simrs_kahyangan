<?
	if ($bill->num_rows() == 1) {
		$b = $bill->row();
		$bl_note 		= $b->bl_note;
		$bl_discount 	= $b->bl_discount;
	}else{
		$bl_note 		= "";
		$bl_discount 	= "";
	}
?>
<style type="text/css">
	.table td:first-child{
		border-left: 1px solid #dddddd !important;
	}
</style>
<br>
<center>
	<b>Struk Pembayaran</b><br>
	<b>Rumah Sakit Bersalin Khayangan</b>
</center>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12" style="background:#E5E5E5;">
			<table class="info" style="width:800px">
				<tr>
					<td>
						<table>
							<tr>
								<td><b>Nomer RM</b></td>
								<td><b>: <?=$this->ptn->sd_rekmed;?></b></td>
							</tr>
							<tr>
								<td><b>Nama</b></td>
								<td><b>: <?=$this->ptn->sd_name;?></b></td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<td><b>Jenis Kelamin</b></td>
								<td><b>: <?=strtolower($this->ptn->sd_sex) == 'l'? 'Laki-laki' : 'Perempuan'; ?></b></td>
							</tr>
							<tr>
								<td><b>Umur</b></td>
								<td><b>: <?=get_umur($this->ptn);?> </b></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">			
			<b>Detail Transaksi : </b>
			<table class="table table-bordered table-striped table_tr" style="border-left:none;margin-bottom:0px;">
				<thead>
					<tr>
						<th>No.</th>
						<th>Jenis</th>
						<th>Harga Item</th>
						<th>Qty</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="5" style="text-align:left;font-weight:bold;border-left:0px;">Pembelian Obat</td>
					</tr>
					<?$i=$ztot=0;foreach ($obat as $key): $i++?>
						<tr>
							<td style="border-left:0px;text-align:center;width:20px;"><?=$i;?></td>
							<td>
								<?if ($key->is_racik == 1): ?>
								 	<b>(racik)</b> <?=$key->recipe_racik;?>
								<?else:?>
									<b>(<?=$key->mdcn_code;?>)</b><?=$key->mdcn_name;?>
								<?endif;?> 
							</td>		 	
							<td class="money"> <?=int_to_money($key->harga);?></td>
							<td class="money"> <?=$key->recipe_qty;?></td>
							<td class="money"> 
								<?
									$itot = $key->recipe_qty*$key->harga;
									$ztot+= $itot;
									echo int_to_money($itot);
								?>
							</td>
						</tr>	
					<?endforeach;?>
					<tr>
						<td colspan="5" style="text-align:left;font-weight:bold;border-left:0px;">Tindakan / jasa medis</td>
					</tr>
					<?foreach ($treat->result() as $key): $i++;?>
						<tr>
							<td><?=$i;?></td>
							<td><b>(<?=$key->treat_code;?>)</b> <?=$key->treat_name;?></td>
							<td class="money"><?=int_to_money($key->treat_tarif_harga);?></td>
							<td class="money"><?=$key->jumlah;?></td>
							<td class="money"> 
								<?
									$itot = $key->treat_tarif_harga*$key->jumlah;
									$ztot+= $itot;
									echo int_to_money($itot);
								?>
							</td>
						</tr>
					<?endforeach;?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Total</td>
						<td class="money"><?=int_to_money($ztot);?></td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Diskon</td>
						<td class="money"><?=int_to_money(empty($bl_discount)?0:$bl_discount);?></td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Grand Total</td>
						<td class="money"><?=int_to_money($ztot - $bl_discount);?></td>
					</tr>

				</tfoot>
			</table>
		</div>
	</div>
</div>