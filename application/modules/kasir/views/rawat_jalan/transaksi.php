<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" style="background:#E5E5E5;">
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Nomor RM</b>
                    </div>
                    <?php $sd_rekmed = $patient->sd_rekmed;?>
                    <div style="float:left;margin-left:10px;">
                        <b>: <?=$patient->sd_rekmed?></b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Nama</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: <?=$patient->sd_name?></b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Jenis Kelamin</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: <?=strtolower($patient->sd_sex) == 'l'? 'laki-laki' : 'perempuan' ?></b>
                    </div>
                </div>
            </div>

            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Umur</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: <?=$patient->sd_age?> Tahun</b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Gol. Darah</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: <?=$patient->sd_blood_tp?></b>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='row-fluid' style="padding:10px">
    	<div class="widget-content nopadding">
	    	<div class="span7">
				<style>
					td.curr{
						text-align:right;
					}

					.table#table_transaksi tr.active{
						background-color: #F8D995 !important;
					}

					.table#table_transaksi tr:even{
						background-color: #f60;
					}

					.table-striped tbody tr:nth-child(2n+1) td, .table-striped tbody tr:nth-child(2n+1) th{
						background-color: transparent;
					}
					

				</style>
				<script>
					$(function(){
						/*$('.dyntable').dataTable( {
							//"sPaginationType": "bootstrap",
							"sScrollY": "200px",
							  "bFilter": false,
							"bPaginate": false,
					    });*/
					
					$('table#table_transaksi tr').click(function(){
						var rm = $(this).find('.rm').val();
						$(".table#table_transaksi tr.active").removeClass('active');
						var url = '<?=cur_url()?>../../get_detail_transaksi/'+rm;
						//$.get(url);
						$(".detail_transaksi").load(url);
						$(this).addClass('active');
					})

					$("table").click(function(){
					})

					$('.rm').parent().hide();
					})

				</script>
				<style>
					table#table_transaksi td,th{
						border: 1px solid #DDDDDD;
					}
				</style>
				<?=form_open(base_url().'kasir/rawat_jalan/create',array('class' => 'form-horizontal','id' => 'form')); ?>
				<table id='table_transaksi' class="table dyntable">
					<thead>
						<tr>
							<th>Kode</th>
							<th>Rekening</th>
							<th>Total</th>
							<th>Tanggungan</th>
						</tr>
					</thead>
					<tbody>
						<!--<?$total_amount = 0?>
						<?$total_sum = 0?>
						<?php $i=1; foreach($bill as $bill):?>
							<tr>
							<td><input type="hidden" class="rm" value="<?=$bill->medic."/".$bill->bill?>"></td>
							<td><?=$i++?></td>
							<td><?=$bill->nama?></td>
							<?php $total_amount += $bill->total?>
							<td class="curr"><?=$bill->total?></td>
							<?php $total_sum += $bill->sum?>
							<td class="curr"><?=$bill->sum?></td>
						</tr>
						<?endforeach?>-->
						<?=$bill_detail?>
					</tbody>
				</table>

				<!-- <div class='detail_transaksi'>
				<h3>Detil</h3>
				<table  class="table table-bordered table-striped dyntable">
					<thead>
						<tr>
							<th>Tanggal</th>
							<th>Keterangan</th>
							<th>Kelas</th>
							<th>Harga</th>
							<th>Jml</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td><td></td><td></td><td></td><td></td>
						</tr>
						<tr>
							<td></td><td></td><td></td><td></td><td></td>
						</tr>
						<tr>
							<td></td><td></td><td></td><td></td><td></td>
						</tr>
						<tr>
							<td></td><td></td><td></td><td></td><td></td>
						</tr>
						<tr>
							<td></td><td></td><td></td><td></td><td></td>
						</tr>
						<tr>
							<td></td><td></td><td></td><td></td><td></td>
						</tr>
					</tbody>
				</table>
			</div> -->
		</div>
			<div class="span5" style="margin-left:1%">
				Catatan Rekening<br/>
				<textarea rows=5 style="width:95%;" name="note"></textarea>
				Asuransi<br/>
				Jenis Asuransi 
				<select style="max-width:120px" name="ins_id">
					<option value="0"> - Tanpa Asuransi - </option>
					<?foreach($insurance as $ins):?>//
						<option value="<?=$ins->ins_id?>"><?=$ins->ins_name?></option>
					<?endforeach;?>
				</select>
				<input type="text" name="ins_no" />
				<br/>
				<style>
					#harga td{
						vertical-align: middle;padding:4px;
					}
					#harga input{
						margin:0px;
						width:150px;
					}
				</style>
				<input type="hidden" name="mdc_id" value="<?=$medical_id?>">
				<input type="hidden" name="sd_rekmed" value="<?=$sd_rekmed?>">
				<style>
					.money{
						text-align: right;
					}
				</style>
				<script type="text/javascript">
					$(function(){
						$('#patient_pay').blur(function(){
							bayar = parseFloat($(this).val());
							$(this).val(formatCurrency(bayar));
							must_pay = parseFloat($('#total_pay').val().replace(',',''));
							$('#kurang_bayar').val(formatCurrency(must_pay-bayar))
						})
					})
					function formatCurrency(num) {
						num = num.toString().replace(/\$|\,/g, '');
						if (isNaN(num)) num = "0";
						sign = (num == (num = Math.abs(num)));
						num = Math.floor(num * 100 + 0.50000000001);
						cents = num % 100;
						num = Math.floor(num / 100).toString();
						if (cents < 10) cents = "0" + cents;
						for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
						num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
						return (((sign) ? '' : '-') + num + '.' + cents);
					}
				</script>
				<table id="harga">
					<tr>
						<td>Total</td><td><input type="text" class="money" name="total_amount" readonly="readonly" value="<?=number_format($total,2)?>" /></td>
						<td>Tertanggung</td><td><input type="text" class="money" id="total_pay" name="total_pay" val="<?=$tertanggung?>" readonly="readonly" value="<?=number_format($tertanggung,2)?>" /></td>
					</tr>
					<tr>
						<td>Dokter</td><td><input type="text" class="money" name="doctor_fee" readonly="readonly" value="<?=number_format(0,2)?>" /></td>
						<?php $dis = $tertanggung-$total_sum == 0? 'disabled="disabled"' : '' ;?>
						<td>Membayar</td><td><input type="text" class="money" name="patient_pay" id="patient_pay" <?=$dis?> value="<?=number_format(0,2)?>" /></td>
					</tr>
					<tr>
						<td></td><td></td>
						<td>Kurang Bayar</td><td><input type="text" class="money" id="kurang_bayar" readonly="readonly" value="<?=number_format($total_sum,2)?>" /></td>
					</tr>
				</table>
				<div style="text-align:right;margin-top:20px">
					<button class="btn btn-success">Simpan</button>
					<a href="#myModal" id="cetak" role="button" class="btn hide" data-toggle="modal">Launch demo modal</a> 
				</div>
			</div>
			<?=form_close()?>
		</div>
    </div>
</div>


<!-- Modal -->
<div id="myModal" style="height:500px;width:700px" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Cetak Transaksi</h3>
  </div>
  <div class="modal-body" style="height:600px;padding:10px 0px 10px 10px">
    <iframe src="" name="ifr" id="ifr" style="margin:0px;padding:0px;border:none;width:100%;height:90%"></iframe>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Keluar</button>
    <button id="cetakIframe" class="btn btn-primary">Cetak</button>
  </div>
</div>

<script type="text/javascript">
	$("#form").submit(function(){
	    var url  = $(this).attr('action');
	    var data = $(this).serialize();
	    $.post(url,data, function(data){
	        $("#cetak").trigger('click');
			$("#ifr").attr('src',data);
	    }); 
	    return false;
	})

	$("#cetakIframe").click(function(){
	    ifr.print();
	    window.location = "<?=base_url()?>kasir/rawat_jalan/select/transaction_id";
	})
</script>