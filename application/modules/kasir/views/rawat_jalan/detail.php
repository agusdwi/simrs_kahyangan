<?
	if ($bill->num_rows() == 1) {
		$b = $bill->row();
		$bl_note 		= $b->bl_note;
		$bl_discount 	= $b->bl_discount;
	}else{
		$bl_note 		= "";
		$bl_discount 	= "";
	}
?>
<style type="text/css">
	.table td:first-child{
		border-left: 1px solid #dddddd !important;
	}
</style>
<?=$this->load->view('a_info');?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">			
			<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all;" style="background-color:white;padding-left:0px;margin-left:0px;" >
				<?=form_open('',array('id' => 'form_bill')); ?>
					<div style="padding:0px 10px 0px 10px">
						<b>Detail Transaksi : </b>
						<table class="table table-bordered table-striped table_tr" style="border-left:none;margin-bottom:0px;">
							<thead>
								<tr>
									<th>No.</th>
									<th>Jenis</th>
									<th>Harga Item</th>
									<th>Qty</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="5" style="text-align:left;font-weight:bold;border-left:0px;">Pembelian Obat</td>
								</tr>
								<?$i=$ztot=0;foreach ($obat as $key): $i++?>
									<tr>
										<td style="border-left:0px;text-align:center;width:20px;"><?=$i;?></td>
										<td>
											<?if ($key->is_racik == 1): ?>
											 	<b>(racik)</b> <?=$key->recipe_racik;?>
											<?else:?>
												<b>(<?=$key->mdcn_code;?>)</b><?=$key->mdcn_name;?>
											<?endif;?> 
										</td>		 	
										<td class="money"> <?=int_to_money($key->harga);?></td>
										<td class="money"> <?=$key->recipe_qty;?></td>
										<td class="money"> 
											<?
												$itot = $key->recipe_qty*$key->harga;
												$ztot+= $itot;
												echo int_to_money($itot);
											?>
										</td>
									</tr>	
								<?endforeach;?>
								<tr>
									<td colspan="5" style="text-align:left;font-weight:bold;border-left:0px;">Tindakan / jasa medis</td>
								</tr>
								<?foreach ($treat->result() as $key): $i++;?>
									<tr>
										<td><?=$i;?></td>
										<td><b>(<?=$key->treat_code;?>)</b> <?=$key->treat_name;?></td>
										<td class="money"><?=int_to_money($key->treat_tarif_harga);?></td>
										<td class="money"><?=$key->jumlah;?></td>
										<td class="money"> 
											<?
												$itot = $key->treat_tarif_harga*$key->jumlah;
												$ztot+= $itot;
												echo int_to_money($itot);
											?>
										</td>
									</tr>
								<?endforeach;?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Total</td>
									<td class="money"><?=int_to_money($ztot);?></td>
								</tr>
								<tr>
									<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Diskon</td>
									<td class="money"><?=int_to_money(empty($bl_discount)?0:$bl_discount);?></td>
								</tr>
								<tr>
									<td colspan="4" style="text-align:right;font-weight:bold;border-left:0px;">Grand Total</td>
									<td class="money"><?=int_to_money($ztot - $bl_discount);?></td>
								</tr>

							</tfoot>
						</table>
						<div id="dform" class="well" style="width:365px;margin-left:10px;margin-top:10px;padding:10px">
							<input type="hidden" name="bill[bl_total]" value="<?=$ztot;?>">
							<b>Catatan</b>
							<textarea style="margin-bottom:0px;width:350px" rows="2" name="bill[bl_note]" placeholder="catatan pembayaran" ><?=$bl_note;?></textarea><br>
							<br>
							<p><b>Diskon / potongan harga</b> Rp.<input type="text" class="tx_small" name="bill[bl_discount]" value="<?=$bl_discount;?>"></p>
						</div>
					</div>
					<div class="form-actions" style="margin-top: 10px;margin-bottom: 0px;">
						<a href="<?=cur_url(-2)?>" name="savenext" class="btn btn-danger">kembali</a>

						<button type="button" id="selesai" name="savenext" style="float:right;margin-left:5px" class="btn btn-success">Selesai</button>
						<button type="submit" name="savenext" style="float:right;margin-left:5px" class="btn btn-info">Simpan &amp; Cetak Struk</button>
						
						<div class="clear"></div>
					</div>
				<?=form_close()?>
			</div>
		</div>
	</div>
</div>

<a href="#myModal" id="cetak" role="button" class="btn hide" data-toggle="modal">Launch demo modal</a> 
<div id="myModal" style="height:500px;width:700px" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Cetak Transaksi</h3>
	</div>
	<div class="modal-body" style="height:600px;padding:10px 0px 10px 10px">
		<iframe src="" name="ifr" id="ifr" style="margin:0px;padding:0px;border:none;width:100%;height:90%"></iframe>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Keluar</button>
		<button id="cetakIframe" class="btn btn-primary">Cetak</button>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("#form_bill").submit(function(e){
			e.preventDefault();
			var data = $(this).serialize();
			$.post('',data, function(data){
				$("#ifr").attr('src',data);
				$("#cetak").trigger('click');
			}); 
		})
		$("#cetakIframe").click(function(){
			ifr.print();
		})
		$("#selesai").click(function(e){
			e.preventDefault();
			var data = $("#form_bill").serialize();
			$.post('',data, function(data){
				window.location = "<?=cur_url(-2)?>";
			}); 
		})
	})
</script>