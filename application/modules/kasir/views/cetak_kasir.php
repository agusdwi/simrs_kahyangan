<table width="100%;margin-top:20px">
	<tr>
		<td style="width:60%">&nbsp;</td>
		<td style="width:40%">&nbsp;</td>
	</tr>
	<tr>
		<td>No.RM : <b><?=$patient->sd_rekmed?></b></td>
		<td>Umur : <?=$patient->sd_age?> Thn</td>
	</tr>
	<tr>
		<td>Nama Pasien : <?=$patient->sd_name?></td>
		<td>Gol.Darah : <?=$patient->sd_blood_tp?></td>
	</tr>
	<tr>
		<td>Alamat Pasien : <?=$patient->sd_address?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<table class="table table-bordered def_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
	<thead>
		<tr>
			<!-- <th>No</th>
			<th>Nama</th>
			<th>Kemasan</th>
			<th>Jumlah</th>
			<th>Harga</th> -->
			<th>No</th>
			<th>Nama</th>
			<th>Total</th>
			<th>Tertanggung</th>
		</tr>
	</thead>
		<tbody>
			<?foreach($detail as $i => $v):?>
				<tr>
					<td><?=$i+1?></td>
					<td><?=$v->nama?></td>
					<td class="curr"><?=number_format($v->total)?></td>
					<td class="curr"><?=number_format($v->sum)?></td>
				</tr>	
			<?endforeach;?>
			<!-- <tr>
				<td>1</td				<td>Amoxsan 60 Dry Syr.</td>
				<td>Botol</td>
				<td class="curr">2</td>
				<td class="curr"><?=number_format(25162)?></td>-->
		</tbody>
		<tr>
			<td colspan=2>TOTAL</td><td><?=$total->total_amount?></td><td> <?=$total->total_pay?></td>
		</tr>
		<tr>
			<td colspan=3>Terbayar</td><td><?=$total->patient_pay?></td>
		</tr>
		<tr>
			<td colspan=3>Kurang Bayar</td><td><?=$total->total_pay-$total->patient_pay?></td>
		</tr> 
</table>

<table width="100%;margin-top:20px" text-align="right">
	<tr>
		<td style="width:60%"><textarea style="width:100%"><?=$total->note?></textarea></td>
		<td style="width:40%">&nbsp;</td>
	</tr>
</table>