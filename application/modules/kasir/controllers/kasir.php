<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kasir extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 4
			);
	}

	public function index(){
		$data['main_view']	= 'index';
		$data['title']		= 'Kasir ';
		$cf					= $this->cf;
		$data['cf']			=  $cf;
		
		 $this->load->view('template',$data);
	}
}


