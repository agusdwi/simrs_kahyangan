<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rawat_jalan extends CI_Controller {
	// bill status
	// 2 => open bill
	// 3 => lunas
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 4
		);
		$this->load->model('Mpasien');
	}

	public function index(){
		$data['main_view']	= 'rawat_jalan/index';
		$data['title']		= 'Kasir Rawat Jalan';
		$data['desc']		= 'Description. ';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}

	function select($id,$act=""){
		if ($this->input->is_ajax_request()) {
			$input = $this->input->post();
			$this->db->where('mdc_id', $id);
			$this->db->update('trx_bill', $input['bill']); 

			$data = array('mdc_status' =>  3);
			$this->db->where('mdc_id', $id);
			$this->db->update('trx_medical', $data);
			echo cur_url().'cetak';
		}else{
			$this->mdc 		= $this->db->get_where('trx_medical',array('mdc_id'=>$id))->row();
			$this->ptn 		= $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$this->mdc->sd_rekmed))->row();

			$data['cf']			=  $this->cf;
			$data['title']		= 'Kasir Rawat Jalan';
			$data['treat']		= $this->db->get_where('v_treat_param_harga',array('mdc_id'=>$id,'cls_id'=>$this->mdc->tp_insurance));
			$data['obat']		= $this->Mpasien->get_harga_obat($id,$this->mdc->tp_insurance);
			$data['bill']		= $this->db->get_where('trx_bill',array('mdc_id'=>$id));

			if ($act=="cetak") {
				$data['main_view']	= 'rawat_jalan/detail_cetak';
				$this->load->view('template_print',$data);			
			}else{
				$data['main_view']	= 'rawat_jalan/detail';
				$this->load->view('template',$data);			
			}
		}
	}

	function cetak($mdc_id){
		$data['main_view']	= 'kasir/cetak_kasir';
		$data['patient']	= $this->db->where('sd_rekmed',$sd_rekmed)->get('ptn_social_data')->row();
		$data['detail']		= $this->db->where('medic',$mdc_id)->get('v_trx_bill4')->result();
		$data['total']		= $this->db->where('mdc_id',$mdc_id)->get('trx_medical')->row();
		$this->load->view('template_print',$data);
	}

	function get_medical($id = 2){
		$config['sTable'] 			= 'v_medical_bill';
		$config['aColumns'] 		= array('day_date','sd_name','pl_name');
		$config['key'] 				= 'mdc_id';
		$config['where'][]			= ("mdc_status = '$id'");
		$config['php_format'] 		= array('','','','money','money');
		$config['searchColumn'] 	= array('day_date','sd_name');
		$config['aksi'] = array(
								'stat'  	=> true,
								'key'		=> 'mdc_id',
								'pilih'	=> base_url().'kasir/rawat_jalan/select/',
								);
		init_datatable($config);
	}
}