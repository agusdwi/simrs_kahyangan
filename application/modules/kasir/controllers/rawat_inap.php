<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rawat_inap extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 4
			);
		$this->load->model('Mpasien');
	}

	private function tdata($id,&$data){
		$this->rnp 		= $this->db->get_where('v_rnp_aktif',array('rnp_id'=>$id))->row();
		$this->ptn 		= $this->db->get_where('ptn_social_data',array('sd_rekmed'=>$this->rnp->sd_rekmed))->row();
	}

	public function index(){
		redirect('kasir/rawat_inap/aktif');
	}

	function aktif(){
		$data['main_view']	= 'rawat_inap/index';
		$data['title']		= 'Kasir Rawat Inap ';
		$data['desc']		= 'Description. ';
		$data['cf']			=  $this->cf;
		$data['ds']			=  $this->db->get_where('v_rnp_medical',array('rnp_status'=>2));
		$this->load->view('template',$data);
	}

	function arsip($date=''){
		if($date=='')
			redirect(base_url('kasir/rawat_inap/arsip/'.DATE('d-m-Y')));
		$data['main_view']	= 'rawat_inap/arsip';
		$data['title']		= 'Kasir Rawat Inap Arsip ';
		$data['desc']		= 'Description. ';
		$data['cf']			=  $this->cf;
		$data['ds']			=  $this->db->get_where('v_rnp_medical',array('rnp_status'=>3,'dtin'=>format_date_time($date,false)));
		$this->load->view('template',$data);
	}

	function detail($id=""){
		if ($id=="") {
			die('<br><br><center><h1>Silahkan pilih pasien disebelah kiri</h1></center>');
		}
		$this->tdata($id,$data);
		$data['ds']			= $this->Mpasien->get_total_harga_ranap($id);
		$data['bill']		= $this->db->get_where('rnp_bill',array('mdc_id'=>$id));
		$data['main_view']	= 'rawat_inap/detail';		
		$this->load->view('iframe',$data);
	}

	function tutup(){
		$id = $this->input->post('id');
		$data = array('rnp_status' =>  3);
		$this->db->where('rnp_id', $id);
		$this->db->update('rnp_medical', $data); 
	}

}