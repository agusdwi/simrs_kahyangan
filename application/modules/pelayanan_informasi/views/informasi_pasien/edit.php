<script type="text/javascript" src='<?=base_url()?>assets/js/jquery.chosen.js'></script> 
<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span5" >
			<div class="title"><h3>Edit Pasien </h3></div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span6">
				<input type="hidden" class='crsf' name="<?php echo $this->security->get_csrf_token_name()?>" value="<?php echo $this->security->get_csrf_hash()?>" >
				<?=form_open(base_url().'pelayanan_informasi/informasi_pasien/save/'.$pasien->sd_rekmed,array('class' => 'form-horizontal frm_search stdform','id' => 'form')); ?>
                                <label class="control-label">Nomor RM</label>
                                <div class="controls">
                                	<?php
                                		$rekmed=explode('-', $pasien->sd_rekmed);
                                	?>
                                    <input id="rm_1" name="rm_1" class="pull-left" readonly="readonly" style="width:35px" type="text" value="<?=$rekmed[0];?>"> 
                                    <input id="rm_2" name="rm_2" class="pull-left" readonly="readonly" style="width:80px;margin-left:5px" type="text" value="<?=$rekmed[1];?>"> 
                                    <? $rm_child = array("a","b","c","d","e","f","g","h","i","j");?>
                                    <select id="rm_3" name="rm_3" style="width:40px;margin-left:5px" class="pull-left">
                                        <option>-</option>
                                        <?foreach ($rm_child as $key => $value): ?>
                                            <option><?=$value;?></option>
                                        <?endforeach ?>
                                    </select>
                                    <div class="clearfix"></div>
                                </div>
                                <input class="small" type="text" style="display:none" id="rm"  value="">
                                <label class="control-label">Nama Pasien</label>
                                <div class="controls">
                                    <input type="text" style="width: 255px" placeholder="Nama Pasien" id="sd_name" name="sd_name" autofocus value="<?=$pasien->sd_name?>">
                                </div>
                                <label class="control-label">Nama Keluarga</label>
                                <div class="controls">
                                    <? $klg_jenis = array("Suami","Istri","Bapak","Ibu","Anak","Saudara","Orang Lain");?>
                                    <select name="sd_keluarga_jenis" class="pull-left" style="width:100px">
                                        <?foreach ($klg_jenis as $key => $value){
                                        	if($value==$pasien->sd_keluarga_jenis){
                                        		?>
                                        		<option value="<?=$value;?>" selected><?=$value;?></option>
                                        		<?php
                                        	}else{
                                        		?>
                                        		<option value="<?=$value;?>"><?=$value;?></option>
                                        		<?php
                                        	}
                                        	?>
                                        <?}?>
                                    </select>&nbsp;&nbsp;
                                    <input type="text" style="width:150px"  placeholder="Nama Keluarga" id="sd_keluarga_nama" name="sd_keluarga_nama" value="<?=$pasien->sd_keluarga_nama?>">
                                </div>
                                <label class="control-label">Jenis Kelamin</label>
                                <div class="controls">
                                    <table>
                                        <tr>
                                        	<?php
                                        	if($pasien->sd_sex=='P'){
                                        		?>
                                        		<td style="width:100px"><label><input type="radio" name="sd_sex" value="P" checked="checked"/> Perempuan</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_sex" value="L"/> Laki-laki</label></td>
                                        		<?php
                                        	}else{
                                        		?>
                                        		<td style="width:100px"><label><input type="radio" name="sd_sex" value="P"/> Perempuan</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_sex" value="L" checked="checked"/> Laki-laki</label></td>
                                        		<?php
                                        	}
                                        	?>
                                            
                                        </tr>
                                    </table>
                                </div>
                                <label class="control-label">Tempat Lahir</label>
                                <div class="controls">
                                	<select name="sd_place_of_birth" id="tempat_lahir" class="chzn-select" style="width: 270px">
                            			<?php
                            			foreach($city_provide->result() as $city){
                            				if($city->mre_name==$pasien->sd_place_of_birth){
                            					?>
                            						<option value="<?=$city->mre_name?>" selected><?=$city->mre_name?></option>
                            					<?php
                            				}else{
                            					?>
                            					<option value="<?=$city->mre_name?>"><?=$city->mre_name?></option>
                            					<?php
                            				}
                            			}
                            			?>
                                	</select>
                                </div>
                                <label class="control-label">Tgl Lahir</label>
                                <div class="controls">
                                	<?php
                                		$tgllahir=explode('-', $pasien->sd_date_of_birth);
                                	?>
                                    <select  name="tgl0" style="min-width:30px;width:90px" style="float:left" id="tgl" >
                                        <?php
                                        	for($i=1;$i<=31;$i++){
                                        		if($i<10){
                                        			$i='0'.$i;
													if($i==$tgllahir[2]){
                                        			?>
                                        			<option value="<?=$i?>" selected ><?=$i?></option>
                                        			<?php
                                        		}else{
                                        			?>
                                        			<option value="<?=$i?>" ><?=$i?></option>
                                        			<?php
                                        		}
                                        		}else{
                                        			if($i==$tgllahir[2]){
                                        			?>
                                        			<option value="<?=$i?>" selected ><?=$i?></option>
                                        			<?php
                                        		}else{
                                        			?>
                                        			<option value="<?=$i?>" ><?=$i?></option>
                                        			<?php
                                        		}
                                        		}	
                                        	}
                                        ?>
                                    </select>
                                    <select name="tgl1" style="width:90px" id="bln">
                                        <?php
                                        $bulan=array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
                                        	for($i=1;$i<=12;$i++){
                                        		if($i<10){
                                        			$j='0'.$i;
													if($i==$tgllahir[1]){
                                        			?>
                                        			<option value="<?=$j?>" selected ><?=$bulan[$i]?></option>
                                        			<?php
                                        		}else{
                                        			?>
                                        			<option value="<?=$j?>" ><?=$bulan[$i]?></option>
                                        			<?php
                                        		}	
                                        		}else{
                                        			if($i==$tgllahir[1]){
                                        			?>
                                        			<option value="<?=$i?>" selected ><?=$bulan[$i]?></option>
                                        			<?php
                                        		}else{
                                        			?>
                                        			<option value="<?=$i?>" ><?=$bulan[$i]?></option>
                                        			<?php
                                        		}	
                                        		}	
                                        		
                                        	}
                                        ?>
                                    </select>
                                    <select name="tgl2" style="width:90px" id="thn">
                                        <?php
                                        	for($i=date('Y');$i>=1950;$i--){
                                        		if($i==$tgllahir[0]){
                                        			?>
                                        			<option value="<?=$i?>" selected ><?=$i?></option>
                                        			<?php
                                        		}else{
                                        			?>
                                        			<option value="<?=$i?>" ><?=$i?></option>
                                        			<?php
                                        		}
                                        	}
                                        ?>
                                    </select>
                                    <label for="tgl" generated="true" class="error"></label>
                                </div>
                                <label class="control-label">Umur</label>
                                <div class="controls">
                                    <input type="text" style="width:40px" id="umur" name="sd_age" placeholder="0" value="<?=$pasien->sd_age?>"> Tahun
                                </div>
                                <label class="control-label">Gol.Darah</label>
                                <div class="controls">
                                	<?php
                                		if($pasien->sd_blood_tp=='A'){
                                			?>
                                	<table>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="-" /> -</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="A" checked="checked" /> A</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="B" /> B</label></td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="AB" /> AB</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="O" /> O</label></td>
                                        </tr>
                                    </table>		
                                			<?php
                                		}elseif($pasien->sd_blood_tp=='B'){
                                			?>
                                	<table>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="-" /> -</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="A" /> A</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="B" checked="checked" /> B</label></td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="AB" /> AB</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="O" /> O</label></td>
                                        </tr>
                                    </table>		
                                			<?php
                                		}elseif($pasien->sd_blood_tp=='AB'){
                                			?>
                                	<table>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="-" /> -</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="A" /> A</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="B" /> B</label></td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="AB" checked="checked" /> AB</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="O" /> O</label></td>
                                        </tr>
                                    </table>
                                			<?php
                                		}elseif($pasien->sd_blood_tp=='0'){
                                			?>
                                		<table>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="-" /> -</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="A" /> A</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="B" /> B</label></td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="AB" /> AB</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="O" checked="checked" /> O</label></td>
                                        </tr>
                                    </table>	
                                			<?php
                                		}else{
                                			?>
                                		<table>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="-" checked="checked" /> -</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="A" /> A</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="B" /> B</label></td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="AB" /> AB</label></td>
                                            <td style="width:100px"><label><input type="radio" name="sd_blood_tp" value="O" /> O</label></td>
                                        </tr>
                                    </table>	
                                			<?php
                                		}
                                	?>
                                </div>
                                <label class="control-label">Alergi</label>
                                <div class="controls">
                                    <select name="sd_alergi" id="alergi" class="chzn-select" style="width: 270px">
                                    	<option value="">Tidak ada alergi</option>
                                    	<?php
                                    	foreach($alergi_provide->result() as $alergi){
                                    		if($alergi->alg_name==$pasien->sd_alergi){
                            					?>
                            						<option value="<?=$alergi->alg_name?>" selected><?=$alergi->alg_name?></option>
                            					<?php
                            				}else{
                            					?>
                            					<option value="<?=$alergi->alg_name?>"><?=$alergi->alg_name?></option>
                            					<?php
                            				}
                                    	}
                                    	?>
                                    </select>
                                </div>
                                <label class="control-label">Dusun/Kampung/Jln</label>
                                <div class="controls">
                                    <input type="text" style="width: 255px" name="sd_reg_street" id="street" placeholder="Dusun/Kampung/Jln" value="<?=$pasien->sd_reg_street?>">
                                </div>
                                <label class="control-label">&nbsp;</label>
                                <div class="controls">
                                	<?php
                                	$rtrw=explode('/', $pasien->sd_rt_rw);
                                	?>
                                    <table style="width:156px">
                                        <tr>
                                            <td style="width:4%">
                                                <label>RT</label>
                                            </td>
                                            <td style="width:10px">
                                                <label style="width:40px"><input type="text" value="<?=$rtrw[0]?>" id="rt" name="rt0" placeholder="RT"></label>
                                            </td>
                                            <td style="width:4%">
                                                <label>RW</label>
                                            </td>
                                            <td style="width:10px">
                                                <label style="width:40px"><input type="text" value="<?=$rtrw[1]?>" id="rw" name="rt1" placeholder="RT"></label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <label class="control-label">Kelurahan/Desa</label>
                                <div class="controls">
                                    <input type="text" style="width: 255px" name="sd_reg_desa" value="<?=$pasien->sd_reg_desa?>" id="desa" placeholder="Kelurahan/Desa">
                                </div>
                                <label class="control-label">Kecamatan</label>
                                <div class="controls">
                                    <input type="text" style="width: 255px" name="sd_reg_kec" value="<?=$pasien->sd_reg_kec?>" id="kec" placeholder="Kecamatan">
                                </div>
                                <label class="control-label">Provinsi</label>
                                <div class="controls">
                                    <select name="sd_reg_prov" id="provinsi" class="chzn-select" style="width: 270px">
                                        <?foreach ($province->result() as $key){
                                        	if($key->mpr_id==$pasien->sd_reg_prov){
                                        		?>
                                        		<option value="<?=$key->mpr_id?>" selected ><?=$key->mpr_name?></option>
                                        		<?php
                                        	}else{
                                        	?>
                                            <option value="<?=$key->mpr_id?>"><?=$key->mpr_name?></option>
                                        <?
											}
										}
										?>
                                    </select>
                                </div>
                                <label class="control-label">Kabupaten/Kotamadya</label>
                                <div class="controls">
                                    <select style="width:270px"  name="sd_reg_kab" id="kota" class="chzn-select" >
                                    	<?php
                            			foreach($city_provide->result() as $city){
                            				if($city->mre_id==$pasien->sd_reg_kab){
                            					?>
                            						<option value="<?=$city->mre_id?>" selected><?=$city->mre_name?></option>
                            					<?php
                            				}else{
                            					?>
                            					<option value="<?=$city->mre_id?>"><?=$city->mre_name?></option>
                            					<?php
                            				}
                            			}
                            			?>
                                    </select>
                                </div>
                            </div>
                            <div class="span6">
                                <label class="control-label">Warga Negara</label>
                                <div class="controls">
                                     <ul class="thumbnails" style="margin-bottom:0px" >
                                        <?foreach ($nationality->result() as $key){
                                        	if($key->mna_name==$pasien->sd_citizen){
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_citizen" checked="checked" value="<?=$key->mna_name?>" />  <?=$key->mna_name?></li>	
                                        		<?php
                                        	}else{
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_citizen" value="<?=$key->mna_name?>" />  <?=$key->mna_name?></li>
                                        	<?php
                                        	}	
                                        }?>
                                    </ul>
                                </div>
                                <label class="control-label">Status</label>
                                <div class="controls">
                                    <ul class="thumbnails" style="margin-bottom:0px" >
                                    	<?php
                                    	if($pasien->sd_marital_st=='belum kawin'){
                                    		?>
                                    		<li class="list"><input type="radio" name="sd_marital_st" value="belum kawin" checked="checked" />  Belum Kawin</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="kawin" />  Kawin</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="duda/janda" />  Duda/Janda</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="tidak kawin" />  Tidak Kawin</li>
                                    		<?php
                                    	}elseif($pasien->sd_marital_st=='kawin'){
                                    		?>
                                    		<li class="list"><input type="radio" name="sd_marital_st" value="belum kawin" />  Belum Kawin</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="kawin" checked="checked" />  Kawin</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="duda/janda" />  Duda/Janda</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="tidak kawin" />  Tidak Kawin</li>
                                    		<?php
                                    	}elseif($pasien->sd_marital_st=='duda/janda'){
                                    		?>
                                    		<li class="list"><input type="radio" name="sd_marital_st" value="belum kawin" />  Belum Kawin</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="kawin" />  Kawin</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="duda/janda" checked="checked" />  Duda/Janda</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="tidak kawin" />  Tidak Kawin</li>
                                    		<?php
                                    	}elseif($pasien->sd_marital_st=='tidak kawin'){
                                    		?>
                                    		<li class="list"><input type="radio" name="sd_marital_st" value="belum kawin" />  Belum Kawin</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="kawin" />  Kawin</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="duda/janda" />  Duda/Janda</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="tidak kawin" checked="checked" />  Tidak Kawin</li>
                                    		<?php
                                    	}else{
                                    		?>
                                    		<li class="list"><input type="radio" name="sd_marital_st" value="belum kawin" />  Belum Kawin</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="kawin" />  Kawin</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="duda/janda" />  Duda/Janda</li>
                                        <li class="list"><input type="radio" name="sd_marital_st" value="tidak kawin" />  Tidak Kawin</li>
                                    		<?php
                                    	}
                                    	?>
                                    </ul>
                                </div>
                                <label class="control-label">Agama</label>
                                <div class="controls">
                                    <ul class="thumbnails" style="margin-bottom:0px" >
                                        <?foreach ($religi->result() as $key){
                                        	if($pasien->sd_religion==$key->mr_name){
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_religion" checked="checked" value="<?=$key->mr_name?>" />  <?=$key->mr_name?></li>
                                        		<?php
											}else{
												?>
												<li class="list"><input type="radio" name="sd_religion" value="<?=$key->mr_name?>" />  <?=$key->mr_name?></li>
												<?php
											}    
                                        }?>
                                    </ul>
                                </div>
                                <label class="control-label">Pendidikan</label>
                                <div class="controls">
                                    <ul class="thumbnails" style="margin-bottom:0px" >
                                        <?foreach ($education->result() as $key){
                                        	if($pasien->sd_education=='TK'){
                                        	?>
                                        	<li class="list"><input type="radio" name="sd_education" checked="checked" value="<?=$key->med_name?>" />  <?=$key->med_name?></li>
                                        	<?php	
                                        	}elseif($pasien->sd_education=='SD'){
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_education" checked="checked" value="<?=$key->med_name?>" />  <?=$key->med_name?></li>
                                        		<?php
                                        	}elseif($pasien->sd_education=='SMP'){
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_education" checked="checked" value="<?=$key->med_name?>" />  <?=$key->med_name?></li>
                                        		<?php
                                        	}elseif($pasien->sd_education=='SMA'){
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_education" checked="checked" value="<?=$key->med_name?>" />  <?=$key->med_name?></li>
                                        		<?php
                                        	}elseif($pasien->sd_education=='D2'){
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_education" checked="checked" value="<?=$key->med_name?>" />  <?=$key->med_name?></li>
                                        		<?php
                                        	}elseif($pasien->sd_education=='D3'){
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_education" checked="checked" value="<?=$key->med_name?>" />  <?=$key->med_name?></li>
                                        		<?php
                                        	}elseif($pasien->sd_education=='S1'){
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_education" checked="checked" value="<?=$key->med_name?>" />  <?=$key->med_name?></li>
                                        		<?php
                                        	}elseif($pasien->sd_education=='S2'){
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_education" checked="checked" value="<?=$key->med_name?>" />  <?=$key->med_name?></li>
                                        		<?php
                                        	}else{
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_education" value="<?=$key->med_name?>" />  <?=$key->med_name?></li>
                                        		<?php
                                        	}
                                        }?>
                                    </ul>
                                </div>
                                <label class="control-label">Pekerjaan</label>
                                <div class="controls">
                                    <ul class="thumbnails" style="margin-bottom:0px" >
                                        <?foreach ($occupation->result() as $key){
                                        	if($pasien->sd_occupation==$key->mo_name){
                                        	?>
                                        	<li class="list"><input type="radio" name="sd_occupation" checked="checked" value="<?=$key->mo_name?>" />  <?=$key->mo_name?></li>
                                        		<?php
                                        	}else{
                                        		?>
                                        		<li class="list"><input type="radio" name="sd_occupation" value="<?=$key->mo_name?>" />  <?=$key->mo_name?></li>
                                        		<?php
                                        	}
                                        }?>
                                    </ul>
                                </div>
                                <label class="control-label">No.Telp</label>
                                <div class="controls">
                                    <input type="text" style="width: 270px" name="sd_telp" placeholder="No.Telp" value="<?=$pasien->sd_telp?>">
                                </div>
                            </div>
                            <div class="controls">
                            	<br /><br />
                                    <button type="submit" class="btn btn-primary"><i class="icon-white icon-check"></i> Simpan</button>
                            </div>
                   </form>
			</div>
		</div>
	</div>
</div>
<script>
	$('.chzn-select').chosen();
	//$('#tempat_lahir').trigger("liszt:updated");
</script>