<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : ''; ?></h1>
</div>
<div class="container-fluid">
	<div class="row-fluid">
		<a href="<?=base_url(); ?>pelayanan_informasi/informasi_pasien/edit/<?=$pasien->sd_rekmed?>" style="float:right" class="btn btn-warning"><i class="icon icon-edit icon-white"></i> Edit</a>
		<div class="span5" >
			<div class="title"><h3>Detail Pasien </h3></div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12">
				<table id='tb_dokter' class="table table-bordered">
					<tbody>
					
									<tr>
										<td>No. REKMED</td>
										<td><b><?=$pasien -> sd_rekmed ?></b></td>
									</tr>
									<tr>
										<td>Nama Pasien</td>
										<td><?=$pasien -> sd_name ?></td>
									</tr>
									<tr>
										<td>Nama Keluarga</td>
										<td><?=$pasien -> sd_keluarga_jenis ?> : <?=$pasien -> sd_keluarga_nama ?></td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td><?=$pasien -> sd_sex ?></td>
									</tr>
									<tr>
										<td>Tempat Lahir</td>
										<td><?=$pasien -> sd_place_of_birth ?></td>
									</tr>
									<tr>
										<td>Tgl lahir</td>
										<td><?=$pasien -> sd_date_of_birth ?></td>
									</tr>
									<tr>
										<td>Gol. Darah</td>
										<td><?=$pasien -> sd_blood_tp ?></td>
									</tr>
									<tr>
										<td>Alergi</td>
										<td><?=$pasien -> sd_alergi ?></td>
									</tr>
									<tr>
										<?php
										$kab = $this -> db -> get_where('mst_regency', array('mre_id' => $pasien -> sd_reg_kab)) -> row();
										$prov = $this -> db -> get_where('mst_province', array('mpr_id' => $pasien -> sd_reg_prov)) -> row();
										?>
										<td>Alamat</td>
										<td><?=$pasien -> sd_reg_street ?>, RT/RW <?=$pasien -> sd_rt_rw ?>, <?=$pasien -> sd_reg_desa ?>, <?=$pasien -> sd_reg_kec ?>, <?=$kab -> mre_name ?>, <?=$prov -> mpr_name ?> </td>
									</tr>
									<tr>
										<td>WNI</td>
										<td><?=$pasien -> sd_citizen ?></td>
									</tr>
									<tr>
										<td>Status</td>
										<td><?=$pasien -> sd_marital_st ?></td>
									</tr>
									<tr>
										<td>Agama</td>
										<td><?=$pasien -> sd_religion ?></td>
									</tr>
									<tr>
										<td>Pendidikan</td>
										<td><?=$pasien -> sd_education ?></td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td><?=$pasien -> sd_occupation ?></td>
									</tr>
									<tr>
										<td>No. Telp</td>
										<td><?=$pasien -> sd_telp ?></td>
									</tr>
								
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
