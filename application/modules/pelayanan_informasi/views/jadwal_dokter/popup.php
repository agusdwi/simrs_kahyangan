<script src="<?=base_url()?>assets/js/valid.js"></script>
<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.gritter.css" />
<script type="text/javascript" src='<?=base_url()?>assets/js/jquery.gritter.min.js'></script>
<script type="text/javascript" src='<?=base_url()?>assets/js/jquery.chosen.js'></script> 
<script>
$(function(){
    $('input[name="tanggal"]').val(getDateRaw2(0));
    $('input[name="tanggal2"]').val(getDateRaw2(30));
    $('.chzn').chosen();
    $('.jam:eq(0)').prop('checked',true);
    if($('.jam:eq(0)').is(":checked")){
        id = $('.jam:eq(0)').val();
        url = BASE+'pelayanan_informasi/jadwal_dokter/get_jam/'+id;
        $.getJSON(url,function(data,v){
            $('.jam_hide').val('');
            $('.jam_hide').val(data.shift_start+' '+data.shift_end);
        })
    }
    $('.jam').change(function(){
        id = $(this).val();
        url = BASE+'pelayanan_informasi/jadwal_dokter/get_jam/'+id;
        $.getJSON(url,function(data,v){
            $('.jam_hide').val('');
            $('.jam_hide').val(data.shift_start+' '+data.shift_end);
        })
    })

    $('.nama_fx').flexbox(BASE+'pelayanan_informasi/jadwal_dokter/get_dokter',{
        paging: false,
        showArrow: false ,
        maxVisibleRows : 10,
        width : 250,
        resultTemplate: '{name}',

    });

    $(".add_schdl").validate({
        rules: {
            'nama_input':"required",
            'nama': "required",     
            'poli' : "required",
            'tanggal' : "required"
        },
        Message:{
            'poli' : "Pilih poli terlebih dulu",
        },
        submitHandler: function(form) {
            if($("#nama").val() == ""){
                alert('silahkan memilih dokter');
            }else if(!check_day()){
                alert('silahkan memilih hari');
            }else{
                $.ajax({
                    url: 'jadwal_dokter/add_schdl',
                    type: "POST",
                    crossDomain: true,
                    data: $('.add_schdl').serialize(),
                    dataType: "json",
                    success: function( data ) {
                        $('.close').click();
                        window.location.reload();
                    },
                    error:function(){
                        $('.error_submit').html('terjadi kesalahan saat menyimpan data, mohon ulangi sekali lagi ');
                    }
                });
            }
            return false;
        },
        errorPlacement: function(error, element) {
            error.appendTo( element.parents(".controls"));
        }
    });

    now = new Date();
    $('.datepicker').datepicker({
        minDate: 0,
        dateFormat: "dd/mm/yy"
    });
})
var d = false;
function check_day(){
    var collection = $(".cx_day");
    collection.each(function(a,i) {
        if($(i).attr('checked') == 'checked'){
            d = true;
        }
    })
    return d;
}
</script>

<style type="text/css" media="screen">
.alert{
    background-color: transparent;
    border: 0px;
}

#gritter-notice-wrapper{
    right: 13%;
    top: 100px;
}
.form-horizontal .controls {
    margin-left: 125px;
    padding: 5px 0;
}
.frm-modal-add-scdle{
    overflow: hidden;
    height: 400px;
}
.chzn-container .chzn-results{
    max-height: 140px;
}
</style>

<?=form_open(cur_url().'add_schdl',array('class' => 'frm-modal-add-scdle form-horizontal add_schdl stdform','id' => 'add_schdl2 form basic_validate','name' =>'add_schdl2','novalidate'=>"novalidate")); ?>
<div id="advances" style="margin-top:10px;">
    <div class='span12' >
        <div class="control-group" style="border:none;">
            <label class="control-label" style="width:20%;">Nama</label>
            <div class="controls" >
                <select id="nama" name="nama" class="chzn">
                    <option value="">-- Pilih dokter --</option>
                    <?php
                    foreach($doks->result() as $dok){
                        ?>
                        <option value="<?=$dok->dr_id?>"><?=$dok->dr_name?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="control-group" style="border:none;">    
            <label class="control-label" style="width:20%;">Poli</label>
            <div class="controls">
                <select name='poli' required id='poli' style="width:140px;float:left;" >
                    <option value="">-- Poli --</option>
                    <?foreach($poli2 as $poli2):?>
                       <option value="<?=$poli2->pl_id?>"><?=$poli2->pl_name?></option>
                    <?endforeach;?>
                </select>
            </div>
        </div>

        <div class="control-group" style="border:none;">
            <label class="control-label" style="width:20%;">Tanggal Mulai</label>
            <div class="controls" >
                <input type="text" required name='tanggal' class="datepicker" data-date-format="yyyy-mm-dd">
                <input type="hidden" name='jam_hide' class="jam_hide" >
            </div>
        </div>
        <div class="control-group" style="border:none;">
            <label class="control-label" style="width:20%;">Tanggal Selesai</label>
            <div class="controls" >
                <input type="text" required name='tanggal2' class="datepicker" data-date-format="yyyy-mm-dd">
                <input type="hidden" name='jam_hide2' class="jam_hide" >
            </div>
        </div>
        <div class="control-group" style="border:none;">
            <label class="control-label" style="width:20%;">Jam</label>
            <div class="controls">
                <table>
                    <tr>
                        <td>
                            <select name="jam_mulai" class="chzn" style="width: 70px;">
                                <?for($i=0;$i<24;$i++): ?>
                                    <option><?=sprintf('%1$02d', $i);?></option>        
                                <?endfor;?>
                            </select>	
                        </td>
                        <td>
                            &nbsp;:&nbsp;
                        </td>
                        <td>
                            <select name="menit_mulai" class="chzn" style="width: 70px;">
                                <?for($i=0;$i<24;$i++): ?>
                                    <option><?=sprintf('%1$02d', $i);?></option>        
                                <?endfor;?>
                            </select>
                        </td>
                        <td>
                            &nbsp;s.d.&nbsp;
                        </td>
                        <td>
                            <select name="jam_selesai" class="chzn" style="width: 70px;">
                                <?for($i=0;$i<24;$i++): ?>
                                    <option><?=sprintf('%1$02d', $i);?></option>        
                                <?endfor;?>
                            </select>
                        </td>
                        <td>
                            &nbsp;:&nbsp;
                        </td>
                        <td>
                            <select name="menit_selesai" class="chzn" style="width: 70px;">
                                <?for($i=0;$i<24;$i++): ?>
                                    <option><?=sprintf('%1$02d', $i);?></option>        
                                <?endfor;?>
                            </select>				
                        </td>
                    </tr>
                </table> 
            </div>
        </div> 
        <div class="control-group" style="border:none;">
            <label class="control-label" style="width:20%;">Setiap Hari</label>
            <div class="controls">
                <table style="width:100%">
                    <tr>
                        <td><input type="checkbox" name="day[mon]" class="cx_day"> Senin</td>
                        <td><input type="checkbox" name="day[tue]" class="cx_day"> Selasa</td>
                        <td><input type="checkbox" name="day[wed]" class="cx_day"> Rabu</td>
                        <td><input type="checkbox" name="day[thu]" class="cx_day"> Kamis</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="day[fri]" class="cx_day"> Jumat</td>
                        <td><input type="checkbox" name="day[sat]" class="cx_day"> Sabtu</td>
                        <td><input type="checkbox" name="day[sun]" class="cx_day"> Minggu</td>
                    </tr>
                </table>
            </div>
        </div>
        <br clear="all">   <br><br>
        <label class='error_submit' style='background: url("../assets/img/caution.png") no-repeat 2px 4px;padding-left: 19px;font-weight: bold;color: #C00 !important;clear: both;'></label>
        <input type="submit" value="Simpan" style='float:right' class="btn tambah_schdl btn-success">
    </div>
    <div class="clear"></div>
</div>
</form>