  <script>
    var otb;
    $(function(){
         otb = $('.tabel-dokter').dataTable( {
              
                "sPaginationType": "bootstrap",
                "sScrollY": "400px",
                "bPaginate": false,
                "bFilter": true,
                "bRetrieve": true,
                 "bDestroy": true,
                "bSort": false,
                "oLanguage": {
                  "sEmptyTable": "Jadwal tidak tersedia"
                }
            });
    })

 </script>
 <table id="tb_doctor" cellpadding="0" cellspacing="0" border="0" class="tabel-dokter table table-bordered def_table_y dataTable tb_scrol">
         
            <thead>
                <tr  role="row">
                    <th class="head0">No.</th>
                    <th class="head1">Nama Dokter</th>
                    <th class="head0">Poli</th>
                    <th class="head1">Jam</th>
                     
                    
                </tr>
            </thead>
            
            <tbody>
                <?php $i=1; foreach ($datas->result() as $doctor):?>
                <?
                    $d1 = explode(" ", $doctor->sch_time_start);
                    $d2 = explode(" ", $doctor->sch_time_end);
                    $d1 = substr($d1[1], 0,5);
                    $d2 = substr($d2[1], 0,5);
                ?>
                <tr>
                    <?php echo '<td>'.$i++.'</td>'?>
                    <?php echo '<td>'.$doctor->dr_name.'</td>'?>
                    <?php echo '<td>'.$doctor->pl_name.'</td>'?>
                    <td>
                        <?php echo $d1.' - '.$d2?>
                        <a class="label label-important ajaxDelete onhover load" href="<?=cur_url('-2').'delete/'.$doctor->sch_id;?>">
                            delete
                        </a>
                    </td>
                </tr>
                <?php endforeach?>               
            </tbody>
            <tfoot>
            </tfoot>
        </table>