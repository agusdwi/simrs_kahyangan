<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jadwal_dokter extends CI_Controller {
		function __construct() {
			parent::__construct();
			$this->cf = array(
					'modul_id'	=> 2
				);
		}
	

	public function index(){
		$data['main_view']	= 'jadwal_dokter/index';
		$data['title']		= 'Jadwal Dokter';
		$data['cf']			=  $this->cf;
		$data['desc']		= 'Description. ';
		$data['current']	= 6;
		$data['poli']		= $this->db->query('select * from trx_poly')->result();
		$data['jam']		= $this->db->query('select * from mst_shift')->result();
		$this->load->view('template',$data);
	}

	function create(){
		$data = $this->input->post('ds');
		$this->db->insert('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		redirect(cur_url(-1));
	}

	function update($id){
		$data = $this->input->post('ds');	
		$this->db->where('', $id);
		$this->db->update('', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil diperbarui'));
		redirect(cur_url(-2));
	}

	function delete($id){
		$this->db->delete('trx_dr_schedule', array('sch_id' => $id)); 
		echo "success";
	}

	function search(){
		$nama = $this->input->post('nama');
		$poli = $this->input->post('poli');
		$ruang = $this->input->post('ruang');
		$jam = $this->input->post('jam');
 		$tgl = $this->input->post('tgl_frm');

		$this->db->select('*');
		$this->db->from('v_sch_doctor');
		if($nama != null){
			$this->db->like('lower(dr_name)',"$nama",'both');
		}

		if($poli != null){
			$this->db->where('pl_id',"$poli");
		}
  
		if($jam != null){
			$this->db->where('(sch_shift)',"$jam");
		}
		
		$this->db->like('(sch_time_start::text)',"$tgl");
		$this->db->order_by("dr_name", "asc");
		$data['datas'] = $this->db->get();
 		$this->load->view('jadwal_dokter/data',$data);
	}

	function loaddata($tgl=0){
		if($tgl==0){
			$tgl=date('Y-m-d');
		}
		//$tgl = $this->input->post('tgl');
		$tgl=explode('-',$tgl);
		$d=$tgl[2];
		if($d<10){
			$d='0'.$d;
		}
		$m=$tgl[1];
		if($m<10){
			$m='0'.$m;
		}
		$y=$tgl[0];
		$tgl=$y.'-'.$m.'-'.$d;
		//$tgl='2013-11-08';
		$data['datas'] = $this->db->query("select * from v_sch_doctor where sch_time_start like '%$tgl%' ");
		$this->load->view('jadwal_dokter/data',$data);

	}

	function get_dokter(){
		$query = $this->input->get('q'); 
		$this->db->select('*');
		$this->db->from('trx_doctor');
		if($query!=null){
			$this->db->like('lower(dr_name)', "$query",'both');
		}
		$data = $this->db->get()->result();
		//var_dump($data);
		//echo $this->db->last_query();die();
		foreach ($data as $datas) {
			$data['results'][] = array(
				'id'	=> "$datas->dr_id",
				'name'	=> "$datas->dr_name"
			);
		}

		echo json_encode($data);
	}

	function add_schdl(){
		$input = $this->input->post();
		
		$jam1 = $input['jam_mulai'].":".$input['menit_mulai'].":"."00";
		$jam2 = $input['jam_selesai'].":".$input['menit_selesai'].":"."00";

		$d = array();
		$date1 = DateTime::createFromFormat('j/m/Y', $input['tanggal']);
		$date2 = DateTime::createFromFormat('j/m/Y', $input['tanggal2']);
		
		$time = getMicroTime();

		while ($date1 <= $date2) {
			$id = strtolower($date1->format("D"));
			if(isset($input['day'][$id])){
				$d = array(
					"dr_id"				=> $input['nama'],
					"sch_time_start"	=> $date1->format('Y-m-d')." ".$jam1,
					"sch_time_end"		=> $date1->format('Y-m-d')." ".$jam2,
					"pl_id"				=> $input['poli'],
					"insert_time"		=> $time
				);
				$this->db->insert('trx_dr_schedule',$d);
			}
			$date1->modify('+1 day');
		}
	}

	function get_pop(){
		$data['jam2']	= $this->db->query('select * from mst_shift')->result();
		$data['poli2']	= $this->db->query('select * from trx_poly')->result();
		$data['doks']	= $this->db->order_by('dr_name')->get_where('trx_doctor',array('dr_status'=>1));
		$this->load->view('jadwal_dokter/popup',$data);
	}

	function get_jam($id=''){
		$data = $this->db->query("select * from mst_shift where shift_id = $id")->result();
		//$data = $this->db->get('mst_shift')->result();
		echo json_encode($data[0]);
	}
	function tes(){
		$data = array(//'sch_id'=>$id_sch,
			//'dr_id'=>$nama,
			//'sch_id'=>39,
			'dr_id'=>1,
			//'sch_shift'=>$jam,
			'sch_shift'=>3,
			//'pl_id'=>$poli,
			'pl_id'=>2,
			'modi_id'=>'2',
			//'sch_time_start'=>$tgl.' '.$jam2[0],
			'sch_time_start'=>'2013-11-07 08:00:00',
			//'sch_time_end'=>$tgl2.' '.$jam2[1]);
			'sch_time_end'=>'2013-11-07 11:00:00');
			//print_r($data);
		$this->db->insert('trx_dr_schedule', $data);
	}
}