<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Informasi_pasien extends CI_Controller {
		function __construct() {
			parent::__construct();
			$this->cf = array(
					'modul_id'	=> 2
				);
		}

	public function index(){
		$data['main_view']	= 'informasi_pasien/index';
		$data['title']		= 'Daftar Pasien ';
		$data['cf']			=  $this->cf;
		$data['current']	= 5;
		$data['desc']		= 'Description. ';
		$this->load->view('template',$data);
	}

	function dt_all_pasien(){
		$config['sTable'] 			= 'ptn_social_data';
		/*$config['join'][] 		= array('trx_dokter_spesialis b','a.dr_cd = b.dr_cd');
		$config['join'][] 			= array('trx_spesialis c','c.spesialis_cd = b.spesialis_cd');*/
		$config['aColumns'] 		= array('sd_rekmed','name_address','sd_age','sd_blood_tp','sd_reg_date');
		$config['key'] 				= 'sd_rekmed';
		$config['aColumns_format'] 	= array("sd_rekmed","sd_name||'<br/>'||sd_address as name_address","sd_age||' tahun' as sd_age","sd_blood_tp","sd_reg_date");
		$config['searchColumn'] 	= array('sd_name','sd_address');
		$config['aksi'] = array(
								'stat'  	=> true,
								'key'		=> 'sd_rekmed',//
								/*'edit'   	=> base_url().'rawat_jalan/master__dokter/update/',
								'delete' 	=> base_url().'rawat_jalan/master__dokter/delete/dr/',*/
								'detail'	=> base_url().'pelayanan_informasi/informasi_pasien/detail/',
								);
		init_datatable($config);
	}

	function detail($id){
		$data['pasien']		= db_conv($this->db->get_where('ptn_social_data',array('sd_rekmed'=>$id)));
		$data['cf']			=  $this->cf;
		$data['current']	= 5;
		$data['title']		= 'Detail Pasien ';
		$data['main_view']	= 'informasi_pasien/detail';
		$data['detail'] 	= db_conv($this->db->get_where('ptn_social_data',array('sd_rekmed'=>$id)));
		$this->load->view('template',$data);
	}
	function edit($id){
		$data['pasien']		= db_conv($this->db->get_where('ptn_social_data',array('sd_rekmed'=>$id)));
		$data['cf']			=  $this->cf;
		$data['current']	= 5;
		$data['title']		= 'Edit Pasien ';
		$data['main_view']	= 'informasi_pasien/edit';
		$data['detail'] 	= db_conv($this->db->get_where('ptn_social_data',array('sd_rekmed'=>$id)));
		$data['religi']		= $this->db->get('mst_religion');
		$data['regency']	= $this->db->get('mst_regency');
		$data['province']	= $this->db->get('mst_province');
		$data['occupation']	= $this->db->get('mst_occupation');
		$data['nationality']= $this->db->get('mst_nationality');
		$data['education']  = $this->db->get('mst_education');
		$data['city_provide'] = $this->db->get('mst_regency');
		$data['alergi_provide'] = $this->db->get('mst_alergi');
		$data['desc']		= 'Description. ';
		$this->load->view('template',$data);
	}
	function save($id){
		//echo $this->input->post('sd_name');
		$this->db->from('ptn_social_data');
		if($this->input->post('rt0')<10){
			$rt0='0'.$this->input->post('rt0');
		}else{
			$rt0=$this->input->post('rt0');
		}
		if($this->input->post('rt1')<10){
			$rt1='0'.$this->input->post('rt0');
		}else{
			$rt1=$this->input->post('rt1');
		}
		$data=array(
			'sd_name'			=>	$this->input->post('sd_name'),
			'sd_keluarga_jenis'	=>	$this->input->post('sd_keluarga_jenis'),
			'sd_keluarga_nama'	=>	$this->input->post('sd_keluarga_nama'),
			'sd_sex'			=>	$this->input->post('sd_sex'),
			'sd_place_of_birth'	=>	$this->input->post('sd_place_of_birth'),
			'sd_date_of_birth'	=>	$this->input->post('tgl2').'-'.$this->input->post('tgl1').'-'.$this->input->post('tgl0'),
			'sd_age'			=>	$this->input->post('sd_age'),
			'sd_blood_tp'		=>	$this->input->post('sd_blood_tp'),
			'sd_alergi'			=>	$this->input->post('sd_alergi'),
			'sd_reg_street'		=>	$this->input->post('sd_reg_street'),
			'sd_rt_rw'			=>	$this->input->post('rt0').'/'.$this->input->post('rt1'),
			'sd_reg_desa'		=>	$this->input->post('sd_reg_desa'),
			'sd_reg_kec'		=>	$this->input->post('sd_reg_kec'),
			'sd_reg_prov'		=>	$this->input->post('sd_reg_prov'),
			'sd_reg_kab'		=>	$this->input->post('sd_reg_kab'),
			'sd_citizen'		=>	$this->input->post('sd_citizen'),
			'sd_marital_st'		=>	$this->input->post('sd_marital_st'),
			'sd_religion'		=>	$this->input->post('sd_religion'),
			'sd_education'		=>	$this->input->post('sd_education'),
			'sd_occupation'		=>	$this->input->post('sd_occupation'),
			'sd_address'		=>	$this->input->post('sd_reg_street').' '.
									$this->input->post('rt0').'/'.
									$this->input->post('rt1').', '.
									$this->input->post('sd_reg_desa').', '.
									$this->input->post('sd_reg_kec'),
			'sd_telp'			=>	$this->input->post('sd_telp'),
			'modi_datetime'		=>	date('Y-m-d H:i:s'),
		);
		$this->db->where('sd_rekmed',$id);
		$this->db->update('ptn_social_data',$data);
		return redirect('pelayanan_informasi/informasi_pasien/detail/'.$id);
		//echo $this->db->last_query();
	}
}