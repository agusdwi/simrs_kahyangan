<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>user/tipe" class="btn btn-success pull-right"><i class="icon-chevron-left icon-white"></i> Daftar Tipe Pengguna</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-align-justify"></i>                                  
					</span>
					<h5>Form Tipe Pengguna</h5>
				</div>
				<div class="widget-content nopadding">
					<?=form_open('',array('class' => 'form-horizontal form','id' => 'form_dokter')); ?>
						<div class="control-group">
							<label class="control-label">Tipe Pengguna *</label>
							<div class="controls">
								<input class="medium" type="text" id="usr_nama" name="ds[tipe_nama]" autofocus="autofocus" value="<?=$ds->tipe_nama;?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Default Home *</label>
							<div class="controls">
								<input class="medium" type="text" id="usr_nama" name="ds[tipe_homebase]" value="<?=$ds->tipe_homebase;?>">
							</div>
						</div>
						<div class="form-actions">
							<button id="reset" class="btn btn-warning" type="reset">Reset</button>
							<button class="btn btn-primary" id="next" type="submit">Simpan</button>
						</div>
					<?=form_close()?>
				</div>
			</div>                      
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#form_dokter").validate({
			rules: {
				"ds[tipe_nama]"		:"required",
				"ds[tipe_homebase]"	:"required"
			}
		});
	})
</script>