<div class="pageheader notab">
	<h1 class="pagetitle"><?=(isset($title)) ? $title : '';?>
		<a href="<?=base_url()?>user" class="btn btn-success pull-right"><i class="icon-chevron-left icon-white"></i> Daftar Pengguna</a>
	</h1>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="icon-align-justify"></i>                                  
					</span>
					<h5>Form Pengguna</h5>
				</div>
				<div class="widget-content nopadding">
					<?=form_open('',array('class' => 'form-horizontal form','id' => 'form_dokter')); ?>
						<input type="hidden" name="old" value="<?=$ds->username;?>">
						<div class="control-group">
							<label class="control-label">Nama Pengguna *</label>
							<div class="controls">
								<input class="medium" type="text" id="usr_nama" name="ds[nama]" autofocus="autofocus" value="<?=$ds->nama;?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Username *</label>
							<div class="controls">
								<input class="medium" type="text" id="usr_username" name="ds[username]" value="<?=$ds->username;?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Password *</label>
							<div class="controls">
								<input class="medium" type="password" id="usr_password" name="ds[password]">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tipe *</label>
							<div class="controls">
								<select name="ds[user_tipe]">
									<?foreach ($tipe->result() as $key): ?>
										<option <?=(($key->tipe_id == $ds->user_tipe)? 'selected="selected"':'');?> value="<?=$key->tipe_id;?>"><?=$key->tipe_nama;?></option>
									<?endforeach ?>
								</select>
							</div>
						</div>
						<div class="form-actions">
							<button id="reset" class="btn btn-warning" type="reset">Reset</button>
							<button class="btn btn-primary" id="next" type="submit">Simpan</button>
						</div>
					<?=form_close()?>
				</div>
			</div>                      
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#form_dokter").validate({
			rules: {
				"ds[nama]":"required",
				"ds[username]":"required",
				"ds[password]":"required"
			}
		});
	})
</script>