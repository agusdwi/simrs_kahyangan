<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tipe extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array('modul_id' => 8);
		$this->table = 'com_user_tipe';
	}

	public function index() {
		$data['main_view'] 	= 'tipe/index';
		$data['title'] 		= 'Pengelolaan Tipe Pengguna';
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 34;
		$data['ds'] 		= $this->db->get_where($this->table,array('status'=>1));
		$this->load->view('template', $data);
	}

	function add(){
		if(is_post()){
			$data = $this->input->post('ds');
			$this->db->insert($this->table,$data);
			$this->session->set_flashdata('message','data berhasil di simpan');
			redirect(cur_url(-1));
		}else{
			$data['main_view'] 	= 'tipe/add';
			$data['title'] 		= 'Tambah Tipe Pengguna';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 34;
			$this->load->view('template', $data);
		}
	}

	function edit($id){
		if (is_post()) {
			$data = $this->input->post('ds');
			$this->db->where('tipe_id', $id);
			$this->db->update($this->table, $data); 
			$this->session->set_flashdata('message','data berhasil di update');
			redirect(cur_url(-2));
		}else{
			$data['main_view'] 	= 'tipe/edit';
			$data['title'] 		= 'Edit Tipe Pengguna';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 34;
			$data['ds'] 		= $this->db->get_where($this->table,array('tipe_id'=>$id))->row();
			$this->load->view('template', $data);
		}
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('tipe_id', $id);
		$this->db->update($this->table, $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(cur_url(-2));
	}
}