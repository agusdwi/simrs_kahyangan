<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array('modul_id' => 8);
		$this->table = 'com_user';
	}

	public function index() {
		$data['main_view'] 	= 'user/index';
		$data['title'] 		= 'Pengelolaan Data Pengguna';
		$data['cf'] 		= $this->cf;
		$data['current'] 	= 33;
		$data['ds'] 		= $this->db->get_where('v_user',array('status'=>1));
		$this->load->view('template', $data);
	}

	function add(){
		if(is_post()){
			$data = $this->input->post('ds');
			if ($this->db->get_where($this->table,array('username'=>$data['username']))->num_rows() > 0) {
				$this->session->set_flashdata('message','maaf username telah dipakai');
				redirect(cur_url());	
			}
			$data['password'] = md5($data['password'].encryptkey());
			$this->db->insert($this->table,$data);
			$this->session->set_flashdata('message','data berhasil di simpan');
			redirect(cur_url(-1));
		}else{
			$data['main_view'] 	= 'add';
			$data['title'] 		= 'Tambah Data Pengguna';
			$data['cf'] 		= $this->cf;
			$data['current'] 	= 33;
			$data['tipe'] 		= $this->db->get_where('com_user_tipe',array('status'=>1));
			$this->load->view('template', $data);
		}
	}

	function edit($id){
		if (is_post()) {
			$old = $this->input->post('old');
			$data = $this->input->post('ds');
			if ($old != $data['username']) 
				if ($this->db->get_where($this->table,array('username'=>$data['username']))->num_rows() > 0) {
					$this->session->set_flashdata('message','maaf username telah dipakai');
					redirect(cur_url());
				}
			$data['password'] = md5($data['password'].encryptkey());
			$this->db->where('user_id', $id);
			$this->db->update($this->table, $data); 
			$this->session->set_flashdata('message','data berhasil di update');
			redirect(cur_url(-2));
		}else{
			$data['main_view'] = 'edit';
			$data['title'] = 'Edit Data Pengguna';
			$data['cf'] = $this->cf;
			$data['current'] = 33;
			$data['ds'] = $this->db->get_where($this->table,array('user_id'=>$id))->row();
			$data['tipe'] 		= $this->db->get_where('com_user_tipe',array('status'=>1));
			$this->load->view('template', $data);
		}
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('user_id', $id);
		$this->db->update($this->table, $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect(cur_url(-2));
	}

}