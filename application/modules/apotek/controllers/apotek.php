<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apotek extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 5
			);
	}

	public function index(){
		$data['main_view']	= 'index';
		$data['title']		= 'Apotek';
		$data['cf']			=  $this->cf;
		$data['histori']	= $this->db->get_where('v_transaksi_resep',array('recipe_status'=>2));
		$this->load->view('template',$data);
	}
}