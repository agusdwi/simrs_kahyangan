<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembelian_langsung extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 5
			);
	}

	public function index(){
		$data['main_view']	= 'pembelian_langsung/index';
		$data['title']		= 'Pembelian Langsung';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}

	function select($medical_id){
		$data['main_view']	= 'apotek/pembelian_langsung/detail_pembelian';
		$data['title']		= 'Pembelian Langsung';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}

	function create(){
		$rm = "1";
		$data = $this->input->post('ds');
		// $this->db->insert('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		echo cur_url(-1)."cetak/$rm";
	}

	function cetak($rm){
		$data['main_view']	= 'apotek/cetak';
		$this->load->view('template_print',$data);
	}
}