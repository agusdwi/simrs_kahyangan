<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resep_pasien extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 5
			);
	}

	public function index(){
		$data['main_view']	= 'resep_pasien/index';
		$data['title']		= 'Resep Pasien';
		$data['resep_pasien'] = $this->db->get_where('v_transaksi_resep',array('recipe_status'=>1));
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}
	function select($resep_id){
		$data['main_view']	= 'apotek/resep_pasien/detail_resep';
		$data['data_pasien']= $this->db->get_where('v_transaksi_resep',array('recipe_id'=>$resep_id));
		$data['resep_pasien'] = $this->db->get_where('v_transaksi_detail_resep',array('recipe_id'=>$resep_id));
		$data['title']		= 'Resep Pasien';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}

	function create($resep_id){
		$data = $this->input->post('ds');
		// $this->db->insert('', $data);
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		echo cur_url(-1)."cetak/$resep_id";
	}

	function cetak($recipe_id){
		$data['main_view']	= 'apotek/cetak';
		$data['data_pasien']= $this->db->get_where('v_transaksi_resep',array('recipe_id'=>$recipe_id));
		$data['resep_pasien'] = $this->db->get_where('v_transaksi_detail_resep',array('recipe_id'=>$recipe_id));
		$this->load->view('template_print',$data);
	}

	function simpan($recipe_id){
		// debug_array($recipe_id);
		$data = $this->input->post('ds');
		$mdc_id = $data['mdc_id'];
		$dt_stat['recipe_status'] = 2;
		$this->db->where('mdc_id',$mdc_id);
		$this->db->update('trx_medical',$dt_stat);
		echo cur_url(-2)."cetak/$recipe_id";
	}
}