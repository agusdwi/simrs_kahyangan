<!--
Agus assign
view kedua :
	<br>berisi detail obat yang digunakan
	<br>berisi data diri pasien, sama seperti pembayaran yang di kerjakan hanief han
	<br>simpan, simpan dan bayar muncul pop up struk-->

<style>
	.table_tr thead th{
		height: 28px;
		vertical-align: middle;
		font-size: 13px;
	}
</style>
<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>
<div class="container-fluid">
    <div class="row-fluid">
    	<?
    	foreach ($data_pasien->result() as $key => $value) {
   			?>
	        <div class="span12" style="background:#E5E5E5;">
	        	<div class="title" style="margin-bottom:0px;"><h3>Data Pasien</h3></div>
	            <br clear="all">
	            <div class="span2">
	                <div style="padding:10px;float:left;width:100%;">
	                    <div style="float:left;">
	                        <b>Medical Id</b>
	                    </div>
	                    <div style="float:left;margin-left:10px;">
	                        <b>: <?=$value->mdc_id?></b>
	                    </div>
	                </div>
	            </div>
	            <div class="span2">
	                <div style="padding:10px;float:left;width:100%;">
	                    <div style="float:left;">
	                        <b>Nomor RM</b>
	                    </div>
	                    <div style="float:left;margin-left:10px;">
	                        <b>: <?=$value->sd_rekmed?></b>
	                    </div>
	                </div>
	            </div>
	            <div class="span2">
	                <div style="padding:10px;float:left;width:100%;">
	                    <div style="float:left;">
	                        <b>Nama</b>
	                    </div>
	                    <div style="float:left;margin-left:10px;">
	                        <b>: <?=$value->sd_name?></b>
	                    </div>
	                </div>
	            </div>
	            <div class="span2">
	                <div style="padding:10px;float:left;width:100%;">
	                    <div style="float:left;">
	                        <b>Jenis Kelamin</b>
	                    </div>
	                    <div style="float:left;margin-left:10px;">
	                    	<? if($value->sd_sex == 'l'){ $sex = 'Laki-laki';}else{ $sex = 'Perempuan';} ?>
	                        <b>: <?=$sex?></b>
	                    </div>
	                </div>
	            </div>

	            <div class="span2">
	                <div style="padding:10px;float:left;width:100%;">
	                    <div style="float:left;">
	                        <b>Umur</b>
	                    </div>
	                    <div style="float:left;margin-left:10px;">
	                        <b>: <?=$value->sd_age?> Tahun</b>
	                    </div>
	                </div>
	            </div>
	            <div class="span1">
	                <div style="padding:10px;float:left;width:100%;">
	                    <div style="float:left;">
	                        <b>Gol. Darah</b>
	                    </div>
	                    <div style="float:left;margin-left:10px;">
	                        <b>: <?=$value->sd_blood_tp?></b>
	                    </div>
	                </div>
	            </div>
	        </div>
        	<?
        	$mdc_id = $value->mdc_id;
        	$dokter = $value->dr_name;
        	$resep_id = $value->recipe_id;
    		}
        ?>
    </div>
    <div class='row-fluid' style="padding:10px">
    	<div class="widget-content nopadding">
	    	<div class="span10">
				<style>
					td.curr{
						text-align:right;
					}
				</style>
				<script>
					$(function(){
						$('.dyntable').dataTable( {
							"sPaginationType": "bootstrap",
							"sScrollY": "200px",
							  "bFilter": false,
							"bPaginate": false,
					    });
					})
				</script>
				<table class="table table-bordered def_table_y dataTable tb_scrol table_tr" style="border:1px solid #CCC;">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Kemasan</th>
							<th>Aturan</th>
							<th>Jumlah</th>
						</tr>
					</thead>
					<tbody>
						<?
							$i = 0;
							foreach ($resep_pasien->result() as $key => $value) {
								$i++;
								?>
									<tr>
										<td style="text-align:center;"><?=$i?></td>
										<td><?=$value->im_name?></td>
										<td><?=$value->im_unit?></td>
										<td><?=$value->recipe_rule?></td>
										<td style="text-align:center;"><?=$value->recipe_qty?></td>
									</tr>
								<?
							}
						?>
					</tbody>
				</table>
			</div>
			<div class="span10">
				<?=form_open(base_url().'apotek/resep_pasien/simpan/'.$resep_id,array('class' => 'form-horizontal','id' => 'form')); ?>
				<input type="hidden" name="ds[mdc_id]" value="<?=$mdc_id?>">
				<!-- Dokter : <b><?=$dokter?></b><br> -->
				<!-- Catatan Resep<br/> -->
				<!-- <textarea rows=5 style="width:95%;"></textarea> -->
				<style>
					/*#harga td{
						vertical-align: middle;padding:4px;
					}
					#harga input{
						margin:0px;
						width:150px;
						text-align: right;
					}*/
				</style>
				
				<div style="text-align:right;margin-top:20px">
					<button class="btn btn-warning">Batal</button>
					<button class="btn btn-primary">Simpan</button>
					<a href="#myModal" id="cetak" role="button" class="btn hide" data-toggle="modal">Launch demo modal</a>
				</div>
				<?=form_close()?>
			</div>
		</div>
    </div>
</div>

<div id="myModal" style="height:500px;width:720px;margin-left:-320px;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Cetak Faktur</h3>
  </div>
  <div class="modal-body">
    <iframe src="" name="ifr" id="ifr" style="margin:0px;padding:0px;border:none;width:99%;height:100%"></iframe>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Keluar</button>
    <button id="cetakIframe" class="btn btn-primary">Cetak</button>
  </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(function(){
        $(".form-horizontal").submit(function(){
            var url  = $(this).attr('action');
            var data = $(this).serialize();
            $.post(url,data, function(data){
                $("#cetak").trigger('click');
				$("#ifr").attr('src',data);
            }); 
            return false;
        })

        $("#cetakIframe").click(function(){
            ifr.print();
            window.location = "<?=base_url()?>apotek/resep_pasien";
        })
    })
</script>