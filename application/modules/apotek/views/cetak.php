
<table class="table table-bordered def_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:99%;margin-top:20px;border:0px;">
<?
	foreach ($data_pasien->result() as $key => $value) {
		?>
			<tr>
				<td style="border:0px;">No. RM</td><td style="border:0px;"><?=$value->sd_rekmed?></td>
				<td style="border:0px;">Umur</td><td style="border:0px;"><?=$value->sd_age?> Tahun</td>
			</tr>
			<tr>
				<td style="border:0px;">Nama</td><td style="border:0px;"><?=$value->sd_name?></td>
				<td style="border:0px;">Gol. Darah</td><td style="border:0px;"><?=$value->sd_blood_tp?></td>
			</tr>
			<tr>
				<td style="border:0px;">Alamat</td><td style="border:0px;"><?=$value->sd_address?></td>
				<td style="border:0px;"></td><td style="border:0px;"></td>
			</tr>
		<?
	}
?>
</table>



<table class="table table-bordered def_table_y dataTable tb_scrol" align="center" style="margin-left:0px;width:100%; ">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Kemasan</th>
			<th>Aturan</th>
			<th>Jumlah</th>
		</tr>
	</thead>
	<tbody>
		<?
			$i = 0;
			foreach ($resep_pasien->result() as $key => $value) {
				$i++;
				?>
					<tr>
						<td style="text-align:center;"><?=$i?></td>
						<td><?=$value->im_name?></td>
						<td><?=$value->im_unit?></td>
						<td><?=$value->recipe_rule?></td>
						<td style="text-align:center;"><?=$value->recipe_qty?></td>
					</tr>
				<?
			}
		?>
	</tbody>
</table>
