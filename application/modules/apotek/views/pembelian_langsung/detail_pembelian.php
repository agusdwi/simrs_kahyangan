<!--
Agus assign
view kedua :
	<br>berisi detail obat yang digunakan
	<br>berisi data diri pasien, sama seperti pembayaran yang di kerjakan hanief han
	<br>simpan, simpan dan bayar muncul pop up struk-->
<div class="pageheader notab">
    <h1 class="pagetitle"><?=(isset($title)) ? $title : '';?></h1>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" style="background:#E5E5E5;">
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Nomor RM</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: 082312</b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Nama</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: Sigit Hanafi</b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Jenis Kelamin</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: laki-laki</b>
                    </div>
                </div>
            </div>

            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Umur</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: 32 Tahun</b>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div style="padding:10px;float:left;width:100%;">
                    <div style="float:left;">
                        <b>Gol. Darah</b>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <b>: O</b>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='row-fluid' style="padding:10px">
    	<div class="widget-content nopadding">
	    	<div class="span7">
				<style>
					td.curr{
						text-align:right;
					}
					td.curr input{
						width: 50px;
						margin:0px;
					}
				</style>
				<script>
					$(function(){
						$('.dyntable').dataTable( {
							"sPaginationType": "bootstrap",
							"sScrollY": "200px",
							  "bFilter": false,
							"bPaginate": false,
					    });
					})
				</script>
				<table class="table table-bordered table-striped dyntable">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Kemasan</th>
							<th>Jumlah</th>
							<th>Harga</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Amoxsan 60 Dry Syr.</td>
							<td>Botol</td>
							<td class="curr"><input type="text" name="obat[1]" value="<?=number_format(0,2)?>"></td>
							<td class="curr"><?=number_format(25162)?></td>
						</tr>
						<tr>
							<td>2</td>
							<td>Amoxsan 250 Mg Kaps.</td>
							<td>Kapsul</td>
							<td class="curr"><input type="text" name="obat[2]" value="<?=number_format(0,2)?>"></td>
							<td class="curr"><?=number_format(1732.5)?></td>
						</tr>
						<tr>
							<td>3</td>
							<td>Amoxsan 500 Mg Kaps.</td>
							<td>Kapsul</td>
							<td class="curr"><input type="text" name="obat[3]" value="<?=number_format(0,2)?>"></td>
							<td class="curr"><?=number_format(3664.38)?></td>
						</tr>
						<tr>
							<td>4</td>
							<td>Amoxsan Forte 60 Ml Dry Syr.</td>
							<td>Botol</td>
							<td class="curr"><input type="text" name="obat[4]" value="<?=number_format(0,2)?>"></td>
							<td class="curr"><?=number_format(35887.5)?></td>
						</tr>
						<tr>
							<td>5</td>
							<td>Amoxsan Inj 1 Gr</td>
							<td>Flacon</td>
							<td class="curr"><input type="text" name="obat[5]" value="<?=number_format(0,2)?>"></td>
							<td class="curr"><?=number_format(24564.38)?></td>
						</tr>
						<tr>
							<td>6</td>
							<td>Amoxsan Paed Drops</td>
							<td></td>
							<td class="curr"><input type="text" name="obat[6]" value="<?=number_format(0,2)?>"></td>
							<td class="curr"><?=number_format(25162.50)?></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="span5" style="margin-left:1%">
				<?=form_open(base_url().'apotek/pembelian_langsung/create',array('class' => 'form-horizontal','id' => 'form')); ?>
				Catatan Resep<br/>
				<textarea rows=5 style="width:95%;"></textarea>
				<style>
					#harga td{
						vertical-align: middle;padding:4px;
					}
					#harga input{
						margin:0px;
						width:150px;
						text-align: right;
					}
				</style>
				<table id="harga">
					<tr>
						<td>Dokter</td><td><input type="text" disabled="disabled" value="<?=number_format(0,2)?>" /></td>
						<td>Tertanggung</td><td><input type="text" disabled="disabled" value="<?=number_format(0,2)?>" /></td>
					</tr>
					<tr>
						<td></td><td></td>
						<td>Total</td><td><input type="text" disabled="disabled" value="<?=number_format(0,2)?>" /></td>
					</tr>
				</table>
				<div style="text-align:right;margin-top:20px">
					<button class="btn btn-warning">Batal</button>
					<button class="btn btn-success">Simpan Saja</button>
					<button class="btn btn-primary">Simpan dan Bayar</button> <!-- Muncul struk-->
					<button class="btn btn-primary"><i class="icon-print"></i> Cetak</button>
					<a href="#myModal" id="cetak" role="button" class="btn hide" data-toggle="modal">Launch demo modal</a>
				</div>
				<?=form_close()?>
			</div>
		</div>
    </div>
</div>

<div id="myModal" style="height:500px;width:720px;margin-left:-320px" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="myModalLabel">Cetak Faktur</h3>
  </div>
  <div class="modal-body">
    <iframe src="" name="ifr" id="ifr" style="margin:0px;padding:0px;border:none;width:100%;height:90%"></iframe>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Keluar</button>
    <button id="cetakIframe" class="btn btn-primary">Cetak</button>
  </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(function(){
        $(".form-horizontal").submit(function(){
            var url  = $(this).attr('action');
            var data = $(this).serialize();
            $.post(url,data, function(data){
                $("#cetak").trigger('click');
				$("#ifr").attr('src',data);
            }); 
            return false;
        })

        $("#cetakIframe").click(function(){
            ifr.print();
            window.location = "<?=base_url()?>apotek/pembelian_langsung/select/medical_id";
        })
    })
</script>