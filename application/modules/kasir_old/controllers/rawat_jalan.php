<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rawat_jalan extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->cf = array(
				'modul_id'	=> 4
		);
	}

	public function index(){
		$data['main_view']	= 'rawat_jalan/index';
		$data['title']		= 'Kasir Rawat Jalan';
		$data['desc']		= 'Description. ';
		$data['cf']			=  $this->cf;
		$this->load->view('template',$data);
	}

	function select($medical_id){
		$data['main_view']	= 'rawat_jalan/transaksi';
		$data['title']		= 'Detail Transaksi';
		$cf					= $this->cf;
		$data['cf']			=  $cf;
		$data['insurance']	= $this->db->get('mst_insurance')->result();
		//$data['bill']		= $this->db->query("SELECT * from v_trx_bill4 where medic = '$medical_id'")->result();
		$a = $this->db->where('mdc_id',$medical_id)->get('trx_medical')->row();
		$data['patient']	= $this->db->where('sd_rekmed',$a->sd_rekmed)->get('ptn_social_data')->row();
		$data['medical_id'] = $medical_id;
		//parsing detail bill
		$mstr_bill = $this->db->get('mst_bill')->result();
		$trx_bill = $this->db->where('mdc_id',$medical_id)->get('trx_bill')->result();

		for($i=0;$i<count($mstr_bill);$i++){
			$bill[$i] = array();
		}
		//$bill = array();
		foreach($trx_bill as $trx){
			$bill[$trx->bill_id][] = $trx;
		}
		$tab = '';
		$hrf = 'a';
		$total_amount = 0;
		$tertanggung = 0;
		foreach ($mstr_bill as $i => $v){
			if(count($bill[$v->bill_id]) > 0){
				$tab .= "<tr><td>".$hrf.".</td><td colspan=3>".$v->bill_name."</td></tr>";
				foreach ($bill[$v->bill_id] as $j => $b) {
					$tab .= "<tr><td style='text-align:right'>".($j+1).".</td><td>".$b->desc."</td><td>".$b->amount."</td><td>".$b->tanggungan."</td></tr>";
					$total_amount += $b->amount;
					$tertanggung+= $b->tanggungan;
				}
				$hrf++;
			}
		}
		$data['bill_detail'] = $tab;
		$data['total'] = $total_amount;
		$data['tertanggung'] = $tertanggung;
		//end parsing detail bill
		$this->load->view('template',$data);	
	}

	function get_detail_transaksi($mdc_id,$bill_id){
		$data['detail'] = $this->db->query("select * from trx_bill where mdc_id = '$mdc_id' and bill_id ='$bill_id'")->result();
		$this->load->view('rawat_jalan/detil_transaksi',$data);

	}

	function create(){
		$data = $_POST;
		/*
		["mdc_id"]=>
		["note"]=>
		string(16) "sdasdasdasdasdsa"
		["total_amount"]=>
		string(9) "1,200,000"
		["total_pay"]=>
		string(9) "1,200,000"
		["doctor_fee"]=>
		string(1) "0"
		["earnest_pay"]=>
		string(1) "0"
		["nondoctor_fee"]=>
		string(1) "0"
		}*/
		$sd_rekmed = $this->input->post('sd_rekmed');
		foreach ($data as $i => $v) {
			if($i !== 'mdc_id' && $i !== 'note' && $i !== 'ins_no' && $i !== 'ins_id'){
				$data[$i] = preg_replace('/[^0-9]/', '', $v)/100;
			}
		}
		$data['mdc_status'] = 3;
		unset($data['sd_rekmed']);
		/*$this->db->insert('trx_medical_bill',$data)*/;
		$this->db->where('mdc_id', $data['mdc_id']);
		$this->db->update('trx_medical', $data); 
		$this->session->set_flashdata('message',array('success','Data berhasil di buat'));
		echo cur_url(-1)."cetak/".$data['mdc_id']."/".$sd_rekmed;
	} 

	function cetak($mdc_id, $sd_rekmed){
		$data['main_view']	= 'kasir/cetak_kasir';
		$data['patient']	= $this->db->where('sd_rekmed',$sd_rekmed)->get('ptn_social_data')->row();
		$data['detail']		= $this->db->where('medic',$mdc_id)->get('v_trx_bill4')->result();
		$data['total']		= $this->db->where('mdc_id',$mdc_id)->get('trx_medical')->row();
		$this->load->view('template_print',$data);
	}

	function get_medical($id = 2){
		$config['sTable'] 			= 'v_medical_bill';
		$config['aColumns'] 		= array('day_date','sd_name','pl_name','total_amount','total_pay');
		$config['key'] 				= 'mdc_id';
		$config['where'][]			= ("mdc_status = '$id'");
		$config['php_format'] 		= array('','','','money','money');
		$config['searchColumn'] 	= array('day_date','sd_name');
		$config['aksi'] = array(
								'stat'  	=> true,
								'key'		=> 'mdc_id',
								'pilih'	=> base_url().'kasir/rawat_jalan/select/',
								);
		init_datatable($config);
	}
}