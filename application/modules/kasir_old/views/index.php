aaa
<script type="text/javascript" charset="utf-8">
	$(function(){
		unicorn.peity();
	})
	unicorn = {
			// === Peity charts === //
			peity: function(){		
				$.fn.peity.defaults.line = {
					strokeWidth: 1,
					delimeter: ",",
					height: 24,
					max: null,
					min: 0,
					width: 50
				};
				$.fn.peity.defaults.bar = {
					delimeter: ",",
					height: 24,
					max: null,
					min: 0,
					width: 50
				};
				$(".peity_line_good span").peity("line", {
					colour: "#B1FFA9",
					strokeColour: "#459D1C"
				});
				$(".peity_line_bad span").peity("line", {
					colour: "#FFC4C7",
					strokeColour: "#BA1E20"
				});	
				$(".peity_line_neutral span").peity("line", {
					colour: "#CCCCCC",
					strokeColour: "#757575"
				});
				$(".peity_bar_good span").peity("bar", {
					colour: "#459D1C"
				});
				$(".peity_bar_bad span").peity("bar", {
					colour: "#BA1E20"
				});	
				$(".peity_bar_neutral span").peity("bar", {
					colour: "#757575"
				});
			},

			// === Tooltip for flot charts === //
			flot_tooltip: function(x, y, contents) {

				$('<div id="tooltip">' + contents + '</div>').css( {
					top: y + 5,
					left: x + 5
				}).appendTo("body").fadeIn(200);
			}
	}
</script>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="pageheader notab">
		    <h1 class="pagetitle"><?=$title?></h1>
		</div>
	</div>
	<div class="row-fluid">
        <div class="span6 center" style="text-align: center;">                 
			<ul class="quickstats">
				<li>
					<a href="<?=base_url()?>kasir/rawat_jalan"><img alt="" src="<?=base_url()?>assets/img/icons/medicoicons/prescription.png"><span>rawat jalan</span></a>
				</li>
				<li>
					<a href="<?=base_url()?>kasir/IGD"><img alt="" src="<?=base_url()?>assets/img/icons/medicoicons/ecg.png"><span>IGD</span></a>
				</li>
				<li>
					<a href="<?=base_url()?>kasir/rawat_inap"><img alt="" src="<?=base_url()?>assets/img/icons/medicoicons/hospital.png"><span>Rawat Inap</span></a>
				</li>
			</ul>
        </div>  
		<div class="span6 center" style="text-align: center;">					
			<div class="title" style="padding:0px"><h3>Transaksi Hari Ini</h3></div>
								<ul class="stat-boxes">
									<li>
										<div class="left peity_bar_good"><span><span style="display: none;">2,4,9,7,12,10,12</span><canvas width="50" height="24"></canvas></span>+20%</div>
										<div class="right">
											<strong>132</strong>
											Transaksi
										</div>
									</li>
									<li>
										<div class="left peity_bar_neutral"><span><span style="display: none;">20,15,18,14,10,9,9,9</span><canvas width="50" height="24"></canvas></span>0%</div>
										<div class="right">
											<strong>1433</strong>
											Terhutang
										</div>
									</li>
									<li>
										<div class="left peity_bar_bad"><span><span style="display: none;">3,5,9,7,12,20,10</span><canvas width="50" height="24"></canvas></span>-50%</div>
										<div class="right">
											<strong>8650</strong>
											Pelunasan
										</div>
									</li>
									<li>
										<div class="left peity_line_good"><span><span style="display: none;">12,6,9,23,14,10,17</span><canvas width="50" height="24"></canvas></span>+70%</div>
										<div class="right">
											<strong>8650</strong>
											Orders
										</div>
									</li>
								</ul>
							</div>
    </div>
</div>