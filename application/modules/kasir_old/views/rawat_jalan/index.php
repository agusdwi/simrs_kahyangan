
<style type="text/css" media="screen">
	.frm_search input{margin-top:-1px;float:right;margin-left:2px}
	.frm_search .btn{float:right;margin-left:5px}
	.frm_search .btn span{padding:2px 10px;}
	.search_choice a{font-weight:bold}
	.search_choice a.active{color:#fb9338}
	.search_choice {float:right;margin-right:95px}
	#advance{display:none}
	#filter{border-bottom: 1px dashed #DDD;}
	#body_search{margin-top:10px}
	.tname{font-size:110%}
	#dyntable tbody tr td{border-right:none}
	#dyntable tbody tr td + td + td{border-right:1px solid #DDD}
	#dyntable tr:nth-child(even){
		background:#F7F7F7;
	}
	.dataTables_scrollHead{
		margin-bottom: -22px;
	}
	.dataTables_info{
		margin-top: 20px;
	}
</style>
<script type="text/javascript" charset="utf-8">
	var oTb;
	$(function(){
		$(".search_choice a").click(function(){
			$(".active").removeClass('active');
			$(this).addClass('active');
			if($(this).attr('atr') == 'bsc'){
				$("#advance").hide();
				$("#basic").show();
				$(".mediuminput").focus();
			}else{
				$("#advance").show();
				$("#basic").hide();
				$(".smallinput").focus();
			}
			return false;
		})

		/*$('#dyntable').dataTable( {
			"sPaginationType": "bootstrap",
			"sScrollY": "350px",
			  "bFilter": false,
			"bPaginate": false,
	    });*/
		var d_uri = "<?=base_url()?>kasir/rawat_jalan/get_medical/";
		oTb = $('#dyntable').dataTable( {
			/*"bProcessing": true,
			"bServerSide": true,*/
			"sPaginationType": "bootstrap",
			/*"sScrollY": "350px",
			"bPaginate": false,
			*/
			
			"bLengthChange": false,
			"bPaginate": false,
			"bFilter": true,
			"aoColumns": [null,null,null,null,null,{ "bSortable": false }],//,
			"sAjaxSource": d_uri,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				var newArray = $.merge(aoData, [{ "name": "<?=$this->security->get_csrf_token_name()?>", "value": "<?=$this->security->get_csrf_hash()?>" }]);
				$.ajax( {
					"dataType": 'json',
					"type": "POST",
					"url": sSource,
					"data": aoData, 
					"success": fnCallback
				} );
			},
			"fnDrawCallback": function() {
				$("#dyntable tbody td:nth-child(1)").addClass('center');
				$("#dyntable tbody td:nth-child(1)").wrapInner('<i />');
				$("#dyntable tbody td:nth-child(3)").addClass('center');
				$("#dyntable tbody td:nth-child(4)").addClass('right');
				$("#dyntable tbody td:nth-child(5)").addClass('right');
			}
		});
		//$("#tb_medical_filter").hide();
		$(".chatsearch input").keyup(function(e){
			$("#dyntable_filter input").val($(this).val()).trigger('keyup');
		})

		$('#jantrian').change(function(){
			oTb.fnReloadAjax(d_uri+$(this).val());
		})
	})
</script>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="pageheader notab">
		    <h1 class="pagetitle"><?=$title?></h1>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span5" >
			<div class="title"><h3>Transaksi Pasien</h3></div>
		</div>
		<div class="span5" style="float:right;">
			<div class="widgetbox" style="margin-top:20px;">
				<?=form_open('',array('class'=>'frm_search'));?>
					<!-- <a href="" class="btn btn_orange btn_search radius50"><span>Search</span></a> -->
					<div id="basic">
						<div class="chatsearch" >
                        	<input type="text" name="" placeholder="Search" style="width:91%;margin:auto;">
                        	<br class="clear"/>
                        	<select id="jantrian">
                        		<option value="2" selected="selected">Antrian Aktif</option>
                        		<option value="3">Antrian Closed</option>
                        	</select>
                    	</div>
					</div>

					<!-- <div id="advance">
						<input type="text" class="smallinput" placeholder="masukkan alamat">
						<input type="text" class="smallinput" placeholder="masukkan nama pasien">
					</div>
					<br clear="all">
					<div class="search_choice">
						<a class="active" atr="bsc" href="#">sederhana</a> | <a atr="adv" href="#">pencarian lanjut</a>
					</div> -->
				</form>
			</div>
		</div>
	</div>
	<br clear="all">
	<div class="row-fluid">
		<div class="widgetbox">
			<div class="span12">
				<style>
					table#dyntable .center{
						text-align:center;
						font-size: 12px;
					}
					table#dyntable .right{
						text-align:right;
					}
					table#dyntable td,th{
						border: 1px solid #DDDDDD;
					}
				</style>
				<table class="table tb_scrol" id="dyntable">
				    <thead>
				        <tr>
				        	<th>Hari, Tanggal</th>
				            <th>Nama</th>
				            <th>Unit, Penyakit</th>
				            <th>Total Biaya</th>
				            <th>Total Tanggungan</th>
				            <th style="width:70px"class="head0">Aksi</th>
				        </tr>
				    </thead>
				</table>
			</div>
		</div>
	</div>
</div>
