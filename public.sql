/*
 Navicat Premium Data Transfer

 Source Server         : localhost [postgres]
 Source Server Type    : PostgreSQL
 Source Server Version : 90106
 Source Host           : localhost
 Source Database       : db_5des
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90106
 File Encoding         : utf-8

 Date: 12/08/2012 01:06:10 AM
*/

-- ----------------------------
--  Sequence structure for "com_code_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "com_code_seq";
CREATE SEQUENCE "com_code_seq" INCREMENT 1 START 3 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "com_code_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "com_menu__seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "com_menu__seq";
CREATE SEQUENCE "com_menu__seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "com_menu__seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "com_menu_role_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "com_menu_role_seq";
CREATE SEQUENCE "com_menu_role_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "com_menu_role_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "com_menu_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "com_menu_seq";
CREATE SEQUENCE "com_menu_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "com_menu_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "com_menus_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "com_menus_seq";
CREATE SEQUENCE "com_menus_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "com_menus_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "com_modul_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "com_modul_seq";
CREATE SEQUENCE "com_modul_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "com_modul_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "com_user_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "com_user_seq";
CREATE SEQUENCE "com_user_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "com_user_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "com_user_tipe_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "com_user_tipe_seq";
CREATE SEQUENCE "com_user_tipe_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "com_user_tipe_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "inv_item_master_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "inv_item_master_seq";
CREATE SEQUENCE "inv_item_master_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "inv_item_master_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "inv_item_type_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "inv_item_type_seq";
CREATE SEQUENCE "inv_item_type_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "inv_item_type_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "inv_mst_pos_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "inv_mst_pos_seq";
CREATE SEQUENCE "inv_mst_pos_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "inv_mst_pos_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "inv_purchase_order_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "inv_purchase_order_seq";
CREATE SEQUENCE "inv_purchase_order_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "inv_purchase_order_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "inv_req_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "inv_req_seq";
CREATE SEQUENCE "inv_req_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "inv_req_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_city_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_city_seq";
CREATE SEQUENCE "mst_city_seq" INCREMENT 1 START 29 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_city_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_class_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_class_seq";
CREATE SEQUENCE "mst_class_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_class_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_country_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_country_seq";
CREATE SEQUENCE "mst_country_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_country_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_currency_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_currency_seq";
CREATE SEQUENCE "mst_currency_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_currency_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_diagnosa_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_diagnosa_seq";
CREATE SEQUENCE "mst_diagnosa_seq" INCREMENT 1 START 2 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_diagnosa_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_education_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_education_seq";
CREATE SEQUENCE "mst_education_seq" INCREMENT 1 START 37 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_education_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_insurance_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_insurance_seq";
CREATE SEQUENCE "mst_insurance_seq" INCREMENT 1 START 4 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_insurance_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_medical_objective_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_medical_objective_seq";
CREATE SEQUENCE "mst_medical_objective_seq" INCREMENT 1 START 6 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_medical_objective_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_medical_subjective_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_medical_subjective_seq";
CREATE SEQUENCE "mst_medical_subjective_seq" INCREMENT 1 START 10 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_medical_subjective_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_nationality_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_nationality_seq";
CREATE SEQUENCE "mst_nationality_seq" INCREMENT 1 START 2 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_nationality_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_occupation_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_occupation_seq";
CREATE SEQUENCE "mst_occupation_seq" INCREMENT 1 START 11 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_occupation_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_payment_method_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_payment_method_seq";
CREATE SEQUENCE "mst_payment_method_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_payment_method_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_province_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_province_seq";
CREATE SEQUENCE "mst_province_seq" INCREMENT 1 START 36 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_province_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_regency_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_regency_seq";
CREATE SEQUENCE "mst_regency_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_regency_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_religion_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_religion_seq";
CREATE SEQUENCE "mst_religion_seq" INCREMENT 1 START 6 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_religion_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_room_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_room_seq";
CREATE SEQUENCE "mst_room_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_room_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_shift_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_shift_seq";
CREATE SEQUENCE "mst_shift_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_shift_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_sup_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_sup_seq";
CREATE SEQUENCE "mst_sup_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_sup_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_treathment_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_treathment_seq";
CREATE SEQUENCE "mst_treathment_seq" INCREMENT 1 START 2 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_treathment_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "mst_type_inv_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "mst_type_inv_seq";
CREATE SEQUENCE "mst_type_inv_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "mst_type_inv_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "ptn_family_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "ptn_family_seq";
CREATE SEQUENCE "ptn_family_seq" INCREMENT 1 START 4 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "ptn_family_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "sys_group_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "sys_group_seq";
CREATE SEQUENCE "sys_group_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "sys_group_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "sys_menu_role_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "sys_menu_role_seq";
CREATE SEQUENCE "sys_menu_role_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "sys_menu_role_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "sys_menu_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "sys_menu_seq";
CREATE SEQUENCE "sys_menu_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "sys_menu_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "sys_user_group_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "sys_user_group_seq";
CREATE SEQUENCE "sys_user_group_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "sys_user_group_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "sys_user_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "sys_user_seq";
CREATE SEQUENCE "sys_user_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "sys_user_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "trx_bill_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "trx_bill_seq";
CREATE SEQUENCE "trx_bill_seq" INCREMENT 1 START 32 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "trx_bill_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "trx_diagnosa_treathment_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "trx_diagnosa_treathment_seq";
CREATE SEQUENCE "trx_diagnosa_treathment_seq" INCREMENT 1 START 3 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "trx_diagnosa_treathment_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "trx_doctor_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "trx_doctor_seq";
CREATE SEQUENCE "trx_doctor_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "trx_doctor_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "trx_dr_schedule_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "trx_dr_schedule_seq";
CREATE SEQUENCE "trx_dr_schedule_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "trx_dr_schedule_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "trx_medical_objective_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "trx_medical_objective_seq";
CREATE SEQUENCE "trx_medical_objective_seq" INCREMENT 1 START 2 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "trx_medical_objective_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "trx_medical_subjective_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "trx_medical_subjective_seq";
CREATE SEQUENCE "trx_medical_subjective_seq" INCREMENT 1 START 3 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "trx_medical_subjective_seq" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for "trx_reference_seq"
-- ----------------------------
DROP SEQUENCE IF EXISTS "trx_reference_seq";
CREATE SEQUENCE "trx_reference_seq" INCREMENT 1 START 2 MAXVALUE 9223372036854775807 MINVALUE 0 CACHE 1;
ALTER TABLE "trx_reference_seq" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "mst_education"
-- ----------------------------
DROP TABLE IF EXISTS "mst_education";
CREATE TABLE "mst_education" (
	"med_id" int4 NOT NULL DEFAULT nextval('mst_education_seq'::regclass),
	"med_name" varchar(50),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_education" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_education"
-- ----------------------------
BEGIN;
INSERT INTO "mst_education" VALUES ('29', '-', null, null);
INSERT INTO "mst_education" VALUES ('30', 'TK', null, null);
INSERT INTO "mst_education" VALUES ('31', 'SD', null, null);
INSERT INTO "mst_education" VALUES ('32', 'SMP', null, null);
INSERT INTO "mst_education" VALUES ('33', 'SMA', null, null);
INSERT INTO "mst_education" VALUES ('34', 'D2', null, null);
INSERT INTO "mst_education" VALUES ('35', 'D3', null, null);
INSERT INTO "mst_education" VALUES ('36', 'S1', null, null);
INSERT INTO "mst_education" VALUES ('37', 'S2', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "com_user"
-- ----------------------------
DROP TABLE IF EXISTS "com_user";
CREATE TABLE "com_user" (
	"user_id" int4 NOT NULL DEFAULT nextval('com_user_seq'::regclass),
	"username" varchar(100) NOT NULL,
	"password" varchar(200) NOT NULL,
	"nama" varchar(100),
	"avatar" varchar(200),
	"user_tipe" int4 NOT NULL,
	"jenis_kelamin" varchar(200),
	"email" varchar(100),
	"last_login" timestamp(6) NOT NULL,
	"status" int4 NOT NULL DEFAULT 1
)
WITH (OIDS=FALSE);
ALTER TABLE "com_user" OWNER TO "postgres";

-- ----------------------------
--  Records of "com_user"
-- ----------------------------
BEGIN;
INSERT INTO "com_user" VALUES ('1', 'jike', '49deebdfb953a2f52e2ac0931cf29b72', 'jike', '', '1', 'Laki-laki', 'jike@yahoo.com', '2012-10-09 13:41:45', '1');
COMMIT;

-- ----------------------------
--  Table structure for "com_modul"
-- ----------------------------
DROP TABLE IF EXISTS "com_modul";
CREATE TABLE "com_modul" (
	"modul_id" int2 NOT NULL DEFAULT nextval('com_modul_seq'::regclass),
	"modul_nama" varchar(200) NOT NULL,
	"modul_url" varchar(200) NOT NULL,
	"modul_icon" varchar(200),
	"status" int2 NOT NULL DEFAULT 1
)
WITH (OIDS=FALSE);
ALTER TABLE "com_modul" OWNER TO "postgres";

-- ----------------------------
--  Records of "com_modul"
-- ----------------------------
BEGIN;
INSERT INTO "com_modul" VALUES ('1', 'pendaftaran', 'pendaftaran', '', '1');
INSERT INTO "com_modul" VALUES ('2', 'pelayanan informasi', 'pelayanan_informasi', '', '1');
INSERT INTO "com_modul" VALUES ('3', 'rawat jalan', 'rawat_jalan', '', '1');
INSERT INTO "com_modul" VALUES ('4', 'kasir', 'kasir', '', '1');
INSERT INTO "com_modul" VALUES ('5', 'apotek', 'apotek', '', '1');
INSERT INTO "com_modul" VALUES ('6', 'gudang farmasi', 'gudang_farmasi', '', '1');
COMMIT;

-- ----------------------------
--  Table structure for "mst_treathment"
-- ----------------------------
DROP TABLE IF EXISTS "mst_treathment";
CREATE TABLE "mst_treathment" (
	"treat_id" int4 NOT NULL DEFAULT nextval('mst_treathment_seq'::regclass),
	"treat_name" varchar(50),
	"treat_item_price" int8,
	"treat_paramedic_price" int8
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_treathment" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_treathment"
-- ----------------------------
BEGIN;
INSERT INTO "mst_treathment" VALUES ('1', 'Gibs', '20000', '1000');
INSERT INTO "mst_treathment" VALUES ('2', 'Platina', '50000', '2000');
COMMIT;

-- ----------------------------
--  Table structure for "inv_purchase_order_detail"
-- ----------------------------
DROP TABLE IF EXISTS "inv_purchase_order_detail";
CREATE TABLE "inv_purchase_order_detail" (
	"ipod_id" varchar(50) NOT NULL,
	"ipod_item" varchar(32) NOT NULL,
	"ipod_qty" numeric(10,2) NOT NULL,
	"ipod_sub" numeric(10,2) NOT NULL,
	"ipod_total" numeric(10,2) NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "inv_purchase_order_detail" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "mst_country"
-- ----------------------------
DROP TABLE IF EXISTS "mst_country";
CREATE TABLE "mst_country" (
	"mco_id" int4 NOT NULL DEFAULT nextval('mst_country_seq'::regclass),
	"mco_code" varchar(5),
	"mco_name" varchar(25),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_country" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_country"
-- ----------------------------
BEGIN;
INSERT INTO "mst_country" VALUES ('1', 'AD', 'Andorra', null, null);
INSERT INTO "mst_country" VALUES ('3', 'AG', 'Antigua and Barbuda', null, null);
INSERT INTO "mst_country" VALUES ('4', 'AL', 'Albania', null, null);
INSERT INTO "mst_country" VALUES ('5', 'AM', 'Armenia', null, null);
INSERT INTO "mst_country" VALUES ('6', 'AN', 'Netherlands Antilles', null, null);
INSERT INTO "mst_country" VALUES ('7', 'AR', 'Argentina', null, null);
INSERT INTO "mst_country" VALUES ('8', 'AT', 'Austria', null, null);
INSERT INTO "mst_country" VALUES ('9', 'AU', 'Australia', null, null);
INSERT INTO "mst_country" VALUES ('10', 'AW', 'Aruba', null, null);
INSERT INTO "mst_country" VALUES ('11', 'AZ', 'Azerbaijan', null, null);
INSERT INTO "mst_country" VALUES ('12', 'BA', 'Bosnia Herzegovina', null, null);
INSERT INTO "mst_country" VALUES ('13', 'BB', 'Barbados', null, null);
INSERT INTO "mst_country" VALUES ('14', 'BE', 'Belgium', null, null);
INSERT INTO "mst_country" VALUES ('15', 'BG', 'Bulgaria', null, null);
INSERT INTO "mst_country" VALUES ('16', 'BH', 'Bahrain', null, null);
INSERT INTO "mst_country" VALUES ('17', 'BJ', 'Benin', null, null);
INSERT INTO "mst_country" VALUES ('18', 'BM', 'Bermuda', null, null);
INSERT INTO "mst_country" VALUES ('19', 'BN', 'Brunei Darussalam', null, null);
INSERT INTO "mst_country" VALUES ('21', 'BO', 'Bolivia', null, null);
INSERT INTO "mst_country" VALUES ('22', 'BR', 'Brazil', null, null);
INSERT INTO "mst_country" VALUES ('23', 'BS', 'Bahamas', null, null);
INSERT INTO "mst_country" VALUES ('24', 'BW', 'Botswana', null, null);
INSERT INTO "mst_country" VALUES ('25', 'BY', 'Belarus', null, null);
INSERT INTO "mst_country" VALUES ('26', 'CA', 'Canada', null, null);
INSERT INTO "mst_country" VALUES ('27', 'CD', 'Congo (Democratic Republi', null, null);
INSERT INTO "mst_country" VALUES ('28', 'CG', 'Congo (Republic of)', null, null);
INSERT INTO "mst_country" VALUES ('29', 'CH', 'Switzerland', null, null);
INSERT INTO "mst_country" VALUES ('30', 'CK', 'Cook Islands', null, null);
INSERT INTO "mst_country" VALUES ('31', 'CL', 'Chile', null, null);
INSERT INTO "mst_country" VALUES ('32', 'CM', 'Cameroon', null, null);
INSERT INTO "mst_country" VALUES ('33', 'CN', 'China', null, null);
INSERT INTO "mst_country" VALUES ('34', 'CO', 'Colombia', null, null);
INSERT INTO "mst_country" VALUES ('35', 'CR', 'Costa Rica', null, null);
INSERT INTO "mst_country" VALUES ('36', 'CU', 'Cuba', null, null);
INSERT INTO "mst_country" VALUES ('37', 'CY', 'Cyprus', null, null);
INSERT INTO "mst_country" VALUES ('38', 'CZ', 'Czech Republic', null, null);
INSERT INTO "mst_country" VALUES ('39', 'DE', 'Germany', null, null);
INSERT INTO "mst_country" VALUES ('40', 'DK', 'Denmark', null, null);
INSERT INTO "mst_country" VALUES ('41', 'DO', 'Dominican Republic', null, null);
INSERT INTO "mst_country" VALUES ('42', 'EC', 'Ecuador', null, null);
INSERT INTO "mst_country" VALUES ('43', 'EE', 'Estonia', null, null);
INSERT INTO "mst_country" VALUES ('44', 'EG', 'Egypt', null, null);
INSERT INTO "mst_country" VALUES ('45', 'ES', 'Spain', null, null);
INSERT INTO "mst_country" VALUES ('46', 'ET', 'Ethiopia', null, null);
INSERT INTO "mst_country" VALUES ('47', 'FI', 'Finland', null, null);
INSERT INTO "mst_country" VALUES ('48', 'FJ', 'Fiji', null, null);
INSERT INTO "mst_country" VALUES ('49', 'FM', 'Micronesia', null, null);
INSERT INTO "mst_country" VALUES ('50', 'FR', 'France', null, null);
INSERT INTO "mst_country" VALUES ('51', 'GA', 'Gabon', null, null);
INSERT INTO "mst_country" VALUES ('52', 'GB', 'United Kingdom', null, null);
INSERT INTO "mst_country" VALUES ('53', 'GD', 'Grenada', null, null);
INSERT INTO "mst_country" VALUES ('54', 'GE', 'Georgia', null, null);
INSERT INTO "mst_country" VALUES ('55', 'GI', 'Gibraltar', null, null);
INSERT INTO "mst_country" VALUES ('56', 'GL', 'Greenland', null, null);
INSERT INTO "mst_country" VALUES ('57', 'GM', 'Gambia', null, null);
INSERT INTO "mst_country" VALUES ('58', 'GP', 'Guadeloupe', null, null);
INSERT INTO "mst_country" VALUES ('59', 'GR', 'Greece', null, null);
INSERT INTO "mst_country" VALUES ('60', 'GT', 'Guatemala', null, null);
INSERT INTO "mst_country" VALUES ('61', 'GU', 'Guam', null, null);
INSERT INTO "mst_country" VALUES ('62', 'HK', 'Hong Kong', null, null);
INSERT INTO "mst_country" VALUES ('63', 'HN', 'Honduras', null, null);
INSERT INTO "mst_country" VALUES ('64', 'HR', 'Croatia', null, null);
INSERT INTO "mst_country" VALUES ('65', 'HT', 'Haiti', null, null);
INSERT INTO "mst_country" VALUES ('66', 'HU', 'Hungary', null, null);
INSERT INTO "mst_country" VALUES ('68', 'ID', 'Indonesia', null, null);
INSERT INTO "mst_country" VALUES ('69', 'IE', 'Ireland (Republic of)', null, null);
INSERT INTO "mst_country" VALUES ('70', 'IL', 'Israel', null, null);
INSERT INTO "mst_country" VALUES ('71', 'IN', 'India', null, null);
INSERT INTO "mst_country" VALUES ('72', 'IQ', 'Iraq', null, null);
INSERT INTO "mst_country" VALUES ('73', 'IR', 'Iran', null, null);
INSERT INTO "mst_country" VALUES ('74', 'IS', 'Iceland', null, null);
INSERT INTO "mst_country" VALUES ('75', 'IT', 'Italy', null, null);
INSERT INTO "mst_country" VALUES ('76', 'JM', 'Jamaica', null, null);
INSERT INTO "mst_country" VALUES ('77', 'JO', 'Jordan', null, null);
INSERT INTO "mst_country" VALUES ('78', 'JP', 'Japan', null, null);
INSERT INTO "mst_country" VALUES ('79', 'KE', 'Kenya', null, null);
INSERT INTO "mst_country" VALUES ('80', 'KG', 'Kyrgyzstan', null, null);
INSERT INTO "mst_country" VALUES ('81', 'KH', 'Cambodia', null, null);
INSERT INTO "mst_country" VALUES ('82', 'KN', 'Saint Kitts and Nevis', null, null);
INSERT INTO "mst_country" VALUES ('83', 'KP', 'North Korea', null, null);
INSERT INTO "mst_country" VALUES ('84', 'KR', 'South Korea', null, null);
INSERT INTO "mst_country" VALUES ('85', 'KW', 'Kuwait', null, null);
INSERT INTO "mst_country" VALUES ('86', 'KY', 'Cayman Islands', null, null);
INSERT INTO "mst_country" VALUES ('87', 'KZ', 'Kazakhstan', null, null);
INSERT INTO "mst_country" VALUES ('88', 'LA', 'Laos', null, null);
INSERT INTO "mst_country" VALUES ('89', 'LB', 'Lebanon', null, null);
INSERT INTO "mst_country" VALUES ('90', 'LC', 'Saint Lucia', null, null);
INSERT INTO "mst_country" VALUES ('91', 'LI', 'Liechtenstein', null, null);
INSERT INTO "mst_country" VALUES ('92', 'LK', 'Sri Lanka', null, null);
INSERT INTO "mst_country" VALUES ('93', 'LT', 'Lithuania', null, null);
INSERT INTO "mst_country" VALUES ('94', 'LU', 'Luxembourg', null, null);
INSERT INTO "mst_country" VALUES ('95', 'LV', 'Latvia', null, null);
INSERT INTO "mst_country" VALUES ('96', 'LY', 'Libya', null, null);
INSERT INTO "mst_country" VALUES ('97', 'MA', 'Morocco', null, null);
INSERT INTO "mst_country" VALUES ('98', 'MC', 'Monaco', null, null);
INSERT INTO "mst_country" VALUES ('99', 'MD', 'Moldova', null, null);
INSERT INTO "mst_country" VALUES ('100', 'ME', 'Montenegro', null, null);
INSERT INTO "mst_country" VALUES ('101', 'MF', 'Saint Martin (French part', null, null);
INSERT INTO "mst_country" VALUES ('102', 'MK', 'Macedonia', null, null);
INSERT INTO "mst_country" VALUES ('103', 'ML', 'Mali', null, null);
INSERT INTO "mst_country" VALUES ('104', 'MM', 'Myanmar', null, null);
INSERT INTO "mst_country" VALUES ('105', 'MN', 'Mongolia', null, null);
INSERT INTO "mst_country" VALUES ('106', 'MO', 'Macau', null, null);
INSERT INTO "mst_country" VALUES ('107', 'MP', 'Northern Mariana Islands', null, null);
INSERT INTO "mst_country" VALUES ('108', 'MT', 'Malta', null, null);
INSERT INTO "mst_country" VALUES ('109', 'MU', 'Mauritius', null, null);
INSERT INTO "mst_country" VALUES ('110', 'MV', 'Maldives', null, null);
INSERT INTO "mst_country" VALUES ('111', 'MX', 'Mexico', null, null);
INSERT INTO "mst_country" VALUES ('113', 'MY', 'Malaysia', null, null);
INSERT INTO "mst_country" VALUES ('114', 'MZ', 'Mozambique', null, null);
INSERT INTO "mst_country" VALUES ('115', 'NA', 'Namibia', null, null);
INSERT INTO "mst_country" VALUES ('116', 'NC', 'New Caledonia', null, null);
INSERT INTO "mst_country" VALUES ('117', 'NF', 'Norfolk Island', null, null);
INSERT INTO "mst_country" VALUES ('118', 'NG', 'Nigeria', null, null);
INSERT INTO "mst_country" VALUES ('119', 'NI', 'Nicaragua', null, null);
INSERT INTO "mst_country" VALUES ('120', 'NL', 'Netherlands', null, null);
INSERT INTO "mst_country" VALUES ('121', 'NO', 'Norway', null, null);
INSERT INTO "mst_country" VALUES ('122', 'NP', 'Nepal', null, null);
INSERT INTO "mst_country" VALUES ('123', 'NZ', 'New Zealand', null, null);
INSERT INTO "mst_country" VALUES ('124', 'OM', 'Oman', null, null);
INSERT INTO "mst_country" VALUES ('125', 'PA', 'Panama', null, null);
INSERT INTO "mst_country" VALUES ('126', 'PE', 'Peru', null, null);
INSERT INTO "mst_country" VALUES ('127', 'PF', 'French Polynesia', null, null);
INSERT INTO "mst_country" VALUES ('128', 'PH', 'Philippines', null, null);
INSERT INTO "mst_country" VALUES ('129', 'PK', 'Pakistan', null, null);
INSERT INTO "mst_country" VALUES ('130', 'PL', 'Poland', null, null);
INSERT INTO "mst_country" VALUES ('131', 'PR', 'Puerto Rico', null, null);
INSERT INTO "mst_country" VALUES ('132', 'PT', 'Portugal', null, null);
INSERT INTO "mst_country" VALUES ('133', 'PW', 'Palau', null, null);
INSERT INTO "mst_country" VALUES ('134', 'PY', 'Paraguay', null, null);
INSERT INTO "mst_country" VALUES ('135', 'QA', 'Qatar', null, null);
INSERT INTO "mst_country" VALUES ('136', 'RE', 'Reunion', null, null);
INSERT INTO "mst_country" VALUES ('137', 'RO', 'Romania', null, null);
INSERT INTO "mst_country" VALUES ('138', 'RS', 'Serbia', null, null);
INSERT INTO "mst_country" VALUES ('139', 'RU', 'Russia', null, null);
INSERT INTO "mst_country" VALUES ('140', 'SA', 'Saudi Arabia', null, null);
INSERT INTO "mst_country" VALUES ('141', 'SC', 'Seychelles', null, null);
INSERT INTO "mst_country" VALUES ('142', 'SE', 'Sweden', null, null);
INSERT INTO "mst_country" VALUES ('143', 'SG', 'Singapore', null, null);
INSERT INTO "mst_country" VALUES ('144', 'SH', 'Saint Helena, Ascension a', null, null);
INSERT INTO "mst_country" VALUES ('145', 'SI', 'Slovenia', null, null);
INSERT INTO "mst_country" VALUES ('146', 'SK', 'Slovakia', null, null);
INSERT INTO "mst_country" VALUES ('147', 'SM', 'San Marino (Republic of)', null, null);
INSERT INTO "mst_country" VALUES ('148', 'SN', 'Senegal', null, null);
INSERT INTO "mst_country" VALUES ('149', 'SY', 'Syria', null, null);
INSERT INTO "mst_country" VALUES ('150', 'SZ', 'Swaziland', null, null);
INSERT INTO "mst_country" VALUES ('151', 'TC', 'Turks and Caicos Islands', null, null);
INSERT INTO "mst_country" VALUES ('152', 'TG', 'Togo', null, null);
INSERT INTO "mst_country" VALUES ('153', 'TH', 'Thailand', null, null);
INSERT INTO "mst_country" VALUES ('154', 'TJ', 'Tajikistan', null, null);
INSERT INTO "mst_country" VALUES ('155', 'TM', 'Turkmenistan', null, null);
INSERT INTO "mst_country" VALUES ('156', 'TN', 'Tunisia', null, null);
INSERT INTO "mst_country" VALUES ('157', 'TO', 'Tonga', null, null);
INSERT INTO "mst_country" VALUES ('158', 'TR', 'Turkey', null, null);
INSERT INTO "mst_country" VALUES ('159', 'TT', 'Trinidad & Tobago', null, null);
INSERT INTO "mst_country" VALUES ('160', 'TW', 'Taiwan', null, null);
INSERT INTO "mst_country" VALUES ('161', 'TZ', 'Tanzania', null, null);
INSERT INTO "mst_country" VALUES ('162', 'UA', 'Ukraine', null, null);
INSERT INTO "mst_country" VALUES ('163', 'UG', 'Uganda', null, null);
INSERT INTO "mst_country" VALUES ('164', 'US', 'United States', null, null);
INSERT INTO "mst_country" VALUES ('165', 'UY', 'Uruguay', null, null);
INSERT INTO "mst_country" VALUES ('166', 'UZ', 'Uzbekistan', null, null);
INSERT INTO "mst_country" VALUES ('167', 'VC', 'Saint Vincent and the Gre', null, null);
INSERT INTO "mst_country" VALUES ('168', 'VE', 'Venezuela', null, null);
INSERT INTO "mst_country" VALUES ('169', 'VG', 'British Virgin Islands', null, null);
INSERT INTO "mst_country" VALUES ('170', 'VI', 'Virgin Islands (USA)', null, null);
INSERT INTO "mst_country" VALUES ('171', 'VN', 'Vietnam', null, null);
INSERT INTO "mst_country" VALUES ('172', 'VU', 'Vanuatu', null, null);
INSERT INTO "mst_country" VALUES ('173', 'WS', 'Samoa', null, null);
INSERT INTO "mst_country" VALUES ('174', 'YE', 'Yemen Republic', null, null);
INSERT INTO "mst_country" VALUES ('175', 'ZA', 'South Africa', null, null);
INSERT INTO "mst_country" VALUES ('176', 'ZM', 'Zambia', null, null);
INSERT INTO "mst_country" VALUES ('177', 'ZW', 'Zimbabwe', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "sys_user_group"
-- ----------------------------
DROP TABLE IF EXISTS "sys_user_group";
CREATE TABLE "sys_user_group" (
	"user_group_id" int4 NOT NULL DEFAULT nextval('sys_user_group_seq'::regclass),
	"user_id" int4,
	"group_id" int4,
	"modi_id" varchar(45),
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "sys_user_group" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "trx_dr_schedule"
-- ----------------------------
DROP TABLE IF EXISTS "trx_dr_schedule";
CREATE TABLE "trx_dr_schedule" (
	"sch_id" int8 NOT NULL DEFAULT nextval('trx_dr_schedule_seq'::regclass),
	"dr_id" int8 NOT NULL,
	"sch_shift" varchar(45) NOT NULL,
	"sch_time_start" timestamp(6) NOT NULL,
	"sch_time_end" timestamp(6) NOT NULL,
	"pl_id" int4 NOT NULL,
	"modi_id" int2 NOT NULL,
	"modi_datetime" timestamp(6) NOT NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_dr_schedule" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_dr_schedule"
-- ----------------------------
BEGIN;
INSERT INTO "trx_dr_schedule" VALUES ('1', '1', '3', '2012-11-22 12:00:00', '2012-12-23 20:00:00', '2', '2', '2012-11-22 22:45:30.759');
INSERT INTO "trx_dr_schedule" VALUES ('2', '23', '2', '2012-11-23 12:00:00', '2012-12-23 20:00:00', '1', '2', '2012-11-22 22:57:33.227');
INSERT INTO "trx_dr_schedule" VALUES ('3', '12', '1', '2012-11-21 12:00:00', '2012-12-23 20:00:00', '1', '2', '2012-11-22 22:57:55.638');
INSERT INTO "trx_dr_schedule" VALUES ('23', '7', '1', '2012-11-29 00:01:00', '2012-11-29 08:00:00', '1', '1', '2012-11-25 23:03:04.236');
INSERT INTO "trx_dr_schedule" VALUES ('24', '54', '1', '2012-11-29 00:01:00', '2012-11-29 08:00:00', '1', '1', '2012-11-25 23:03:39.747');
INSERT INTO "trx_dr_schedule" VALUES ('25', '54', '1', '2012-11-29 00:01:00', '2012-11-29 08:00:00', '1', '1', '2012-11-25 23:04:51.388');
INSERT INTO "trx_dr_schedule" VALUES ('26', '54', '1', '2012-11-29 00:01:00', '2012-11-29 08:00:00', '1', '1', '2012-11-25 23:05:18.721');
INSERT INTO "trx_dr_schedule" VALUES ('27', '7', '2', '2012-11-28 08:00:00', '2012-11-28 16:00:00', '2', '1', '2012-11-25 23:12:31.41');
INSERT INTO "trx_dr_schedule" VALUES ('28', '35', '2', '2012-11-29 08:00:00', '2012-11-29 16:00:00', '2', '1', '2012-11-25 23:15:17.615');
INSERT INTO "trx_dr_schedule" VALUES ('29', '37', '2', '2012-11-30 08:00:00', '2012-11-30 16:00:00', '1', '1', '2012-11-25 23:32:45.676');
INSERT INTO "trx_dr_schedule" VALUES ('30', '9', '2', '2012-11-29 08:00:00', '2012-11-29 16:00:00', '2', '1', '2012-11-26 07:31:24.157');
INSERT INTO "trx_dr_schedule" VALUES ('34', '3', '2', '2012-11-28 08:00:00', '2012-11-28 16:00:00', '2', '1', '2012-11-25 22:36:20.598');
COMMIT;

-- ----------------------------
--  Table structure for "trx_poly"
-- ----------------------------
DROP TABLE IF EXISTS "trx_poly";
CREATE TABLE "trx_poly" (
	"pl_id" int4 NOT NULL,
	"pl_name" varchar(45),
	"pl_description" text,
	"pl_status" varchar(45),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_poly" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_poly"
-- ----------------------------
BEGIN;
INSERT INTO "trx_poly" VALUES ('1', 'Poli Gigi', null, '1', null, null);
INSERT INTO "trx_poly" VALUES ('2', 'Poli Tulang', null, '1', null, null);
INSERT INTO "trx_poly" VALUES ('3', 'Poli Gizi', null, '1', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "trx_medical"
-- ----------------------------
DROP TABLE IF EXISTS "trx_medical";
CREATE TABLE "trx_medical" (
	"mdc_id" varchar(10) NOT NULL,
	"sd_rekmed" varchar(10),
	"mdc_in" timestamp(6) NULL,
	"mdc_out" timestamp(6) NULL,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL,
	"mdc_status" int2,
	"pl_id" int2,
	"dr_id" int8,
	"total_amount" int4 DEFAULT 0,
	"total_pay" int4 DEFAULT 0,
	"doctor_fee" int4 DEFAULT 0,
	"earnest_pay" int4 DEFAULT 0,
	"note" text,
	"patient_pay" int4 DEFAULT 0,
	"ins_id" int2,
	"ins_no" varchar,
	"recipe_status" int2 DEFAULT 0,
	"poly_id" int2
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_medical" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_medical"
-- ----------------------------
BEGIN;
INSERT INTO "trx_medical" VALUES ('12110004', '1211012', '2012-12-05 10:42:55', null, null, null, '2', '2', '3', '57000', '57000', '0', '0', null, '0', null, null, '0', null);
INSERT INTO "trx_medical" VALUES ('12110005', '1211012', '2012-12-05 10:42:55', null, null, null, '2', '2', '3', '57000', '57000', '0', '0', null, '0', null, null, '0', null);
INSERT INTO "trx_medical" VALUES ('12110006', '1211012', '2012-12-05 10:42:55', null, null, null, '2', '2', '3', '57000', '57000', '0', '0', null, '0', null, null, '0', null);
INSERT INTO "trx_medical" VALUES ('12110007', '1211012', '2012-12-05 10:42:55', null, null, null, '2', '2', '3', '57000', '57000', '0', '0', null, '0', null, null, '0', null);
INSERT INTO "trx_medical" VALUES ('12110008', '1211012', '2012-12-05 10:42:55', null, null, null, '2', '2', '3', '57000', '57000', '0', '0', null, '0', null, null, '0', null);
INSERT INTO "trx_medical" VALUES ('12110009', '1211012', '2012-12-05 10:42:55', null, null, null, '2', '2', '3', '57000', '57000', '0', '0', null, '0', null, null, '0', null);
INSERT INTO "trx_medical" VALUES ('12110010', '1211012', '2012-12-05 10:42:55', null, null, null, '2', '2', '3', '57000', '57000', '0', '0', null, '0', null, null, '0', null);
INSERT INTO "trx_medical" VALUES ('12120005', '1211005', '2012-12-05 05:04:58', null, null, null, '1', '2', '1', '0', '0', '0', '0', null, '0', null, null, '0', null);
INSERT INTO "trx_medical" VALUES ('12110002', '1211004', '2012-12-05 08:48:22', null, null, null, '3', '2', '3', '89000', '89000', '0', '0', '', '100000', '0', '', '2', null);
INSERT INTO "trx_medical" VALUES ('12110001', '1211003', '2012-12-05 10:32:43', null, null, null, '1', '2', '1', '0', '0', '0', '0', null, '0', null, null, '2', null);
INSERT INTO "trx_medical" VALUES ('12110003', '1211012', '2012-11-29 06:07:03', null, null, null, '3', '2', '3', '57000', '57000', '0', '0', '', '0', '0', '', '0', null);
COMMIT;

-- ----------------------------
--  Table structure for "trx_diagnosa_treathment_detail"
-- ----------------------------
DROP TABLE IF EXISTS "trx_diagnosa_treathment_detail";
CREATE TABLE "trx_diagnosa_treathment_detail" (
	"mdc_id" varchar(45) NOT NULL,
	"dat_id" varchar(45) NOT NULL,
	"dat_case" varchar(10),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL,
	"dat_diag" int4,
	"dat_treat" int4,
	"dat_paramedic_price" int4,
	"dat_item_price" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_diagnosa_treathment_detail" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_diagnosa_treathment_detail"
-- ----------------------------
BEGIN;
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12110001', 'DA-12120002', 'new', null, null, '1', '2', '2000', '50000');
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12110003', 'DA-12120001', 'new', null, null, '1', '2', '2000', '50000');
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12110003', 'DA-12120001', 'new', null, null, '2', '2', '2000', '50000');
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12110004', 'DA-12120001', 'new', null, null, '4', '2', '2000', '50000');
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12110005', 'DA-12120001', 'new', null, null, '5', '2', '2000', '50000');
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12110006', 'DA-12120001', 'new', null, null, '6', '2', '2000', '50000');
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12110007', 'DA-12120001', 'new', null, null, '5', '2', '2000', '50000');
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12110008', 'DA-12120001', 'new', null, null, '5', '2', '2000', '50000');
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12110009', 'DA-12120001', 'new', null, null, '6', '2', '2000', '50000');
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12110001', 'DA-12120001', 'new', null, null, '1', '2', '2000', '50000');
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12120005', 'DA-12120003', 'new', null, null, '2', '2', '2000', '50000');
INSERT INTO "trx_diagnosa_treathment_detail" VALUES ('12110002', 'DA-12120004', 'new', null, null, '2', '2', '2000', '50000');
COMMIT;

-- ----------------------------
--  Table structure for "sys_group"
-- ----------------------------
DROP TABLE IF EXISTS "sys_group";
CREATE TABLE "sys_group" (
	"group_id" int4 NOT NULL DEFAULT nextval('sys_group_seq'::regclass),
	"group_name" varchar(45),
	"group_status" int4,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "sys_group" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "com_menu_role"
-- ----------------------------
DROP TABLE IF EXISTS "com_menu_role";
CREATE TABLE "com_menu_role" (
	"role_id" int4 NOT NULL DEFAULT nextval('com_menu_role_seq'::regclass),
	"group_id" int4 NOT NULL,
	"user_tipe_id" int4 NOT NULL,
	"view" int4 NOT NULL DEFAULT 1
)
WITH (OIDS=FALSE);
ALTER TABLE "com_menu_role" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "mst_type_inv"
-- ----------------------------
DROP TABLE IF EXISTS "mst_type_inv";
CREATE TABLE "mst_type_inv" (
	"mtype_id" int4 NOT NULL DEFAULT nextval('mst_type_inv_seq'::regclass),
	"mtype_name" varchar(50),
	"mtype_status" int4 DEFAULT 1,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_type_inv" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "trx_bill"
-- ----------------------------
DROP TABLE IF EXISTS "trx_bill";
CREATE TABLE "trx_bill" (
	"trx_bill_id" int4 NOT NULL DEFAULT nextval('trx_bill_seq'::regclass),
	"ptn_id" varchar(10),
	"mdc_id" varchar(10),
	"modi_id" varchar(5),
	"modi_datetime" timestamp(6) NULL,
	"bill_id" varchar(10),
	"date" timestamp(6) NULL,
	"desc" varchar(100),
	"class_id" varchar(5),
	"amount" int4,
	"total" int4,
	"tanggungan" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_bill" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_bill"
-- ----------------------------
BEGIN;
INSERT INTO "trx_bill" VALUES ('28', null, '12110003', null, null, '5', null, 'obat pertama  (OB1) x2 (3x1)', null, '5000', null, '5000');
INSERT INTO "trx_bill" VALUES ('29', null, '12110003', null, null, '6', null, 'tulang sakit - Platina (new)', null, '52000', null, '52000');
INSERT INTO "trx_bill" VALUES ('30', null, '12110002', null, null, '5', null, 'obat pertama  (OB1) x10 (3 x 1)', null, '21000', null, '21000');
INSERT INTO "trx_bill" VALUES ('31', null, '12110002', null, null, '5', null, 'obat kedua  (OB2) x5 (2 x 1)', null, '16000', null, '16000');
INSERT INTO "trx_bill" VALUES ('32', null, '12110002', null, null, '6', null, 'tulang keram - Platina (new)', null, '52000', null, '52000');
COMMIT;

-- ----------------------------
--  Table structure for "mst_payment_method"
-- ----------------------------
DROP TABLE IF EXISTS "mst_payment_method";
CREATE TABLE "mst_payment_method" (
	"mpm_id" varchar(20) NOT NULL DEFAULT nextval('mst_payment_method_seq'::regclass),
	"mpm_name" varchar(45),
	"mpm_bank_id" varchar(20),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_payment_method" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "mst_class"
-- ----------------------------
DROP TABLE IF EXISTS "mst_class";
CREATE TABLE "mst_class" (
	"mscl_id" int4 NOT NULL,
	"mscl_name" varchar(50),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_class" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "mst_currency"
-- ----------------------------
DROP TABLE IF EXISTS "mst_currency";
CREATE TABLE "mst_currency" (
	"mc_id" varchar(50) NOT NULL,
	"mc_name" varchar(50),
	"mc_value" numeric(8,2),
	"mc_status" int4 DEFAULT 1,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_currency" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "mst_medical_subjective"
-- ----------------------------
DROP TABLE IF EXISTS "mst_medical_subjective";
CREATE TABLE "mst_medical_subjective" (
	"ms_id" int2 NOT NULL DEFAULT nextval('mst_medical_subjective_seq'::regclass),
	"ms_indication" varchar(45),
	"ms_filter" char(1),
	"modi_id" int2,
	"modi_datetime" time(6),
	"pl_id" int2
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_medical_subjective" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_medical_subjective"
-- ----------------------------
BEGIN;
INSERT INTO "mst_medical_subjective" VALUES ('6', 'Tulang Membengkak', 'S', null, null, '2');
INSERT INTO "mst_medical_subjective" VALUES ('7', 'Patah Tulang', 'S', null, null, '2');
INSERT INTO "mst_medical_subjective" VALUES ('8', 'Tulang Robek', 'S', null, null, '2');
INSERT INTO "mst_medical_subjective" VALUES ('9', 'Tulang Kering Luka', 'S', null, null, '2');
INSERT INTO "mst_medical_subjective" VALUES ('10', 'Gigi Berlubang', 'S', null, null, '1');
COMMIT;

-- ----------------------------
--  Table structure for "mst_medicine"
-- ----------------------------
DROP TABLE IF EXISTS "mst_medicine";
CREATE TABLE "mst_medicine" (
	"mdcn_id" varchar(45) NOT NULL,
	"mdcn_name" varchar(100),
	"mdcn_price" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_medicine" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_medicine"
-- ----------------------------
BEGIN;
INSERT INTO "mst_medicine" VALUES ('OB1', 'obat pertama', '10000');
INSERT INTO "mst_medicine" VALUES ('OB2', 'obat kedua', '20000');
COMMIT;

-- ----------------------------
--  Table structure for "trx_medical_objective"
-- ----------------------------
DROP TABLE IF EXISTS "trx_medical_objective";
CREATE TABLE "trx_medical_objective" (
	"mo_id" int4 NOT NULL DEFAULT nextval('trx_medical_objective_seq'::regclass),
	"mdc_id" varchar(10) NOT NULL,
	"mo_value" varchar(45),
	"mo_desc" text,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_medical_objective" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_medical_objective"
-- ----------------------------
BEGIN;
INSERT INTO "trx_medical_objective" VALUES ('1', '12110001', null, 'kajian objective', null, null);
INSERT INTO "trx_medical_objective" VALUES ('2', '12110001', null, '', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "com_menu"
-- ----------------------------
DROP TABLE IF EXISTS "com_menu";
CREATE TABLE "com_menu" (
	"menu_id" int2 NOT NULL DEFAULT nextval('com_menu_seq'::regclass),
	"modul_id" int2 NOT NULL,
	"menu_nama" varchar(200) NOT NULL,
	"menu_url" varchar(200) NOT NULL,
	"menu_icon" varchar(200),
	"menu_parent" varchar(200),
	"status" int2 NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "com_menu" OWNER TO "postgres";

-- ----------------------------
--  Records of "com_menu"
-- ----------------------------
BEGIN;
INSERT INTO "com_menu" VALUES ('1', '1', 'pendaftaran baru', 'pendaftaran/pendaftaran_baru', null, '', '1');
INSERT INTO "com_menu" VALUES ('2', '1', 'pendaftaran rawat jalan', 'pendaftaran/pendaftaran_rawat_jalan', null, '', '1');
INSERT INTO "com_menu" VALUES ('3', '1', 'pendaftaran rawat inap', 'pendaftaran/pendaftaran_rawat_inap', null, '', '1');
INSERT INTO "com_menu" VALUES ('4', '1', 'IGD', 'pendaftaran/IGD', null, '', '1');
INSERT INTO "com_menu" VALUES ('5', '2', 'informasi pasien', 'pelayanan_informasi/informasi_pasien', null, '', '1');
INSERT INTO "com_menu" VALUES ('6', '2', 'jadwal dokter', 'pelayanan_informasi/jadwal_dokter', null, '', '1');
INSERT INTO "com_menu" VALUES ('7', '2', 'informasi kamar', 'pelayanan_informasi/informasi_kamar', null, '', '1');
INSERT INTO "com_menu" VALUES ('8', '2', 'informasi paket', 'pelayanan_informasi/informasi_paket', null, '', '1');
INSERT INTO "com_menu" VALUES ('9', '3', 'poli gigi', 'rawat_jalan/poli_gigi', null, '', '1');
INSERT INTO "com_menu" VALUES ('10', '3', 'poli tulang', 'rawat_jalan/poli_tulang', null, '', '1');
INSERT INTO "com_menu" VALUES ('11', '4', 'rawat jalan', 'kasir/rawat_jalan', null, '', '1');
INSERT INTO "com_menu" VALUES ('12', '4', 'IGD', 'kasir/IGD', null, '', '1');
INSERT INTO "com_menu" VALUES ('13', '4', 'rawat inap', 'kasir/rawat_inap', null, '', '1');
INSERT INTO "com_menu" VALUES ('14', '5', 'resep pasien', 'apotek/resep_pasien', null, '', '1');
INSERT INTO "com_menu" VALUES ('15', '5', 'pembelian lansung', 'apotek/pembelian_langsung', null, '', '1');
INSERT INTO "com_menu" VALUES ('16', '6', 'purchase request', 'gudang_farmasi/purchase_request', null, '', '1');
INSERT INTO "com_menu" VALUES ('17', '6', 'purchase order', 'gudang_farmasi/purchase_order', null, '', '1');
INSERT INTO "com_menu" VALUES ('18', '6', 'receive item', 'gudang_farmasi/receive_item', null, '', '1');
INSERT INTO "com_menu" VALUES ('19', '6', 'retur', 'gudang_farmasi/retur', null, '', '1');
INSERT INTO "com_menu" VALUES ('20', '6', 'stok', 'gudang_farmasi/stok', null, '', '1');
INSERT INTO "com_menu" VALUES ('21', '6', 'transfer item', 'gudang_farmasi/transfer_item', null, '', '1');
COMMIT;

-- ----------------------------
--  Table structure for "ptn_social_data"
-- ----------------------------
DROP TABLE IF EXISTS "ptn_social_data";
CREATE TABLE "ptn_social_data" (
	"sd_rekmed" varchar(10) NOT NULL,
	"sd_name" varchar(100) NOT NULL,
	"sd_sex" varchar(255),
	"sd_place_of_birth" varchar(150),
	"sd_date_of_birth" varchar(32),
	"sd_age" varchar(32),
	"sd_blood_tp" varchar(255),
	"sd_address" text,
	"sd_rt_rw" varchar(10),
	"sd_reg_desa" varchar(50),
	"sd_reg_kec" varchar(50),
	"sd_reg_kab" varchar(100),
	"sd_reg_prov" varchar(100),
	"sd_citizen" varchar(100),
	"sd_marital_st" varchar(50),
	"sd_religion" varchar(25),
	"sd_education" varchar(100),
	"sd_occupation" varchar(100),
	"sd_telp" varchar(20),
	"sd_reg_date" timestamp(6) NULL,
	"sd_status" int4 DEFAULT 1,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL,
	"sd_reg_street" varchar(100)
)
WITH (OIDS=FALSE);
ALTER TABLE "ptn_social_data" OWNER TO "postgres";

-- ----------------------------
--  Records of "ptn_social_data"
-- ----------------------------
BEGIN;
INSERT INTO "ptn_social_data" VALUES ('1211003', 'Budi', 'l', '', null, '20', null, 'Umbulharjo', 'RT 1 RW 11', null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211004', 'Oryza Nur Fajri', 'l', '', null, '30', null, 'Sleman', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211005', 'Abenture', 'l', '', null, '40', null, 'Sleman', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211006', 'Ahmad Darmadi', 'l', '', null, '50', null, 'Umbulharjo', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211007', 'Feri How', 'l', '', null, null, null, 'Gunung Kidul', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211008', 'Ahmad Darmadi', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211009', 'Abenture', 'l', '', null, null, null, 'Jogjakarta', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211010', 'Ahmad Darmadi', 'l', '', null, null, null, 'Jogjakarta', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211011', 'Oryza Nur Fajri', 'l', '', null, null, null, 'Sleman', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211012', 'Ahmad Darmadi', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211013', 'Ahmad Darmadi', 'l', '', null, null, null, 'Umbulharjo', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211014', 'Budi', 'l', '', null, null, null, 'Jogjakarta', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211015', 'Oryza Nur Fajri', 'l', '', null, null, null, 'Jogjakarta', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211016', 'Feri How', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211017', 'Ahmad Darmadi', 'l', '', null, null, null, 'Gunung Kidul', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211018', 'Oryza Nur Fajri', 'l', '', null, null, null, 'Sleman', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211019', 'Abenture', 'l', '', null, null, null, 'Umbulharjo', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211020', 'Feri How', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211021', 'Ahmad Darmadi', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211022', 'Budi', 'l', '', null, null, null, 'Jogjakarta', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211023', 'Budi', 'l', '', null, null, null, 'Umbulharjo', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211024', 'Budi', 'l', '', null, null, null, 'Gunung Kidul', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211025', 'Budi', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211026', 'Feri How', 'l', '', null, null, null, 'Gunung Kidul', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211027', 'Budi', 'l', '', null, null, null, 'Gunung Kidul', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211028', 'Ahmad Darmadi', 'l', '', null, null, null, 'Gunung Kidul', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211029', 'Abenture', 'l', '', null, null, null, 'Gunung Kidul', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211030', 'Ahmad Darmadi', 'l', '', null, null, null, 'Umbulharjo', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211031', 'Oryza Nur Fajri', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211032', 'Budi', 'l', '', null, null, null, 'Sleman', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211033', 'Oryza Nur Fajri', 'l', '', null, null, null, 'Jogjakarta', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211034', 'Oryza Nur Fajri', 'l', '', null, null, null, 'Gunung Kidul', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211035', 'Ahmad Darmadi', 'l', '', null, null, null, 'Jogjakarta', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211036', 'Ahmad Darmadi', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211037', 'Oryza Nur Fajri', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211038', 'Abenture', 'l', '', null, null, null, 'Gunung Kidul', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211039', 'Feri How', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211040', 'Feri How', 'l', '', null, null, null, 'Gunung Kidul', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211041', 'Oryza Nur Fajri', 'l', '', null, null, null, 'Umbulharjo', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211042', 'Ahmad Darmadi', 'l', '', null, null, null, 'Gunung Kidul', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211043', 'Oryza Nur Fajri', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211044', 'Budi', 'l', '', null, null, null, 'Sleman', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211045', 'Feri How', 'l', '', null, null, null, 'Umbulharjo', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211046', 'Oryza Nur Fajri', 'l', '', null, null, null, 'Kebumen', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211047', 'Oryza Nur Fajri', 'l', '', null, null, null, 'Umbulharjo', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211048', 'Ahmad Darmadi', 'l', '', null, null, null, 'Jogjakarta', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211049', 'Feri How', 'l', '', null, null, null, 'Umbulharjo', null, null, null, null, null, null, null, null, null, null, null, '2012-11-23 00:00:00', null, null, null, null);
INSERT INTO "ptn_social_data" VALUES ('1211050', 'Riki harun', 'L', 'Cilacap', '2010-1-1', '2', 'B', '', '/', '', '', '1', '3', null, 'belum kawin', null, null, null, '', '2012-11-28 11:48:17', '1', null, '2012-11-28 11:48:17', '');
INSERT INTO "ptn_social_data" VALUES ('1211051', 'hanief', 'L', 'Yogya', '2004-8-2', '8', '-', '', '/', '', '', '1', '3', null, 'belum kawin', null, null, null, '', '2012-11-29 12:00:11', '1', null, '2012-11-29 12:00:11', '');
INSERT INTO "ptn_social_data" VALUES ('1212001', 'boerhan', 'L', '', '1990-2-2', '22', '-', '', '/', '', '', '1', '3', null, null, null, null, null, '', '2012-12-05 04:39:33', '1', null, '2012-12-05 04:39:33', '');
INSERT INTO "ptn_social_data" VALUES ('1212002', 'boerhan', 'L', '', '1990-2-2', '22', '-', '', '/', '', '', '1', '3', null, null, null, null, null, '', '2012-12-05 04:39:36', '1', null, '2012-12-05 04:39:36', '');
INSERT INTO "ptn_social_data" VALUES ('1212003', 'boerhan', 'L', '', '1990-2-2', '22', '-', '', '/', '', '', '1', '3', null, null, null, null, null, '', '2012-12-05 04:39:37', '1', null, '2012-12-05 04:39:37', '');
INSERT INTO "ptn_social_data" VALUES ('1212004', 'boerhan', 'L', '', '1990-2-2', '22', '-', '', '/', '', '', '1', '3', null, null, null, null, null, '', '2012-12-05 04:39:39', '1', null, '2012-12-05 04:39:39', '');
INSERT INTO "ptn_social_data" VALUES ('1212005', 'boerhan', 'L', '', '1990-2-2', '22', '-', '', '/', '', '', '1', '3', null, null, null, null, null, '', '2012-12-05 04:39:53', '1', null, '2012-12-05 04:39:53', '');
INSERT INTO "ptn_social_data" VALUES ('1212006', 'boerhan', 'L', '', '1990-2-2', '22', '-', '', '/', '', '', '1', '3', null, null, null, null, null, '', '2012-12-05 04:39:54', '1', null, '2012-12-05 04:39:54', '');
INSERT INTO "ptn_social_data" VALUES ('1212007', 'askdjasd', 'P', 'paispdoa', '2012-1-1', '10', '-', 'sadasd 1/1 asdasd asdasd', '1/1', 'asdasd', 'asdasd', '1', '3', 'WNI', 'belum kawin', 'Katolik', 'SMA', 'BUMN', 'asdasd', '2012-12-05 04:41:56', '1', null, '2012-12-05 04:41:56', 'sadasd');
INSERT INTO "ptn_social_data" VALUES ('1212008', 'askdjasd', 'P', 'paispdoa', '2012-1-1', '10', '-', 'sadasd 1/1 asdasd asdasd', '1/1', 'asdasd', 'asdasd', '1', '3', 'WNI', 'belum kawin', 'Katolik', 'SMA', 'BUMN', 'asdasd', '2012-12-05 04:45:12', '1', null, '2012-12-05 04:45:12', 'sadasd');
INSERT INTO "ptn_social_data" VALUES ('1212009', 'askdjasd', 'P', 'paispdoa', '2012-1-1', '10', '-', 'sadasd 1/1 asdasd asdasd', '1/1', 'asdasd', 'asdasd', '1', '3', 'WNI', 'belum kawin', 'Katolik', 'SMA', 'BUMN', 'asdasd', '2012-12-05 04:45:52', '1', null, '2012-12-05 04:45:52', 'sadasd');
INSERT INTO "ptn_social_data" VALUES ('1212010', 'agus dwi', 'L', 'asdasd', '2009-3-2', '3', '-', 'Indonesia 4/4  ', '4/4', '', '', '1', '3', null, null, null, null, null, '', '2012-12-05 05:01:24', '1', null, '2012-12-05 05:01:24', 'Indonesia');
INSERT INTO "ptn_social_data" VALUES ('1212011', 'Dwi Jayanto', 'L', 'Ponorogo', '1983-5-1', '29', '-', 'Ramawijaya 3/3 Ramawijaya ', '3/3', 'Ramawijaya', '', '1', '3', null, null, null, null, null, '', '2012-12-05 08:43:13', '1', null, '2012-12-05 08:43:13', 'Ramawijaya');
COMMIT;

-- ----------------------------
--  Table structure for "com_user_tipe"
-- ----------------------------
DROP TABLE IF EXISTS "com_user_tipe";
CREATE TABLE "com_user_tipe" (
	"tipe_id" int4 NOT NULL DEFAULT nextval('com_user_tipe_seq'::regclass),
	"tipe_nama" varchar(100) NOT NULL,
	"tipe_home" varchar(200),
	"tipe_homebase" varchar(200) NOT NULL,
	"status" int2 NOT NULL DEFAULT 1
)
WITH (OIDS=FALSE);
ALTER TABLE "com_user_tipe" OWNER TO "postgres";

-- ----------------------------
--  Records of "com_user_tipe"
-- ----------------------------
BEGIN;
INSERT INTO "com_user_tipe" VALUES ('1', 'super_admin', 'super_admin', 'home', '1');
INSERT INTO "com_user_tipe" VALUES ('24', 'admin', 'admin', 'admin', '1');
COMMIT;

-- ----------------------------
--  Table structure for "mst_nationality"
-- ----------------------------
DROP TABLE IF EXISTS "mst_nationality";
CREATE TABLE "mst_nationality" (
	"mna_id" int4 NOT NULL DEFAULT nextval('mst_nationality_seq'::regclass),
	"mna_name" varchar(45),
	"mod_id" int4,
	"mod_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_nationality" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_nationality"
-- ----------------------------
BEGIN;
INSERT INTO "mst_nationality" VALUES ('1', 'WNI', null, null);
INSERT INTO "mst_nationality" VALUES ('2', 'WNA', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "mst_study"
-- ----------------------------
DROP TABLE IF EXISTS "mst_study";
CREATE TABLE "mst_study" (
	"mst_id" int4 NOT NULL,
	"mst_name" varchar(50),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_study" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "inv_recive_item"
-- ----------------------------
DROP TABLE IF EXISTS "inv_recive_item";
CREATE TABLE "inv_recive_item" (
	"iri_id" varchar(32) NOT NULL,
	"iri_invoice" varchar(50) NOT NULL,
	"iri_supplier" varchar(32),
	"iri_date" timestamp(6) NULL,
	"iri_currency" varchar(50),
	"iri_vat" varchar(32),
	"iri_ppn" numeric(8,2),
	"iri_operator" varchar(32),
	"iri_note" varchar(100),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "inv_recive_item" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "sys_menu_role"
-- ----------------------------
DROP TABLE IF EXISTS "sys_menu_role";
CREATE TABLE "sys_menu_role" (
	"menu_role_id" int4 NOT NULL DEFAULT nextval('sys_menu_role_seq'::regclass),
	"menu_id" int4,
	"group_id" int4,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "sys_menu_role" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "inv_purchase_req"
-- ----------------------------
DROP TABLE IF EXISTS "inv_purchase_req";
CREATE TABLE "inv_purchase_req" (
	"ipr_id" varchar(32) NOT NULL DEFAULT nextval('inv_req_seq'::regclass),
	"msup_id" varchar(32),
	"ipr_date" timestamp(6) NULL,
	"ipr_operator" varchar(32),
	"ipr_note" text,
	"ipr_seq" int4,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "inv_purchase_req" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "com_menu__"
-- ----------------------------
DROP TABLE IF EXISTS "com_menu__";
CREATE TABLE "com_menu__" (
	"menu_id" int2 NOT NULL DEFAULT nextval('com_menu__seq'::regclass),
	"menu_parent" int2 DEFAULT 0,
	"menu_nama" varchar(200),
	"menu_url" varchar(200),
	"menu_icon" varchar(200),
	"status" int2 NOT NULL DEFAULT 1
)
WITH (OIDS=FALSE);
ALTER TABLE "com_menu__" OWNER TO "postgres";

-- ----------------------------
--  Records of "com_menu__"
-- ----------------------------
BEGIN;
INSERT INTO "com_menu__" VALUES ('1', '0', 'pendaftaran', 'pendaftaran', '', '1');
INSERT INTO "com_menu__" VALUES ('2', '0', 'pelayanan informasi', 'pelayanan_informasi', '', '1');
INSERT INTO "com_menu__" VALUES ('3', '0', 'rawat jalan', 'rawat_jalan', '', '1');
INSERT INTO "com_menu__" VALUES ('4', '0', 'kasir', 'kasir', '', '1');
INSERT INTO "com_menu__" VALUES ('5', '0', 'apotek', 'apotek', '', '1');
INSERT INTO "com_menu__" VALUES ('6', '0', 'gudang_farmasi', 'gudang farmasi', '', '1');
INSERT INTO "com_menu__" VALUES ('7', '0', 'rawat inap', 'rawat_inap', '', '0');
INSERT INTO "com_menu__" VALUES ('8', '0', 'akuntansi', 'akuntansi', '', '0');
INSERT INTO "com_menu__" VALUES ('9', '0', 'hrd', 'hrd', '', '0');
INSERT INTO "com_menu__" VALUES ('10', '0', 'keuangan', 'keuangan', '', '0');
INSERT INTO "com_menu__" VALUES ('11', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('12', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('13', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('14', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('15', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('16', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('17', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('18', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('19', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('20', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('21', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('22', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('23', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('24', '0', 'reserved', 'reserved', '', '0');
INSERT INTO "com_menu__" VALUES ('25', '0', 'reserved', 'reserved', '', '0');
COMMIT;

-- ----------------------------
--  Table structure for "trx_medical_bill"
-- ----------------------------
DROP TABLE IF EXISTS "trx_medical_bill";
CREATE TABLE "trx_medical_bill" (
	"mdc_id" varchar(10) NOT NULL,
	"total_amount" int2,
	"total_pay" int2,
	"doctor_fee" int2,
	"earnest_pay" int2,
	"nondoctor_fee" int2,
	"ins_id" int2,
	"ins_no" int2,
	"modi_id" int2,
	"modi_datetime" timestamp(6) NULL,
	"note" text
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_medical_bill" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "trx_medical_subjective"
-- ----------------------------
DROP TABLE IF EXISTS "trx_medical_subjective";
CREATE TABLE "trx_medical_subjective" (
	"ms_id" int4 NOT NULL DEFAULT nextval('trx_medical_subjective_seq'::regclass),
	"mdc_id" varchar(10) NOT NULL,
	"ms_value" varchar(45),
	"ms_desc" text,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_medical_subjective" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_medical_subjective"
-- ----------------------------
BEGIN;
INSERT INTO "trx_medical_subjective" VALUES ('1', '12110001', null, 'Kajian Subjective', null, null);
INSERT INTO "trx_medical_subjective" VALUES ('2', '12110001', null, 'Tulang RobekPatah TulangTulang Membengkak', null, null);
INSERT INTO "trx_medical_subjective" VALUES ('3', '12110001', null, 'Tulang RobekPatah TulangTulang Membengkak', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "trx_queue_agent"
-- ----------------------------
DROP TABLE IF EXISTS "trx_queue_agent";
CREATE TABLE "trx_queue_agent" (
	"que_id" varchar(8) NOT NULL,
	"que_tp" varchar(15) NOT NULL,
	"agent_name" varchar(100) NOT NULL,
	"agent_relationship" varchar(20),
	"agent_sex" varchar(1),
	"agent_address" text,
	"agent_telp" varchar(50),
	"agent_id_no" varchar(100) NOT NULL,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_queue_agent" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "mst_supplier"
-- ----------------------------
DROP TABLE IF EXISTS "mst_supplier";
CREATE TABLE "mst_supplier" (
	"msup_id" varchar NOT NULL DEFAULT nextval('mst_sup_seq'::regclass),
	"msup_name" varchar(50),
	"msup_address" text,
	"msup_province" varchar(50),
	"msup_city" varchar(50),
	"msup_zip_code" varchar(12),
	"msup_telp" varchar(14),
	"msup_fax" varchar(12),
	"msup_email" varchar(30),
	"msup_npwp" bit varying(50),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now(),
	"msup_status" int4 DEFAULT 1
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_supplier" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "mst_city"
-- ----------------------------
DROP TABLE IF EXISTS "mst_city";
CREATE TABLE "mst_city" (
	"mci_id" int4 NOT NULL DEFAULT nextval('mst_city_seq'::regclass),
	"mci_code" varchar(5),
	"mci_name" varchar(25),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_city" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_city"
-- ----------------------------
BEGIN;
INSERT INTO "mst_city" VALUES ('3', 'ID', 'Bali', null, null);
INSERT INTO "mst_city" VALUES ('4', 'ID', 'Yogyakarta-Magelang', null, null);
INSERT INTO "mst_city" VALUES ('5', 'ID', 'Bali-Manggis', null, null);
INSERT INTO "mst_city" VALUES ('6', 'ID', 'Lombok-Mataram', null, null);
INSERT INTO "mst_city" VALUES ('7', 'ID', 'Ambon', null, null);
INSERT INTO "mst_city" VALUES ('8', 'ID', 'Anyer', null, null);
INSERT INTO "mst_city" VALUES ('9', 'ID', 'Banjarmasin', null, null);
INSERT INTO "mst_city" VALUES ('10', 'ID', 'Bandung', null, null);
INSERT INTO "mst_city" VALUES ('11', 'ID', 'Bintan Island', null, null);
INSERT INTO "mst_city" VALUES ('12', 'ID', 'Blitar', null, null);
INSERT INTO "mst_city" VALUES ('13', 'ID', 'Bali-Negara', null, null);
INSERT INTO "mst_city" VALUES ('14', 'ID', 'Banyuwangi', null, null);
INSERT INTO "mst_city" VALUES ('15', 'ID', 'Bogor', null, null);
INSERT INTO "mst_city" VALUES ('16', 'ID', 'Balikpapan', null, null);
INSERT INTO "mst_city" VALUES ('17', 'ID', 'Batam Island', null, null);
INSERT INTO "mst_city" VALUES ('18', 'ID', 'Bukittinggi', null, null);
INSERT INTO "mst_city" VALUES ('19', 'ID', 'Cirebon', null, null);
INSERT INTO "mst_city" VALUES ('20', 'ID', 'Cikarang', null, null);
INSERT INTO "mst_city" VALUES ('21', 'ID', 'Bali-Canggu', null, null);
INSERT INTO "mst_city" VALUES ('22', 'ID', 'Bali-Denpasar', null, null);
INSERT INTO "mst_city" VALUES ('23', 'ID', 'Jambi', null, null);
INSERT INTO "mst_city" VALUES ('24', 'ID', 'Jayapura', null, null);
INSERT INTO "mst_city" VALUES ('25', 'ID', 'Bali-Candi Dasa', null, null);
INSERT INTO "mst_city" VALUES ('26', 'ID', 'Garut', null, null);
INSERT INTO "mst_city" VALUES ('27', 'ID', 'Bali-Gianyar', null, null);
INSERT INTO "mst_city" VALUES ('28', 'ID', 'Bali-Jimbaran Bay', null, null);
INSERT INTO "mst_city" VALUES ('29', 'ID', 'Jakarta', null, null);
INSERT INTO "mst_city" VALUES ('30', 'ID', 'Kendari', null, null);
INSERT INTO "mst_city" VALUES ('31', 'ID', 'Bali-Kerobokan', null, null);
INSERT INTO "mst_city" VALUES ('32', 'ID', 'Ketapang', null, null);
INSERT INTO "mst_city" VALUES ('33', 'ID', 'Bali-Kuta Beach', null, null);
INSERT INTO "mst_city" VALUES ('34', 'ID', 'Bali-Legian Beach', null, null);
INSERT INTO "mst_city" VALUES ('35', 'ID', 'Bandung-Lembang', null, null);
INSERT INTO "mst_city" VALUES ('36', 'ID', 'Lembongan Island', null, null);
INSERT INTO "mst_city" VALUES ('37', 'ID', 'Lombok', null, null);
INSERT INTO "mst_city" VALUES ('38', 'ID', 'Malang', null, null);
INSERT INTO "mst_city" VALUES ('39', 'ID', 'Lombok-Medana Beach', null, null);
INSERT INTO "mst_city" VALUES ('40', 'ID', 'Medan', null, null);
INSERT INTO "mst_city" VALUES ('41', 'ID', 'Manokwari', null, null);
INSERT INTO "mst_city" VALUES ('42', 'ID', 'Manado', null, null);
INSERT INTO "mst_city" VALUES ('43', 'ID', 'Moyo Island', null, null);
INSERT INTO "mst_city" VALUES ('44', 'ID', 'Bali-Nusa Dua', null, null);
INSERT INTO "mst_city" VALUES ('45', 'ID', 'Pekalongan', null, null);
INSERT INTO "mst_city" VALUES ('46', 'ID', 'Bali-Pekutatan', null, null);
INSERT INTO "mst_city" VALUES ('47', 'ID', 'Pelabuhan Ratu', null, null);
INSERT INTO "mst_city" VALUES ('48', 'ID', 'Bangka - Pangkal Pinang', null, null);
INSERT INTO "mst_city" VALUES ('49', 'ID', 'Pekanbaru', null, null);
INSERT INTO "mst_city" VALUES ('50', 'ID', 'Palembang', null, null);
INSERT INTO "mst_city" VALUES ('51', 'ID', 'Palu', null, null);
INSERT INTO "mst_city" VALUES ('52', 'ID', 'Pontianak', null, null);
INSERT INTO "mst_city" VALUES ('53', 'ID', 'Parapat', null, null);
INSERT INTO "mst_city" VALUES ('54', 'ID', 'Pasuruan', null, null);
INSERT INTO "mst_city" VALUES ('55', 'ID', 'Lombok - Putri Nyale', null, null);
INSERT INTO "mst_city" VALUES ('56', 'ID', 'Bali-Sanur Beach', null, null);
INSERT INTO "mst_city" VALUES ('57', 'ID', 'Lombok - Putri Nyale', null, null);
INSERT INTO "mst_city" VALUES ('58', 'ID', 'Lombok-Senggigi', null, null);
INSERT INTO "mst_city" VALUES ('59', 'ID', 'Bali-Seminyak', null, null);
INSERT INTO "mst_city" VALUES ('60', 'ID', 'Solo', null, null);
INSERT INTO "mst_city" VALUES ('61', 'ID', 'Samarinda', null, null);
INSERT INTO "mst_city" VALUES ('62', 'ID', 'Surabaya', null, null);
INSERT INTO "mst_city" VALUES ('63', 'ID', 'Subang', null, null);
INSERT INTO "mst_city" VALUES ('64', 'ID', 'Sungailiat', null, null);
INSERT INTO "mst_city" VALUES ('65', 'ID', 'Bali-Tabanan', null, null);
INSERT INTO "mst_city" VALUES ('66', 'ID', 'Tangerang', null, null);
INSERT INTO "mst_city" VALUES ('67', 'ID', 'Tanjung Tabalong', null, null);
INSERT INTO "mst_city" VALUES ('68', 'ID', 'Bandar Lampung', null, null);
INSERT INTO "mst_city" VALUES ('69', 'ID', 'Bali-Uluwatu', null, null);
INSERT INTO "mst_city" VALUES ('70', 'ID', 'Wamena', null, null);
INSERT INTO "mst_city" VALUES ('71', 'ID', 'Yogyakarta-Borobudur', null, null);
INSERT INTO "mst_city" VALUES ('72', 'ID', 'Yogyakarta', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "mst_occupation"
-- ----------------------------
DROP TABLE IF EXISTS "mst_occupation";
CREATE TABLE "mst_occupation" (
	"oc_id" int4 NOT NULL DEFAULT nextval('mst_occupation_seq'::regclass),
	"mo_name" varchar(45),
	"mo_desc" varchar(45),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_occupation" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_occupation"
-- ----------------------------
BEGIN;
INSERT INTO "mst_occupation" VALUES ('1', '-', null, null, null);
INSERT INTO "mst_occupation" VALUES ('2', 'TNI', null, null, null);
INSERT INTO "mst_occupation" VALUES ('3', 'PNS', null, null, null);
INSERT INTO "mst_occupation" VALUES ('4', 'POLRI', null, null, null);
INSERT INTO "mst_occupation" VALUES ('5', 'BUMN', null, null, null);
INSERT INTO "mst_occupation" VALUES ('6', 'SWASTA', null, null, null);
INSERT INTO "mst_occupation" VALUES ('7', 'Mahasiswa', null, null, null);
INSERT INTO "mst_occupation" VALUES ('8', 'Pelajar', null, null, null);
INSERT INTO "mst_occupation" VALUES ('9', 'Tani', null, null, null);
INSERT INTO "mst_occupation" VALUES ('10', 'Dagang', null, null, null);
INSERT INTO "mst_occupation" VALUES ('11', 'Lainnya', null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for "mst_province"
-- ----------------------------
DROP TABLE IF EXISTS "mst_province";
CREATE TABLE "mst_province" (
	"mpr_id" int4 NOT NULL DEFAULT nextval('mst_province_seq'::regclass),
	"mpr_name" varchar(50),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_province" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_province"
-- ----------------------------
BEGIN;
INSERT INTO "mst_province" VALUES ('3', 'Daerah Istimewa Yogyakarta', null, null);
INSERT INTO "mst_province" VALUES ('4', 'Aceh', null, null);
INSERT INTO "mst_province" VALUES ('5', 'Sumatera Utara', null, null);
INSERT INTO "mst_province" VALUES ('6', 'Bengkulu', null, null);
INSERT INTO "mst_province" VALUES ('7', 'Jambi', null, null);
INSERT INTO "mst_province" VALUES ('8', 'Riau', null, null);
INSERT INTO "mst_province" VALUES ('9', 'Sumatera Barat', null, null);
INSERT INTO "mst_province" VALUES ('10', 'Sumatera Selatan', null, null);
INSERT INTO "mst_province" VALUES ('11', 'Lampung', null, null);
INSERT INTO "mst_province" VALUES ('12', 'Kepulauan Bangka Belitung', null, null);
INSERT INTO "mst_province" VALUES ('13', 'Kepulauan Riau', null, null);
INSERT INTO "mst_province" VALUES ('14', 'Banten', null, null);
INSERT INTO "mst_province" VALUES ('15', 'Jawa Barat', null, null);
INSERT INTO "mst_province" VALUES ('16', 'DKI Jakarta', null, null);
INSERT INTO "mst_province" VALUES ('17', 'Jawa Tengah', null, null);
INSERT INTO "mst_province" VALUES ('18', 'Jawa Timur', null, null);
INSERT INTO "mst_province" VALUES ('20', 'Bali', null, null);
INSERT INTO "mst_province" VALUES ('21', 'Nusa Tenggara Barat', null, null);
INSERT INTO "mst_province" VALUES ('22', 'Nusa Tenggara Timur', null, null);
INSERT INTO "mst_province" VALUES ('23', 'Kalimantan Barat', null, null);
INSERT INTO "mst_province" VALUES ('24', 'Kalimantan Selatan', null, null);
INSERT INTO "mst_province" VALUES ('25', 'Kalimantan Tengah', null, null);
INSERT INTO "mst_province" VALUES ('26', 'Kalimantan Timur', null, null);
INSERT INTO "mst_province" VALUES ('27', 'Gorontalo', null, null);
INSERT INTO "mst_province" VALUES ('28', 'Sulawesi Selatan', null, null);
INSERT INTO "mst_province" VALUES ('29', 'Sulawesi Tenggara', null, null);
INSERT INTO "mst_province" VALUES ('30', 'Sulawesi Tengah', null, null);
INSERT INTO "mst_province" VALUES ('31', 'Sulawesi Utara', null, null);
INSERT INTO "mst_province" VALUES ('32', 'Sulawesi Barat', null, null);
INSERT INTO "mst_province" VALUES ('33', 'Maluku', null, null);
INSERT INTO "mst_province" VALUES ('34', 'Maluku Utara', null, null);
INSERT INTO "mst_province" VALUES ('35', 'Papua', null, null);
INSERT INTO "mst_province" VALUES ('36', 'Papua Barat', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "mst_regency"
-- ----------------------------
DROP TABLE IF EXISTS "mst_regency";
CREATE TABLE "mst_regency" (
	"mre_id" int4 NOT NULL DEFAULT nextval('mst_regency_seq'::regclass),
	"mco_code" varchar(32),
	"mpr_id" int4,
	"mre_name" varchar(50),
	"mre_capital" varchar(50),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_regency" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_regency"
-- ----------------------------
BEGIN;
INSERT INTO "mst_regency" VALUES ('1', '1', '1', 'Kabupaten Bireuen', 'Bireuen', null, null);
INSERT INTO "mst_regency" VALUES ('2', '1', '1', 'Kabupaten Gayo Lues', 'Blang Kejeren', null, null);
INSERT INTO "mst_regency" VALUES ('3', '1', '1', 'Kabupaten Aceh Barat Daya', 'Blangpidie', null, null);
INSERT INTO "mst_regency" VALUES ('4', '1', '1', 'Kabupaten Aceh Jaya', 'Calang', null, null);
INSERT INTO "mst_regency" VALUES ('5', '1', '1', 'Kabupaten Aceh Timur', 'Idi Rayeuk', null, null);
INSERT INTO "mst_regency" VALUES ('6', '1', '1', 'Kabupaten Aceh Tamiang', 'Karang Baru', null, null);
INSERT INTO "mst_regency" VALUES ('7', '1', '1', 'Kabupaten Aceh Besar', 'Jantho', null, null);
INSERT INTO "mst_regency" VALUES ('8', '1', '1', 'Kabupaten Aceh Tenggara', 'Kutacane', null, null);
INSERT INTO "mst_regency" VALUES ('9', '1', '1', 'Kabupaten Aceh Utara', 'Lhoksukon', null, null);
INSERT INTO "mst_regency" VALUES ('10', '1', '1', 'Kabupaten Aceh Barat', 'Meulaboh', null, null);
INSERT INTO "mst_regency" VALUES ('11', '1', '1', 'Kabupaten Pidie Jaya', 'Meureudu', null, null);
INSERT INTO "mst_regency" VALUES ('12', '1', '1', 'Kabupaten Pidie', 'Sigli', null, null);
INSERT INTO "mst_regency" VALUES ('13', '1', '1', 'Kabupaten Bener Meriah', 'Simpang Tiga Redelong', null, null);
INSERT INTO "mst_regency" VALUES ('14', '1', '1', 'Kabupaten Simeulue', 'Sinabang', null, null);
INSERT INTO "mst_regency" VALUES ('15', '1', '1', 'Kabupaten Aceh Singkil', 'Singkil', null, null);
INSERT INTO "mst_regency" VALUES ('16', '1', '1', 'Kabupaten Nagan Raya', 'Suka Makmue', null, null);
INSERT INTO "mst_regency" VALUES ('17', '1', '1', 'Kabupaten Aceh Tengah', 'Takengon', null, null);
INSERT INTO "mst_regency" VALUES ('18', '1', '1', 'Kabupaten Aceh Selatan', 'Tapak Tuan', null, null);
INSERT INTO "mst_regency" VALUES ('19', '1', '1', 'Kota Banda Aceh', 'Banda Aceh', null, null);
INSERT INTO "mst_regency" VALUES ('20', '1', '1', 'Kota Langsa', 'Langsa', null, null);
INSERT INTO "mst_regency" VALUES ('21', '1', '1', 'Kota Lhokseumawe', 'Lhokseumawe', null, null);
INSERT INTO "mst_regency" VALUES ('22', '1', '1', 'Kota Sabang', 'Sabang', null, null);
INSERT INTO "mst_regency" VALUES ('23', '1', '1', 'Kota Subulussalam', 'Subulussalam', null, null);
INSERT INTO "mst_regency" VALUES ('24', '1', '2', 'Kabupaten Labuhanbatu Utara', 'Aek Kanopan', null, null);
INSERT INTO "mst_regency" VALUES ('25', '1', '2', 'Kabupaten Toba Samosir', 'Balige', null, null);
INSERT INTO "mst_regency" VALUES ('26', '1', '2', 'Kota Binjai', 'Binjai Kota', null, null);
INSERT INTO "mst_regency" VALUES ('27', '1', '2', 'Kabupaten Humbang Hasundutan', 'Dolok Sanggul', null, null);
INSERT INTO "mst_regency" VALUES ('28', '1', '2', 'Kabupaten Nias', 'Gunung Sitoli', null, null);
INSERT INTO "mst_regency" VALUES ('29', '1', '2', 'Kabupaten Padang Lawas Utara', 'Gunung Tua', null, null);
INSERT INTO "mst_regency" VALUES ('30', '1', '2', 'Kabupaten Karo', 'Kabanjahe', null, null);
INSERT INTO "mst_regency" VALUES ('31', '1', '2', 'Kabupaten Asahan', 'Kisaran', null, null);
INSERT INTO "mst_regency" VALUES ('32', '1', '2', 'Kabupaten Labuhanbatu Selatan', 'Pinang', null, null);
INSERT INTO "mst_regency" VALUES ('33', '1', '2', 'Kabupaten Nias Barat', 'Lahomi', null, null);
INSERT INTO "mst_regency" VALUES ('34', '1', '2', 'Kabupaten Batubara', 'Limapuluh', null, null);
INSERT INTO "mst_regency" VALUES ('35', '1', '2', 'Kabupaten Nias Utara', 'Lotu', null, null);
INSERT INTO "mst_regency" VALUES ('36', '1', '2', 'Kabupaten Deli Serdang', 'Lubuk Pakam', null, null);
INSERT INTO "mst_regency" VALUES ('37', '1', '2', 'Kabupaten Tapanuli Tengah', 'Pandan', null, null);
INSERT INTO "mst_regency" VALUES ('38', '1', '2', 'Kabupaten Samosir', 'Pangururan', null, null);
INSERT INTO "mst_regency" VALUES ('39', '1', '2', 'Kabupaten Mandailing Natal', 'Panyabungan', null, null);
INSERT INTO "mst_regency" VALUES ('40', '1', '2', 'Kabupaten Labuhanbatu', 'Rantau Prapat', null, null);
INSERT INTO "mst_regency" VALUES ('41', '1', '2', 'Kabupaten Simalungun', 'Raya', null, null);
INSERT INTO "mst_regency" VALUES ('42', '1', '2', 'Kabupaten Pakpak Bharat', 'Salak', null, null);
INSERT INTO "mst_regency" VALUES ('43', '1', '2', 'Kabupaten Serdang Bedagai', 'Sei Rampah', null, null);
INSERT INTO "mst_regency" VALUES ('44', '1', '2', 'Kabupaten Padang Lawas', 'Sibuhuan', null, null);
INSERT INTO "mst_regency" VALUES ('45', '1', '2', 'Kabupaten Dairi', 'Sidikalang', null, null);
INSERT INTO "mst_regency" VALUES ('46', '1', '2', 'Kabupaten Tapanuli Selatan', 'Sipirok', null, null);
INSERT INTO "mst_regency" VALUES ('47', '1', '2', 'Kabupaten Langkat', 'Stabat', null, null);
INSERT INTO "mst_regency" VALUES ('48', '1', '2', 'Kabupaten Tapanuli Utara', 'Tarutung', null, null);
INSERT INTO "mst_regency" VALUES ('49', '1', '2', 'Kabupaten Nias Selatan', 'Teluk Dalam', null, null);
INSERT INTO "mst_regency" VALUES ('50', '1', '2', 'Kota Gunungsitoli', 'Gunungsitoli', null, null);
INSERT INTO "mst_regency" VALUES ('51', '1', '2', 'Kota Medan', 'Medan', null, null);
INSERT INTO "mst_regency" VALUES ('52', '1', '2', 'Kota Padangsidempuan', 'Padangsidempuan', null, null);
INSERT INTO "mst_regency" VALUES ('53', '1', '2', 'Kota Pematangsiantar', 'Pematangsiantar', null, null);
INSERT INTO "mst_regency" VALUES ('54', '1', '2', 'Kota Sibolga', 'Sibolga', null, null);
INSERT INTO "mst_regency" VALUES ('55', '1', '2', 'Kota Tanjungbalai', 'Tanjungbalai', null, null);
INSERT INTO "mst_regency" VALUES ('56', '1', '2', 'Kota Tebing Tinggi', 'Tebing Tinggi', null, null);
INSERT INTO "mst_regency" VALUES ('57', '1', '3', 'Kabupaten Bengkulu Utara', 'Arga Makmur', null, null);
INSERT INTO "mst_regency" VALUES ('58', '1', '3', 'Kabupaten Kaur', 'Bintuhan  Kaur Selatan', null, null);
INSERT INTO "mst_regency" VALUES ('59', '1', '3', 'Kabupaten Rejang Lebong', 'Curup', null, null);
INSERT INTO "mst_regency" VALUES ('60', '1', '3', 'Kabupaten Bengkulu Tengah', 'Karang Tinggi', null, null);
INSERT INTO "mst_regency" VALUES ('61', '1', '3', 'Kabupaten Kepahiang', 'Kepahiang', null, null);
INSERT INTO "mst_regency" VALUES ('62', '1', '3', 'Kabupaten Bengkulu Selatan', 'Kota Manna', null, null);
INSERT INTO "mst_regency" VALUES ('63', '1', '3', 'Kabupaten Lebong', 'Muara Aman', null, null);
INSERT INTO "mst_regency" VALUES ('64', '1', '3', 'Kabupaten Mukomuko', 'Mukomuko', null, null);
INSERT INTO "mst_regency" VALUES ('65', '1', '3', 'Kabupaten Seluma', 'Tais', null, null);
INSERT INTO "mst_regency" VALUES ('66', '1', '3', 'Kota Bengkulu', 'Bengkulu', null, null);
INSERT INTO "mst_regency" VALUES ('67', '1', '4', 'Kabupaten Merangin', 'Bangko', null, null);
INSERT INTO "mst_regency" VALUES ('68', '1', '4', 'Kabupaten Tanjung Jabung Barat', 'Kuala Tungkal', null, null);
INSERT INTO "mst_regency" VALUES ('69', '1', '4', 'Kabupaten Batanghari', 'Muara Bulian', null, null);
INSERT INTO "mst_regency" VALUES ('70', '1', '4', 'Kabupaten Bungo', 'Muara Bungo', null, null);
INSERT INTO "mst_regency" VALUES ('71', '1', '4', 'Kabupaten Tanjung Jabung Timur', 'Muara Sabak', null, null);
INSERT INTO "mst_regency" VALUES ('72', '1', '4', 'Kabupaten Tebo', 'Muara Tebo', null, null);
INSERT INTO "mst_regency" VALUES ('73', '1', '4', 'Kabupaten Sarolangun', 'Sarolangun', null, null);
INSERT INTO "mst_regency" VALUES ('74', '1', '4', 'Kabupaten Muaro Jambi', 'Sengeti', null, null);
INSERT INTO "mst_regency" VALUES ('75', '1', '4', 'Kabupaten Kerinci', 'Sungaipenuh', null, null);
INSERT INTO "mst_regency" VALUES ('76', '1', '4', 'Kota Jambi', 'Jambi', null, null);
INSERT INTO "mst_regency" VALUES ('77', '1', '4', 'Kota Sungai Penuh', 'Sungai Penuh', null, null);
INSERT INTO "mst_regency" VALUES ('78', '1', '5', 'Kabupaten Kampar', 'Bangkinang', null, null);
INSERT INTO "mst_regency" VALUES ('79', '1', '5', 'Kabupaten Bengkalis', 'Bengkalis', null, null);
INSERT INTO "mst_regency" VALUES ('80', '1', '5', 'Kabupaten Pelalawan', 'Pangkalan Kerinci', null, null);
INSERT INTO "mst_regency" VALUES ('81', '1', '5', 'Kabupaten Rokan Hulu', 'Pasir Pengaraian', null, null);
INSERT INTO "mst_regency" VALUES ('82', '1', '5', 'Kota Pekanbaru', 'Pekanbaru', null, null);
INSERT INTO "mst_regency" VALUES ('83', '1', '5', 'Kabupaten Indragiri Hulu', 'Rengat', null, null);
INSERT INTO "mst_regency" VALUES ('84', '1', '5', 'Kabupaten Kepulauan Meranti', 'Selatpanjang', null, null);
INSERT INTO "mst_regency" VALUES ('85', '1', '5', 'Kabupaten Siak', 'Siak Sri Indrapura', null, null);
INSERT INTO "mst_regency" VALUES ('86', '1', '5', 'Kabupaten Kuantan Singingi', 'Teluk Kuantan', null, null);
INSERT INTO "mst_regency" VALUES ('87', '1', '5', 'Kabupaten Indragiri Hilir', 'Tembilahan', null, null);
INSERT INTO "mst_regency" VALUES ('88', '1', '5', 'Kabupaten Rokan Hilir', 'Ujung Tanjung', null, null);
INSERT INTO "mst_regency" VALUES ('89', '1', '5', 'Kota Dumai', 'Dumai', null, null);
INSERT INTO "mst_regency" VALUES ('90', '1', '6', 'Kabupaten Solok', 'Arosuka', null, null);
INSERT INTO "mst_regency" VALUES ('91', '1', '6', 'Kabupaten Tanah Datar', 'Batusangkar', null, null);
INSERT INTO "mst_regency" VALUES ('92', '1', '6', 'Kota Bukittinggi', 'Bukittinggi', null, null);
INSERT INTO "mst_regency" VALUES ('93', '1', '6', 'Kabupaten Agam', 'Lubuk Basung', null, null);
INSERT INTO "mst_regency" VALUES ('94', '1', '6', 'Kabupaten Pasaman', 'Lubuk Sikaping', null, null);
INSERT INTO "mst_regency" VALUES ('95', '1', '6', 'Kabupaten Sijunjung', 'Muaro Sijunjung', null, null);
INSERT INTO "mst_regency" VALUES ('96', '1', '6', 'Kota Padang', 'Padang', null, null);
INSERT INTO "mst_regency" VALUES ('97', '1', '6', 'Kabupaten Solok Selatan', 'Padang Aro', null, null);
INSERT INTO "mst_regency" VALUES ('98', '1', '6', 'Kota Padangpanjang', 'Padang Panjang', null, null);
INSERT INTO "mst_regency" VALUES ('99', '1', '6', 'Kabupaten Pesisir Selatan', 'Painan', null, null);
INSERT INTO "mst_regency" VALUES ('100', '1', '6', 'Kota Pariaman', 'Pariaman', null, null);
INSERT INTO "mst_regency" VALUES ('101', '1', '6', 'Kabupaten Padang Pariaman', 'Parit Malintang', null, null);
INSERT INTO "mst_regency" VALUES ('102', '1', '6', 'Kota Payakumbuh', 'Payakumbuh', null, null);
INSERT INTO "mst_regency" VALUES ('103', '1', '6', 'Kabupaten Dharmasraya', 'Pulau Punjung', null, null);
INSERT INTO "mst_regency" VALUES ('104', '1', '6', 'Kabupaten Lima Puluh Kota', 'Sarilamak', null, null);
INSERT INTO "mst_regency" VALUES ('105', '1', '6', 'Kota Sawahlunto', 'Sawahlunto', null, null);
INSERT INTO "mst_regency" VALUES ('106', '1', '6', 'Kabupaten Pasaman Barat', 'Simpang Empat', null, null);
INSERT INTO "mst_regency" VALUES ('107', '1', '6', 'Kota Solok', 'Solok', null, null);
INSERT INTO "mst_regency" VALUES ('108', '1', '6', 'Kabupaten Kepulauan Mentawai', 'Tuapejat', null, null);
INSERT INTO "mst_regency" VALUES ('109', '1', '7', 'Kabupaten Ogan Komering Ulu', 'Baturaja', null, null);
INSERT INTO "mst_regency" VALUES ('110', '1', '7', 'Kabupaten Ogan Ilir', 'Indralaya', null, null);
INSERT INTO "mst_regency" VALUES ('111', '1', '7', 'Kabupaten Ogan Komering Ilir', 'Kota Kayu Agung', null, null);
INSERT INTO "mst_regency" VALUES ('112', '1', '7', 'Kabupaten Lahat', 'Lahat', null, null);
INSERT INTO "mst_regency" VALUES ('113', '1', '7', 'Kabupaten Ogan Komering Ulu Timur', 'Martapura', null, null);
INSERT INTO "mst_regency" VALUES ('114', '1', '7', 'Kabupaten Musi Rawas', 'Muara Beliti Baru', null, null);
INSERT INTO "mst_regency" VALUES ('115', '1', '7', 'Kabupaten Muara Enim', 'Muara Enim', null, null);
INSERT INTO "mst_regency" VALUES ('116', '1', '7', 'Kabupaten Ogan Komering Ulu Selatan', 'Muaradua', null, null);
INSERT INTO "mst_regency" VALUES ('117', '1', '7', 'Kabupaten Banyuasin', 'Pangkalan Balai', null, null);
INSERT INTO "mst_regency" VALUES ('118', '1', '7', 'Kabupaten Musi Banyuasin', 'Sekayu', null, null);
INSERT INTO "mst_regency" VALUES ('119', '1', '7', 'Kabupaten Empat Lawang', 'Tebing Tinggi', null, null);
INSERT INTO "mst_regency" VALUES ('120', '1', '7', 'Kota Lubuklinggau', 'Lubuklinggau', null, null);
INSERT INTO "mst_regency" VALUES ('121', '1', '7', 'Kota Pagar Alam', 'Pagar Alam', null, null);
INSERT INTO "mst_regency" VALUES ('122', '1', '7', 'Kota Palembang', 'Palembang', null, null);
INSERT INTO "mst_regency" VALUES ('123', '1', '7', 'Kota Prabumulih', 'Prabumulih', null, null);
INSERT INTO "mst_regency" VALUES ('124', '1', '8', 'Kabupaten Way Kanan', 'Blambangan Umpu', null, null);
INSERT INTO "mst_regency" VALUES ('125', '1', '8', 'Kabupaten Pesawaran', 'Gedong Tataan', null, null);
INSERT INTO "mst_regency" VALUES ('126', '1', '8', 'Kabupaten Lampung Tengah', 'Gunung Sugih', null, null);
INSERT INTO "mst_regency" VALUES ('127', '1', '8', 'Kabupaten Lampung Selatan', 'Kalianda', null, null);
INSERT INTO "mst_regency" VALUES ('128', '1', '8', 'Kabupaten Tanggamus', 'Agung', null, null);
INSERT INTO "mst_regency" VALUES ('129', '1', '8', 'Kabupaten Lampung Barat', 'Liwa', null, null);
INSERT INTO "mst_regency" VALUES ('130', '1', '8', 'Kabupaten Lampung Utara', 'Kotabumi', null, null);
INSERT INTO "mst_regency" VALUES ('131', '1', '8', 'Kabupaten Tulang Bawang', 'Menggala', null, null);
INSERT INTO "mst_regency" VALUES ('132', '1', '8', 'Kabupaten Lampung Timur', 'Sukadana', null, null);
INSERT INTO "mst_regency" VALUES ('133', '1', '8', 'Kabupaten Tulang Bawang Barat', 'Tulang Bawang Tengah', null, null);
INSERT INTO "mst_regency" VALUES ('134', '1', '8', 'Kabupaten Mesuji', 'Kabupaten Mesuji', null, null);
INSERT INTO "mst_regency" VALUES ('135', '1', '8', 'Kabupaten Pringsewu', 'Kabupaten Pringsewu', null, null);
INSERT INTO "mst_regency" VALUES ('136', '1', '8', 'Kota Bandar Lampung', 'Bandar Lampung', null, null);
INSERT INTO "mst_regency" VALUES ('137', '1', '8', 'Kota Metro', 'Metro', null, null);
INSERT INTO "mst_regency" VALUES ('138', '1', '9', 'Kabupaten Bangka Tengah', 'Koba', null, null);
INSERT INTO "mst_regency" VALUES ('139', '1', '9', 'Kabupaten Belitung Timur', 'Manggar', null, null);
INSERT INTO "mst_regency" VALUES ('140', '1', '9', 'Kabupaten Bangka Selatan', 'Mentok', null, null);
INSERT INTO "mst_regency" VALUES ('141', '1', '9', 'Kabupaten Bangka', 'Sungai Liat', null, null);
INSERT INTO "mst_regency" VALUES ('142', '1', '9', 'Kabupaten Belitung', 'Tanjung Pandan', null, null);
INSERT INTO "mst_regency" VALUES ('143', '1', '9', 'Kabupaten Bangka Barat', 'Toboali', null, null);
INSERT INTO "mst_regency" VALUES ('144', '1', '9', 'Kota Pangkal Pinang', 'Pangkal Pinang', null, null);
INSERT INTO "mst_regency" VALUES ('145', '1', '10', 'Kabupaten Bintan', 'Bandar Seri Bentan', null, null);
INSERT INTO "mst_regency" VALUES ('146', '1', '10', 'Kabupaten Lingga', 'Daik, Lingga', null, null);
INSERT INTO "mst_regency" VALUES ('147', '1', '10', 'Kabupaten Natuna', 'Ranai, Bunguran Timur', null, null);
INSERT INTO "mst_regency" VALUES ('148', '1', '10', 'Kabupaten Karimun', 'Tanjung Balai Karimun', null, null);
INSERT INTO "mst_regency" VALUES ('149', '1', '10', 'Kabupaten Kepulauan Anambas', 'Tarempa', null, null);
INSERT INTO "mst_regency" VALUES ('150', '1', '10', 'Kota Batam', 'Batam', null, null);
INSERT INTO "mst_regency" VALUES ('151', '1', '10', 'Kota Tanjung Pinang', 'Tanjung Pinang', null, null);
INSERT INTO "mst_regency" VALUES ('152', '1', '11', 'Kota Tangerang Selatan', 'Ciputat', null, null);
INSERT INTO "mst_regency" VALUES ('153', '1', '11', 'Kabupaten Serang', 'Ciruas', null, null);
INSERT INTO "mst_regency" VALUES ('154', '1', '11', 'Kabupaten Pandeglang', 'Pandeglang', null, null);
INSERT INTO "mst_regency" VALUES ('155', '1', '11', 'Kabupaten Lebak', 'Rangkasbitung', null, null);
INSERT INTO "mst_regency" VALUES ('156', '1', '11', 'Kabupaten Tangerang', 'Tigaraksa', null, null);
INSERT INTO "mst_regency" VALUES ('157', '1', '11', 'Kota Cilegon', 'Cilegon', null, null);
INSERT INTO "mst_regency" VALUES ('158', '1', '11', 'Kota Serang', 'Serang', null, null);
INSERT INTO "mst_regency" VALUES ('159', '1', '11', 'Kota Tangerang', 'Tangerang', null, null);
INSERT INTO "mst_regency" VALUES ('160', '1', '12', 'Kota Bandung', 'Bandung', null, null);
INSERT INTO "mst_regency" VALUES ('161', '1', '12', 'Kota Banjar', 'Banjar', null, null);
INSERT INTO "mst_regency" VALUES ('162', '1', '12', 'Kota Bekasi', 'Bekasi', null, null);
INSERT INTO "mst_regency" VALUES ('163', '1', '12', 'Kota Bogor', 'Bogor', null, null);
INSERT INTO "mst_regency" VALUES ('164', '1', '12', 'Kabupaten Ciamis', 'Ciamis', null, null);
INSERT INTO "mst_regency" VALUES ('165', '1', '12', 'Kabupaten Cianjur', 'Cianjur', null, null);
INSERT INTO "mst_regency" VALUES ('166', '1', '12', 'Kabupaten Bogor', 'Cibinong', null, null);
INSERT INTO "mst_regency" VALUES ('167', '1', '12', 'Kabupaten Bekasi', 'Cikarang', null, null);
INSERT INTO "mst_regency" VALUES ('168', '1', '12', 'Kota Cimahi', 'Cimahi', null, null);
INSERT INTO "mst_regency" VALUES ('169', '1', '12', 'Kota Cirebon', 'Cirebon', null, null);
INSERT INTO "mst_regency" VALUES ('170', '1', '12', 'Kota Sukabumi', 'Cisaat', null, null);
INSERT INTO "mst_regency" VALUES ('171', '1', '12', 'Kota Depok', 'Depok', null, null);
INSERT INTO "mst_regency" VALUES ('172', '1', '12', 'Kabupaten Garut', 'Garut', null, null);
INSERT INTO "mst_regency" VALUES ('173', '1', '12', 'Kabupaten Indramayu', 'Indramayu', null, null);
INSERT INTO "mst_regency" VALUES ('174', '1', '12', 'Kabupaten Karawang', 'Karawang', null, null);
INSERT INTO "mst_regency" VALUES ('175', '1', '12', 'Kabupaten Kuningan', 'Kuningan', null, null);
INSERT INTO "mst_regency" VALUES ('176', '1', '12', 'Kabupaten Majalengka', 'Majalengka', null, null);
INSERT INTO "mst_regency" VALUES ('177', '1', '12', 'Kabupaten Bandung Barat', 'Ngamprah', null, null);
INSERT INTO "mst_regency" VALUES ('178', '1', '12', 'Kabupaten Sukabumi', 'Sukabumi', null, null);
INSERT INTO "mst_regency" VALUES ('179', '1', '12', 'Kabupaten Purwakarta', 'Purwakarta', null, null);
INSERT INTO "mst_regency" VALUES ('180', '1', '12', 'Kabupaten Tasikmalaya', 'Singaparna', null, null);
INSERT INTO "mst_regency" VALUES ('181', '1', '12', 'Kabupaten Bandung', 'Soreang', null, null);
INSERT INTO "mst_regency" VALUES ('182', '1', '12', 'Kabupaten Subang', 'Subang', null, null);
INSERT INTO "mst_regency" VALUES ('183', '1', '12', 'Kabupaten Cirebon', 'Sumber', null, null);
INSERT INTO "mst_regency" VALUES ('184', '1', '12', 'Kabupaten Sumedang', 'Sumedang', null, null);
INSERT INTO "mst_regency" VALUES ('185', '1', '12', 'Kota Tasikmalaya', 'Tasikmalaya', null, null);
INSERT INTO "mst_regency" VALUES ('186', '1', '13', 'Kabupaten Kepulauan Seribu', 'Pulau Pramuka', null, null);
INSERT INTO "mst_regency" VALUES ('187', '1', '13', 'Kota Jakarta Barat', 'Jakarta Barat', null, null);
INSERT INTO "mst_regency" VALUES ('188', '1', '13', 'Kota Jakarta Pusat', 'Jakarta Pusat', null, null);
INSERT INTO "mst_regency" VALUES ('189', '1', '13', 'Kota Jakarta Selatan', 'Jakarta Selatan', null, null);
INSERT INTO "mst_regency" VALUES ('190', '1', '13', 'Kota Jakarta Timur', 'Jakarta Timur', null, null);
INSERT INTO "mst_regency" VALUES ('191', '1', '13', 'Kota Jakarta Utara', 'Jakarta Utara', null, null);
INSERT INTO "mst_regency" VALUES ('192', '1', '14', 'Kabupaten Banjarnegara', 'Banjarnegara', null, null);
INSERT INTO "mst_regency" VALUES ('193', '1', '14', 'Kabupaten Batang', 'Batang', null, null);
INSERT INTO "mst_regency" VALUES ('194', '1', '14', 'Kabupaten Blora', 'Blora', null, null);
INSERT INTO "mst_regency" VALUES ('195', '1', '14', 'Kabupaten Boyolali', 'Boyolali', null, null);
INSERT INTO "mst_regency" VALUES ('196', '1', '14', 'Kabupaten Brebes', 'Brebes', null, null);
INSERT INTO "mst_regency" VALUES ('197', '1', '14', 'Kabupaten Cilacap', 'Cilacap', null, null);
INSERT INTO "mst_regency" VALUES ('198', '1', '14', 'Kabupaten Demak', 'Demak', null, null);
INSERT INTO "mst_regency" VALUES ('199', '1', '14', 'Kabupaten Jepara', 'Jepara', null, null);
INSERT INTO "mst_regency" VALUES ('200', '1', '14', 'Kabupaten Pekalongan', 'Kajen', null, null);
INSERT INTO "mst_regency" VALUES ('201', '1', '14', 'Kabupaten Karanganyar', 'Karanganyar', null, null);
INSERT INTO "mst_regency" VALUES ('202', '1', '14', 'Kabupaten Kebumen', 'Kebumen', null, null);
INSERT INTO "mst_regency" VALUES ('203', '1', '14', 'Kabupaten Kendal', 'Kendal', null, null);
INSERT INTO "mst_regency" VALUES ('204', '1', '14', 'Kabupaten Klaten', 'Klaten', null, null);
INSERT INTO "mst_regency" VALUES ('205', '1', '14', 'Kabupaten Kudus', 'Kudus', null, null);
INSERT INTO "mst_regency" VALUES ('206', '1', '14', 'Kabupaten Magelang', 'Mungkid', null, null);
INSERT INTO "mst_regency" VALUES ('207', '1', '14', 'Kabupaten Pati', 'Pati', null, null);
INSERT INTO "mst_regency" VALUES ('208', '1', '14', 'Kabupaten Pemalang', 'Pemalang', null, null);
INSERT INTO "mst_regency" VALUES ('209', '1', '14', 'Kabupaten Purbalingga', 'Purbalingga', null, null);
INSERT INTO "mst_regency" VALUES ('210', '1', '14', 'Kabupaten Grobogan', 'Purwodadi', null, null);
INSERT INTO "mst_regency" VALUES ('211', '1', '14', 'Kabupaten Banyumas', 'Purwokerto', null, null);
INSERT INTO "mst_regency" VALUES ('212', '1', '14', 'Kabupaten Purworejo', 'Purworejo', null, null);
INSERT INTO "mst_regency" VALUES ('213', '1', '14', 'Kabupaten Rembang', 'Rembang', null, null);
INSERT INTO "mst_regency" VALUES ('214', '1', '14', 'Kabupaten Tegal', 'Slawi', null, null);
INSERT INTO "mst_regency" VALUES ('215', '1', '14', 'Kabupaten Sragen', 'Sragen', null, null);
INSERT INTO "mst_regency" VALUES ('216', '1', '14', 'Kabupaten Sukoharjo', 'Sukoharjo', null, null);
INSERT INTO "mst_regency" VALUES ('217', '1', '14', 'Kabupaten Temanggung', 'Temanggung', null, null);
INSERT INTO "mst_regency" VALUES ('218', '1', '14', 'Kabupaten Semarang', 'Ungaran', null, null);
INSERT INTO "mst_regency" VALUES ('219', '1', '14', 'Kabupaten Wonogiri', 'Wonogiri', null, null);
INSERT INTO "mst_regency" VALUES ('220', '1', '14', 'Kabupaten Wonosobo', 'Wonosobo', null, null);
INSERT INTO "mst_regency" VALUES ('221', '1', '14', 'Kota Magelang', 'Magelang', null, null);
INSERT INTO "mst_regency" VALUES ('222', '1', '14', 'Kota Pekalongan', 'Pekalongan', null, null);
INSERT INTO "mst_regency" VALUES ('223', '1', '14', 'Kota Salatiga', 'Salatiga', null, null);
INSERT INTO "mst_regency" VALUES ('224', '1', '14', 'Kota Semarang', 'Semarang', null, null);
INSERT INTO "mst_regency" VALUES ('225', '1', '14', 'Kota Surakarta', 'Surakarta', null, null);
INSERT INTO "mst_regency" VALUES ('226', '1', '14', 'Kota Tegal', 'Tegal', null, null);
INSERT INTO "mst_regency" VALUES ('227', '1', '15', 'Kabupaten Bangkalan', 'Bangkalan', null, null);
INSERT INTO "mst_regency" VALUES ('228', '1', '15', 'Kabupaten Banyuwangi', 'Banyuwangi', null, null);
INSERT INTO "mst_regency" VALUES ('229', '1', '15', 'Kabupaten Bojonegoro', 'Bojonegoro', null, null);
INSERT INTO "mst_regency" VALUES ('230', '1', '15', 'Kabupaten Bondowoso', 'Bondowoso', null, null);
INSERT INTO "mst_regency" VALUES ('231', '1', '15', 'Kabupaten Gresik', 'Gresik', null, null);
INSERT INTO "mst_regency" VALUES ('232', '1', '15', 'Kabupaten Jember', 'Jember', null, null);
INSERT INTO "mst_regency" VALUES ('233', '1', '15', 'Kabupaten Jombang', 'Jombang', null, null);
INSERT INTO "mst_regency" VALUES ('234', '1', '15', 'Kabupaten Blitar', 'Kanigoro', null, null);
INSERT INTO "mst_regency" VALUES ('235', '1', '15', 'Kabupaten Kediri', 'Kediri', null, null);
INSERT INTO "mst_regency" VALUES ('236', '1', '15', 'Kabupaten Malang', 'Kepanjen', null, null);
INSERT INTO "mst_regency" VALUES ('237', '1', '15', 'Kabupaten Probolinggo', 'Kraksaan', null, null);
INSERT INTO "mst_regency" VALUES ('238', '1', '15', 'Kabupaten Lamongan', 'Lamongan', null, null);
INSERT INTO "mst_regency" VALUES ('239', '1', '15', 'Kabupaten Lumajang', 'Lumajang', null, null);
INSERT INTO "mst_regency" VALUES ('240', '1', '15', 'Kabupaten Madiun', 'Madiun', null, null);
INSERT INTO "mst_regency" VALUES ('241', '1', '15', 'Kabupaten Magetan', 'Magetan', null, null);
INSERT INTO "mst_regency" VALUES ('242', '1', '15', 'Kabupaten Mojokerto', 'Mojokerto', null, null);
INSERT INTO "mst_regency" VALUES ('243', '1', '15', 'Kabupaten Nganjuk', 'Nganjuk', null, null);
INSERT INTO "mst_regency" VALUES ('244', '1', '15', 'Kabupaten Ngawi', 'Ngawi', null, null);
INSERT INTO "mst_regency" VALUES ('245', '1', '15', 'Kabupaten Pacitan', 'Pacitan', null, null);
INSERT INTO "mst_regency" VALUES ('246', '1', '15', 'Kabupaten Pamekasan', 'Pamekasan', null, null);
INSERT INTO "mst_regency" VALUES ('247', '1', '15', 'Kabupaten Pasuruan', 'Pasuruan', null, null);
INSERT INTO "mst_regency" VALUES ('248', '1', '15', 'Kabupaten Ponorogo', 'Ponorogo', null, null);
INSERT INTO "mst_regency" VALUES ('249', '1', '15', 'Kabupaten Sampang', 'Sampang', null, null);
INSERT INTO "mst_regency" VALUES ('250', '1', '15', 'Kabupaten Sidoarjo', 'Sidoarjo', null, null);
INSERT INTO "mst_regency" VALUES ('251', '1', '15', 'Kabupaten Situbondo', 'Situbondo', null, null);
INSERT INTO "mst_regency" VALUES ('252', '1', '15', 'Kabupaten Sumenep', 'Sumenep', null, null);
INSERT INTO "mst_regency" VALUES ('253', '1', '15', 'Kabupaten Trenggalek', 'Trenggalek', null, null);
INSERT INTO "mst_regency" VALUES ('254', '1', '15', 'Kabupaten Tuban', 'Tuban', null, null);
INSERT INTO "mst_regency" VALUES ('255', '1', '15', 'Kabupaten Tulungagung', 'Tulungagung', null, null);
INSERT INTO "mst_regency" VALUES ('256', '1', '15', 'Kota Batu[8]', 'Batu', null, null);
INSERT INTO "mst_regency" VALUES ('257', '1', '15', 'Kota Blitar', 'Blitar', null, null);
INSERT INTO "mst_regency" VALUES ('258', '1', '15', 'Kota Kediri', 'Kediri', null, null);
INSERT INTO "mst_regency" VALUES ('259', '1', '15', 'Kota Madiun', 'Madiun', null, null);
INSERT INTO "mst_regency" VALUES ('260', '1', '15', 'Kota Malang', 'Malang', null, null);
INSERT INTO "mst_regency" VALUES ('261', '1', '15', 'Kota Mojokerto', 'Mojokerto', null, null);
INSERT INTO "mst_regency" VALUES ('262', '1', '15', 'Kota Pasuruan', 'Pasuruan', null, null);
INSERT INTO "mst_regency" VALUES ('263', '1', '15', 'Kota Probolinggo', 'Probolinggo', null, null);
INSERT INTO "mst_regency" VALUES ('264', '1', '15', 'Kota Surabaya', 'Surabaya', null, null);
INSERT INTO "mst_regency" VALUES ('265', '1', '16', 'Kabupaten Bantul', 'Bantul', null, null);
INSERT INTO "mst_regency" VALUES ('266', '1', '16', 'Kabupaten Sleman', 'Sleman', null, null);
INSERT INTO "mst_regency" VALUES ('267', '1', '16', 'Kabupaten Kulon Progo', 'Wates', null, null);
INSERT INTO "mst_regency" VALUES ('268', '1', '16', 'Kabupaten Gunung Kidul', 'Wonosari', null, null);
INSERT INTO "mst_regency" VALUES ('269', '1', '16', 'Kota Yogyakarta', 'Yogyakarta', null, null);
INSERT INTO "mst_regency" VALUES ('270', '1', '17', 'Kabupaten Badung', 'Badung', null, null);
INSERT INTO "mst_regency" VALUES ('271', '1', '17', 'Kabupaten Bangli', 'Bangli', null, null);
INSERT INTO "mst_regency" VALUES ('272', '1', '17', 'Kabupaten Gianyar', 'Gianyar', null, null);
INSERT INTO "mst_regency" VALUES ('273', '1', '17', 'Kabupaten Karangasem', 'Karangasem', null, null);
INSERT INTO "mst_regency" VALUES ('274', '1', '17', 'Kabupaten Klungkung', 'Klungkung', null, null);
INSERT INTO "mst_regency" VALUES ('275', '1', '17', 'Kabupaten Jembrana', 'Negara', null, null);
INSERT INTO "mst_regency" VALUES ('276', '1', '17', 'Kabupaten Buleleng', 'Singaraja', null, null);
INSERT INTO "mst_regency" VALUES ('277', '1', '17', 'Kabupaten Tabanan', 'Tabanan', null, null);
INSERT INTO "mst_regency" VALUES ('278', '1', '17', 'Kota Denpasar', 'Denpasar', null, null);
INSERT INTO "mst_regency" VALUES ('279', '1', '18', 'Kabupaten Dompu', 'Dompu', null, null);
INSERT INTO "mst_regency" VALUES ('280', '1', '18', 'Kabupaten Lombok Barat', 'Mataram', null, null);
INSERT INTO "mst_regency" VALUES ('281', '1', '18', 'Kabupaten Lombok Tengah', 'Praya', null, null);
INSERT INTO "mst_regency" VALUES ('282', '1', '18', 'Kabupaten Bima', 'Raba', null, null);
INSERT INTO "mst_regency" VALUES ('283', '1', '18', 'Kabupaten Lombok Timur', 'Selong', null, null);
INSERT INTO "mst_regency" VALUES ('284', '1', '18', 'Kabupaten Sumbawa', 'Sumbawa Besar', null, null);
INSERT INTO "mst_regency" VALUES ('285', '1', '18', 'Kabupaten Sumbawa Barat', 'Taliwang', null, null);
INSERT INTO "mst_regency" VALUES ('286', '1', '18', 'Kabupaten Lombok Utara', 'Tanjung', null, null);
INSERT INTO "mst_regency" VALUES ('287', '1', '18', 'Kota Bima', 'Bima', null, null);
INSERT INTO "mst_regency" VALUES ('288', '1', '18', 'Kota Mataram', 'Mataram', null, null);
INSERT INTO "mst_regency" VALUES ('289', '1', '19', 'Kabupaten Belu', 'Atambua', null, null);
INSERT INTO "mst_regency" VALUES ('290', '1', '19', 'Kabupaten Rote Ndao', 'Baa', null, null);
INSERT INTO "mst_regency" VALUES ('291', '1', '19', 'Kabupaten Ngada', 'Bajawa', null, null);
INSERT INTO "mst_regency" VALUES ('292', '1', '19', 'Kabupaten Manggarai Timur', 'Borong', null, null);
INSERT INTO "mst_regency" VALUES ('293', '1', '19', 'Kabupaten Ende', 'Ende', null, null);
INSERT INTO "mst_regency" VALUES ('294', '1', '19', 'Kabupaten Alor', 'Kalabahi', null, null);
INSERT INTO "mst_regency" VALUES ('295', '1', '19', 'Kabupaten Timor Tengah Utara', 'Kefamenanu', null, null);
INSERT INTO "mst_regency" VALUES ('296', '1', '19', 'Kabupaten Kupang', 'Kupang', null, null);
INSERT INTO "mst_regency" VALUES ('297', '1', '19', 'Kota Kupang', 'Kupang', null, null);
INSERT INTO "mst_regency" VALUES ('298', '1', '19', 'Kabupaten Manggarai Barat', 'Labuan Bajo', null, null);
INSERT INTO "mst_regency" VALUES ('299', '1', '19', 'Kabupaten Flores Timur', 'Larantuka', null, null);
INSERT INTO "mst_regency" VALUES ('300', '1', '19', 'Kabupaten Lembata', 'Lewoleba', null, null);
INSERT INTO "mst_regency" VALUES ('301', '1', '19', 'Kabupaten Sikka', 'Maumere', null, null);
INSERT INTO "mst_regency" VALUES ('302', '1', '19', 'Kabupaten Nagekeo', 'Mbay', null, null);
INSERT INTO "mst_regency" VALUES ('303', '1', '19', 'Kabupaten Manggarai', 'Ruteng', null, null);
INSERT INTO "mst_regency" VALUES ('304', '1', '19', 'Kabupaten Sabu Raijua', 'Seba', null, null);
INSERT INTO "mst_regency" VALUES ('305', '1', '19', 'Kabupaten Timor Tengah Selatan', 'Soe', null, null);
INSERT INTO "mst_regency" VALUES ('306', '1', '19', 'Kabupaten Sumba Barat Daya', 'Tambolaka', null, null);
INSERT INTO "mst_regency" VALUES ('307', '1', '19', 'Kabupaten Sumba Tengah', 'Waibakul', null, null);
INSERT INTO "mst_regency" VALUES ('308', '1', '19', 'Kabupaten Sumba Barat', 'Waikabubak', null, null);
INSERT INTO "mst_regency" VALUES ('309', '1', '19', 'Kabupaten Sumba Timur', 'Waingapu', null, null);
INSERT INTO "mst_regency" VALUES ('310', '1', '20', 'Kabupaten Bengkayang', 'Bengkayang', null, null);
INSERT INTO "mst_regency" VALUES ('311', '1', '20', 'Kabupaten Ketapang', 'Ketapang', null, null);
INSERT INTO "mst_regency" VALUES ('312', '1', '20', 'Kabupaten Pontianak', 'Mempawah', null, null);
INSERT INTO "mst_regency" VALUES ('313', '1', '20', 'Kabupaten Melawi', 'Nanga Pinoh', null, null);
INSERT INTO "mst_regency" VALUES ('314', '1', '20', 'Kabupaten Landak', 'Ngabang', null, null);
INSERT INTO "mst_regency" VALUES ('315', '1', '20', 'Kabupaten Kapuas Hulu', 'Putussibau', null, null);
INSERT INTO "mst_regency" VALUES ('316', '1', '20', 'Kabupaten Sambas', 'Sambas', null, null);
INSERT INTO "mst_regency" VALUES ('317', '1', '20', 'Kabupaten Sanggau', 'Sanggau', null, null);
INSERT INTO "mst_regency" VALUES ('318', '1', '20', 'Kabupaten Sekadau', 'Sekadau', null, null);
INSERT INTO "mst_regency" VALUES ('319', '1', '20', 'Kabupaten Sintang', 'Sintang', null, null);
INSERT INTO "mst_regency" VALUES ('320', '1', '20', 'Kabupaten Kayong Utara', 'Sukadana', null, null);
INSERT INTO "mst_regency" VALUES ('321', '1', '20', 'Kabupaten Kubu Raya', 'Sungai Raya', null, null);
INSERT INTO "mst_regency" VALUES ('322', '1', '20', 'Kota Pontianak', 'Pontianak', null, null);
INSERT INTO "mst_regency" VALUES ('323', '1', '20', 'Kota Singkawang', 'Singkawang', null, null);
INSERT INTO "mst_regency" VALUES ('324', '1', '21', 'Kabupaten Hulu Sungai Utara', 'Amuntai', null, null);
INSERT INTO "mst_regency" VALUES ('325', '1', '21', 'Kota Banjarbaru', 'Banjarbaru', null, null);
INSERT INTO "mst_regency" VALUES ('326', '1', '21', 'Kota Banjarmasin', 'Banjarmasin', null, null);
INSERT INTO "mst_regency" VALUES ('327', '1', '21', 'Kabupaten Hulu Sungai Tengah', 'Barabai', null, null);
INSERT INTO "mst_regency" VALUES ('328', '1', '21', 'Kabupaten Tanah Bumbu', 'Batulicin', null, null);
INSERT INTO "mst_regency" VALUES ('329', '1', '21', 'Kabupaten Hulu Sungai Selatan', 'Kandangan', null, null);
INSERT INTO "mst_regency" VALUES ('330', '1', '21', 'Kabupaten Kotabaru', 'Kotabaru', null, null);
INSERT INTO "mst_regency" VALUES ('331', '1', '21', 'Kabupaten Barito Kuala', 'Marabahan', null, null);
INSERT INTO "mst_regency" VALUES ('332', '1', '21', 'Kabupaten Banjar', 'Martapura', null, null);
INSERT INTO "mst_regency" VALUES ('333', '1', '21', 'Kabupaten Balangan', 'Paringin', null, null);
INSERT INTO "mst_regency" VALUES ('334', '1', '21', 'Kabupaten Tanah Laut', 'Pelaihari', null, null);
INSERT INTO "mst_regency" VALUES ('335', '1', '21', 'Kabupaten Tapin', 'Rantau', null, null);
INSERT INTO "mst_regency" VALUES ('336', '1', '21', 'Kabupaten Tabalong', 'Tanjung', null, null);
INSERT INTO "mst_regency" VALUES ('337', '1', '22', 'Kabupaten Barito Selatan', 'Buntok', null, null);
INSERT INTO "mst_regency" VALUES ('338', '1', '22', 'Kabupaten Katingan', 'Kasongan', null, null);
INSERT INTO "mst_regency" VALUES ('339', '1', '22', 'Kabupaten Kapuas', 'Kuala Kapuas', null, null);
INSERT INTO "mst_regency" VALUES ('340', '1', '22', 'Kabupaten Gunung Mas', 'Kuala Kurun', null, null);
INSERT INTO "mst_regency" VALUES ('341', '1', '22', 'Kabupaten Seruyan', 'Kuala Pembuang', null, null);
INSERT INTO "mst_regency" VALUES ('342', '1', '22', 'Kabupaten Barito Utara', 'Muara Teweh', null, null);
INSERT INTO "mst_regency" VALUES ('343', '1', '22', 'Kabupaten Lamandau', 'Nanga Bulik', null, null);
INSERT INTO "mst_regency" VALUES ('344', '1', '22', 'Kabupaten Kotawaringin Barat', 'Pangkalan Bun', null, null);
INSERT INTO "mst_regency" VALUES ('345', '1', '22', 'Kabupaten Pulang Pisau', 'Pulang Pisau', null, null);
INSERT INTO "mst_regency" VALUES ('346', '1', '22', 'Kabupaten Murung Raya', 'Purukcahu', null, null);
INSERT INTO "mst_regency" VALUES ('347', '1', '22', 'Kabupaten Kotawaringin Timur', 'Sampit', null, null);
INSERT INTO "mst_regency" VALUES ('348', '1', '22', 'Kabupaten Sukamara', 'Sukamara', null, null);
INSERT INTO "mst_regency" VALUES ('349', '1', '22', 'Kabupaten Barito Timur', 'Tamiang', null, null);
INSERT INTO "mst_regency" VALUES ('350', '1', '22', 'Kota Palangka Raya', 'Palangka Raya', null, null);
INSERT INTO "mst_regency" VALUES ('351', '1', '23', 'Kabupaten Malinau', 'Malinau', null, null);
INSERT INTO "mst_regency" VALUES ('352', '1', '23', 'Kabupaten Nunukan', 'Nunukan', null, null);
INSERT INTO "mst_regency" VALUES ('353', '1', '23', 'Kabupaten Penajam Paser Utara', 'Penajam', null, null);
INSERT INTO "mst_regency" VALUES ('354', '1', '23', 'Kabupaten Kutai Timur', 'Sangatta', null, null);
INSERT INTO "mst_regency" VALUES ('355', '1', '23', 'Kabupaten Kutai Barat', 'Sendawar', null, null);
INSERT INTO "mst_regency" VALUES ('356', '1', '23', 'Kabupaten Paser', 'Tanah Grogot', null, null);
INSERT INTO "mst_regency" VALUES ('357', '1', '23', 'Kabupaten Berau', 'Tanjungredep', null, null);
INSERT INTO "mst_regency" VALUES ('358', '1', '23', 'Kabupaten Bulungan', 'Tanjungselor', null, null);
INSERT INTO "mst_regency" VALUES ('359', '1', '23', 'Kabupaten Kutai Kartanegara', 'Tenggarong', null, null);
INSERT INTO "mst_regency" VALUES ('360', '1', '23', 'Kabupaten Tana Tidung', 'Tideng Pale', null, null);
INSERT INTO "mst_regency" VALUES ('361', '1', '23', 'Kota Balikpapan', 'Balikpapan', null, null);
INSERT INTO "mst_regency" VALUES ('362', '1', '23', 'Kota Bontang', 'Bontang', null, null);
INSERT INTO "mst_regency" VALUES ('363', '1', '23', 'Kota Samarinda', 'Samarinda', null, null);
INSERT INTO "mst_regency" VALUES ('364', '1', '23', 'Kota Tarakan', 'Tarakan', null, null);
INSERT INTO "mst_regency" VALUES ('365', '1', '24', 'Kabupaten Gorontalo', 'Gorontalo', null, null);
INSERT INTO "mst_regency" VALUES ('366', '1', '24', 'Kabupaten Gorontalo Utara', 'Kwandang', null, null);
INSERT INTO "mst_regency" VALUES ('367', '1', '24', 'Kabupaten Pohuwato', 'Marisa', null, null);
INSERT INTO "mst_regency" VALUES ('368', '1', '24', 'Kabupaten Boalemo', 'Marisa/Tilamuta', null, null);
INSERT INTO "mst_regency" VALUES ('369', '1', '24', 'Kabupaten Bone Bolango', 'Suwawa', null, null);
INSERT INTO "mst_regency" VALUES ('370', '1', '24', 'Kota Gorontalo', 'Gorontalo', null, null);
INSERT INTO "mst_regency" VALUES ('371', '1', '25', 'Kabupaten Bantaeng', 'Bantaeng', null, null);
INSERT INTO "mst_regency" VALUES ('372', '1', '25', 'Kabupaten Barru', 'Barru', null, null);
INSERT INTO "mst_regency" VALUES ('373', '1', '25', 'Kabupaten Kepulauan Selayar', 'Benteng', null, null);
INSERT INTO "mst_regency" VALUES ('374', '1', '25', 'Kabupaten Bulukumba', 'Bulukumba', null, null);
INSERT INTO "mst_regency" VALUES ('375', '1', '25', 'Kabupaten Enrekang', 'Enrekang', null, null);
INSERT INTO "mst_regency" VALUES ('376', '1', '25', 'Kabupaten Jeneponto', 'Jeneponto', null, null);
INSERT INTO "mst_regency" VALUES ('377', '1', '25', 'Kabupaten Tana Toraja', 'Makale', null, null);
INSERT INTO "mst_regency" VALUES ('378', '1', '25', 'Kabupaten Luwu Timur', 'Malili', null, null);
INSERT INTO "mst_regency" VALUES ('379', '1', '25', 'Kabupaten Maros', 'Maros', null, null);
INSERT INTO "mst_regency" VALUES ('380', '1', '25', 'Kabupaten Luwu Utara', 'Masamba', null, null);
INSERT INTO "mst_regency" VALUES ('381', '1', '25', 'Kabupaten Luwu', 'Palopo', null, null);
INSERT INTO "mst_regency" VALUES ('382', '1', '25', 'Kabupaten Pangkajene dan Kepulauan', 'Pangkajene', null, null);
INSERT INTO "mst_regency" VALUES ('383', '1', '25', 'Kabupaten Pinrang', 'Pinrang', null, null);
INSERT INTO "mst_regency" VALUES ('384', '1', '25', 'Kabupaten Toraja Utara', 'Rantepao', null, null);
INSERT INTO "mst_regency" VALUES ('385', '1', '25', 'Kabupaten Wajo', 'Sengkang', null, null);
INSERT INTO "mst_regency" VALUES ('386', '1', '25', 'Kabupaten Sidenreng Rappang', 'Sidenreng', null, null);
INSERT INTO "mst_regency" VALUES ('387', '1', '25', 'Kabupaten Sinjai', 'Sinjai', null, null);
INSERT INTO "mst_regency" VALUES ('388', '1', '25', 'Kabupaten Gowa', 'Sunggu Minasa', null, null);
INSERT INTO "mst_regency" VALUES ('389', '1', '25', 'Kabupaten Takalar', 'Takalar', null, null);
INSERT INTO "mst_regency" VALUES ('390', '1', '25', 'Kabupaten Bone', 'Watampone', null, null);
INSERT INTO "mst_regency" VALUES ('391', '1', '25', 'Kabupaten Soppeng', 'Watan Soppeng', null, null);
INSERT INTO "mst_regency" VALUES ('392', '1', '25', 'Kota Makassar', 'Makassar', null, null);
INSERT INTO "mst_regency" VALUES ('393', '1', '25', 'Kota Palopo', 'Palopo', null, null);
INSERT INTO "mst_regency" VALUES ('394', '1', '25', 'Kota Parepare', 'Parepare', null, null);
INSERT INTO "mst_regency" VALUES ('395', '1', '26', 'Kabupaten Konawe Selatan', 'Andolo', null, null);
INSERT INTO "mst_regency" VALUES ('396', '1', '26', 'Kabupaten Buton', 'BauBau', null, null);
INSERT INTO "mst_regency" VALUES ('397', '1', '26', 'Kabupaten Buton Utara', 'Buranga', null, null);
INSERT INTO "mst_regency" VALUES ('398', '1', '26', 'Kabupaten Kolaka', 'Kolaka', null, null);
INSERT INTO "mst_regency" VALUES ('399', '1', '26', 'Kabupaten Kolaka Utara', 'Lasusua', null, null);
INSERT INTO "mst_regency" VALUES ('400', '1', '26', 'Kabupaten Muna', 'Raha', null, null);
INSERT INTO "mst_regency" VALUES ('401', '1', '26', 'Kabupaten Bombana', 'Rumbia', null, null);
INSERT INTO "mst_regency" VALUES ('402', '1', '26', 'Kabupaten Konawe', 'Unaaha', null, null);
INSERT INTO "mst_regency" VALUES ('403', '1', '26', 'Kabupaten Konawe Utara', 'Wanggudu', null, null);
INSERT INTO "mst_regency" VALUES ('404', '1', '26', 'Kabupaten Wakatobi', 'WangiWangi', null, null);
INSERT INTO "mst_regency" VALUES ('405', '1', '26', 'Kota BauBau', 'BauBau', null, null);
INSERT INTO "mst_regency" VALUES ('406', '1', '26', 'Kota Kendari', 'Kendari', null, null);
INSERT INTO "mst_regency" VALUES ('407', '1', '27', 'Kabupaten Tojo UnaUna', 'Ampana', null, null);
INSERT INTO "mst_regency" VALUES ('408', '1', '27', 'Kabupaten Banggai Kepulauan', 'Banggai', null, null);
INSERT INTO "mst_regency" VALUES ('409', '1', '27', 'Kabupaten Morowali', 'Bungku', null, null);
INSERT INTO "mst_regency" VALUES ('410', '1', '27', 'Kabupaten Buol', 'Buol', null, null);
INSERT INTO "mst_regency" VALUES ('411', '1', '27', 'Kabupaten Donggala', 'Donggala', null, null);
INSERT INTO "mst_regency" VALUES ('412', '1', '27', 'Kabupaten Banggai', 'Luwuk', null, null);
INSERT INTO "mst_regency" VALUES ('413', '1', '27', 'Kabupaten Parigi Moutong', 'Parigi', null, null);
INSERT INTO "mst_regency" VALUES ('414', '1', '27', 'Kabupaten Poso', 'Poso', null, null);
INSERT INTO "mst_regency" VALUES ('415', '1', '27', 'Kabupaten Sigi', 'Sigi Biromaru', null, null);
INSERT INTO "mst_regency" VALUES ('416', '1', '27', 'Kabupaten ToliToli', 'ToliToli', null, null);
INSERT INTO "mst_regency" VALUES ('417', '1', '27', 'Kota Palu', 'Palu', null, null);
INSERT INTO "mst_regency" VALUES ('418', '1', '28', 'Kabupaten Minahasa Utara', 'Airmadidi', null, null);
INSERT INTO "mst_regency" VALUES ('419', '1', '28', 'Kabupaten Minahasa Selatan', 'Amurang', null, null);
INSERT INTO "mst_regency" VALUES ('420', '1', '28', 'Kabupaten Bolaang Mongondow Selatan', 'Bolaang Uki', null, null);
INSERT INTO "mst_regency" VALUES ('421', '1', '28', 'Kabupaten Bolaang Mongondow Utara', 'Boroko', null, null);
INSERT INTO "mst_regency" VALUES ('422', '1', '28', 'Kabupaten Bolaang Mongondow', 'Kotamobagu', null, null);
INSERT INTO "mst_regency" VALUES ('423', '1', '28', 'Kabupaten Kepulauan Talaud', 'Melonguane', null, null);
INSERT INTO "mst_regency" VALUES ('424', '1', '28', 'Kabupaten Kepulauan Siau Tagulandang Bia', 'Ondong Siau', null, null);
INSERT INTO "mst_regency" VALUES ('425', '1', '28', 'Kabupaten Minahasa Tenggara', 'Ratahan', null, null);
INSERT INTO "mst_regency" VALUES ('426', '1', '28', 'Kabupaten Kepulauan Sangihe', 'Tahuna', null, null);
INSERT INTO "mst_regency" VALUES ('427', '1', '28', 'Kabupaten Minahasa', 'Tondano', null, null);
INSERT INTO "mst_regency" VALUES ('428', '1', '28', 'Kabupaten Bolaang Mongondow Timur', 'Tutuyan', null, null);
INSERT INTO "mst_regency" VALUES ('429', '1', '28', 'Kota Bitung', 'Bitung', null, null);
INSERT INTO "mst_regency" VALUES ('430', '1', '28', 'Kota Kotamobagu', 'Kotamobagu', null, null);
INSERT INTO "mst_regency" VALUES ('431', '1', '28', 'Kota Manado', 'Manado', null, null);
INSERT INTO "mst_regency" VALUES ('432', '1', '28', 'Kota Tomohon', 'Tomohon', null, null);
INSERT INTO "mst_regency" VALUES ('433', '1', '29', 'Kabupaten Majene', 'Majene', null, null);
INSERT INTO "mst_regency" VALUES ('434', '1', '29', 'Kabupaten Mamasa', 'Mamasa', null, null);
INSERT INTO "mst_regency" VALUES ('435', '1', '29', 'Kabupaten Mamuju', 'Mamuju', null, null);
INSERT INTO "mst_regency" VALUES ('436', '1', '29', 'Kabupaten Mamuju Utara', 'Pasangkayu', null, null);
INSERT INTO "mst_regency" VALUES ('437', '1', '29', 'Kabupaten Polewali Mandar', 'Polewali', null, null);
INSERT INTO "mst_regency" VALUES ('438', '1', '30', 'Kabupaten Seram Bagian Timur', 'Dataran Hunimoa', null, null);
INSERT INTO "mst_regency" VALUES ('439', '1', '30', 'Kabupaten Seram Bagian Barat', 'Dataran Hunipopu', null, null);
INSERT INTO "mst_regency" VALUES ('440', '1', '30', 'Kabupaten Maluku Tengah', 'Masohi', null, null);
INSERT INTO "mst_regency" VALUES ('441', '1', '30', 'Kabupaten Buru', 'Namlea', null, null);
INSERT INTO "mst_regency" VALUES ('442', '1', '30', 'Kabupaten Buru Selatan', 'Namrole', null, null);
INSERT INTO "mst_regency" VALUES ('443', '1', '30', 'Kabupaten Kepulauan Aru', 'Oobo', null, null);
INSERT INTO "mst_regency" VALUES ('444', '1', '30', 'Kabupaten Maluku Tenggara Barat', 'Saumlaki', null, null);
INSERT INTO "mst_regency" VALUES ('445', '1', '30', 'Kabupaten Maluku Barat Daya', 'Tiakur', null, null);
INSERT INTO "mst_regency" VALUES ('446', '1', '30', 'Kabupaten Maluku Tenggara', 'Tual', null, null);
INSERT INTO "mst_regency" VALUES ('447', '1', '30', 'Kota Ambon', 'Ambon', null, null);
INSERT INTO "mst_regency" VALUES ('448', '1', '30', 'Kota Tual', 'Tual', null, null);
INSERT INTO "mst_regency" VALUES ('449', '1', '31', 'Kabupaten Halmahera Barat', 'Jailolo', null, null);
INSERT INTO "mst_regency" VALUES ('450', '1', '31', 'Kabupaten Halmahera Selatan', 'Labuha', null, null);
INSERT INTO "mst_regency" VALUES ('451', '1', '31', 'Kabupaten Halmahera Timur', 'Maba', null, null);
INSERT INTO "mst_regency" VALUES ('452', '1', '31', 'Kabupaten Pulau Morotai', 'Morotai Selatan', null, null);
INSERT INTO "mst_regency" VALUES ('453', '1', '31', 'Kabupaten Kepulauan Sula', 'Sanana', null, null);
INSERT INTO "mst_regency" VALUES ('454', '1', '31', 'Kabupaten Halmahera Utara', 'Tobelo', null, null);
INSERT INTO "mst_regency" VALUES ('455', '1', '31', 'Kabupaten Halmahera Tengah', 'Weda', null, null);
INSERT INTO "mst_regency" VALUES ('456', '1', '31', 'Kota Ternate', 'Ternate', null, null);
INSERT INTO "mst_regency" VALUES ('457', '1', '31', 'Kota Tidore Kepulauan', 'Tidore Kepulauan', null, null);
INSERT INTO "mst_regency" VALUES ('458', '1', '32', 'Kabupaten Asmat', 'Agats', null, null);
INSERT INTO "mst_regency" VALUES ('459', '1', '32', 'Kabupaten Biak Numfor', 'Biak', null, null);
INSERT INTO "mst_regency" VALUES ('460', '1', '32', 'Kabupaten Waropen', 'Botawa', null, null);
INSERT INTO "mst_regency" VALUES ('461', '1', '32', 'Kabupaten Mamberamo Raya', 'Burmeso', null, null);
INSERT INTO "mst_regency" VALUES ('462', '1', '32', 'Kabupaten Yalimo', 'Elelim', null, null);
INSERT INTO "mst_regency" VALUES ('463', '1', '32', 'Kabupaten Paniai', 'Enarotali', null, null);
INSERT INTO "mst_regency" VALUES ('464', '1', '32', 'Kabupaten Puncak', 'Ilaga', null, null);
INSERT INTO "mst_regency" VALUES ('465', '1', '32', 'Kabupaten Tolikara', 'Karubaga', null, null);
INSERT INTO "mst_regency" VALUES ('466', '1', '32', 'Kabupaten Nduga', 'Kenyam', null, null);
INSERT INTO "mst_regency" VALUES ('467', '1', '32', 'Kabupaten Mappi', 'Kepi', null, null);
INSERT INTO "mst_regency" VALUES ('468', '1', '32', 'Kabupaten Dogiyai', 'Kigamani', null, null);
INSERT INTO "mst_regency" VALUES ('469', '1', '32', 'Kabupaten Mamberamo Tengah', 'Kobakma', null, null);
INSERT INTO "mst_regency" VALUES ('470', '1', '32', 'Kabupaten Puncak Jaya', 'Kotamulia', null, null);
INSERT INTO "mst_regency" VALUES ('471', '1', '32', 'Kabupaten Merauke', 'Merauke', null, null);
INSERT INTO "mst_regency" VALUES ('472', '1', '32', 'Kabupaten Nabire', 'Nabire', null, null);
INSERT INTO "mst_regency" VALUES ('473', '1', '32', 'Kabupaten Pegunungan Bintang', 'Oksibil', null, null);
INSERT INTO "mst_regency" VALUES ('474', '1', '32', 'Kabupaten Sarmi', 'Sarmi', null, null);
INSERT INTO "mst_regency" VALUES ('475', '1', '32', 'Kabupaten Jayapura', 'Sentani', null, null);
INSERT INTO "mst_regency" VALUES ('476', '1', '32', 'Kabupaten Kepulauan Yapen', 'Serui', null, null);
INSERT INTO "mst_regency" VALUES ('477', '1', '32', 'Kabupaten Supiori', 'Sorendiweri', null, null);
INSERT INTO "mst_regency" VALUES ('478', '1', '32', 'Kabupaten Intan Jaya', 'Sugapa', null, null);
INSERT INTO "mst_regency" VALUES ('479', '1', '32', 'Kabupaten Yahukimo', 'Sumohai', null, null);
INSERT INTO "mst_regency" VALUES ('480', '1', '32', 'Kabupaten Boven Digoel', 'Tanah Merah', null, null);
INSERT INTO "mst_regency" VALUES ('481', '1', '32', 'Kabupaten Deiyai', 'Tigi', null, null);
INSERT INTO "mst_regency" VALUES ('482', '1', '32', 'Kabupaten Mimika', 'Timika', null, null);
INSERT INTO "mst_regency" VALUES ('483', '1', '32', 'Kabupaten Lanny Jaya', 'Tiom', null, null);
INSERT INTO "mst_regency" VALUES ('484', '1', '32', 'Kabupaten Jayawijaya', 'Wamena', null, null);
INSERT INTO "mst_regency" VALUES ('485', '1', '32', 'Kabupaten Keerom', 'Waris', null, null);
INSERT INTO "mst_regency" VALUES ('486', '1', '32', 'Kota Jayapura', 'Jayapura', null, null);
INSERT INTO "mst_regency" VALUES ('487', '1', '33', 'Kabupaten Teluk Bintuni', 'Bintuni', null, null);
INSERT INTO "mst_regency" VALUES ('488', '1', '33', 'Kabupaten Fakfak', 'Fakfak', null, null);
INSERT INTO "mst_regency" VALUES ('489', '1', '33', 'Kabupaten Tambrauw', 'Fef', null, null);
INSERT INTO "mst_regency" VALUES ('490', '1', '33', 'Kabupaten Kaimana', 'Kaimana', null, null);
INSERT INTO "mst_regency" VALUES ('491', '1', '33', 'Kabupaten Maybrat', 'Kumurkek', null, null);
INSERT INTO "mst_regency" VALUES ('492', '1', '33', 'Kabupaten Manokwari', 'Manokwari', null, null);
INSERT INTO "mst_regency" VALUES ('493', '1', '33', 'Kabupaten Teluk Wondama', 'Rasiei', null, null);
INSERT INTO "mst_regency" VALUES ('494', '1', '33', 'Kabupaten Sorong', 'Sorong', null, null);
INSERT INTO "mst_regency" VALUES ('495', '1', '33', 'Kabupaten Sorong Selatan', 'Teminabuan', null, null);
INSERT INTO "mst_regency" VALUES ('496', '1', '33', 'Kabupaten Raja Ampat', 'Waisai', null, null);
INSERT INTO "mst_regency" VALUES ('497', '1', '33', 'Kota Sorong', 'Sorong', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "trx_diagnosa_treathment"
-- ----------------------------
DROP TABLE IF EXISTS "trx_diagnosa_treathment";
CREATE TABLE "trx_diagnosa_treathment" (
	"mdc_id" varchar(45),
	"dat_id" varchar(45) NOT NULL DEFAULT nextval('trx_diagnosa_treathment_seq'::regclass),
	"modi_id" int2,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_diagnosa_treathment" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_diagnosa_treathment"
-- ----------------------------
BEGIN;
INSERT INTO "trx_diagnosa_treathment" VALUES ('12110003', 'DA-12120001', null, null);
INSERT INTO "trx_diagnosa_treathment" VALUES ('12110001', 'DA-12120002', null, null);
INSERT INTO "trx_diagnosa_treathment" VALUES ('12120005', 'DA-12120003', null, null);
INSERT INTO "trx_diagnosa_treathment" VALUES ('12110002', 'DA-12120004', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "trx_reg_poli"
-- ----------------------------
DROP TABLE IF EXISTS "trx_reg_poli";
CREATE TABLE "trx_reg_poli" (
	"rp_id" int4 NOT NULL,
	"pl_id" int4,
	"sd_rekmed" varchar(10),
	"dr_id" int4,
	"rp_datetime" timestamp(6) NULL,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_reg_poli" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "mst_diagnosa"
-- ----------------------------
DROP TABLE IF EXISTS "mst_diagnosa";
CREATE TABLE "mst_diagnosa" (
	"diag_id" int4 NOT NULL DEFAULT nextval('mst_diagnosa_seq'::regclass),
	"diag_name" varchar(50)
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_diagnosa" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_diagnosa"
-- ----------------------------
BEGIN;
INSERT INTO "mst_diagnosa" VALUES ('1', 'tulang sakit');
INSERT INTO "mst_diagnosa" VALUES ('2', 'tulang keram');
INSERT INTO "mst_diagnosa" VALUES ('3', 'disposisi tulang belakang');
INSERT INTO "mst_diagnosa" VALUES ('4', 'disposisi pergelangan kaki');
INSERT INTO "mst_diagnosa" VALUES ('5', 'flu tulang');
INSERT INTO "mst_diagnosa" VALUES ('6', 'sakit a');
INSERT INTO "mst_diagnosa" VALUES ('7', 'sakit b');
COMMIT;

-- ----------------------------
--  Table structure for "mst_religion"
-- ----------------------------
DROP TABLE IF EXISTS "mst_religion";
CREATE TABLE "mst_religion" (
	"mr_id" int4 NOT NULL DEFAULT nextval('mst_religion_seq'::regclass),
	"mr_name" varchar(20),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_religion" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_religion"
-- ----------------------------
BEGIN;
INSERT INTO "mst_religion" VALUES ('1', 'Islam', null, null);
INSERT INTO "mst_religion" VALUES ('2', 'Hindu', null, null);
INSERT INTO "mst_religion" VALUES ('3', 'Kristen', null, null);
INSERT INTO "mst_religion" VALUES ('4', 'Budha', null, null);
INSERT INTO "mst_religion" VALUES ('5', 'Katolik', null, null);
INSERT INTO "mst_religion" VALUES ('6', 'Lainnya', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "sys_menu"
-- ----------------------------
DROP TABLE IF EXISTS "sys_menu";
CREATE TABLE "sys_menu" (
	"menu_id" int4 NOT NULL DEFAULT nextval('sys_menu_seq'::regclass),
	"menu_parent" int4 DEFAULT 0,
	"menu_url" varchar(255),
	"menu_name" varchar(45),
	"menu_status" int4 DEFAULT 1,
	"modul_id" int4,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "sys_menu" OWNER TO "postgres";

-- ----------------------------
--  Records of "sys_menu"
-- ----------------------------
BEGIN;
INSERT INTO "sys_menu" VALUES ('1', '0', 'pendaftaran/pendaftaran_baru', 'pendaftaran baru', '1', '1', null, null);
INSERT INTO "sys_menu" VALUES ('2', '0', 'pendaftaran/pendaftaran_rawat_jalan', 'pendaftaran rawat jalan', '1', '1', null, null);
INSERT INTO "sys_menu" VALUES ('3', '0', 'pendaftaran/pendaftaran_rawat_inap', 'pendaftaran rawat inap', '1', '1', null, null);
INSERT INTO "sys_menu" VALUES ('4', '0', 'pendaftaran/IGD', 'IGD', '1', '1', null, null);
INSERT INTO "sys_menu" VALUES ('5', '0', 'pelayanan_informasi/informasi_pasien', 'informasi pasien', '1', '2', null, null);
INSERT INTO "sys_menu" VALUES ('6', '0', 'pelayanan_informasi/jadwal_dokter', 'jadwal dokter', '1', '2', null, null);
INSERT INTO "sys_menu" VALUES ('7', '0', 'pelayanan_informasi/informasi_kamar', 'informasi kamar', '1', '2', null, null);
INSERT INTO "sys_menu" VALUES ('8', '0', 'pelayanan_informasi/informasi_paket', 'informasi paket', '1', '2', null, null);
INSERT INTO "sys_menu" VALUES ('9', '0', 'rawat_jalan/poli_gigi', 'poli gigi', '1', '3', null, null);
INSERT INTO "sys_menu" VALUES ('10', '0', 'rawat_jalan/poli_tulang', 'poli tulang', '1', '3', null, null);
INSERT INTO "sys_menu" VALUES ('11', '0', 'kasir/rawat_jalan', 'rawat jalan', '1', '4', null, null);
INSERT INTO "sys_menu" VALUES ('12', '0', 'kasir/IGD', 'IGD', '1', '4', null, null);
INSERT INTO "sys_menu" VALUES ('13', '0', 'kasir/rawat_inap', 'rawat inap', '1', '4', null, null);
INSERT INTO "sys_menu" VALUES ('14', '0', 'apotek/resep_pasien', 'resep pasien', '1', '5', null, null);
INSERT INTO "sys_menu" VALUES ('15', '0', 'apotek/pembelian_langsung', 'pembelian langsung', '1', '5', null, null);
INSERT INTO "sys_menu" VALUES ('16', '0', 'gudang_farmasi/purchase_request', 'purchase request', '1', '6', null, null);
INSERT INTO "sys_menu" VALUES ('17', '0', 'gudang_farmasi/purchase_order', 'purchase order', '1', '6', null, null);
INSERT INTO "sys_menu" VALUES ('18', '0', 'gudang_farmasi/receive_item', 'receive item', '1', '6', null, null);
INSERT INTO "sys_menu" VALUES ('19', '0', 'gudang_farmasi/retur', 'retur', '1', '6', null, null);
INSERT INTO "sys_menu" VALUES ('20', '0', 'gudang_farmasi/stok', 'stok', '1', '6', null, null);
INSERT INTO "sys_menu" VALUES ('21', '0', 'gudang_farmasi/transfer_item', 'transfer item', '1', '6', null, null);
INSERT INTO "sys_menu" VALUES ('22', '0', 'pendaftaran/daftar_pasien', 'daftar pasien', '1', '1', null, null);
INSERT INTO "sys_menu" VALUES ('24', '0', 'gudang_farmasi/pos_item', 'pos_item', '1', '6', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "inv_item_type"
-- ----------------------------
DROP TABLE IF EXISTS "inv_item_type";
CREATE TABLE "inv_item_type" (
	"it_id" int4 NOT NULL DEFAULT nextval('inv_item_type_seq'::regclass),
	"it_name" varchar(50),
	"it_status" int4 DEFAULT 1,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "inv_item_type" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "trx_recipe"
-- ----------------------------
DROP TABLE IF EXISTS "trx_recipe";
CREATE TABLE "trx_recipe" (
	"mdc_id" varchar(45),
	"recipe_id" varchar(45) NOT NULL,
	"recipe_paramedic_price" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_recipe" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_recipe"
-- ----------------------------
BEGIN;
INSERT INTO "trx_recipe" VALUES ('12110003', 'RS-12120001', '1000');
INSERT INTO "trx_recipe" VALUES ('12110001', 'RS-12120002', '1000');
INSERT INTO "trx_recipe" VALUES ('12120005', 'RS-12120003', '1000');
INSERT INTO "trx_recipe" VALUES ('12110002', 'RS-12120004', '1000');
COMMIT;

-- ----------------------------
--  Table structure for "mst_insurance"
-- ----------------------------
DROP TABLE IF EXISTS "mst_insurance";
CREATE TABLE "mst_insurance" (
	"ins_id" int2 NOT NULL DEFAULT nextval('mst_insurance_seq'::regclass),
	"ins_name" varchar,
	"modi_id" int2,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_insurance" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_insurance"
-- ----------------------------
BEGIN;
INSERT INTO "mst_insurance" VALUES ('1', 'ASKES', null, null);
INSERT INTO "mst_insurance" VALUES ('2', 'Jamkesmas', null, null);
INSERT INTO "mst_insurance" VALUES ('3', 'Jamkesda', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "com_status"
-- ----------------------------
DROP TABLE IF EXISTS "com_status";
CREATE TABLE "com_status" (
	"cs_id" int4,
	"cs_desc" varchar(50),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "com_status" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "mst_shift"
-- ----------------------------
DROP TABLE IF EXISTS "mst_shift";
CREATE TABLE "mst_shift" (
	"shift_id" int4 NOT NULL DEFAULT nextval('mst_shift_seq'::regclass),
	"shift_nama" varchar(100),
	"shift_start" time(6),
	"shift_end" time(6),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_shift" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_shift"
-- ----------------------------
BEGIN;
INSERT INTO "mst_shift" VALUES ('1', 'shift malam', '00:01:00', '08:00:00', null, '2012-11-25 08:44:59.037');
INSERT INTO "mst_shift" VALUES ('2', 'shift pagi', '08:00:00', '16:00:00', null, '2012-11-25 08:46:03.734');
INSERT INTO "mst_shift" VALUES ('3', 'shift sore', '16:00:00', '24:00:00', null, '2012-11-25 08:46:30.586');
COMMIT;

-- ----------------------------
--  Table structure for "inv_mst_pos"
-- ----------------------------
DROP TABLE IF EXISTS "inv_mst_pos";
CREATE TABLE "inv_mst_pos" (
	"imp_id" varchar(20) NOT NULL DEFAULT nextval('inv_mst_pos_seq'::regclass),
	"imp_name" varchar(100),
	"imp_parent" varchar(20),
	"imp_description" varchar(1000),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now(),
	"imp_status" int2 DEFAULT 1
)
WITH (OIDS=FALSE);
ALTER TABLE "inv_mst_pos" OWNER TO "postgres";

-- ----------------------------
--  Records of "inv_mst_pos"
-- ----------------------------
BEGIN;
INSERT INTO "inv_mst_pos" VALUES ('1', 'a', '0', 'as', null, '2012-12-03 17:17:37.296', '1');
INSERT INTO "inv_mst_pos" VALUES ('12', 'rumah sakit', '2', 'desc', null, '2012-12-03 00:55:23.84', '1');
INSERT INTO "inv_mst_pos" VALUES ('2', 'coba', '0', 'desc', null, '2012-12-03 21:42:33.478', '1');
INSERT INTO "inv_mst_pos" VALUES ('3', 'asd', '12', 'cobas', null, '2012-12-04 18:59:19.066', '1');
INSERT INTO "inv_mst_pos" VALUES ('32', 'melati', '1', 'sadar', null, '2012-12-03 00:56:00.472', '1');
COMMIT;

-- ----------------------------
--  Table structure for "mst_medical_objective"
-- ----------------------------
DROP TABLE IF EXISTS "mst_medical_objective";
CREATE TABLE "mst_medical_objective" (
	"mo_id" int2 NOT NULL DEFAULT nextval('mst_medical_objective_seq'::regclass),
	"mo_indication" varchar(45),
	"mo_filter" char(1),
	"modi_id" int2,
	"modi_datetime" timestamp(6) NULL,
	"pl_id" int2
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_medical_objective" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_medical_objective"
-- ----------------------------
BEGIN;
INSERT INTO "mst_medical_objective" VALUES ('6', 'Tulang Geser', 'N', null, null, '2');
COMMIT;

-- ----------------------------
--  Table structure for "mst_bill"
-- ----------------------------
DROP TABLE IF EXISTS "mst_bill";
CREATE TABLE "mst_bill" (
	"bill_id" varchar(10) NOT NULL,
	"modi_id" varchar(5),
	"modi_datetime" timestamp(6) NULL,
	"bill_name" varchar(50)
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_bill" OWNER TO "postgres";

-- ----------------------------
--  Records of "mst_bill"
-- ----------------------------
BEGIN;
INSERT INTO "mst_bill" VALUES ('1', null, null, 'Biaya Perawatan');
INSERT INTO "mst_bill" VALUES ('2', '', null, 'PPPK/Poli Bedah');
INSERT INTO "mst_bill" VALUES ('3', null, null, 'Biaya Makan');
INSERT INTO "mst_bill" VALUES ('4', null, null, 'Obat-obat infus dll');
INSERT INTO "mst_bill" VALUES ('5', null, null, 'Obat dan Resep');
INSERT INTO "mst_bill" VALUES ('6', null, null, 'Tindakan');
COMMIT;

-- ----------------------------
--  Table structure for "trx_doctor"
-- ----------------------------
DROP TABLE IF EXISTS "trx_doctor";
CREATE TABLE "trx_doctor" (
	"dr_id" int8 NOT NULL DEFAULT nextval('trx_doctor_seq'::regclass),
	"emp_id" int8 NOT NULL,
	"dr_name" varchar(200) NOT NULL,
	"modi_id" int2 NOT NULL,
	"modi_datetime" timestamp(6) NOT NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_doctor" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_doctor"
-- ----------------------------
BEGIN;
INSERT INTO "trx_doctor" VALUES ('1', '1', 'Krishna Yuniar', '12', '2012-12-12 00:00:00');
INSERT INTO "trx_doctor" VALUES ('2', '5', 'Dr. Sigit 2', '12', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('3', '5', 'Dr. Sigit 3', '8', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('4', '3', 'Dr. Sigit 4', '12', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('5', '4', 'Dr. Sigit 5', '14', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('6', '3', 'Dr. Sigit 6', '8', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('7', '4', 'Dr. Sigit 7', '12', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('8', '1', 'Dr. Sigit 8', '15', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('9', '3', 'Dr. Sigit 9', '14', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('10', '5', 'Dr. Sigit 10', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('11', '2', 'Dr. Sigit 11', '9', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('12', '3', 'Dr. Sigit 12', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('13', '5', 'Dr. Sigit 13', '11', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('14', '3', 'Dr. Sigit 14', '15', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('15', '4', 'Dr. Sigit 15', '8', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('16', '1', 'Dr. Sigit 16', '15', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('17', '1', 'Dr. Sigit 17', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('18', '3', 'Dr. Sigit 18', '11', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('19', '3', 'Dr. Sigit 19', '15', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('20', '5', 'Dr. Sigit 20', '9', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('21', '3', 'Dr. Sigit 21', '7', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('22', '4', 'Dr. Sigit 22', '9', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('23', '3', 'Dr. Sigit 23', '7', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('24', '3', 'Dr. Sigit 24', '7', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('25', '5', 'Dr. Sigit 25', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('26', '2', 'Dr. Sigit 26', '15', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('27', '2', 'Dr. Sigit 27', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('28', '5', 'Dr. Sigit 28', '8', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('29', '2', 'Dr. Sigit 29', '11', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('30', '3', 'Dr. Sigit 30', '8', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('31', '5', 'Dr. Sigit 31', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('32', '3', 'Dr. Sigit 32', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('33', '1', 'Dr. Sigit 33', '6', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('34', '1', 'Dr. Sigit 34', '15', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('35', '5', 'Dr. Sigit 35', '15', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('36', '2', 'Dr. Sigit 36', '9', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('37', '1', 'Dr. Sigit 37', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('38', '3', 'Dr. Sigit 38', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('39', '1', 'Dr. Sigit 39', '7', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('40', '5', 'Dr. Sigit 40', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('41', '2', 'Dr. Sigit 41', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('42', '4', 'Dr. Sigit 42', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('43', '1', 'Dr. Sigit 43', '11', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('44', '1', 'Dr. Sigit 44', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('45', '2', 'Dr. Sigit 45', '14', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('46', '3', 'Dr. Sigit 46', '14', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('47', '5', 'Dr. Sigit 47', '6', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('48', '4', 'Dr. Sigit 48', '7', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('49', '5', 'Dr. Sigit 49', '12', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('50', '3', 'Dr. Sigit 50', '12', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('51', '2', 'Dr. Sigit 51', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('52', '3', 'Dr. Sigit 52', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('53', '4', 'Dr. Sigit 53', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('54', '2', 'Dr. Sigit 54', '11', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('55', '4', 'Dr. Sigit 55', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('56', '1', 'Dr. Sigit 56', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('57', '4', 'Dr. Sigit 57', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('58', '1', 'Dr. Sigit 58', '9', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('59', '5', 'Dr. Sigit 59', '12', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('60', '2', 'Dr. Sigit 60', '6', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('61', '2', 'Dr. Sigit 61', '11', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('62', '2', 'Dr. Sigit 62', '14', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('63', '5', 'Dr. Sigit 63', '6', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('64', '5', 'Dr. Sigit 64', '9', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('65', '3', 'Dr. Sigit 65', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('66', '4', 'Dr. Sigit 66', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('67', '2', 'Dr. Sigit 67', '8', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('68', '3', 'Dr. Sigit 68', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('69', '1', 'Dr. Sigit 69', '7', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('70', '1', 'Dr. Sigit 70', '13', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('71', '1', 'Dr. Sigit 71', '6', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('72', '1', 'Dr. Sigit 72', '6', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('73', '4', 'Dr. Sigit 73', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('74', '2', 'Dr. Sigit 74', '12', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('75', '3', 'Dr. Sigit 75', '8', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('76', '2', 'Dr. Sigit 76', '15', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('77', '1', 'Dr. Sigit 77', '11', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('78', '5', 'Dr. Sigit 78', '8', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('79', '4', 'Dr. Sigit 79', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('80', '5', 'Dr. Sigit 80', '15', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('81', '1', 'Dr. Sigit 81', '7', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('82', '4', 'Dr. Sigit 82', '7', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('83', '3', 'Dr. Sigit 83', '10', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('84', '3', 'Dr. Sigit 84', '12', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('85', '3', 'Dr. Sigit 85', '14', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('86', '4', 'Dr. Sigit 86', '8', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('87', '2', 'Dr. Sigit 87', '7', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('88', '1', 'Dr. Sigit 88', '14', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('89', '4', 'Dr. Sigit 89', '11', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('90', '2', 'Dr. Sigit 90', '8', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('91', '1', 'Dr. Sigit 91', '7', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('92', '2', 'Dr. Sigit 92', '9', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('93', '5', 'Dr. Sigit 93', '6', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('94', '2', 'Dr. Sigit 94', '15', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('95', '3', 'Dr. Sigit 95', '7', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('96', '4', 'Dr. Sigit 96', '14', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('97', '3', 'Dr. Sigit 97', '15', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('98', '4', 'Dr. Sigit 98', '6', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('99', '2', 'Dr. Sigit 99', '6', '2012-09-23 00:00:00');
INSERT INTO "trx_doctor" VALUES ('100', '2', 'Dr. Sigit 100', '7', '2012-09-23 00:00:00');
COMMIT;

-- ----------------------------
--  Table structure for "inv_item_master"
-- ----------------------------
DROP TABLE IF EXISTS "inv_item_master";
CREATE TABLE "inv_item_master" (
	"im_id" varchar(20) NOT NULL DEFAULT nextval('inv_item_master_seq'::regclass),
	"im_unit" varchar(20),
	"im_name" varchar(100),
	"im_barcode" varchar(20),
	"im_currency_id" varchar(20),
	"im_item_price_buy" numeric(8,2),
	"im_item_price" numeric(8,2),
	"im_ppn" numeric(6,2),
	"im_reorder_point" numeric(10,2),
	"im_min_stock" numeric(10,2),
	"im_max_stock" numeric(10,2),
	"im_status" int4 DEFAULT 1,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL,
	"im_type" int4,
	"im_vat_status" varchar(50)
)
WITH (OIDS=FALSE);
ALTER TABLE "inv_item_master" OWNER TO "postgres";

-- ----------------------------
--  Records of "inv_item_master"
-- ----------------------------
BEGIN;
INSERT INTO "inv_item_master" VALUES ('OB1', 'tablet', 'Sanaflu', null, null, '2000.00', '2000.00', null, null, null, null, null, null, null, null, null);
INSERT INTO "inv_item_master" VALUES ('OB2', 'tablet', 'Paramex', null, null, '3000.00', '3000.00', null, null, null, null, null, null, null, null, null);
INSERT INTO "inv_item_master" VALUES ('OB3', 'tablet', 'Komix', null, null, '2500.00', '2500.00', null, null, null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for "mst_room"
-- ----------------------------
DROP TABLE IF EXISTS "mst_room";
CREATE TABLE "mst_room" (
	"msro_id" int4 NOT NULL,
	"mscl_id" int4,
	"msro_name" varchar(50),
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "mst_room" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "inv_purchase_order"
-- ----------------------------
DROP TABLE IF EXISTS "inv_purchase_order";
CREATE TABLE "inv_purchase_order" (
	"ipo_id" varchar(32) NOT NULL DEFAULT nextval('inv_purchase_order_seq'::regclass),
	"ipo_invoice" varchar(32),
	"ipo_supplier" varchar(32),
	"ipo_date_req" timestamp(6) NULL,
	"ipo_currency" varchar(32),
	"ipo_vat" varchar(32),
	"ipo_ppn" numeric(8,2),
	"ipo_address" varchar(100),
	"ipo_date_order" timestamp(6) NULL,
	"ipo_operator" varchar(32),
	"ipo_note" text,
	"ipo_status" int4,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now()
)
WITH (OIDS=FALSE);
ALTER TABLE "inv_purchase_order" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "trx_queue_outpatient"
-- ----------------------------
DROP TABLE IF EXISTS "trx_queue_outpatient";
CREATE TABLE "trx_queue_outpatient" (
	"queo_id" varchar(8) NOT NULL,
	"sd_rekmed" varchar(10) NOT NULL,
	"queo_no" int4 NOT NULL,
	"queo_datetime" timestamp(6) NOT NULL,
	"dr_id" int8 NOT NULL,
	"pl_id" int4 NOT NULL,
	"sch_id" varchar(20) NOT NULL,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL DEFAULT now(),
	"queo_status" int2 DEFAULT 1
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_queue_outpatient" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_queue_outpatient"
-- ----------------------------
BEGIN;
INSERT INTO "trx_queue_outpatient" VALUES ('12110001', '1211003', '1', '2012-11-28 12:34:16', '1', '2', '1', '1', '2012-11-28 12:34:41', '1');
INSERT INTO "trx_queue_outpatient" VALUES ('12110002', '1211004', '2', '2012-11-28 00:00:00', '3', '2', '1', null, '2012-11-28 22:47:47.169', '1');
INSERT INTO "trx_queue_outpatient" VALUES ('12110003', '1211012', '3', '2012-11-29 00:00:00', '3', '2', '1', null, '2012-11-29 10:58:25.536', '1');
INSERT INTO "trx_queue_outpatient" VALUES ('12120001', '1212007', '1', '2012-12-05 00:00:00', '1', '2', '16:00-24:00', '1', '2012-12-05 11:46:16.917294', '1');
INSERT INTO "trx_queue_outpatient" VALUES ('12120002', '1212010', '2', '2012-12-05 00:00:00', '1', '2', '16:00-24:00', '1', '2012-12-05 12:02:50.572927', '1');
INSERT INTO "trx_queue_outpatient" VALUES ('12120003', '1212010', '3', '2012-12-05 00:00:00', '1', '2', '16:00-24:00', '1', '2012-12-05 12:03:20.873874', '1');
INSERT INTO "trx_queue_outpatient" VALUES ('12120004', '1212007', '4', '2012-12-05 00:00:00', '1', '2', '16:00-24:00', '1', '2012-12-05 12:04:03.005781', '1');
INSERT INTO "trx_queue_outpatient" VALUES ('12120005', '1211005', '5', '2012-12-05 00:00:00', '1', '2', '16:00-24:00', '1', '2012-12-05 12:04:43.934298', '1');
INSERT INTO "trx_queue_outpatient" VALUES ('12120006', '1212011', '6', '2012-12-05 00:00:00', '0', '0', '0', '1', '2012-12-05 15:44:24.111501', '1');
INSERT INTO "trx_queue_outpatient" VALUES ('12120007', '1212011', '7', '2012-12-05 00:00:00', '1', '2', '16:00-24:00', '1', '2012-12-05 15:44:49.711202', '1');
COMMIT;

-- ----------------------------
--  Table structure for "trx_reference"
-- ----------------------------
DROP TABLE IF EXISTS "trx_reference";
CREATE TABLE "trx_reference" (
	"mdc_id" varchar(10),
	"ref_id" int2 NOT NULL DEFAULT nextval('trx_reference_seq'::regclass),
	"ref_date" date,
	"ref_date_start" date,
	"ref_date_end" date,
	"ref_description" text,
	"ref_category" varchar(20),
	"modi_id" int2,
	"modi_datetime" time(6),
	"ref_number" varchar(20)
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_reference" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "trx_recipe_detail"
-- ----------------------------
DROP TABLE IF EXISTS "trx_recipe_detail";
CREATE TABLE "trx_recipe_detail" (
	"mdc_id" varchar(45),
	"recipe_id" varchar(45) NOT NULL,
	"recipe_medicine" varchar(10),
	"recipe_rule" varchar(20),
	"recipe_qty" int4,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL,
	"recipe_number" int2,
	"recipe_item_price" int4,
	"price" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "trx_recipe_detail" OWNER TO "postgres";

-- ----------------------------
--  Records of "trx_recipe_detail"
-- ----------------------------
BEGIN;
INSERT INTO "trx_recipe_detail" VALUES ('12110001', 'RS-12120002', 'OB1', '3x1', '10', null, null, '1', '20000', null);
INSERT INTO "trx_recipe_detail" VALUES ('12110001', 'RS-12120002', 'OB2', '3x1', '10', null, null, '2', '30000', null);
INSERT INTO "trx_recipe_detail" VALUES ('12110003', 'RS-12120001', 'OB1', '3x1', '2', null, null, '1', '4000', null);
INSERT INTO "trx_recipe_detail" VALUES ('12120005', 'RS-12120003', 'OB1', '3 x 1 sehari', '10', null, null, '1', '20000', null);
INSERT INTO "trx_recipe_detail" VALUES ('12120005', 'RS-12120003', 'OB2', '3 x 2', '5', null, null, '2', '15000', null);
INSERT INTO "trx_recipe_detail" VALUES ('12110002', 'RS-12120004', 'OB1', '3 x 1', '10', null, null, '1', '20000', null);
INSERT INTO "trx_recipe_detail" VALUES ('12110002', 'RS-12120004', 'OB2', '2 x 1', '5', null, null, '2', '15000', null);
COMMIT;

-- ----------------------------
--  Table structure for "com_menus"
-- ----------------------------
DROP TABLE IF EXISTS "com_menus";
CREATE TABLE "com_menus" (
	"menu_id" int2 NOT NULL DEFAULT nextval('com_menus_seq'::regclass),
	"modul_id" int2 NOT NULL,
	"menu_nama" varchar(200) NOT NULL,
	"menu_url" varchar(200) NOT NULL,
	"menu_icon" varchar(200),
	"menu_parent" varchar(200),
	"status" int2 NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "com_menus" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "ptn_family"
-- ----------------------------
DROP TABLE IF EXISTS "ptn_family";
CREATE TABLE "ptn_family" (
	"fm_id" int4 NOT NULL DEFAULT nextval('ptn_family_seq'::regclass),
	"sd_rekmed" varchar(10),
	"fm_sex" varchar(255),
	"fm_relation" varchar(20),
	"fm_address" text,
	"fm_telp" varchar(20),
	"fm_phone" varchar(20),
	"fm_rekmed" varchar(20),
	"fm_status" int4,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL,
	"fm_name" varchar(50)
)
WITH (OIDS=FALSE);
ALTER TABLE "ptn_family" OWNER TO "postgres";

-- ----------------------------
--  Records of "ptn_family"
-- ----------------------------
BEGIN;
INSERT INTO "ptn_family" VALUES ('3', '1212009', 'laki-laki', 'Orang Tua', 'asdasd', 'asadasd', 'asdsad', null, null, null, null, 'aasdsad');
INSERT INTO "ptn_family" VALUES ('4', '1212009', '', '', '', '', '', null, null, null, null, 'sadsadsad');
INSERT INTO "ptn_family" VALUES ('4', '1212011', 'laki-laki', 'Orang Tua', 'sadas', '324', '234', null, null, null, null, 'agus');
COMMIT;

-- ----------------------------
--  Table structure for "inv_purchase_req_detail"
-- ----------------------------
DROP TABLE IF EXISTS "inv_purchase_req_detail";
CREATE TABLE "inv_purchase_req_detail" (
	"iprd_id" varchar(50) NOT NULL,
	"iprd_item" varchar(32) NOT NULL,
	"iprd_pos" varchar(32) NOT NULL,
	"iprd_qty" numeric(10,2) NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "inv_purchase_req_detail" OWNER TO "postgres";

-- ----------------------------
--  Table structure for "com_code"
-- ----------------------------
DROP TABLE IF EXISTS "com_code";
CREATE TABLE "com_code" (
	"id" int4 NOT NULL DEFAULT nextval('com_code_seq'::regclass),
	"title" varchar(200) NOT NULL,
	"value_1" text,
	"value_2" text,
	"value_3" text,
	"status" int4 NOT NULL DEFAULT 1
)
WITH (OIDS=FALSE);
ALTER TABLE "com_code" OWNER TO "postgres";

-- ----------------------------
--  Records of "com_code"
-- ----------------------------
BEGIN;
INSERT INTO "com_code" VALUES ('1', 'format_po', 'PR-', 'null', 'null', '1');
INSERT INTO "com_code" VALUES ('2', 'format_resep', 'RS-', 'null', 'null', '1');
INSERT INTO "com_code" VALUES ('3', 'format_diagnosa', 'DA-', 'null', 'null', '1');
INSERT INTO "com_code" VALUES ('4', 'jasa_resep', '1000', null, null, '1');
INSERT INTO "com_code" VALUES ('5', 'format_inv_po', 'INV_PO-', 'null', 'null', '1');
INSERT INTO "com_code" VALUES ('6', 'format_recive_item', 'RI-', 'null', 'null', '1');
INSERT INTO "com_code" VALUES ('7', 'format_inv_recive', 'INV_RI-', 'null', 'null', '1');
COMMIT;

-- ----------------------------
--  Table structure for "sys_user"
-- ----------------------------
DROP TABLE IF EXISTS "sys_user";
CREATE TABLE "sys_user" (
	"user_id" int4 NOT NULL DEFAULT nextval('sys_user_seq'::regclass),
	"emp_id" int4,
	"user_name" varchar(45),
	"user_password" varchar(255),
	"usr_tp_id" int4,
	"user_last_login" timestamp(6) NULL,
	"user_status" int4,
	"modi_id" int4,
	"modi_datetime" timestamp(6) NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "sys_user" OWNER TO "postgres";

-- ----------------------------
--  View structure for "v_detail_diagnosa"
-- ----------------------------
DROP VIEW IF EXISTS "v_detail_diagnosa";
CREATE VIEW "v_detail_diagnosa" AS SELECT tdt.dat_id, tdt.mdc_id, tdtd.dat_diag, tdtd.dat_treat, tdtd.dat_case, tdtd.dat_paramedic_price, tdtd.dat_item_price, mt.treat_name, md.diag_name FROM (((trx_diagnosa_treathment tdt JOIN trx_diagnosa_treathment_detail tdtd ON (((tdt.dat_id)::text = (tdtd.dat_id)::text))) JOIN mst_treathment mt ON ((tdtd.dat_treat = mt.treat_id))) JOIN mst_diagnosa md ON ((tdtd.dat_diag = md.diag_id)));

-- ----------------------------
--  View structure for "v_detail_resep"
-- ----------------------------
DROP VIEW IF EXISTS "v_detail_resep";
CREATE VIEW "v_detail_resep" AS SELECT tr.mdc_id, tr.recipe_id, trd.recipe_medicine, im.im_name, trd.recipe_qty, trd.recipe_rule, tr.recipe_paramedic_price, trd.recipe_item_price, im.im_item_price FROM ((trx_recipe tr JOIN trx_recipe_detail trd ON (((tr.mdc_id)::text = (trd.mdc_id)::text))) JOIN inv_item_master im ON (((trd.recipe_medicine)::text = (im.im_id)::text)));

-- ----------------------------
--  View structure for "v_medical_bill"
-- ----------------------------
DROP VIEW IF EXISTS "v_medical_bill";
CREATE VIEW "v_medical_bill" AS SELECT tm.mdc_id, to_char(tm.mdc_in, 'Day, DD-MM-YYYY'::text) AS day_date, psd.sd_name, tp.pl_name, tm.total_amount, tm.total_pay, tm.mdc_status FROM ((trx_medical tm JOIN ptn_social_data psd ON (((psd.sd_rekmed)::text = (tm.sd_rekmed)::text))) JOIN trx_poly tp ON ((tm.pl_id = tp.pl_id)));

-- ----------------------------
--  View structure for "v_medical_obat"
-- ----------------------------
DROP VIEW IF EXISTS "v_medical_obat";
CREATE VIEW "v_medical_obat" AS SELECT tm.mdc_id, tm.sd_rekmed, trp.recipe_medicine, trp.recipe_qty, trp.recipe_rule FROM (trx_medical tm JOIN trx_recipe_detail trp ON (((trp.mdc_id)::text = (tm.mdc_id)::text)));

-- ----------------------------
--  View structure for "v_patient"
-- ----------------------------
DROP VIEW IF EXISTS "v_patient";
CREATE VIEW "v_patient" AS SELECT ptn_social_data.sd_rekmed AS ptn_rekmed, ptn_social_data.sd_name AS ptn_name, ptn_social_data.sd_address AS ptn_address, ptn_social_data.sd_reg_date AS ptn_reg_date FROM ptn_social_data;

-- ----------------------------
--  View structure for "v_queue_poli"
-- ----------------------------
DROP VIEW IF EXISTS "v_queue_poli";
CREATE VIEW "v_queue_poli" AS SELECT psc.sd_rekmed, psc.sd_name, psc.sd_sex, psc.sd_place_of_birth, psc.sd_date_of_birth, psc.sd_age, psc.sd_blood_tp, psc.sd_address, psc.sd_rt_rw, psc.sd_reg_desa, psc.sd_reg_kec, psc.sd_reg_kab, psc.sd_reg_prov, psc.sd_citizen, psc.sd_marital_st, psc.sd_religion, psc.sd_education, psc.sd_occupation, psc.sd_telp, psc.sd_reg_date, psc.sd_status, psc.modi_id, psc.modi_datetime, psc.sd_reg_street, tqo.pl_id, tqo.queo_id, tqo.queo_status, tqo.dr_id FROM (trx_queue_outpatient tqo JOIN ptn_social_data psc ON (((tqo.sd_rekmed)::text = (psc.sd_rekmed)::text)));

-- ----------------------------
--  View structure for "v_ringkasan"
-- ----------------------------
DROP VIEW IF EXISTS "v_ringkasan";
CREATE VIEW "v_ringkasan" AS SELECT tm.mdc_id, tm.mdc_in, tm.sd_rekmed, tm.mdc_status, td.dr_name, tms.ms_desc, tmo.mo_desc FROM (((trx_medical tm JOIN trx_doctor td ON ((tm.dr_id = td.dr_id))) JOIN trx_medical_subjective tms ON (((tms.mdc_id)::text = (tm.mdc_id)::text))) JOIN trx_medical_objective tmo ON (((tmo.mdc_id)::text = (tm.mdc_id)::text)));

-- ----------------------------
--  View structure for "v_sch_doctor"
-- ----------------------------
DROP VIEW IF EXISTS "v_sch_doctor";
CREATE VIEW "v_sch_doctor" AS SELECT a.dr_id, a.emp_id, a.dr_name, a.modi_id, a.modi_datetime, b.pl_id, b.sch_time_start, b.sch_time_end, c.shift_end, c.shift_start, d.pl_name, b.sch_shift FROM (((trx_doctor a JOIN trx_dr_schedule b ON ((a.dr_id = b.dr_id))) JOIN mst_shift c ON (((c.shift_id)::text = (b.sch_shift)::text))) JOIN trx_poly d ON ((b.pl_id = d.pl_id)));

-- ----------------------------
--  View structure for "v_supplier"
-- ----------------------------
DROP VIEW IF EXISTS "v_supplier";
CREATE VIEW "v_supplier" AS SELECT msup.msup_id, msup.msup_name, msup.msup_address FROM mst_supplier msup;

-- ----------------------------
--  View structure for "v_transaksi_detail_resep"
-- ----------------------------
DROP VIEW IF EXISTS "v_transaksi_detail_resep";
CREATE VIEW "v_transaksi_detail_resep" AS SELECT tr.mdc_id, tr.recipe_id, trd.recipe_medicine, im.im_name, trd.recipe_qty, trd.recipe_rule, tr.recipe_paramedic_price, trd.recipe_item_price, im.im_item_price, im.im_unit FROM ((trx_recipe tr JOIN trx_recipe_detail trd ON (((tr.mdc_id)::text = (trd.mdc_id)::text))) JOIN inv_item_master im ON (((trd.recipe_medicine)::text = (im.im_id)::text)));

-- ----------------------------
--  View structure for "v_transaksi_resep"
-- ----------------------------
DROP VIEW IF EXISTS "v_transaksi_resep";
CREATE VIEW "v_transaksi_resep" AS SELECT tm.mdc_id, tm.sd_rekmed, psd.sd_name, psd.sd_address, psd.sd_age, td.dr_name, tp.pl_name, tm.mdc_in, tr.recipe_id, psd.sd_blood_tp, psd.sd_sex, tm.recipe_status FROM ((((trx_medical tm JOIN trx_recipe tr ON (((tm.mdc_id)::text = (tr.mdc_id)::text))) JOIN ptn_social_data psd ON (((tm.sd_rekmed)::text = (psd.sd_rekmed)::text))) JOIN trx_doctor td ON ((tm.dr_id = td.dr_id))) JOIN trx_poly tp ON ((tm.pl_id = tp.pl_id)));

-- ----------------------------
--  View structure for "v_bill_detail"
-- ----------------------------
DROP VIEW IF EXISTS "v_bill_detail";
CREATE VIEW "v_bill_detail" AS SELECT a.date, a."desc", a.amount, a.total FROM (trx_bill a JOIN mst_bill b ON (((a.bill_id)::text = (b.bill_id)::text)));

-- ----------------------------
--  View structure for "v_lap_10_penyakit"
-- ----------------------------
DROP VIEW IF EXISTS "v_lap_10_penyakit";
CREATE VIEW "v_lap_10_penyakit" AS SELECT mst_diagnosa.diag_name, count(mst_diagnosa.diag_name) AS jumlah FROM (trx_diagnosa_treathment_detail JOIN mst_diagnosa ON ((mst_diagnosa.diag_id = trx_diagnosa_treathment_detail.dat_diag))) WHERE ((trx_diagnosa_treathment_detail.mdc_id)::text IN (SELECT trx_medical.mdc_id FROM trx_medical)) GROUP BY mst_diagnosa.diag_name LIMIT 10;

-- ----------------------------
--  View structure for "v_kunjungan_pasien"
-- ----------------------------
DROP VIEW IF EXISTS "v_kunjungan_pasien";
CREATE VIEW "v_kunjungan_pasien" AS SELECT tp.pl_name, pd.sd_rekmed, pd.sd_name, pd.sd_age, pd.sd_address FROM ((trx_medical tm JOIN ptn_social_data pd ON (((tm.sd_rekmed)::text = (pd.sd_rekmed)::text))) JOIN trx_poly tp ON ((tp.pl_id = tm.pl_id))) ORDER BY tm.mdc_id DESC;

-- ----------------------------
--  View structure for "v_data_patient"
-- ----------------------------
DROP VIEW IF EXISTS "v_data_patient";
CREATE VIEW "v_data_patient" AS SELECT psd.sd_rekmed, psd.sd_name, psd.sd_age, psd.sd_blood_tp, psd.sd_address FROM ptn_social_data psd ORDER BY psd.sd_rekmed DESC;

-- ----------------------------
--  View structure for "v_ipo"
-- ----------------------------
DROP VIEW IF EXISTS "v_ipo";
CREATE VIEW "v_ipo" AS SELECT ipo.ipo_id, ms.msup_name AS ipo_supplier, ipo.ipo_date_req AS ipo_req, ipo.ipo_date_order AS ipo_order, cs.cs_desc AS ipo_status FROM ((inv_purchase_order ipo JOIN mst_supplier ms ON (((ipo.ipo_supplier)::text = (ms.msup_id)::text))) JOIN com_status cs ON ((ipo.ipo_status = cs.cs_id)));

-- ----------------------------
--  View structure for "v_trx_bill4"
-- ----------------------------
DROP VIEW IF EXISTS "v_trx_bill4";
CREATE VIEW "v_trx_bill4" AS SELECT b.mdc_id AS medic, b.bill_id AS bill, sum(CASE WHEN (b.amount > 0) THEN b.amount ELSE 0 END) AS total, sum(CASE WHEN (b.tanggungan > 0) THEN b.tanggungan ELSE 0 END) AS sum, a.bill_name AS nama FROM (mst_bill a LEFT JOIN trx_bill b ON (((a.bill_id)::text = (b.bill_id)::text))) GROUP BY a.bill_name, b.mdc_id, b.bill_id;

-- ----------------------------
--  Primary key structure for table "mst_education"
-- ----------------------------
ALTER TABLE "mst_education" ADD CONSTRAINT "mst_education_pkey" PRIMARY KEY ("med_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "com_user"
-- ----------------------------
ALTER TABLE "com_user" ADD CONSTRAINT "com_user_pkey" PRIMARY KEY ("user_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "com_modul"
-- ----------------------------
ALTER TABLE "com_modul" ADD CONSTRAINT "com_modul_pkey" PRIMARY KEY ("modul_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_treathment"
-- ----------------------------
ALTER TABLE "mst_treathment" ADD CONSTRAINT "mst_treathment_pkey" PRIMARY KEY ("treat_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_country"
-- ----------------------------
ALTER TABLE "mst_country" ADD CONSTRAINT "mst_country_pkey" PRIMARY KEY ("mco_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "sys_user_group"
-- ----------------------------
ALTER TABLE "sys_user_group" ADD CONSTRAINT "sys_user_group_pkey" PRIMARY KEY ("user_group_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_dr_schedule"
-- ----------------------------
ALTER TABLE "trx_dr_schedule" ADD CONSTRAINT "trx_dr_schedule_pkey" PRIMARY KEY ("sch_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_poly"
-- ----------------------------
ALTER TABLE "trx_poly" ADD CONSTRAINT "trx_poly_pkey" PRIMARY KEY ("pl_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_medical"
-- ----------------------------
ALTER TABLE "trx_medical" ADD CONSTRAINT "trx_medical_pkey" PRIMARY KEY ("mdc_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "sys_group"
-- ----------------------------
ALTER TABLE "sys_group" ADD CONSTRAINT "sys_group_pkey" PRIMARY KEY ("group_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "com_menu_role"
-- ----------------------------
ALTER TABLE "com_menu_role" ADD CONSTRAINT "com_menu_role_pkey" PRIMARY KEY ("role_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_type_inv"
-- ----------------------------
ALTER TABLE "mst_type_inv" ADD CONSTRAINT "mst_type_inv_pkey" PRIMARY KEY ("mtype_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_bill"
-- ----------------------------
ALTER TABLE "trx_bill" ADD CONSTRAINT "trx_bill_pkey" PRIMARY KEY ("trx_bill_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_payment_method"
-- ----------------------------
ALTER TABLE "mst_payment_method" ADD CONSTRAINT "mst_payment_method_pkey" PRIMARY KEY ("mpm_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_class"
-- ----------------------------
ALTER TABLE "mst_class" ADD CONSTRAINT "mst_class_pkey" PRIMARY KEY ("mscl_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_currency"
-- ----------------------------
ALTER TABLE "mst_currency" ADD CONSTRAINT "mst_currency_pkey" PRIMARY KEY ("mc_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_medical_subjective"
-- ----------------------------
ALTER TABLE "mst_medical_subjective" ADD CONSTRAINT "mst_medical_subjective_pkey" PRIMARY KEY ("ms_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_medicine"
-- ----------------------------
ALTER TABLE "mst_medicine" ADD CONSTRAINT "mst_medicine_pkey" PRIMARY KEY ("mdcn_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_medical_objective"
-- ----------------------------
ALTER TABLE "trx_medical_objective" ADD CONSTRAINT "trx_medical_objective_pkey" PRIMARY KEY ("mo_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "com_menu"
-- ----------------------------
ALTER TABLE "com_menu" ADD CONSTRAINT "com_menu_pkey" PRIMARY KEY ("menu_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "ptn_social_data"
-- ----------------------------
ALTER TABLE "ptn_social_data" ADD CONSTRAINT "ptn_social_data_pkey" PRIMARY KEY ("sd_rekmed") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "com_user_tipe"
-- ----------------------------
ALTER TABLE "com_user_tipe" ADD CONSTRAINT "com_user_tipe_pkey" PRIMARY KEY ("tipe_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_nationality"
-- ----------------------------
ALTER TABLE "mst_nationality" ADD CONSTRAINT "mst_nationality_pkey" PRIMARY KEY ("mna_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_study"
-- ----------------------------
ALTER TABLE "mst_study" ADD CONSTRAINT "mst_study_pkey" PRIMARY KEY ("mst_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "inv_recive_item"
-- ----------------------------
ALTER TABLE "inv_recive_item" ADD CONSTRAINT "inv_recive_item_pkey" PRIMARY KEY ("iri_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "sys_menu_role"
-- ----------------------------
ALTER TABLE "sys_menu_role" ADD CONSTRAINT "sys_menu_role_pkey" PRIMARY KEY ("menu_role_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "inv_purchase_req"
-- ----------------------------
ALTER TABLE "inv_purchase_req" ADD CONSTRAINT "inv_purchase_req_pkey" PRIMARY KEY ("ipr_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "com_menu__"
-- ----------------------------
ALTER TABLE "com_menu__" ADD CONSTRAINT "com_menu___pkey" PRIMARY KEY ("menu_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_medical_bill"
-- ----------------------------
ALTER TABLE "trx_medical_bill" ADD CONSTRAINT "trx_medical_bill_pkey" PRIMARY KEY ("mdc_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_medical_subjective"
-- ----------------------------
ALTER TABLE "trx_medical_subjective" ADD CONSTRAINT "trx_medical_subjective_pkey" PRIMARY KEY ("ms_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_supplier"
-- ----------------------------
ALTER TABLE "mst_supplier" ADD CONSTRAINT "mst_supplier_pkey" PRIMARY KEY ("msup_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_city"
-- ----------------------------
ALTER TABLE "mst_city" ADD CONSTRAINT "mst_city_pkey" PRIMARY KEY ("mci_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_occupation"
-- ----------------------------
ALTER TABLE "mst_occupation" ADD CONSTRAINT "mst_occupation_pkey" PRIMARY KEY ("oc_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_province"
-- ----------------------------
ALTER TABLE "mst_province" ADD CONSTRAINT "mst_province_pkey" PRIMARY KEY ("mpr_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_regency"
-- ----------------------------
ALTER TABLE "mst_regency" ADD CONSTRAINT "mst_regency_pkey" PRIMARY KEY ("mre_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_diagnosa_treathment"
-- ----------------------------
ALTER TABLE "trx_diagnosa_treathment" ADD CONSTRAINT "trx_diagnosa_treathment_pkey" PRIMARY KEY ("dat_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_reg_poli"
-- ----------------------------
ALTER TABLE "trx_reg_poli" ADD CONSTRAINT "trx_reg_poli_pkey" PRIMARY KEY ("rp_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_diagnosa"
-- ----------------------------
ALTER TABLE "mst_diagnosa" ADD CONSTRAINT "mst_diagnosa_pkey" PRIMARY KEY ("diag_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_religion"
-- ----------------------------
ALTER TABLE "mst_religion" ADD CONSTRAINT "mst_religion_pkey" PRIMARY KEY ("mr_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "inv_item_type"
-- ----------------------------
ALTER TABLE "inv_item_type" ADD CONSTRAINT "inv_item_type_pkey" PRIMARY KEY ("it_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_recipe"
-- ----------------------------
ALTER TABLE "trx_recipe" ADD CONSTRAINT "trx_recipe_pkey" PRIMARY KEY ("recipe_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_insurance"
-- ----------------------------
ALTER TABLE "mst_insurance" ADD CONSTRAINT "mst_insurance_pkey" PRIMARY KEY ("ins_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_shift"
-- ----------------------------
ALTER TABLE "mst_shift" ADD CONSTRAINT "mst_shift_pkey" PRIMARY KEY ("shift_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "inv_mst_pos"
-- ----------------------------
ALTER TABLE "inv_mst_pos" ADD CONSTRAINT "inv_mst_pos_pkey" PRIMARY KEY ("imp_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_medical_objective"
-- ----------------------------
ALTER TABLE "mst_medical_objective" ADD CONSTRAINT "mst_medical_objective_pkey" PRIMARY KEY ("mo_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_bill"
-- ----------------------------
ALTER TABLE "mst_bill" ADD CONSTRAINT "mst_bill_pkey" PRIMARY KEY ("bill_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_doctor"
-- ----------------------------
ALTER TABLE "trx_doctor" ADD CONSTRAINT "trx_doctor_pkey" PRIMARY KEY ("dr_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "inv_item_master"
-- ----------------------------
ALTER TABLE "inv_item_master" ADD CONSTRAINT "inv_item_master_pkey" PRIMARY KEY ("im_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "mst_room"
-- ----------------------------
ALTER TABLE "mst_room" ADD CONSTRAINT "mst_room_pkey" PRIMARY KEY ("msro_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "inv_purchase_order"
-- ----------------------------
ALTER TABLE "inv_purchase_order" ADD CONSTRAINT "inv_purchase_order_pkey" PRIMARY KEY ("ipo_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_queue_outpatient"
-- ----------------------------
ALTER TABLE "trx_queue_outpatient" ADD CONSTRAINT "trx_queue_outpatient_pkey" PRIMARY KEY ("queo_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "trx_reference"
-- ----------------------------
ALTER TABLE "trx_reference" ADD CONSTRAINT "trx_reference_pkey" PRIMARY KEY ("ref_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "com_menus"
-- ----------------------------
ALTER TABLE "com_menus" ADD CONSTRAINT "com_menus_pkey" PRIMARY KEY ("menu_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "com_code"
-- ----------------------------
ALTER TABLE "com_code" ADD CONSTRAINT "com_code_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table "sys_user"
-- ----------------------------
ALTER TABLE "sys_user" ADD CONSTRAINT "sys_user_pkey" PRIMARY KEY ("user_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

